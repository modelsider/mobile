fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## Android

### android apk_dev_major

```sh
[bundle exec] fastlane android apk_dev_major
```

Android dev apk (testing)

### android apk_dev_minor

```sh
[bundle exec] fastlane android apk_dev_minor
```

Android dev apk (testing)

### android apk_dev_patch

```sh
[bundle exec] fastlane android apk_dev_patch
```

Android dev apk (testing)

### android production_google_play_major

```sh
[bundle exec] fastlane android production_google_play_major
```

Deploy a new version to the Google Play (production)

### android production_google_play_minor

```sh
[bundle exec] fastlane android production_google_play_minor
```

Deploy a new version to the Google Play (production)

### android production_google_play_patch

```sh
[bundle exec] fastlane android production_google_play_patch
```

Deploy a new version to the Google Play (production)

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
