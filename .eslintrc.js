module.exports = {
	root: true,
	extends: [
		'eslint:recommended',
		'plugin:jest/recommended',
		'@react-native-community',
	],
};
