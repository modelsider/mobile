const icons = {
  bottomTab: {
    home: require('./icons/bottom-tab/home.png'),
    agencies: require('./icons/bottom-tab/agencies.png'),
    bookmarks: require('./icons/bottom-tab/bookmarks.png'),
    news: require('./icons/bottom-tab/news.png'),
  },
  closeInstagramButton: require('./icons/close-button-instagram.png'),
  numberOfRated: require('./icons/number-of-rated.png'),
  anonymous: require('./icons/anonymus/anonymus.png'),
  defaultUser: require('./icons/default/defaultUser.png'),
  deletedUser: require('./icons/deleted/deletedUser.png'),
  star: require('./icons/star/star.png'),
  logo: require('./icons/logo/logo.png'),
  instagram: require('./icons/instagram/instagram.png'),
  plus: require('./icons/plus/plus.png'),
};

const gifs = {
  splash: require('./splash.gif'),
};

const lottie = {
  timer: require('./lottie/rate_agency_waiting_lottie.json'),
  splash: require('./lottie/splash.json'),
};

export { icons, lottie, gifs };
