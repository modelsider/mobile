import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import App from './app/App';
import React from 'react';

function headlessCheck({ isHeadless }) {
  if (isHeadless) {
    // App has been launched in the background by iOS, ignore
    return null;
  }

  return <App />;
}

AppRegistry.registerComponent(appName, () => headlessCheck);
