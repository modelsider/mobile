module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        extensions: [
          '.js',
          '.jsx',
          '.ts',
          '.tsx',
          '.android.js',
          '.android.tsx',
          '.ios.js',
          '.ios.tsx',
        ],
        root: ['./app'],
        alias: {
          assets: './app/assets',
          components: './app/components',
          hooks: './app/hooks',
          navigator: './app/navigator',
          screens: './app/screens',
          services: './app/services',
          store: './app/store',
          styles: './app/styles',
          utils: './app/utils',
        },
      },
    ],
  ],
};
