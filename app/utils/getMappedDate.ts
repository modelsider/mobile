import isUndefined from 'lodash/isUndefined';

export const getMappedDate = (date: any, isLocaleDate = false) => {
  if (isUndefined(date)) {
    return;
  }
  if (date === 'Present') {
    return date;
  }

  let result = new Date(date?._seconds * 1000);

  if (isLocaleDate) {
    return result?.toLocaleDateString();
  }

  return result || new Date();
};
