export const transformUsername = (username: string) => username.trim().replace(/@/g, '');
