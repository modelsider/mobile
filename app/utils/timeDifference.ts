export const timeDifference = (previousDate: any) => {
  const msPerMinute = 60 * 1000;
  const msPerHour = msPerMinute * 60;
  const msPerDay = msPerHour * 24;
  const msPerMonth = msPerDay * 30;
  const msPerYear = msPerDay * 365;

  const elapsed = new Date() - new Date(previousDate?._seconds * 1000);

  const parse = (time: number, label: string) => {
    const roundTime = Math.round(elapsed / time);
    const end = roundTime === 1 ? '' : 's';

    return `${roundTime} ${label}${end} ago`;
  };

  if (elapsed < msPerMinute) {
    return 'Now';
  } else if (elapsed < msPerHour) {
    return parse(msPerMinute, 'minute');
  } else if (elapsed < msPerDay) {
    return parse(msPerHour, 'hour');
  } else if (elapsed < msPerMonth) {
    return parse(msPerDay, 'day');
  } else if (elapsed < msPerYear) {
    return parse(msPerMonth, 'month');
  } else {
    return parse(msPerYear, 'year');
  }
};
