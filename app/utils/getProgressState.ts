import { IProgressState } from '../types/types';

const getProgressState = ({ index, length }: { index: number; length: number }) => {
  const data: IProgressState[] = Array.from({ length: length! }, (_, i) => ({
    index: i + 1,
    check: false,
    border: false,
  }));

  const state = data.map((dotsState) => {
    if (dotsState.index < index) {
      return {
        ...dotsState,
        check: true,
        border: true,
      };
    } else if (dotsState.index === index) {
      return {
        ...dotsState,
        border: true,
      };
    } else {
      return dotsState;
    }
  });
  return state;
};

export { getProgressState };
