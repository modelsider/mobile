import uuid from 'uuidv4';

const onlyNumbers = /\D/g;

export const generateId = () => +uuid().replace(onlyNumbers, '').slice(0, 10);
