import { resolveConditionHandler } from 'utils/resolveConditionHandler';
import { ADDITIONAL_COLOURS, MAIN_COLOURS } from 'styles/colours';

export const getValidateInputColor = (isError: boolean, editable: boolean, color: string) => {
  return resolveConditionHandler(
    isError,
    ADDITIONAL_COLOURS['primary-red'],
    resolveConditionHandler(editable, MAIN_COLOURS['primary-black'], color),
  );
};

export const getValidateBorderColor = (
  isError: boolean,
  isSuccess: boolean,
  editable: boolean,
  borderColor: string,
) => {
  return resolveConditionHandler(
    isError,
    ADDITIONAL_COLOURS['primary-red'],
    resolveConditionHandler(
      isSuccess,
      ADDITIONAL_COLOURS['primary-green'],
      resolveConditionHandler(editable, borderColor, ADDITIONAL_COLOURS['secondary-grey']),
    ),
  );
};

export const getValidateBackgroundColor = (editable: boolean, backgroundColor: string) => {
  return editable ? backgroundColor : ADDITIONAL_COLOURS['secondary-grey'];
};

export const getValidateShadowColor = (isError: boolean, isSuccess: boolean) => {
  return resolveConditionHandler(
    isError,
    ADDITIONAL_COLOURS['primary-red'],
    resolveConditionHandler(isSuccess, ADDITIONAL_COLOURS['primary-green'], MAIN_COLOURS['primary-white']),
  );
};

export const getValidateShadowOpacity = (isError: boolean, isSuccess: boolean) => {
  return isError || isSuccess ? 0.5 : 0;
};
