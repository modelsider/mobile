const getDifferenceInYears = (date: any) => {
  const dateParts = date.split('/');

  const endDate = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
  const currentDate = new Date();

  const diffInMilliseconds = currentDate.getTime() - endDate.getTime();
  const diffInDays = diffInMilliseconds / (24 * 60 * 60 * 1000);
  const diffInYears = diffInDays / 365.25;

  return diffInYears;
};

export { getDifferenceInYears };
