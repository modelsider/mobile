export const resolveConditionHandler = (condition: boolean | undefined, conditionTrue: any, conditionFalse: any) =>
  condition ? conditionTrue : conditionFalse;
