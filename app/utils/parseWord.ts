import { PATTERN } from '../components/news/NewsItem';

export const parseWord = (matchingString: string) => {
  const match = matchingString.match(PATTERN);
  return match[1] || match[2] || match[3] || match[4] || match[5];
};
