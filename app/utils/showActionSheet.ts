import { ActionSheet } from 'react-native-cross-actionsheet';

interface IOption {
  text: string;
  onPress: () => any;
  destructive?: boolean;
}
interface ICancel {
  onPress?: () => void;
}

interface IActionSheet {
  title: string;
  options: IOption[];
  cancel?: ICancel;
}

export const showActionSheet = async ({ title, options, cancel }: IActionSheet) => {
  await ActionSheet.options({
    title,
    options,
    cancel,
  });
};
