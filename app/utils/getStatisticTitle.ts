import { agencyType } from '../types/types';
import { RATED_STATISTICS_TITLES, ratedStatisticsType } from 'constants';

export const getStatisticTitle = ({
  type,
  statisticType,
}: {
  type: agencyType;
  statisticType: ratedStatisticsType;
}) => {
  const title = RATED_STATISTICS_TITLES[type].find(({ type: itemType }) => itemType === statisticType)?.title;
  return title || '';
};
