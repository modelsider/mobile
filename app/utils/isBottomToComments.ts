const PADDING_TO_BOTTOM = 200;

export const isBottomToComments = ({ layoutMeasurement, contentOffset, contentSize, commentsLength }: any) => {
  return layoutMeasurement.height + contentOffset.y >= contentSize.height - PADDING_TO_BOTTOM * commentsLength;
};
