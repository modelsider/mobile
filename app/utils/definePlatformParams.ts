import { Platform } from 'react-native';

export const isIosPlatform = () => Platform.OS === 'ios';

export const definePlatformParam = <T>(ios: T, android: T) => (isIosPlatform() ? ios : android);
