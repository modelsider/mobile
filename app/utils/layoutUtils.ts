import { Dimensions, PixelRatio, Platform, ScaledSize, StatusBar } from 'react-native';

const X_W = 375;
const X_H = 812;

const XSMAX_W = 414;
const XSMAX_H = 896;

const IPHONE7_W = 375;

const { height, width } = Dimensions.get('window');

const defaultAspectRatio = 16 / 9;
const currentScreenAspectRatio = height / width;

let isIPhoneX = false;

if (Platform.OS === 'ios') {
  isIPhoneX = (width >= X_W && height >= X_H) || (width >= XSMAX_W && height >= XSMAX_H);
}

export const getStatusBarHeight = (skipAndroid: boolean = false): number | undefined => {
  if (Platform.OS === 'ios') {
    return isIPhoneX ? 44 : 20;
  }

  if (skipAndroid) {
    return 0;
  }
  return StatusBar.currentHeight;
};

const k = Platform.OS === 'ios' ? 1 : defaultAspectRatio < currentScreenAspectRatio ? 1 : 0.94;

export const convertPtToResponsive = (size: number): number => {
  return PixelRatio.roundToNearestPixel((width * size * k) / IPHONE7_W);
};

export function isIphoneX() {
  const dim = Dimensions.get('window');

  return Platform.OS === 'ios' && (isIPhoneXSize(dim) || isIPhoneXrSize(dim));
}

export function isIPhoneXSize(dim: ScaledSize) {
  return dim.height === 812;
}

export function isIPhoneXrSize(dim: ScaledSize) {
  return dim.height === 896;
}

export const inRangeValue = (value: number, min: number, max: number) =>
  value >= max ? max : value <= min && value !== 0 ? min : value;

export const getScreenDimensions = () => {
  const dimensions = Dimensions.get('window');
  return {
    height: dimensions.height,
    width: dimensions.width,
  };
};
