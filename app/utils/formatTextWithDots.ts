const DEFAULT_LENGTH = 12;

export const formatTextWithDots = ({ text, length = DEFAULT_LENGTH }: { text: string; length?: number }) => {
  return text?.length > length ? `${text?.slice(0, length)}...` : text;
};
