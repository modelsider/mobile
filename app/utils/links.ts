export const getInstagramText = (instagram: string) => `@${instagram.substring(26, instagram.length - 1)}`;

export const getWebText = (web: string) => web.substring(12, web.length - 1);
