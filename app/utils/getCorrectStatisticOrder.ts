import { statisticOrder } from '../constants';

export const getCorrectStatisticOrder = (
  a: any, // todo: add later
  b: any, // todo: add later
  type: string,
) => {
  return statisticOrder?.[type].indexOf(a.type) - statisticOrder?.[type].indexOf(b.type);
};
