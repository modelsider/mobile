const month = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

export const getMonth = (index: number) => {
  return month[index];
};

export const formatDate = (date: any, withDay = false) => {
  if (date === 'Present') {
    return date;
  }

  const convertedDate = new Date(date?._seconds * 1000);

  const actualDate = convertedDate.toString() === 'Invalid Date' ? new Date(date) : convertedDate;

  const day = actualDate.getDate();
  const month = getMonth(actualDate.getMonth());
  const year = actualDate.getFullYear();

  if (withDay) {
    return `${day} ${month?.slice(0, 3)} ${year}`;
  }

  return `${month?.slice(0, 3)} ${year}`;
};
