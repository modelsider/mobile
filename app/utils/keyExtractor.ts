import uuid from 'uuidv4';

export const keyExtractor = () => `${uuid()}`;
