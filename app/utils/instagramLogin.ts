import qs from 'qs';

export const getResponseAuthInstagram = ({
  url,
}: {
  url: string;
}): { access_token?: string; code?: string; user_id?: string } => {
  const response = url.match(/(#|\?)(.*)/);
  const parsedResponse = qs.parse(response![2]);
  return parsedResponse;
};

export const getFormattedCode = ({ code }: { code: string }) =>
  (code = code.split('#_').join(''));
