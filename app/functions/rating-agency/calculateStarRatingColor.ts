import { ADDITIONAL_COLOURS } from 'styles/colours';

export const calculateStarRatingColor = (rating: number) => {
  return rating <= 2.0
    ? ADDITIONAL_COLOURS['primary-red']
    : rating <= 3.0 && rating >= 2.0
    ? ADDITIONAL_COLOURS['primary-yellow']
    : ADDITIONAL_COLOURS['primary-green'];
};

export const calculateStarRatingBackgroundColor = (rating: number) => {
  return rating <= 2.0
    ? ADDITIONAL_COLOURS['secondary-grey']
    : rating <= 3.0 && rating >= 2.0
    ? ADDITIONAL_COLOURS['secondary-yellow']
    : ADDITIONAL_COLOURS['secondary-green'];
};

export const calculateRateAgencyColor = (rating: number) => {
  return rating <= 2
    ? ADDITIONAL_COLOURS['primary-red']
    : rating <= 3 && rating >= 2
    ? ADDITIONAL_COLOURS['primary-yellow']
    : ADDITIONAL_COLOURS['primary-green'];
};

export const calculateRateAgencyEmoji = (rating: number) => {
  const emojis = {
    1: '😡',
    2: '😞',
    3: '😐',
    4: '😊',
    5: '😍',
  };

  return emojis[rating];
};

export const calculateRatedStatsColor = ({ rating }: { rating: number }) => {
  return rating <= 40
    ? ADDITIONAL_COLOURS['primary-red']
    : rating <= 60 && rating >= 41
    ? ADDITIONAL_COLOURS['primary-yellow']
    : ADDITIONAL_COLOURS['primary-green'];
};

export const calculateRatedItem = ({ rating }: { rating: number }) => {
  if (rating <= 20) {
    return 1;
  } else if (rating >= 20 && rating <= 40) {
    return 2;
  } else if (rating >= 40 && rating <= 60) {
    return 3;
  } else if (rating >= 60 && rating <= 80) {
    return 4
  } else {
    return 5
  }
};
