import { createSelector } from 'reselect';
import { RootState } from 'store/types/store';
import { commentType, IComment } from 'types/types';

const commentsSelector = (state: RootState) => state.comments;

const selectAgencyComments = createSelector(commentsSelector, ({ comments }): IComment[] =>
  comments.filter((comment) => !comment?.isReported),
);

const selectCommentReplies = createSelector(commentsSelector, ({ replies }) => replies);

const selectComment = createSelector(commentsSelector, ({ comment }): IComment => comment!);

const selectSortedAgencyCommentsByLikes = createSelector(commentsSelector, ({ comments }): IComment[] =>
  [...comments].sort((commentA, commentB) => commentB.likes_count - commentA.likes_count),
);

const selectFilteredCommentsByType = ({ type }: { type: commentType }) =>
  createSelector(commentsSelector, ({ comments }): IComment[] =>
    [...comments].filter((comment) => comment.type === type),
  );

const selectCommentIndex = createSelector(commentsSelector, ({ clickedCommentIndex }) => clickedCommentIndex || 0);

export {
  selectCommentIndex,
  selectCommentReplies,
  commentsSelector,
  selectAgencyComments,
  selectComment,
  selectSortedAgencyCommentsByLikes,
  selectFilteredCommentsByType,
};
