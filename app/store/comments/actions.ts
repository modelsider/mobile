import { createAsyncThunk } from '@reduxjs/toolkit';
import {
  addCommentFB,
  addReportComment,
  deleteCommentFB,
  dislikeCommentFB,
  getCommentByIdFB,
  getCommentsByAgencyIdFB,
  likeCommentFB,
  updateCommentFB,
} from 'services/comments';
import { IComment, ICommentLikeProps, IReply, agencyType as typeAgency, IPost } from 'types/types';
import { addReplyFB, getRepliesByCommentDocIdFB } from 'services/replies';
import { getLastItemFromArray } from 'utils/getLastItemFromArray';

const prefix_comments = '@comments/';
const prefix_replies = '@replies/';

export const setCommentsByAgencyId = createAsyncThunk(
  `${prefix_comments}setCommentsByAgencyId`,
  async ({
    agencyId,
    userId,
    agencyType,
  }: {
    agencyId: number;
    userId: string;
    agencyType: typeAgency;
  }): Promise<IComment[]> => {
    const comments = await getCommentsByAgencyIdFB({
      agencyId,
      userId,
      agencyType,
    });
    return comments;
  },
);

export const setCommentById = createAsyncThunk(
  `${prefix_comments}setCommentById`,
  async ({ commentId, userId }: { commentId: number; userId: string }): Promise<IComment> => {
    const comment = await getCommentByIdFB({ commentId, userId });
    return comment;
  },
);

export const toggleCommentLike = createAsyncThunk(
  `${prefix_comments}toggleCommentLike`,
  async ({ userId, commentId, isLikeLocal, likesCountLocal }: ICommentLikeProps) => {
    // if (isLikeLocal) {
    //   await likeCommentFB({ userId, commentId, isLikeLocal, likesCountLocal });
    // } else {
    //   await dislikeCommentFB({
    //     userId,
    //     commentId,
    //     isLikeLocal,
    //     likesCountLocal,
    //   });
    // }
  },
);

export const addComment = createAsyncThunk(
  `${prefix_comments}addComment`,
  async ({ comment }: { comment: IComment | IPost }): Promise<void> => {
    await addCommentFB({ comment });
  },
);

export const updateComment = createAsyncThunk(
  `${prefix_comments}updateComment`,
  async ({
    docId,
    comment,
  }: {
    docId: string;
    comment: IComment | IPost; // should be IComment partial
  }): Promise<void> => {
    await updateCommentFB({ docId, comment });
  },
);

export const deleteComment = createAsyncThunk(
  `${prefix_comments}deleteCommentFB`,
  async ({ docId }: { docId: string }): Promise<void> => {
    await deleteCommentFB({ docId });
  },
);

export const setUploadedComments = createAsyncThunk(
  `${prefix_comments}setUploadedComments`,
  async ({
    comments,
    userId,
    agencyId,
    agencyType,
  }: {
    agencyId: number;
    userId: string;
    comments: IComment[];
    agencyType: typeAgency;
  }): Promise<IComment[]> => {
    const startAfter = getLastItemFromArray(comments)?.created_at;

    const uploadedComments = await getCommentsByAgencyIdFB({
      agencyId,
      userId,
      startAfter,
      agencyType,
    });
    return uploadedComments;
  },
);

export const setRepliesByCommentDocId = createAsyncThunk(
  `${prefix_replies}setRepliesByCommentId`,
  async ({ docId }: { docId: string }): Promise<IReply[]> => {
    const replies = await getRepliesByCommentDocIdFB({ docId });
    return replies;
  },
);

export const addReply = createAsyncThunk(
  `${prefix_replies}addReply`,
  async ({
    commentDocId,
    reply,
    repliesCount,
  }: {
    commentDocId: string;
    reply: IReply;
    repliesCount: number;
  }): Promise<void> => {
    await addReplyFB({ commentDocId, reply, repliesCount });
  },
);

export const setUploadedReplies = createAsyncThunk(
  `${prefix_replies}setUploadedReplies`,
  async ({ commentDocId, replies }: { commentDocId: string; replies: IReply[] }): Promise<IReply[]> => {
    const startAfter = getLastItemFromArray(replies)?.created_at;

    const uploadedReplies = await getRepliesByCommentDocIdFB({
      docId: commentDocId,
      startAfter,
    });
    return uploadedReplies;
  },
);

export const setReportComment = createAsyncThunk(
  `${prefix_comments}setReportComment`,
  async ({
    userIdWhoReported,
    commentId,
    userId,
  }: {
    userIdWhoReported: string;
    commentId: number;
    userId: string;
  }): Promise<void> => {
    await addReportComment({ userIdWhoReported, commentId, userId });
  },
);
