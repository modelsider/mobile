import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IComment, IReply } from 'types/types';
import {
  setCommentById,
  setCommentsByAgencyId,
  setRepliesByCommentDocId,
  setUploadedComments,
  setUploadedReplies,
} from './actions';
import uniqBy from 'lodash/uniqBy';

interface IInitialState {
  comments: IComment[];
  comment: IComment | null;
  replies: IReply[];
  clickedCommentIndex: number;
}

const initialState: IInitialState = {
  comments: [],
  comment: null,
  replies: [],
  clickedCommentIndex: 0,
};

const { reducer, actions } = createSlice({
  name: 'comments',
  initialState,
  reducers: {
    setClickedCommentIndex: (state, { payload: { commentIndex } }: PayloadAction<{ commentIndex: number }>) => {
      state.clickedCommentIndex = commentIndex;
    },
    resetCommentState: (state) => {
      state.comment = initialState.comment;
    },
    resetCommentsState: (state) => {
      state.comment = initialState.comment;
      state.comments = initialState.comments;
      state.replies = initialState.replies;
      state.clickedCommentIndex = initialState.clickedCommentIndex;
    },
  },
  extraReducers: (builder) => {
    builder.addCase<any, PayloadAction<IComment[]>>(setCommentsByAgencyId.fulfilled, (state, { payload }) => {
      state.comments = payload;
    });
    builder.addCase<any, PayloadAction<IComment>>(setCommentById.fulfilled, (state, { payload }) => {
      state.comment = payload;
    });
    builder.addCase<any, PayloadAction<IComment[]>>(setUploadedComments.fulfilled, (state, { payload }) => {
      state.comments = uniqBy([...state.comments, ...payload], 'id');
    });
    builder.addCase<any, PayloadAction<IReply[]>>(setRepliesByCommentDocId.fulfilled, (state, { payload }) => {
      state.replies = payload;
    });
    builder.addCase<any, PayloadAction<IReply[]>>(setUploadedReplies.fulfilled, (state, { payload }) => {
      state.replies = uniqBy([...state.replies, ...payload], 'id');
    });
  },
});
export const { setClickedCommentIndex, resetCommentsState, resetCommentState } = actions;
export default reducer;
