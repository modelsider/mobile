import { createSelector } from 'reselect';
import { RootState } from 'store/types/store';
import { IAgency, ISuggestAgency } from '../../types/types';

const agencySelector = (state: RootState) => state.agency;

export const selectAgencyType = createSelector(agencySelector, ({ selectedAgencyType }) => selectedAgencyType);

export const selectSuggestAgency = createSelector(
  agencySelector,
  ({ suggestAgency }) => suggestAgency ?? ({} as ISuggestAgency),
);

export const selectAgency = createSelector(agencySelector, ({ agency }) => agency ?? ({} as IAgency));

export const selectRatedAgencies = createSelector(agencySelector, ({ ratedAgencies }) => ratedAgencies);

export const selectWhoRatedAgencies = createSelector(agencySelector, ({ whoRatedAgencies }) => whoRatedAgencies);

export const selectAgencyId = createSelector(agencySelector, ({ agencyId }) => agencyId);

export const selectAgencyById = createSelector(agencySelector, ({ agencyId, ratedAgencies }) =>
  ratedAgencies.find(({ id }) => id === agencyId),
);

export const selectAgencyByIdAndType = createSelector(
  agencySelector,
  ({ agencyId, selectedAgencyType, ratedAgencies }) =>
    ratedAgencies.find(({ id }) => id === agencyId)?.[selectedAgencyType],
);

export const selectAgencyLoading = createSelector(agencySelector, ({ isLoadingAgency }) => isLoadingAgency);

export const selectWhoRatedAgency = ({ userId }: { userId: string }) =>
  createSelector(agencySelector, ({ agencyId, selectedAgencyType, whoRatedAgencies }) =>
    whoRatedAgencies.find(
      ({ agency_id, type, user_id }) => agency_id === agencyId && type === selectedAgencyType && user_id === userId,
    ),
  );

export const selectAgencies = createSelector(agencySelector, ({ agencies }) => agencies ?? ([] as IAgency[]));
