import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { setAgency, setAllAgencies, setRatedAgencies, setUploadedAgencies } from './actions';
import { agencyType, IAgency, ISuggestAgency, IWhoRatedAgency } from 'types/types';
import uniqBy from 'lodash/uniqBy';

interface IAgencyState {
  selectedAgencyType: agencyType;
  suggestAgency: ISuggestAgency | null;
  agency: IAgency | null;
  ratedAgencies: IAgency[];
  agencyId: number;
  isLoadingAgency: boolean; // need for HeaderAgency.tsx
  whoRatedAgencies: IWhoRatedAgency[];
  agencies: IAgency[];
}

const initialState: IAgencyState = {
  selectedAgencyType: 'mother',
  suggestAgency: null,
  agency: null,
  ratedAgencies: [],
  agencyId: 0,
  isLoadingAgency: false,
  whoRatedAgencies: [],
  agencies: [],
};

const { reducer, actions } = createSlice({
  name: 'agency',
  initialState,
  reducers: {
    selectAgencyType: (state, { payload: { type } }: PayloadAction<{ type: agencyType }>) => {
      state.selectedAgencyType = type;
    },
    setAgencyId: (state, { payload: { agencyId } }: PayloadAction<{ agencyId: number }>) => {
      state.agencyId = agencyId;
    },
    setSuggestAgency: (state, { payload: { agency } }: PayloadAction<{ agency: ISuggestAgency }>) => {
      state.suggestAgency = { ...state.suggestAgency!, ...agency };
    },
    resetAgencyState: (state) => {
      state.agency = initialState.agency;
      state.agencyId = initialState.agencyId;
      state.suggestAgency = initialState.suggestAgency;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(setAgency.pending, (state) => {
      state.isLoadingAgency = true;
    });
    builder.addCase(setAgency.fulfilled, (state, { payload }) => {
      state.agency = payload;
      state.isLoadingAgency = false;
    });
    builder.addCase(setAgency.rejected, (state) => {
      state.isLoadingAgency = false;
    });

    builder.addCase(setRatedAgencies.fulfilled, (state, { payload: { agencies, whoRatedAgencies } }) => {
      state.ratedAgencies = agencies;
      state.whoRatedAgencies = whoRatedAgencies;
    });

    builder.addCase(setUploadedAgencies.fulfilled, (state, { payload: { agencies, whoRatedAgencies } }) => {
      state.ratedAgencies = uniqBy([...state.ratedAgencies, ...agencies], 'id');
      state.whoRatedAgencies = uniqBy([...state.whoRatedAgencies, ...whoRatedAgencies], 'id');
    });

    builder.addCase(setAllAgencies.fulfilled, (state, { payload }) => {
      state.agencies = payload;
    });
  },
});

export const { selectAgencyType, setSuggestAgency, setAgencyId, resetAgencyState } = actions;

export default reducer;
