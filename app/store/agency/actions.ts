import { createAsyncThunk } from '@reduxjs/toolkit';
import {
  addWhoRatedAgencyFB,
  getAgenciesFB,
  getAgencyById,
  getRatedAgenciesFB,
  removeWhoRatedAgencyFB,
  setIsRatingAgencyNowFB,
  updateRatedAgencyFB,
  updateWhoRatedAgencyFB,
} from 'services/agencies';
import { agencyType, IAgency, IAgencyTab, IWhoRatedAgency } from 'types/types';
import { getCorrectStatisticOrder } from '../../utils/getCorrectStatisticOrder';
import { getLastItemFromArray } from '../../utils/getLastItemFromArray';

const prefix = '@agency/';

export const setRatedAgencies = createAsyncThunk(
  `${prefix}setRatedAgencies`,
  async ({
    userId,
    type,
  }: {
    userId: string;
    type: agencyType;
  }): Promise<{ agencies: IAgency[]; whoRatedAgencies: IWhoRatedAgency[] }> => {
    const { mappedAgencies, whoRatedAgencies } = await getRatedAgenciesFB({
      userId,
      type,
    });
    return {
      agencies: mappedAgencies,
      whoRatedAgencies,
    };
  },
);

export const setUploadedAgencies = createAsyncThunk(
  `${prefix}setUploadedAgencies`,
  async ({
    agencies,
    userId,
    type,
  }: {
    userId: string;
    agencies: IAgency[];
    type: agencyType;
  }): Promise<{ agencies: IAgency[]; whoRatedAgencies: IWhoRatedAgency[] }> => {
    const startAfter = getLastItemFromArray(agencies).agency_id;

    const { mappedAgencies, whoRatedAgencies } = await getRatedAgenciesFB({
      userId,
      type,
      startAfter,
    });

    return {
      agencies: mappedAgencies,
      whoRatedAgencies,
    };
  },
);

export const updateRatedAgency = createAsyncThunk(
  `${prefix}updateRatedAgency`,
  async ({
    docId,
    data,
  }: {
    docId: string;
    data: IAgencyTab; // todo: add interface
  }): Promise<boolean> => {
    const response = await updateRatedAgencyFB({ docId, data });
    return response;
  },
);

export const setAgency = createAsyncThunk(
  `${prefix}setAgency`,
  async ({ agencyId, userId, type }: { agencyId: number; userId: string; type: agencyType }): Promise<IAgency> => {
    const agency = await getAgencyById({ userId, agencyId, type });

    const sortedAgencyStatistics = agency?.[type]?.statistics.sort((a, b) => getCorrectStatisticOrder(a, b, type));

    const correctAgency = {
      ...agency,
      [type]: { ...agency?.[type], statistics: sortedAgencyStatistics },
      isClosed: agency?.closed_at?.length !== 0,
    };
    return correctAgency;
  },
);

export const addWhoRatedAgency = createAsyncThunk(
  `${prefix}addWhoRatedAgency`,
  async ({ whoRatedAgency }: { whoRatedAgency: IWhoRatedAgency }): Promise<boolean> => {
    const response = await addWhoRatedAgencyFB({ data: whoRatedAgency });
    return response;
  },
);

export const updateWhoRatedAgency = createAsyncThunk(
  `${prefix}updateWhoRatedAgency`,
  async ({
    docId,
    data,
  }: {
    docId: string;
    data: IWhoRatedAgency | { start_working_at: any; finish_working_at: any };
  }): Promise<boolean> => {
    const response = await updateWhoRatedAgencyFB({ docId, data });
    return response;
  },
);

export const removeWhoRatedAgency = createAsyncThunk(
  `${prefix}removeRatedAgency`,
  async ({ docId }: { docId: string }): Promise<void> => {
    await removeWhoRatedAgencyFB({ docId });
  },
);

export const setIsRatingAgencyNow = createAsyncThunk(
  `${prefix}setIsRatingAgencyNow`,
  async ({ isRatingNow, docId }: { isRatingNow: boolean; docId: string }): Promise<void> => {
    await setIsRatingAgencyNowFB({ isRatingNow, docId });
  },
);

export const setAllAgencies = createAsyncThunk(`${prefix}setAllAgencies`, async (): Promise<IAgency[]> => {
  const { agencies } = await getAgenciesFB();
  return agencies;
});
