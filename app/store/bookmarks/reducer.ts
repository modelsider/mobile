import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IAgency, IComment } from 'types/types';
import {
  setBookmarkedAgencies,
  setBookmarkedComments,
  setUploadedBookmarkedAgencies,
  setUploadedBookmarkedComments,
} from './actions';
import uniqBy from 'lodash/uniqBy';

interface IInitialState {
  agencies: IAgency[];
  comments: IComment[];
}

const initialState: IInitialState = {
  agencies: [],
  comments: [],
};

const { reducer, actions } = createSlice({
  name: 'bookmarks',
  initialState,
  reducers: {
    clearBookmarks: (state) => {
      state.agencies = initialState.agencies;
      state.comments = initialState.comments;
    },
  },
  extraReducers: (builder) => {
    builder.addCase<any, PayloadAction<IAgency[]>>(setBookmarkedAgencies.fulfilled, (state, { payload }) => {
      state.agencies = payload;
    });
    builder.addCase<any, PayloadAction<IComment[]>>(setBookmarkedComments.fulfilled, (state, { payload }) => {
      state.comments = payload;
    });
    builder.addCase<any, PayloadAction<IComment[]>>(setUploadedBookmarkedComments.fulfilled, (state, { payload }) => {
      state.comments = uniqBy([...state.comments, ...payload], 'id');
    });
    builder.addCase<any, PayloadAction<IAgency[]>>(setUploadedBookmarkedAgencies.fulfilled, (state, { payload }) => {
      state.agencies = uniqBy([...state.agencies, ...payload], 'id');
    });
  },
});

export const { clearBookmarks } = actions;

export default reducer;
