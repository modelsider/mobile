import { createSelector } from 'reselect';
import { RootState } from 'store/types/store';

const bookmarksSelector = (state: RootState) => state.bookmarks;

const selectBookmarkedAgencies = createSelector(bookmarksSelector, ({ agencies }) => agencies);

const selectBookmarkedComments = createSelector(bookmarksSelector, ({ comments }) => comments);

export { selectBookmarkedAgencies, selectBookmarkedComments };
