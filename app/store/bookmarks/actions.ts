import { createAsyncThunk } from '@reduxjs/toolkit';
import { IAgency, IComment, INewBookmark, IReply } from 'types/types';
import { addBookmarkFB, getBookmarkFB, removeBookmarkFB } from 'services/bookmarks';
import { getBookmarkedAgenciesFB } from 'services/agencies';
import { getBookmarkedCommentsFB } from 'services/comments';
import { getLastItemFromArray } from 'utils/getLastItemFromArray';

const prefix = '@bookmarks/';

// todo: remove
export const toggleBookmark = createAsyncThunk(
  `${prefix}addBookmark`,
  async ({ userId, bookmarkId, type, isBookmarkLocal, created_at }: INewBookmark) => {
    const bookmark = await getBookmarkFB({
      userId,
      bookmarkId,
      type,
    });

    if (isBookmarkLocal) {
      await addBookmarkFB({ userId, bookmarkId, type, created_at });
    } else {
      await removeBookmarkFB({ docId: bookmark?.docId });
    }
  },
);

export const setBookmarkedAgencies = createAsyncThunk(
  `${prefix}setBookmarkedAgencies`,
  async ({ userId }: { userId: number | string }) => {
    const agencies = await getBookmarkedAgenciesFB({ userId });
    return agencies;
  },
);

export const setUploadedBookmarkedAgencies = createAsyncThunk(
  `${prefix}setUploadedBookmarkedAgencies`,
  async ({ userId, bookmarkedAgencies }: { userId: string; bookmarkedAgencies: IAgency[] }): Promise<IAgency[]> => {
    const startAfter = getLastItemFromArray(bookmarkedAgencies).id;
    const uploadedBookmarkedAgencies = await getBookmarkedAgenciesFB({
      userId,
      startAfter,
    });
    return uploadedBookmarkedAgencies;
  },
);

export const setBookmarkedComments = createAsyncThunk(
  `${prefix}setBookmarkedComments`,
  async ({ userId }: { userId: string }) => {
    const comments = await getBookmarkedCommentsFB({ userId });
    return comments;
  },
);

export const setUploadedBookmarkedComments = createAsyncThunk(
  `${prefix}setUploadedBookmarkedComments`,
  async ({
    userId,
    bookmarkedComments,
  }: {
    userId: string;
    bookmarkedComments: IComment[];
  }): Promise<IReply[] | IComment[]> => {
    const startAfter = getLastItemFromArray(bookmarkedComments).bookmarkCreatedAt;

    const uploadedBookmarkedComments = await getBookmarkedCommentsFB({
      userId,
      startAfter,
    });
    return uploadedBookmarkedComments;
  },
);
