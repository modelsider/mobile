import rootReducer from '../reducers/rootReducer';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';

export type RootState = ReturnType<typeof rootReducer>;

export type DispatchType = ThunkDispatch<RootState, undefined, AnyAction>;
