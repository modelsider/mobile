import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { setUpdateUserInfo } from './actions';

export type statusType = 'waiting' | 'approved' | 'refused' | 'default' | '';

export interface IUserInfo {
  id: string;
  name: string;
  dateOfBirth: string;
  location: string;
  email: string;
  userName: string;
  profilePictureUrl: string;
  status: statusType;
  notifications: {
    bookmarksAgencies: boolean;
    bookmarksComments: boolean;
    likes: boolean;
    replies: boolean;
    news: boolean;
  };
  docId: string;
  is_anonymous?: boolean;
  created_at?: any;
  createdAt?: any; // todo: change with created_at
  is_first_time_login: boolean | null;
  gender: string;
  size: string;
  height: string;
}

interface IAuthState {
  userInfo: IUserInfo;
  isUpdateInstagramData: boolean;
}

const initialState: IAuthState = {
  userInfo: {
    id: '',
    name: '',
    dateOfBirth: '',
    location: '',
    email: '',
    userName: '',
    profilePictureUrl: '',
    status: '',
    docId: '',
    created_at: '',
    notifications: {
      bookmarksAgencies: true,
      bookmarksComments: true,
      likes: true,
      replies: true,
      news: true,
    },
    is_first_time_login: null,
    gender: '',
    height: '',
    size: '',
  },
  isUpdateInstagramData: false,
};

const { reducer, actions } = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setIsUpdateInstagramData: (state, { payload: { isUpdate } }: PayloadAction<{ isUpdate: boolean }>) => {
      state.isUpdateInstagramData = isUpdate;
    },
    clearUserInfoState: (state) => {
      state.userInfo = initialState.userInfo;
    },
    setUserStatus: (state, { payload: { status } }: PayloadAction<{ status: statusType }>) => {
      state.userInfo.status = status;
    },
    setName: (state, { payload: { name } }: PayloadAction<{ name: string }>) => {
      state.userInfo.name = name;
    },
    setUserName: (state, { payload: { userName } }: PayloadAction<{ userName: string }>) => {
      state.userInfo.userName = userName;
    },
    setProfilePictureUrl: (state, { payload: { url } }: PayloadAction<{ url: string }>) => {
      state.userInfo.profilePictureUrl = url;
    },
    setUserId: (state, { payload: { id } }: PayloadAction<{ id: string }>) => {
      state.userInfo.id = id;
    },
    setUserDateOfBirth: (state, { payload: { dateOfBirth } }: PayloadAction<{ dateOfBirth: any }>) => {
      state.userInfo.dateOfBirth = dateOfBirth;
    },
    setUserLocation: (state, { payload: { location } }: PayloadAction<{ location: string }>) => {
      state.userInfo.location = location;
    },
    setUserEmail: (state, { payload: { email } }: PayloadAction<{ email: string }>) => {
      state.userInfo.email = email;
    },
    setIsFirstTimeLogin: (
      state,
      { payload: { is_first_time_login } }: PayloadAction<{ is_first_time_login: boolean }>,
    ) => {
      state.userInfo.is_first_time_login = is_first_time_login;
    },
    setGenderSizeHeight: (
      state,
      { payload: { gender, height, size } }: PayloadAction<{ gender: string; size: string; height: string }>,
    ) => {
      state.userInfo.gender = gender;
      state.userInfo.height = height;
      state.userInfo.size = size;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(setUpdateUserInfo.fulfilled, (state, { payload: { userInfo } }) => {
      state.userInfo = { ...state.userInfo, ...userInfo };
    });
  },
});

export const {
  setUserStatus,
  setName,
  setUserName,
  setUserId,
  setUserDateOfBirth,
  setUserLocation,
  setUserEmail,
  setIsUpdateInstagramData,
  clearUserInfoState,
  setProfilePictureUrl,
  setIsFirstTimeLogin,
  setGenderSizeHeight,
} = actions;

export default reducer;
