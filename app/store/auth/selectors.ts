import { createSelector } from 'reselect';
import { RootState } from 'store/types/store';
import { IUserInfo } from './reducer';

const authSelector = (state: RootState) => state.auth;

export const selectUserInfo = createSelector(authSelector, (auth) => auth.userInfo || ({} as IUserInfo));

export const selectIsUpdateInstagramData = createSelector(
  authSelector,
  ({ isUpdateInstagramData }) => isUpdateInstagramData,
);
