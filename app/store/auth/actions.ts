import { createAsyncThunk } from '@reduxjs/toolkit';
import { IUserInfo } from './reducer';
import { getUser } from 'services/users';

const prefix = '@auth/';

export const setUpdateUserInfo = createAsyncThunk<
  { userInfo: IUserInfo | any }, // todo: fix
  { userId: string }
>(`${prefix}setUpdateUserInfo`, async ({ userId }) => {
  const userInfo = await getUser({ id: userId });
  return { userInfo };
});
