import app from '../app/reducer';
import auth from '../auth/reducer';
import agency from '../agency/reducer';
import rateAgency from '../rate-agency/reducer';
import bookmarks from '../bookmarks/reducer';
import comments from '../comments/reducer';
import news from '../news/reducer';
import users from '../users/reducer';
import countries from '../countries/reducer';
import feed from '../feed/reducer';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  auth,
  app,
  agency,
  rateAgency,
  bookmarks,
  comments,
  news,
  users,
  countries,
  feed,
});

export default rootReducer;
