import { createSelector } from 'reselect';
import { RootState } from 'store/types/store';

const appSelector = (state: RootState) => state.app;

const selectStatusBar = createSelector(appSelector, ({ isStatusBar }) => isStatusBar);

const selectStatusBarBg = createSelector(appSelector, ({ statusBarBg }) => statusBarBg);

const selectIsGlobalLoader = createSelector(appSelector, ({ isGlobalLoader }) => isGlobalLoader);

const selectIsLogoAppLoader = createSelector(appSelector, ({ isLogoAppLoader }) => isLogoAppLoader);

const selectTabViewType = createSelector(appSelector, ({ tabViewType }) => tabViewType);
const selectHasRating = createSelector(appSelector, ({ hasRating }) => hasRating);

export {
  selectStatusBar,
  selectStatusBarBg,
  selectIsGlobalLoader,
  selectIsLogoAppLoader,
  selectTabViewType,
  selectHasRating,
};
