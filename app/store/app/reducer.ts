import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { tabViewType } from 'types/types';

interface IAppState {
  isStatusBar: boolean;
  statusBarBg: string;
  isGlobalLoader: boolean;
  isLogoAppLoader: boolean;
  tabViewType: tabViewType;
  hasRating: boolean | null;
}

const initialState: IAppState = {
  isStatusBar: true,
  statusBarBg: '',
  isGlobalLoader: false,
  isLogoAppLoader: false,
  tabViewType: 'login',
  hasRating: null,
};

const { reducer, actions } = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setStatusBar: (state, { payload: { isStatusBar } }: PayloadAction<{ isStatusBar: boolean }>) => {
      state.isStatusBar = isStatusBar;
    },
    setStatusBarBg: (state, { payload: { bg } }: PayloadAction<{ bg: string }>) => {
      state.statusBarBg = bg;
    },
    setGlobalLoader: (state, { payload: { isLoading } }: PayloadAction<{ isLoading: boolean }>) => {
      state.isGlobalLoader = isLoading;
    },
    setLogoAppLoader: (state, { payload: { isLoading } }: PayloadAction<{ isLoading: boolean }>) => {
      state.isLogoAppLoader = isLoading;
    },
    setTabView: (state, { payload: { type } }: PayloadAction<{ type: tabViewType }>) => {
      state.tabViewType = type;
    },
    setIfRating: (state, { payload: { hasRating } }: PayloadAction<{ hasRating: boolean | null }>) => {
      state.hasRating = hasRating;
    },
  },
});

export const { setTabView, setStatusBar, setStatusBarBg, setGlobalLoader, setLogoAppLoader, setIfRating } = actions;

export default reducer;
