import { configureStore, createImmutableStateInvariantMiddleware } from '@reduxjs/toolkit';
import { persistReducer, persistStore } from 'redux-persist';
import thunk from 'redux-thunk';
import AsyncStorage from '@react-native-async-storage/async-storage';
import rootReducer from './reducers/rootReducer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['feed'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const immutableInvariantMiddleware = createImmutableStateInvariantMiddleware({});

const store = configureStore({
  reducer: persistedReducer,
  devTools: true,
  middleware: [thunk, immutableInvariantMiddleware],
});

const persistor = persistStore(store);
export { store, persistor };
