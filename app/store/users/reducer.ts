import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { setUserProfileById, setUsersByAgencyId } from './actions';
import { IUserInfo } from 'store/auth/reducer';

interface IInitialState {
  users: IUserInfo[];
  user: IUserInfo | null;
  selectedUserProfileId: string | null;
}

const initialState: IInitialState = {
  users: [],
  user: null,
  selectedUserProfileId: null,
};

const { reducer, actions } = createSlice({
  name: 'users',
  initialState,
  reducers: {
    setSelectedUserProfileId: (state, { payload: { userId } }: PayloadAction<{ userId: string | null }>) => {
      state.selectedUserProfileId = userId;
    },
    resetUserProfileStore: (state) => {
      state.user = initialState.user;
      state.users = initialState.users;
      state.selectedUserProfileId = initialState.selectedUserProfileId;
    },
  },
  extraReducers: (builder) => {
    builder.addCase<any, PayloadAction<IUserInfo[]>>(setUsersByAgencyId.fulfilled, (state, { payload }) => {
      state.users = payload;
    });
    builder.addCase<any, PayloadAction<IUserInfo>>(setUserProfileById.fulfilled, (state, { payload }) => {
      state.user = payload;
    });
  },
});

export const { setSelectedUserProfileId, resetUserProfileStore } = actions;

export default reducer;
