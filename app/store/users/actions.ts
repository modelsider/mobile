import { createAsyncThunk } from '@reduxjs/toolkit';
import { getUser, getUsersByAgencyIdFB } from 'services/users';
import { IUserInfo } from 'store/auth/reducer';
import { agencyType } from 'types/types';
const prefix = '@users/';

export const setUsersByAgencyId = createAsyncThunk(
  `${prefix}setUsers`,
  async ({ agencyId, type }: { agencyId: number; type: agencyType }): Promise<IUserInfo[]> => {
    const users = await getUsersByAgencyIdFB({ agencyId, type });
    return users;
  },
);

export const setUserProfileById = createAsyncThunk(
  `${prefix}setUserProfileById`,
  async ({ id }: { id: string }): Promise<IUserInfo> => {
    const user = await getUser({ id });
    return user;
  },
);
