import { createSelector } from 'reselect';
import { RootState } from 'store/types/store';
import { IUserInfo } from '../auth/reducer';

const usersSelector = (state: RootState) => state.users;

export const selectUsers = createSelector(usersSelector, ({ users }): IUserInfo[] => users || ([] as IUserInfo[]));

export const selectUserProfile = createSelector(usersSelector, ({ user }): IUserInfo => user! || ({} as IUserInfo));

export const selectUserProfileId = createSelector(
  usersSelector,
  ({ selectedUserProfileId }): string => selectedUserProfileId!,
);
