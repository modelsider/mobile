import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ICountry } from 'types/types';
import { setCountries } from './actions';

interface IInitialState {
  countries: ICountry[] | null;
}

const initialState: IInitialState = {
  countries: [],
};

const { reducer } = createSlice({
  reducers: {},
  name: 'countries',
  initialState,
  extraReducers: (builder) => {
    builder.addCase<any, PayloadAction<ICountry[] | null>>(setCountries.fulfilled, (state, { payload }) => {
      state.countries = payload;
    });
  },
});

export default reducer;
