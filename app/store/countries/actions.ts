import { createAsyncThunk } from '@reduxjs/toolkit';
import { ICountry } from 'types/types';
import { getCountriesFB } from 'services/countries';

const prefix = '@countries/';

export const setCountries = createAsyncThunk(`${prefix}setCountries`, async (): Promise<ICountry[] | null> => {
  const countries = await getCountriesFB();
  return countries;
});
