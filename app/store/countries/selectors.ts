import { createSelector } from 'reselect';
import { RootState } from 'store/types/store';

const countriesSelector = (state: RootState) => state.countries;

const selectCountries = createSelector(countriesSelector, ({ countries }) => countries);

export { selectCountries };
