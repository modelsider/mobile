import { createSelector } from 'reselect';
import { RootState } from 'store/types/store';
import omit from 'lodash/omit';
import { agencyType } from '../../types/types';

const rateAgencySelector = (state: RootState) => state.rateAgency;

export const selectLastRatedAgency = ({ type }: { type: agencyType }) =>
  createSelector(rateAgencySelector, ({ lastRatedAgency }) => {
    // remove empty booking or mother depends on type
    return omit(lastRatedAgency, type === 'mother' ? 'booking' : 'mother');
  });

export const selectIsUpdateDates = createSelector(
  rateAgencySelector,
  ({ isUpdateDates }) => isUpdateDates
);

export const selectIsUpdateOnlyRating = createSelector(
  rateAgencySelector,
  ({ isUpdateOnlyRating }) => isUpdateOnlyRating
);

export const selectModelsEarnedMoney = createSelector(
  rateAgencySelector,
  ({ modelsEarnedMoney }) => modelsEarnedMoney
);

export const selectDotsStateCount = createSelector(
  rateAgencySelector,
  ({ checkDotsStateCount }) => checkDotsStateCount
);
