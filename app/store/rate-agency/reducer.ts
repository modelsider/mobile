import { createSlice, current, PayloadAction } from '@reduxjs/toolkit';
import { agencyType, IWhoRatedAgency } from 'types/types';
import uniqBy from 'lodash/uniqBy';

interface IInitialState {
  lastRatedAgency: IWhoRatedAgency | any;
  isUpdateDates: boolean;
  isUpdateOnlyRating: boolean;
  modelsEarnedMoney: number;
  checkDotsStateCount: number;
}

const initialState: IInitialState = {
  lastRatedAgency: {
    mother: [],
    booking: [],
  },
  isUpdateDates: false,
  isUpdateOnlyRating: false,
  modelsEarnedMoney: 0,
  checkDotsStateCount: 0,
};

const { reducer, actions } = createSlice({
  name: 'rateAgency',
  initialState,
  reducers: {
    setWorkingTime: (state, { payload: { agency } }: PayloadAction<{ agency: IWhoRatedAgency }>) => {
      const key = Object.keys(agency)[0];
      const value = Object.values(agency)[0];

      state.lastRatedAgency = { ...state.lastRatedAgency!, [key]: value };
    },
    setRatedAgency: (
      state,
      { payload: { type, statistic } }: PayloadAction<{ type: agencyType; statistic: any }>, // todo: add interface
    ) => {
      const statistics = [...current(state).lastRatedAgency[type], statistic].reverse();

      state.lastRatedAgency = {
        ...state.lastRatedAgency,
        [type]: uniqBy(statistics, 'type'),
      };
    },
    resetRatedAgency: (state) => {
      state.lastRatedAgency = initialState.lastRatedAgency;
      state.modelsEarnedMoney = initialState.modelsEarnedMoney;
      state.checkDotsStateCount = initialState.checkDotsStateCount;
      state.isUpdateDates = initialState.isUpdateDates;
      state.isUpdateOnlyRating = initialState.isUpdateOnlyRating;
    },
    setIsUpdateDates: (state, { payload: { isUpdate } }: PayloadAction<{ isUpdate: boolean }>) => {
      state.isUpdateDates = isUpdate;
    },
    setIsUpdateOnlyRating: (state, { payload: { isUpdate } }: PayloadAction<{ isUpdate: boolean }>) => {
      state.isUpdateOnlyRating = isUpdate;
    },
    setModelsEarnedMoney: (state, { payload: { count } }: PayloadAction<{ count: number }>) => {
      state.modelsEarnedMoney = count;
    },
    setCheckDotsStateCount: (state, { payload: { count } }: PayloadAction<{ count: number }>) => {
      state.checkDotsStateCount = count;
    },
  },
});

export const {
  setWorkingTime,
  resetRatedAgency,
  setRatedAgency,
  setIsUpdateDates,
  setIsUpdateOnlyRating,
  setModelsEarnedMoney,
  setCheckDotsStateCount,
} = actions;

export default reducer;
