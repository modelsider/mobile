import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { setFeed, setRatings } from './actions';
import { IAgency, IComment, IPost } from 'types/types';

interface IUpdateFeedPayload {
  id: number;
  likesCount?: number;
  isLike?: boolean;
  isBookmark?: boolean;
}

interface IFeedState {
  ratings: IAgency[];
  feed: (IComment | IPost)[];
  loading: boolean;
}

const initialState: IFeedState = {
  feed: [],
  ratings: [],
  loading: false,
};

const { reducer, actions } = createSlice({
  name: 'feed',
  initialState,
  reducers: {
    updateFeed: (state, { payload }: PayloadAction<IUpdateFeedPayload>) => {
      const { id, likesCount, isLike, isBookmark } = payload;
      const newFeed = state.feed.map((feedItem) =>
        feedItem?.id === id
          ? {
              ...feedItem,
              likes_count: likesCount ?? feedItem?.likes_count,
              isLike: isLike ?? feedItem?.isLike,
              isBookmark: isBookmark ?? feedItem?.isBookmark,
            }
          : feedItem,
      );
      return { ...state, feed: newFeed };
    },
    resetFeed: (state) => {
      state.feed = initialState.feed;
    },
    resetRatings: (state) => {
      state.ratings = initialState.ratings;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(setFeed.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(setFeed.fulfilled, (state, { payload }) => {
      state.feed = [...state.feed, ...payload];
      state.loading = false;
    });
    builder.addCase(setFeed.rejected, (state) => {
      state.loading = false;
    });

    builder.addCase(setRatings.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(setRatings.fulfilled, (state, { payload }) => {
      state.ratings = [...state.ratings, ...payload];
      state.loading = false;
    });
    builder.addCase(setRatings.rejected, (state) => {
      state.loading = false;
    });
  },
});

export const { updateFeed, resetFeed, resetRatings } = actions;

export default reducer;
