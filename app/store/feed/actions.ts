import { createAsyncThunk } from '@reduxjs/toolkit';
import { IAgency, IComment, IPost } from 'types/types';
import { getFeedFB, getRatingsFB } from 'services/feed';
import { getLastItemFromArray } from '../../utils/getLastItemFromArray';

const prefix = '@feed/';

export const setFeed = createAsyncThunk(
  `${prefix}setFeed`,
  async ({ feed }: { feed: (IComment | IPost)[] }): Promise<(IComment | IPost)[]> => {
    const startAfter = getLastItemFromArray(feed)?.created_at;

    const feedData = await getFeedFB({ startAfter: feed.length ? startAfter : undefined });
    return feedData;
  },
);

export const setRatings = createAsyncThunk(
  `${prefix}setRatings`,
  async ({ agencies }: { agencies: IAgency[] }): Promise<IAgency[]> => {
    const startAfter = getLastItemFromArray(agencies)?.created_at;

    const data = await getRatingsFB({ startAfter: agencies.length ? startAfter : undefined });
    return data;
  },
);
