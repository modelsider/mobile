import { createSelector } from 'reselect';
import { RootState } from 'store/types/store';

const feedSelector = (state: RootState) => state.feed;

const selectFeed = createSelector(feedSelector, ({ feed }) => feed);
const selectFeedLoading = createSelector(feedSelector, ({ loading }) => loading);

const selectRatings = createSelector(feedSelector, ({ ratings }) => ratings);

export { selectFeed, selectFeedLoading, selectRatings };
