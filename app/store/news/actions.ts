import { createAsyncThunk } from '@reduxjs/toolkit';
import { INews } from 'types/types';
import { getNewsCounterFB, getNewsFB, removeNewsCounterCollectionFB } from 'services/news';

const prefix = '@news/';

const setNews = createAsyncThunk(`${prefix}setNews`, async ({ userId }: { userId: string }): Promise<INews[]> => {
  const news = await getNewsFB({ userId });
  return news;
});

const setNewsCounter = createAsyncThunk(
  `${prefix}setNewsCounter`,
  async ({ userId }: { userId: string }): Promise<number> => {
    const newsCounter = await getNewsCounterFB({ userId });
    return newsCounter;
  },
);

const removeNewsCounter = createAsyncThunk(
  `${prefix}removeNewsCounter`,
  async ({ userId }: { userId: string }): Promise<void> => {
    await removeNewsCounterCollectionFB({ userId });
  },
);

export { setNews, setNewsCounter, removeNewsCounter };
