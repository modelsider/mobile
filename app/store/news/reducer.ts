import { createSlice } from '@reduxjs/toolkit';
import { INews } from 'types/types';
import { removeNewsCounter, setNews, setNewsCounter } from './actions';

interface INewsState {
  news: INews[];
  newsCounter: number;
}

const initialState: INewsState = {
  news: [],
  newsCounter: 0,
};

const { reducer, actions } = createSlice({
  name: 'news',
  initialState,
  reducers: {
    resetNewsCounter: (state) => {
      state.newsCounter = initialState.newsCounter;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(setNews.fulfilled, (state, { payload }) => {
      state.news = payload;
    });
    builder.addCase(setNewsCounter.fulfilled, (state, { payload }) => {
      state.newsCounter = payload;
    });
    builder.addCase(removeNewsCounter.fulfilled, (state) => {
      state.newsCounter = initialState.newsCounter;
    });
  },
});

export const { resetNewsCounter } = actions;

export default reducer;
