import { createSelector } from 'reselect';
import { RootState } from 'store/types/store';

const newsSelector = (state: RootState) => state.news;

const selectNews = createSelector(newsSelector, ({ news }) => news);

const selectNewsCounter = createSelector(newsSelector, ({ newsCounter }) => newsCounter);

export { selectNews, selectNewsCounter };
