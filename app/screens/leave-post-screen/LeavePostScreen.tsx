import React, { FC, useLayoutEffect } from 'react';
import { Keyboard, Pressable } from 'react-native';
import { DefaultBackground } from 'components/default-background';
import { PrimaryButton } from 'components/buttons';
import { usePost } from 'hooks/index';
import { Flex, VStack } from 'native-base';
import { CommentAttachedImages, CommentInput } from 'components/comments';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { IPost } from 'types/types';

type Route = {
  params: {
    isEdit: boolean;
    post?: IPost;
  };
};

export const LeavePostScreen: FC = () => {
  const {
    params: { isEdit, post },
  } = useRoute<RouteProp<Route>>();

  const navigation = useNavigation();

  const {
    postValue,
    setPostValue,
    postValueCounter,
    isDisabledSubmitButton,
    attachments,
    isAnonymous,
    handleToggleIsAnonymous,
    handleAddAttachments,
    loading,
    handleCreatePost,
  } = usePost(isEdit, post);

  const buttonTitle = isEdit ? 'Update' : 'Submit';
  const headerTitle = isEdit ? 'Edit post' : 'New post';

  useLayoutEffect(() => {
    navigation.setOptions({
      title: headerTitle,
    });
  }, [headerTitle, navigation]);

  return (
    <DefaultBackground withPaddingVertical={true}>
      <Pressable onPress={() => Keyboard.dismiss()}>
        <Flex justifyContent="space-between" height="100%">
          <VStack space="4">
            <CommentInput
              setValue={setPostValue}
              value={postValue!}
              valueCounter={postValueCounter}
              isAnonymous={isAnonymous}
              handleToggleIsAnonymous={handleToggleIsAnonymous}
              handleAddAttachments={handleAddAttachments}
            />

            <CommentAttachedImages attachments={attachments} />
          </VStack>

          <PrimaryButton
            onPress={handleCreatePost}
            text={buttonTitle}
            disabled={isDisabledSubmitButton}
            isLoading={loading}
          />
        </Flex>
      </Pressable>
    </DefaultBackground>
  );
};
