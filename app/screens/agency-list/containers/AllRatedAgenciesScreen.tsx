import React, { FC, useCallback, useState } from 'react';
import { useAppBackHandler } from 'hooks/index';
import { DefaultBackground } from 'components/default-background';
import { getAllRatedAgenciesFB } from 'services/agencies';
import { IAgency } from 'types/types';
import { useFocusEffect } from '@react-navigation/native';
import { AgencyList } from '../components/AgencyList';

const AllRatedAgenciesScreen: FC = (): JSX.Element => {
  const [allRatedAgencies, setAllRatedAgencies] = useState<IAgency[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  useFocusEffect(
    useCallback(() => {
      getAllRatedAgenciesFB()
        .then(({ agencies }) => {
          setLoading(true);
          setAllRatedAgencies(agencies);
        })
        .catch((e) => console.log('fetchAllRatedAgencies error: ', e))
        .finally(() => setLoading(false));
    }, []),
  );

  useAppBackHandler();

  return (
    <DefaultBackground withPadding={true}>
      <AgencyList agencies={allRatedAgencies} loading={loading} />
    </DefaultBackground>
  );
};

export default AllRatedAgenciesScreen;
