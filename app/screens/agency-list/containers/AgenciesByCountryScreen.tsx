import React, { FC, useLayoutEffect } from 'react';
import { useAppBackHandler, useTabView } from 'hooks/index';
import { DefaultBackground } from 'components/default-background';
import { CustomTabView } from 'components/custom-tab-view';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';

type Route = {
  params: {
    country: string;
  };
};

const AgenciesByCountryScreen: FC = (): JSX.Element => {
  const {
    params: { country },
  } = useRoute<RouteProp<Route>>();

  const navigation = useNavigation();

  const { tabs, tabRoutes } = useTabView({ tabType: 'AgenciesByCountry' });

  useLayoutEffect(() => {
    navigation.setOptions({
      title: country,
    });
  }, [country, navigation]);

  useAppBackHandler();

  return (
    <DefaultBackground withPadding={true}>
      <CustomTabView tabs={tabs} tabRoutes={tabRoutes} />
    </DefaultBackground>
  );
};

export default AgenciesByCountryScreen;
