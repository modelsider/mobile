import React, { FC, useCallback, useMemo, useState } from 'react';
import { Box, Divider, HStack, ScrollView, Text, VStack } from 'native-base';
import { ListItem } from 'components/list-item/ListItem';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { useFocusEffect } from '@react-navigation/native';
import { getListsFB } from 'services/lists';
import { IList } from 'types/types';
import { ActivityIndicator } from 'react-native';
import { useSelector } from 'react-redux';
import { selectHasRating } from '../../../store/app/selectors';

export const AgencyListNames: FC = (): JSX.Element => {
  const [lists, setLists] = useState<IList[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  const hasRating = useSelector(selectHasRating);

  const setListsFromFB = useCallback(async () => {
    try {
      setLoading(true);
      const allLists = await getListsFB();
      setLists(allLists);
    } catch (e) {
      console.log('getLists error: ', e);
    } finally {
      setLoading(false);
    }
  }, []);

  const handleNavigateToCountryListItem = useCallback(
    ({ country, isAllRatedScreen = false }: { country?: string, isAllRatedScreen?: boolean }) => {
      if (hasRating) {
        if (isAllRatedScreen) {
          return NavigationService.navigate({ routeName: ROUTES.AllRatedAgencies });
        }
        NavigationService.navigateWithParams({ routeName: ROUTES.AgenciesByCountry, params: { country } });
      } else {
        NavigationService.navigate({ routeName: ROUTES.LeaveRating });
      }
    },
    [hasRating],
  );

  useFocusEffect(
    useCallback(() => {
      setListsFromFB();
    }, [setListsFromFB]),
  );

  const ALL_RATED_LIST = [
    { title: 'All rated', handleNavigate: () => handleNavigateToCountryListItem({ isAllRatedScreen: true }) },
  ];

  const mappedList = useMemo(
    () =>
      lists.map(({ country }) => {
        return {
          title: country,
          handleNavigate: () => handleNavigateToCountryListItem({ country }),
        };
      }),
    [handleNavigateToCountryListItem, lists],
  );

  return (
    <ScrollView my="12" showsVerticalScrollIndicator={false}>
      <VStack>
        <HStack justifyContent="center" space="xs">
          <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="xl" textAlign="center">
            Pick a list
          </Text>
          {loading && <ActivityIndicator />}
        </HStack>

        {[...ALL_RATED_LIST, ...mappedList].map(({ title, handleNavigate }, index) => {
          return (
            <Box key={index}>
              <ListItem title={title} onPress={handleNavigate} />
              <Divider thickness={1} bg="gray.300" />
            </Box>
          );
        })}
      </VStack>
    </ScrollView>
  );
};
