import React, { FC } from 'react';
import { useAgencies } from 'hooks/index';
import { IAgency } from 'types/types';
import { Box } from 'native-base';
import AgencyItem from 'components/agency-item/AgencyItem';
import { AgenciesSkeleton } from 'components/skeleton';
import { FlashList } from '@shopify/flash-list';

interface IProps {
  agencies: IAgency[];
  loading: boolean;
}

const ListEmptyComponent: FC<{ loading: boolean }> = ({ loading }): JSX.Element | null => {
  return loading ? null : <AgenciesSkeleton withDate={false} />;
};

const RenderItem: FC<{ agency: IAgency; onNavigate: ({ agencyId }: { agencyId: number }) => void }> = ({
  agency,
  onNavigate,
}): JSX.Element => {
  const agencyId = agency.id;
  const type = agency.booking.is_rated ? 'booking' : agency.mother.is_rated ? 'mother' : '';
  // @ts-ignore
  const isRated = agency[type]?.total_rating;

  return (
    <Box py="3">
      <AgencyItem
        // @ts-ignore
        type={type}
        agency={agency}
        isRated={isRated}
        withDate={false}
        onPress={() => onNavigate({ agencyId })}
      />
    </Box>
  );
};

export const AgencyList: FC<IProps> = ({ agencies, loading }): JSX.Element => {
  const { handleNavigateToAgency } = useAgencies();

  return (
    <FlashList
      estimatedItemSize={200}
      data={loading ? [] : agencies}
      renderItem={({ item }) => <RenderItem agency={item} onNavigate={handleNavigateToAgency} />}
      ListEmptyComponent={() => <ListEmptyComponent loading={loading} />}
      showsVerticalScrollIndicator={false}
      contentContainerStyle={{ paddingBottom: 50 }}
    />
  );
};
