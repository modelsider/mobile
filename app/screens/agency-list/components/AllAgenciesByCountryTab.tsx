import React, { FC, useCallback, useState } from 'react';
import { getAllAgenciesByCountryFB } from 'services/agencies';
import { IAgency } from 'types/types';
import { RouteProp, useFocusEffect, useRoute } from '@react-navigation/native';
import { AgencyList } from './AgencyList';

type Route = {
  params: {
    country: string;
  };
};

export const AllAgenciesByCountryTab: FC = (): JSX.Element => {
  const {
    params: { country },
  } = useRoute<RouteProp<Route>>();

  const [allAgencies, setAllAgencies] = useState<IAgency[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  useFocusEffect(
    useCallback(() => {
      getAllAgenciesByCountryFB({ country })
        .then(({ agencies }) => {
          setLoading(true);
          setAllAgencies(agencies);
        })
        .finally(() => setLoading(false));
    }, [country]),
  );

  return <AgencyList agencies={allAgencies} loading={loading} />;
};
