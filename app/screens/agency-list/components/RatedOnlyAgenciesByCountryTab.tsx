import React, { FC, useCallback, useState } from 'react';
import { getRatedOnlyAgenciesByCountryFB } from 'services/agencies';
import { IAgency } from 'types/types';
import { RouteProp, useFocusEffect, useRoute } from '@react-navigation/native';
import { AgencyList } from './AgencyList';

type Route = {
  params: {
    country: string;
  };
};

export const RatedOnlyAgenciesByCountryTab: FC = (): JSX.Element => {
  const {
    params: { country },
  } = useRoute<RouteProp<Route>>();

  const [ratedOnlyAgencies, setRatedOnlyAllAgencies] = useState<IAgency[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  useFocusEffect(
    useCallback(() => {
      getRatedOnlyAgenciesByCountryFB({ country })
        .then(({ agencies }) => {
          setLoading(true);
          setRatedOnlyAllAgencies(agencies);
        })
        .finally(() => setLoading(false));
    }, [country]),
  );

  return <AgencyList agencies={ratedOnlyAgencies} loading={loading} />;
};
