import React, { FC, useCallback, useState } from 'react';
import { LayoutAnimation, ScrollView } from 'react-native';
import Toast from 'react-native-toast-message';
import Clipboard from '@react-native-clipboard/clipboard';
import { DefaultBackground } from 'components/default-background';
import { SUPPORT, SUPPORT_CONTENT } from 'screens/support-screen/config';
import { useAppBackHandler } from 'hooks/index';
import { ExpandableText } from 'components/expand/ExpandableText';
import TermsPrivacyInfo from '../components/TermsPrivacyInfo';
import { ISupportContent } from '../../../types/types';
import { Pressable, Text, useTheme, VStack } from 'native-base';

export const SupportScreen: FC = () => {
  useAppBackHandler();
  const { space } = useTheme();

  const [listDataSource, setListDataSource] = useState<ISupportContent[]>(SUPPORT_CONTENT);

  const showToast = useCallback(() => {
    Clipboard.setString(SUPPORT.email);

    Toast.show({
      text1: 'Copied to clipboard!',
    });
  }, []);

  const onUpdateLayout = useCallback(
    (index: number) => {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      const array = [...listDataSource];
      array[index].isExpanded = !array[index].isExpanded;
      setListDataSource(array);
    },
    [listDataSource],
  );

  return (
    <DefaultBackground>
      <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingVertical: space['10'] }}>
        {listDataSource?.map((item, index) => (
          <ExpandableText key={index} item={item} onUpdateLayout={() => onUpdateLayout(index)} />
        ))}

        <VStack space="xs" py="8">
          <Text textAlign="center" fontFamily="body" fontWeight="200" fontStyle="normal" fontSize="sm" color="gray.500">
            If you have any further questions please send an email to:
          </Text>

          <Pressable onPress={showToast}>
            <Text textAlign="center" fontFamily="body" fontWeight="700" fontStyle="normal" fontSize="lg" color="black">
              {SUPPORT.email}
            </Text>
          </Pressable>
        </VStack>

        <TermsPrivacyInfo />
      </ScrollView>
    </DefaultBackground>
  );
};
