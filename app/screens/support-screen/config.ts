import { ISupportContent } from '../../types/types';

export const SUPPORT = {
  email: 'git@modelsider.com',
};

export const SUPPORT_CONTENT: ISupportContent[] = [
  {
    isExpanded: false,
    title: 'How to rate agency I worked with?',
    description:
      'To rate the agency you worked with,  go to the Search button and type an agency name.\n' +
      '\n' +
      'Choose an agency from a drop-down list, taking into consideration a city and a country.\n' +
      '\n' +
      'At the agency profile, press the button “Rate Mother” at the bottom of the page. Swipe left to “Rate Booking”.',
  },
  {
    isExpanded: false,
    title: 'Can I change or delete my given rating?',
    description:
      'You can change or delete your given rating, at any time, on your profile.\n' +
      '\n' +
      'Tap and hold at any agency you have rated and select the Update rating button or the Delete rating button.',
  },
  {
    isExpanded: false,
    title: 'Is my given rating anonymous?',
    description:
      'All ratings that you give to the agencies are anonymous.\n' +
      '\n' +
      'We will never show our answers to anyone. It is only collecting statistics.\n' +
      '\n' +
      'You can only choose to be visible to others as a model who rated agency.',
  },
  {
    isExpanded: false,
    title: 'Why I can’t rate an agency?',
    description:
      "We can't accept your agency's feedback if you worked with an agency more than three years ago.\n" +
      'Too old experience may not reflect the current situation in the agency.\n' +
      'Our main point is to keep our statistics actual.',
  },
  {
    isExpanded: false,
    title: 'What if I can’t find agency?',
    description:
      'If you could not find an agency in our database, please show us an agency via the “Suggest agency” button.\n' +
      '\n' +
      'It’s located on the “Search” page.\n' +
      '\n' +
      'We will review your request and add the agency shortly if it fits our requirements.\n' +
      '\n' +
      'Thank you for helping us keep our agency database actual.',
  },
  {
    isExpanded: false,
    title: 'Can I rate same agency a few times?',
    description:
      'No, you can not rate the same agency a few times, nor anyone else can do it.\n' +
      'If you have worked with the agency over a few years and your experience with them has changed, you can update it.\n' +
      'Tap and hold on to the agency you want to update in your profile and select the “Update rating” button.',
  },
  {
    isExpanded: false,
    title: 'Can I delete my profile?',
    description:
      'Yes, you can delete your profile.\n' +
      'To do that, go to your profile. Press three dots in the upper right corner.\n' +
      'Select “Delete account”.\n' +
      'Your comments won\'t be deleted automatically, but other users will see your name as a "Deleted Account".\n' +
      "Also your agency's ratings will be kept in the system for another three years.\n" +
      'You always have a right to delete all your comments and ratings at any time manually before deleting the account.',
  },
  {
    isExpanded: false,
    title: 'Can my account be blocked?',
    description:
      'Yes, your account can be blocked or even deleted if you violate our terms of use.\n' +
      '\n' +
      'We do not accept:\n' +
      '\n' +
      '- pretending to be someone else\n' +
      '- hate speech\n' +
      '- verbal abuse\n' +
      '- suspicious behaviour (rate too many agencies or agencies you never worked with)\n' +
      '- spam',
  },
  {
    isExpanded: false,
    title: 'How to report a comment?',
    description:
      'You can report any comment that you find offensive. To do that, tap and hold on the comment and choose “report” in a menu. We will remove this comment from your feed and take the necessary measures within next 24 hours and block the user if required.',
  },
];
