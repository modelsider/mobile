import React, { FC, useCallback } from 'react';
import { Flex, Text } from 'native-base';
import { InAppBrowser } from 'react-native-inappbrowser-reborn';
// @ts-ignore
import { TERMS_AND_CONDITIONS_LINK, PRIVACY_POLICY_LINK } from '@env';

const LINKS = {
  terms: TERMS_AND_CONDITIONS_LINK,
  policy: PRIVACY_POLICY_LINK,
};

const TermsPrivacyInfo: FC = () => {
  const handleOpenLink = useCallback(async ({ type }: { type: 'terms' | 'policy' }) => {
    if (await InAppBrowser.isAvailable()) {
      await InAppBrowser.open(LINKS[type], {
        modalEnabled: false,
      });
    }
  }, []);

  return (
    <Flex flexDirection="row" justifyContent="space-between">
      <Text
        onPress={() => handleOpenLink({ type: 'terms' })}
        underline={true}
        fontFamily="body"
        fontWeight="300"
        fontStyle="normal"
        fontSize="sm"
        color="gray.300"
      >
        Terms of Use
      </Text>

      <Text
        onPress={() => handleOpenLink({ type: 'policy' })}
        underline={true}
        fontFamily="body"
        fontWeight="300"
        fontStyle="normal"
        fontSize="sm"
        color="gray.300"
      >
        Privacy policy
      </Text>
    </Flex>
  );
};

export default TermsPrivacyInfo;
