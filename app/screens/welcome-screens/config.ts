import { EMOJIS } from 'components/emoji-icon/emoji';
import { ROUTES } from 'navigator/constants/routes';

const WELCOME_SCREENS_CONFIG = {
  welcome: {
    id: 1,
    title: 'Howdy!',
    emoji: EMOJIS.hello,
    description:
      'Welcome to Modelsider.\nThe one and only place\nfor fashion models\nto rate and review\nmodelling agencies.',
    routeName: ROUTES.WelcomeRateAgency,
  },
  rateAgency: {
    id: 2,
    title: 'Rate agency',
    emoji: '',
    description:
      'Rate agencies\nanonymously to help\nothers have realistic\nexpectations. Give back\nto the community.',
    routeName: ROUTES.WelcomeCheckRatings,
  },
  checkRatings: {
    id: 3,
    title: 'Check ratings',
    emoji: '',
    description: 'Get to know your mother\nor booking agency\nbefore you start work\nwith them.',
    routeName: ROUTES.WelcomeModelsider,
  },
  modelsider: {
    id: 4,
    title: '',
    emoji: EMOJIS.hands,
    description: 'Together we can create a\nsafe and pleasant working\nenvironment for fashion\nmodels.',
    routeName: ROUTES.Auth,
  },
};

export { WELCOME_SCREENS_CONFIG };
