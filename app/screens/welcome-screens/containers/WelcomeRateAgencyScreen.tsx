import React, { FC } from 'react';
import WelcomeScreensTemplate from '../components/WelcomeScreensTemplate';

export const WelcomeRateAgencyScreen: FC = () => {
  return <WelcomeScreensTemplate type="rateAgency" index={2} />;
};
