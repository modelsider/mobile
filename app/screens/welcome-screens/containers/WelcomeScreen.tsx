import React, { FC } from 'react';
import WelcomeScreensTemplate from '../components/WelcomeScreensTemplate';

export const WelcomeScreen: FC = () => {
  return <WelcomeScreensTemplate type="welcome" index={1} />;
};
