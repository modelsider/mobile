import React, { FC } from 'react';
import WelcomeScreensTemplate from '../components/WelcomeScreensTemplate';

export const WelcomeCheckRatingsScreen: FC = () => {
  return <WelcomeScreensTemplate type="checkRatings" index={3} />;
};
