import React, { FC } from 'react';
import WelcomeScreensTemplate from '../components/WelcomeScreensTemplate';

export const WelcomeModelsiderScreen: FC = () => {
  return <WelcomeScreensTemplate type="modelsider" index={4} />;
};
