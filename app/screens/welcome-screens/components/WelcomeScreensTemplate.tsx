import React, { FC, useCallback, useMemo } from 'react';
import { Swipeable } from 'react-native-gesture-handler';
import { DefaultBackgroundElements } from 'components/default-background';
import { PrimaryButton } from 'components/buttons';
import { ProgressWelcome } from 'components/progress';
import { Greetings } from 'screens/welcome-screens/components/Greetings';
import { AnimatedGhostContainer } from 'components/animations/AnimatedGhostContainer';
import { Ghost } from 'components/UI/ghost/Ghost';
import { Flex, useTheme, VStack } from 'native-base';
import { DirectionType, welcomeScreenType } from 'types/types';
import NavigationService from 'navigator/NavigationService';
import { useAppBackHandler } from 'hooks/index';
import { WELCOME_SCREENS_CONFIG } from '../config';
import { LogoIcon } from 'components/logo-icon/LogoIcon';
import { useDimensions } from '@react-native-community/hooks';
import { MIN_HEIGHT } from 'constants/index';

interface IProps {
  type: welcomeScreenType;
  index: number;
}

const WelcomeScreensTemplate: FC<IProps> = ({ type, index }) => {
  const {
    screen: { height },
  } = useDimensions();

  const { space } = useTheme();

  const { emoji, description, title, routeName } = WELCOME_SCREENS_CONFIG[type];

  const handleSwipe = useCallback(
    (direction: DirectionType) => {
      if (direction === 'right' && index !== 4) {
        return NavigationService.navigate({ routeName });
      }
      if (direction === 'left' && index !== 1) {
        return NavigationService.navigateBack();
      }
    },
    [index, routeName],
  );

  const ghost = useMemo(() => {
    if (type === 'welcome') {
      return (
        <AnimatedGhostContainer right={-130} bottom={320} rightAnimation={true} translateAnimationX={true}>
          <Ghost rotate={-40} type="right" />
        </AnimatedGhostContainer>
      );
    } else if (type === 'rateAgency') {
      return (
        <AnimatedGhostContainer left={-40} bottom={320} leftAnimation={true} translateAnimationX={true}>
          <Ghost rotate={-140} />
        </AnimatedGhostContainer>
      );
    } else if (type === 'checkRatings') {
      if (height < MIN_HEIGHT) {
        return null;
      }
      return (
        <AnimatedGhostContainer top={130} zIndex={0} translateAnimationY={true}>
          <Ghost rotate={180} />
        </AnimatedGhostContainer>
      );
    } else if (type === 'modelsider') {
      return null;
    }
  }, [height, type]);

  useAppBackHandler();

  return (
    <DefaultBackgroundElements type="templateElements1" withPaddingVertical={true}>
      <Swipeable onSwipeableWillClose={handleSwipe}>
        <Flex justifyContent="space-between" alignItems="center" height="100%">
          <LogoIcon />
          {ghost}
          <Greetings title={title} emoji={emoji} description={description} withTitleEmoji={type === 'modelsider'} />
          <VStack width="100%" px={space['2']} alignItems="center" space="10">
            <ProgressWelcome currentIndex={index} />
            <PrimaryButton onPress={() => NavigationService.navigate({ routeName })} text="Next" disabled={false} />
          </VStack>
        </Flex>
      </Swipeable>
    </DefaultBackgroundElements>
  );
};

export default WelcomeScreensTemplate;
