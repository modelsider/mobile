import React, { FC } from 'react';
import { EMOJIS } from 'components/emoji-icon/emoji';
import { Box, Text } from 'native-base';

export const TitleWithEmoji: FC = () => {
  return (
    <Box
      alignItems="center"
      bg="white"
      py="1"
      px="4"
      bottom="10"
      borderRadius="5"
      shadow="9"
      style={{ transform: [{ rotate: '-8deg' }] }}
    >
      <Text fontFamily="mono" fontStyle="normal" fontSize="2xl">{`Modelsider ${EMOJIS.hearts}`}</Text>
    </Box>
  );
};
