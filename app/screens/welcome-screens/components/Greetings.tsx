import React, { FC } from 'react';
import { Circle } from 'components/circles/circle/Circle';
import { EMOJIS } from 'components/emoji-icon/emoji';
import { TitleWithEmoji } from './TitleWithEmoji';
import { Text, useTheme, VStack } from 'native-base';
import { getScreenDimensions } from 'utils/layoutUtils';

interface IGreetings {
  title?: string;
  emoji?: string;
  description: string;
  withTitleEmoji: boolean;
}

export const Greetings: FC<IGreetings> = ({ title, description, emoji = '', withTitleEmoji = false }) => {
  const { colors, space } = useTheme();

  return (
    <Circle color={colors.teal['50']} size={getScreenDimensions().width - space['16']}>
      <VStack justifyContent="center" height="100%">
        {!!title && (
          <Text fontFamily="mono" fontSize="3xl" textAlign="center">
            {title}
          </Text>
        )}
        {withTitleEmoji && <TitleWithEmoji />}
        {!!emoji.length && (
          <Text fontSize="md" textAlign="center" bottom={withTitleEmoji ? 13 : 0}>
            {EMOJIS.hands}
          </Text>
        )}
        <Text fontFamily="body" fontSize="lg" fontWeight="300" textAlign="center">
          {description}
        </Text>
      </VStack>
    </Circle>
  );
};
