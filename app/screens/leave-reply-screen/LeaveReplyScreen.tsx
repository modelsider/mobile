import React, { FC, useLayoutEffect } from 'react';
import { Keyboard, Pressable } from 'react-native';
import { DefaultBackground } from 'components/default-background';
import { PrimaryButton } from 'components/buttons';
import { useReplies } from 'hooks/index';
import { CloseIcon, Flex, VStack } from 'native-base';
import { CommentAttachedImages, CommentInput } from 'components/comments';
import NavigationService from 'navigator/NavigationService';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { IReply } from 'types/types';

type Route = {
  params: {
    isEdit?: boolean;
    isPost: boolean;
    reply?: IReply;
  };
};

export const LeaveReplyScreen: FC = () => {
  const {
    params: { isEdit, isPost, reply },
  } = useRoute<RouteProp<Route>>();

  const {
    replyValue,
    setReplyValue,
    replyValueCounter,
    isAnonymous,
    handleToggleIsAnonymous,
    isDisabledReplyButton,
    handleCreateReply,
    isAddedReply,
    attachments,
    handleAddAttachments,
    handleUpdateReply,
  } = useReplies(reply);

  const onPress = isEdit ? handleUpdateReply : handleCreateReply;

  const navigation = useNavigation();
  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Pressable onPress={() => (isPost ? NavigationService.navigateBack() : NavigationService.resetToHome())}>
          <CloseIcon color="black" />
        </Pressable>
      ),
    });
  }, [isPost, navigation]);

  return (
    <DefaultBackground withPaddingVertical={true}>
      <Pressable onPress={() => Keyboard.dismiss()}>
        <Flex justifyContent="space-between" height="100%">
          <VStack space="4">
            <CommentInput
              setValue={setReplyValue}
              value={replyValue}
              valueCounter={replyValueCounter}
              isAnonymous={isAnonymous}
              handleToggleIsAnonymous={handleToggleIsAnonymous}
              handleAddAttachments={handleAddAttachments}
              isCamera={false}
            />

            <CommentAttachedImages attachments={attachments} />
          </VStack>

          <PrimaryButton
            onPress={onPress}
            text="Leave a Reply"
            disabled={isDisabledReplyButton}
            isLoading={isAddedReply}
          />
        </Flex>
      </Pressable>
    </DefaultBackground>
  );
};
