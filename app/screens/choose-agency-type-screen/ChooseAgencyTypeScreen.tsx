import React, { FC } from 'react';
import { DefaultBackgroundElements } from 'components/default-background';
import { Center, Text, VStack } from 'native-base';
import { PrimaryButton } from 'components/buttons';
import { selectAgencyType } from 'store/agency/reducer';
import { useAppDispatch } from 'hooks/index';
import { agencyType } from 'types/types';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { useSelector } from 'react-redux';
import { selectAgency } from 'store/agency/selectors';

export const ChooseAgencyTypeScreen: FC = (): JSX.Element => {
  const dispatch = useAppDispatch();

  const agency = useSelector(selectAgency);

  const handleSelectAgency = ({ type }: { type: agencyType }) => {
    dispatch(selectAgencyType({ type }));
    NavigationService.navigate({ routeName: ROUTES.RateStartWorking });
  };

  return (
    <DefaultBackgroundElements withPaddingVertical withPadding>
      <Center flex={1}>
        <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="xl" textAlign="center">
          {`How "${agency?.agency_name}" represent you?`}
        </Text>
      </Center>
      <VStack space="xl">
        <PrimaryButton onPress={() => handleSelectAgency({ type: 'mother' })} text="Mother agency" />
        <PrimaryButton onPress={() => handleSelectAgency({ type: 'booking' })} text="Booking agency" />
      </VStack>
    </DefaultBackgroundElements>
  );
};
