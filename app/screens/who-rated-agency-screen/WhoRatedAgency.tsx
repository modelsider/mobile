import React, { FC, useEffect } from 'react';
import { FlatList, RefreshControl } from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import { DefaultBackground } from 'components/default-background';
import { useAppDispatch, useUsers } from 'hooks/index';
import { IUserInfo } from 'store/auth/reducer';
import UserItem from 'components/user-item/UserItem';
import { UserItemSkeleton } from 'components/skeleton';
import { Spacer, useTheme } from 'native-base';
import { setStatusBarBg } from 'store/app/reducer';

export const SEPARATOR_HEIGHT = 20;

const WhoRatedAgency: FC = () => {
  const { space } = useTheme();

  const dispatch = useAppDispatch();

  const { isFetch, handleFetchUsers, users, navigateToUserProfile } = useUsers();

  const data = isFetch ? [] : users;
  const isFocused = useIsFocused();

  useEffect(() => {
    dispatch(setStatusBarBg({ bg: '' }));
    if (isFocused) {
      handleFetchUsers();
    }
  }, [dispatch, handleFetchUsers, isFocused]);

  const renderItem = ({ item }: { item: IUserInfo }) => {
    return <UserItem user={item} onPress={() => navigateToUserProfile({ userId: item.id })} />;
  };

  const listEmptyComponent = () => (isFetch ? <UserItemSkeleton /> : null);

  return (
    <DefaultBackground>
      <FlatList
        data={data}
        showsVerticalScrollIndicator={false}
        renderItem={renderItem}
        contentContainerStyle={{ paddingVertical: space['3'] }}
        ItemSeparatorComponent={() => <Spacer size={3} />}
        ListEmptyComponent={listEmptyComponent}
        refreshControl={<RefreshControl refreshing={false} onRefresh={handleFetchUsers} />}
      />
    </DefaultBackground>
  );
};

export default WhoRatedAgency;
