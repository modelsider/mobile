import React, { FC } from 'react';
import uuid from 'uuidv4';
import { FlatList } from 'react-native';
import { ListItem } from 'components/list-item/ListItem';
import { ROUTES } from 'navigator/constants/routes';
import { keyExtractor } from 'utils/keyExtractor';
import { Divider } from 'native-base';
import { useMenu } from 'hooks/index';
import { MENU_ITEMS } from '../config';

export const MenuList: FC = () => {
  const { handleDeleteAccount, handleSignOut, handleNavigate } = useMenu();

  const renderItem = ({ item }: any) => {
    const { title, icon } = item;

    const handlers = {
      [ROUTES.SignOut]: handleSignOut,
      [ROUTES.DeleteAccount]: handleDeleteAccount,
      [ROUTES.Settings]: () => handleNavigate({ routeName: ROUTES.Settings }),
      [ROUTES.Support]: () => handleNavigate({ routeName: ROUTES.Support }),
      [ROUTES.ReportBug]: () => handleNavigate({ routeName: ROUTES.ReportBug }),
    } as any; // todo: fix

    return <ListItem key={uuid()} title={title} icon={icon} onPress={handlers[title]} />;
  };

  const itemSeparatorComponent = () => {
    return <Divider thickness={1} bg="gray.300" />;
  };

  return (
    <FlatList
      keyExtractor={keyExtractor}
      data={MENU_ITEMS}
      renderItem={renderItem}
      ItemSeparatorComponent={itemSeparatorComponent}
    />
  );
};
