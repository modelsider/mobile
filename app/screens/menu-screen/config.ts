import { SettingsIcon } from 'components/UI/svg/SettingsIcon';
import { SupportIcon } from 'components/UI/svg/SupportIcon';
// import { DonateIcon } from 'components/UI/svg/DonateIcon';
import { SignOutIcon } from 'components/UI/svg/SignOutIcon';
import { DeleteIcon } from 'components/UI/svg/DeleteIcon';
import { BugIcon } from 'components/UI/svg/BugIcon';
import { ROUTES } from 'navigator/constants/routes';

export const MENU_ITEMS = [
  {
    icon: SettingsIcon,
    title: ROUTES.Settings,
  },
  {
    icon: SupportIcon,
    title: ROUTES.Support,
  },
  {
    icon: BugIcon,
    title: ROUTES.ReportBug,
  },
  // {
  //   icon: DonateIcon,
  //   title: ROUTES.Donate,
  // },
  {
    icon: SignOutIcon,
    title: ROUTES.SignOut,
  },
  {
    icon: DeleteIcon,
    title: ROUTES.DeleteAccount,
  },
];
