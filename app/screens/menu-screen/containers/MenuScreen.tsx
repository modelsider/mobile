import React, { FC } from 'react';
import { DefaultBackground } from 'components/default-background';
import { MenuList } from '../components/MenuList';

export const MenuScreen: FC = () => {
  return (
    <DefaultBackground>
      <MenuList />
    </DefaultBackground>
  );
};
