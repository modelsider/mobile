import React, { FC, useLayoutEffect } from 'react';
import { Pressable, Keyboard } from 'react-native';
import { DefaultBackground } from 'components/default-background';
import { useLeaveComment, useSortComments } from 'hooks/index';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { Flex, VStack } from 'native-base';
import { PrimaryButton } from 'components/buttons';
import { CommentAttachedImages, CommentsHeader, CommentInput } from 'components/comments';

type Route = {
  params: {
    isEdit: boolean;
  };
};

export const LeaveCommentScreen: FC = () => {
  const {
    params: { isEdit },
  } = useRoute<RouteProp<Route>>();

  const navigation = useNavigation();

  const {
    setCommentValue,
    commentValue,
    commentValueCounter,
    isAnonymous,
    handleToggleIsAnonymous,
    handleCreateComment,
    isAddedComment,
    isDisabledCommentButton,
    handleAddAttachments,
    attachments,
    handleUpdateComment,
  } = useLeaveComment(isEdit);

  const { selectedButtons, handleSortComments } = useSortComments(isEdit);

  const buttonTitle = isEdit ? 'Update a Comment' : 'Leave a Comment';
  const headerTitle = isEdit ? 'Edit' : 'Comment';
  const onPress = isEdit
    ? () => handleUpdateComment({ types: selectedButtons })
    : () => handleCreateComment({ types: selectedButtons });

  useLayoutEffect(() => {
    navigation.setOptions({
      title: headerTitle,
    });
  }, [headerTitle, navigation]);

  return (
    <DefaultBackground withPaddingVertical={true}>
      <Pressable onPress={() => Keyboard.dismiss()}>
        <Flex justifyContent="space-between" height="100%">
          <VStack space="4">
            <CommentsHeader
              onSortComments={handleSortComments}
              selectedButtons={selectedButtons}
              isLeaveComment={true}
            />

            <CommentInput
              setValue={setCommentValue}
              value={commentValue}
              valueCounter={commentValueCounter}
              isAnonymous={isAnonymous}
              handleToggleIsAnonymous={handleToggleIsAnonymous}
              handleAddAttachments={handleAddAttachments}
            />

            <CommentAttachedImages attachments={attachments} />
          </VStack>

          <PrimaryButton
            onPress={onPress}
            text={buttonTitle}
            disabled={isDisabledCommentButton}
            isLoading={isAddedComment}
          />
        </Flex>
      </Pressable>
    </DefaultBackground>
  );
};
