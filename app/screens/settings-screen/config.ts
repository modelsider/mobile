import { ISettingsNotificationsItem, notificationType } from 'types/types';

export const SETTINGS_NOTIFICATIONS_ITEMS: ISettingsNotificationsItem[] = [
  {
    title: 'Likes',
    value: 'likes' as notificationType,
    description: 'Receive a notification, if someone\nlikes your comment',
  },
  {
    title: 'Replies',
    value: 'replies' as notificationType,
    description: 'Receive a notification, if someone\nreplies to your comment. ',
  },
  {
    title: 'Bookmarks',
    value: 'bookmarksAgencies' as notificationType,
    subTitle: 'Agencies',
    description: 'Receive a notification, if someone\nleaves a comment for a bookmarked agency',
  },
  {
    title: 'Bookmarks',
    value: 'bookmarksComments' as notificationType,
    subTitle: 'Comments',
    description: 'Receive a notification, if someone\nleaves a reply for a bookmarked comment',
  },
  {
    title: 'News',
    value: 'news' as notificationType,
    description: 'Receive notification when news are published',
  },
];
