import React, { FC } from 'react';
import DatePicker from 'react-native-date-picker';
import { modeType } from 'components/select/select-date/utils';
import { SettingsItem } from 'screens/settings-screen/components/profile/SettingsItem';
import { useSettingsSelectDate } from 'hooks/index';

interface ISettingsSelectDate {
  title: string;
  mode: string;
  userDateOfBirth: string;
}

const SettingsSelectDate: FC<ISettingsSelectDate> = ({ title, mode, userDateOfBirth }) => {
  const { date, open, onOpen, onCancel, onConfirm, value } = useSettingsSelectDate(userDateOfBirth); // TODO: add interface

  return (
    <>
      <SettingsItem title={value} label="Date of Birth" onPress={onOpen} />
      <DatePicker
        mode={mode as modeType}
        modal
        open={open}
        date={date}
        onConfirm={(selectDate) => onConfirm(selectDate)}
        onCancel={onCancel}
        title={title}
      />
    </>
  );
};

export default SettingsSelectDate;
