import React, { FC } from 'react';
import { Pressable, Text } from 'native-base';

interface ISettingsInstagramAccount {
  title: string;
  label: string;
  onUpdate?: () => void;
}

export const SettingsInstagramAccount: FC<ISettingsInstagramAccount> = ({ title, label, onUpdate }) => {
  return (
    <Pressable alignSelf="center" onLongPress={onUpdate}>
      <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="lg" color="blue.500">
        {title}
      </Text>

      <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="xs" color="gray.300">
        {label}
      </Text>
    </Pressable>
  );
};
