import React, { FC } from 'react';
import { Pressable, Text, useTheme } from 'native-base';

interface ISettingsItem {
  title: string;
  label: string;
  onPress?: () => void;
  isError?: boolean;
}

export const SettingsItem: FC<ISettingsItem> = ({ title, label, onPress = () => {}, isError = false }) => {
  const { colors } = useTheme();

  const text = isError ? `invalid ${label.toLowerCase()}` : title;
  const color = isError ? colors.red['500'] : colors.black;

  return (
    <Pressable onLongPress={onPress}>
      <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="lg" color={color}>
        {text}
      </Text>

      <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="xs" color="gray.300">
        {label}
      </Text>
    </Pressable>
  );
};
