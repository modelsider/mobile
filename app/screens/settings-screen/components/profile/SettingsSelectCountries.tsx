import React, { FC } from 'react';
import { SettingsItem } from './SettingsItem';
import SelectCountries from 'components/select/SelectCountries';

interface ISettingsSelectCountries {
  onChangeText: (value: string) => void;
  country: string;
  label: string;
  isOpenCountry: boolean;
  setIsOpenCountry: (isOpen: boolean) => void;
}

const SettingsSelectCountries: FC<ISettingsSelectCountries> = ({
  onChangeText,
  country,
  label,
  setIsOpenCountry,
  isOpenCountry,
}) => {
  return (
    <>
      <SettingsItem title={country} label={label} onPress={() => setIsOpenCountry(!isOpenCountry)} />
      <SelectCountries isOpen={isOpenCountry} onChange={onChangeText} />
    </>
  );
};

export default SettingsSelectCountries;
