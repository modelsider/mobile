import React, { FC } from 'react';
import { ISettingsNotificationsItem, notificationType } from 'types/types';
import { Box, Flex, HStack, Text, Switch } from 'native-base';

interface IProps {
  onToggleSwitch: (value: notificationType) => void;
}

export const SettingsNotificationsItem: FC<ISettingsNotificationsItem & IProps> = ({
  title,
  subTitle,
  description,
  isEnabled,
  onToggleSwitch,
  value,
}) => {
  return (
    <Flex flexDirection="row" justifyContent="space-between" alignItems="center" py={3}>
      <Box>
        <HStack space="xs" flexDirection="row" alignItems="center">
          <Text fontFamily="body" fontWeight="500" fontStyle="normal" fontSize="lg" color="black">
            {title}
          </Text>
          <Text fontFamily="body" fontWeight="500" fontStyle="normal" fontSize="xs" color="black">
            {subTitle}
          </Text>
        </HStack>
        <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="xs" color="gray.400">
          {description}
        </Text>
      </Box>

      <Switch isChecked={isEnabled} size="sm" colorScheme="emerald" onToggle={() => onToggleSwitch(value)} />
    </Flex>
  );
};
