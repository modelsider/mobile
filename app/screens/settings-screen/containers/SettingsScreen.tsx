import React, { FC } from 'react';
import { DefaultBackground } from 'components/default-background';
import { CustomTabView } from 'components/custom-tab-view';
import { useTabView, useAppBackHandler } from 'hooks/index';

export const SettingsScreen: FC = () => {
  const { tabs, tabRoutes } = useTabView({ tabType: 'Settings' });

  useAppBackHandler();

  return (
    <DefaultBackground>
      <CustomTabView tabs={tabs} tabRoutes={tabRoutes} />
    </DefaultBackground>
  );
};
