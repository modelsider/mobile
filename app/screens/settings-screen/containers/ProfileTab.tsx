import React, { FC } from 'react';
import { SettingsInstagramAccount } from 'screens/settings-screen/components/profile/SettingsInstagramAccount';
import { SettingsItem } from 'screens/settings-screen/components/profile/SettingsItem';
import SettingsSelectDate from 'screens/settings-screen/components/profile/SettingsSelectDate';
import SettingsSelectCountries from 'screens/settings-screen/components/profile/SettingsSelectCountries';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import CustomImage from 'components/image/CustomImage';
import { useSettings } from 'hooks/index';
import { Divider, HStack, ScrollView, Text, VStack } from 'native-base';

const IMAGE_SIZE = 68;

const ProfileTab: FC = () => {
  const { name, email, userName, dateOfBirth } = useSelector(selectUserInfo);
  const {
    profilePicture,
    country,
    isOpenCountry,
    setIsOpenCountry,
    isErrorEmail,
    handleChangeCountry,
    handlePressEmail,
    handleSetProfilePicture,
    handlePressFullName,
  } = useSettings();

  return (
    <ScrollView paddingTop="3">
      <VStack space="2xl">
        <Text fontFamily="heading" fontWeight="400" fontStyle="normal" fontSize="sm" color="gray.300">
          Tap and hold section to edit.
        </Text>

        <HStack space="2xl">
          <CustomImage
            image={profilePicture}
            type="settings"
            resizeMode="cover"
            profileImageSize={IMAGE_SIZE}
            onLongPress={handleSetProfilePicture}
          />
          <Divider orientation="vertical" />
          <SettingsInstagramAccount title={`@${userName}`} label="Instagram" />
        </HStack>

        <SettingsItem title={name} label="Full name" onPress={handlePressFullName} />

        <SettingsSelectDate title="SelectInput Date" mode="date" userDateOfBirth={dateOfBirth} />

        <SettingsItem title={email} label="Email" onPress={handlePressEmail} isError={isErrorEmail} />

        <SettingsSelectCountries
          onChangeText={handleChangeCountry}
          country={country}
          label="Country"
          isOpenCountry={isOpenCountry}
          setIsOpenCountry={setIsOpenCountry}
        />
      </VStack>
    </ScrollView>
  );
};
export default ProfileTab;
