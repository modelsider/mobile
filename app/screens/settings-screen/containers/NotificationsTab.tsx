import React, { FC } from 'react';
import { useNotificationSettings } from 'hooks/index';
import { SettingsNotificationsItem } from 'screens/settings-screen/components/notifications/SettingsNotificationsItem';
import { ScrollView } from 'native-base';

const NotificationsTab: FC = () => {
  const { notificationsState, handleToggleSwitch } = useNotificationSettings();

  return (
    <ScrollView>
      {notificationsState.map((notification) => {
        const { title, subTitle, description, value, isEnabled } = notification;
        return (
          <SettingsNotificationsItem
            key={value}
            title={title}
            subTitle={subTitle}
            description={description}
            isEnabled={isEnabled}
            value={value}
            onToggleSwitch={handleToggleSwitch}
          />
        );
      })}
    </ScrollView>
  );
};

export default NotificationsTab;
