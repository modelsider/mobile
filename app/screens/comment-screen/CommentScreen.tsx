import React, { FC, useCallback, useLayoutEffect } from 'react';
import { ActivityIndicator, RefreshControl, TouchableOpacity } from 'react-native';
import { RouteProp, useFocusEffect, useNavigation, useRoute } from '@react-navigation/native';
import { DefaultBackground } from 'components/default-background';
import { useComments, useReplies, useAppDispatch, useBottomSheet } from 'hooks/index';
import { Comment } from 'components/comments';
import { agencyType, IComment, IPost, IReply } from 'types/types';
import { PrimaryButton } from 'components/buttons';
import { HIT_SLOP } from 'constants/index';
import { selectUserInfo } from 'store/auth/selectors';
import { useSelector } from 'react-redux';
import { setStatusBarBg } from 'store/app/reducer';
import { Box, useTheme, FlatList, ThreeDotsIcon } from 'native-base';
import NavigationService from 'navigator/NavigationService';
import { ArrowBackIcon } from 'components/UI/svg/ArrowBackIcon';
import { selectAgency } from 'store/agency/selectors';
import { selectAgencyType, setAgencyId } from 'store/agency/reducer';
import { useUnMount } from 'hooks/useUnMount';
import { resetCommentsState } from 'store/comments/reducer';

type Route = {
  params: {
    commentOrReplyOrPost: IComment | IReply | IPost;
    commentId: number;
    agencyId?: number; // deeplink
    replyId?: number; // deeplink
    type?: agencyType; // deeplink
    isNavigateToAgency?: boolean;
  };
};

export const CommentScreen: FC = () => {
  const {
    params: { agencyId, isNavigateToAgency = true, type, commentOrReplyOrPost },
  } = useRoute<RouteProp<Route>>();

  const { space, colors, sizes } = useTheme();

  const dispatch = useAppDispatch();

  const authId = useSelector(selectUserInfo).id;
  const agency = useSelector(selectAgency);
  const currentUser = useSelector(selectUserInfo);

  const { isFetchComment, comment, handleCommentSheet, handleReportCommentSheet } = useComments();
  const { handlePostSheet } = useBottomSheet();

  const {
    isFetchReplies,
    replies,
    fetchReplies,
    navigateToReplyToComment,
    fetchUploadedReplies,
    handleMomentumScrollEnd,
    handleMomentumScrollBegin,
  } = useReplies();

  const { handleReportPostSheet } = useBottomSheet();

  const data = isFetchReplies ? [] : replies;

  const isPost = commentOrReplyOrPost?.type === 'Post';
  const isNavigate = isPost || isNavigateToAgency;
  const isCurrentUserPost = commentOrReplyOrPost?.user?.id === currentUser?.id;

  const onRefresh = useCallback(() => {
    fetchReplies({ commentDocId: commentOrReplyOrPost?.docId || comment?.docId! });
  }, [comment?.docId, commentOrReplyOrPost?.docId, fetchReplies]);

  useFocusEffect(
    useCallback(() => {
      onRefresh();
      dispatch(setStatusBarBg({ bg: '' }));

      if (agencyId) {
        dispatch(setAgencyId({ agencyId }));
      }
      if (type) {
        dispatch(selectAgencyType({ type }));
      }
    }, [agencyId, dispatch, onRefresh, type]),
  );

  useUnMount(() => {
    dispatch(resetCommentsState());
  });

  const renderItem = ({ item, index }: { item: IReply; index: number }) => {
    return (
      <Box
        py={space['1']}
        bg="gray.50"
        borderTopWidth={space['0.5']}
        borderTopColor={colors.gray['200']}
        px={space['2']}
      >
        <Comment
          comment={item}
          isReply={true}
          commentIndex={index}
          onRefresh={() => fetchReplies({ commentDocId: comment?.docId! })}
        />
      </Box>
    );
  };

  const ListHeaderComponent = () => {
    return (
      <Box mb={space['2']} px={space['2']}>
        <Comment comment={commentOrReplyOrPost} isOpenedComment={true} isPost={isPost} isNavigateBack={true} />
      </Box>
    );
  };

  // const ListFooterComponent = () => (isMomentumScrollEnd && isFetchComment ? <BottomActivityIndicator /> : null);

  const ListEmptyComponent = () => (isFetchReplies ? <ActivityIndicator size="large" /> : null);

  const navigation = useNavigation();
  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          onPress={
            isPost && isCurrentUserPost
              ? () => handlePostSheet({ post: commentOrReplyOrPost as IPost, onRefresh })
              : isPost && !isCurrentUserPost
              ? () => handleReportPostSheet({ post: commentOrReplyOrPost as IPost, onRefresh })
              : isCurrentUserPost
              ? () => handleCommentSheet({ comment: commentOrReplyOrPost, onRefresh })
              : () => handleReportCommentSheet({ comment: commentOrReplyOrPost })
          }
          hitSlop={HIT_SLOP}
        >
          <ThreeDotsIcon color="black" size={sizes['1.5']} />
        </TouchableOpacity>
      ),
      headerLeft: () => (
        <TouchableOpacity
          hitSlop={HIT_SLOP}
          onPress={() => {
            // isNavigate
            NavigationService.navigateBack();
            // : NavigationService.navigateWithParams({
            //     routeName: ROUTES.Agency,
            //     params: { id: agencyId || agency.id, type },
            //   });
          }}
        >
          <ArrowBackIcon />
        </TouchableOpacity>
      ),
      title: isPost ? 'Post' : 'Comment',
    });
  }, [
    agency?.id,
    agencyId,
    authId,
    comment,
    comment?.is_anonymous,
    comment?.user_id,
    commentOrReplyOrPost,
    handleCommentSheet,
    handlePostSheet,
    handleReportCommentSheet,
    isNavigate,
    isNavigateToAgency,
    isPost,
    navigation,
    sizes,
    type,
  ]);

  return (
    <DefaultBackground withPaddingBottom={true} withPadding={false}>
      <FlatList
        data={data}
        renderItem={renderItem}
        ListHeaderComponent={ListHeaderComponent}
        ListEmptyComponent={ListEmptyComponent}
        onEndReached={fetchUploadedReplies}
        onEndReachedThreshold={0.5}
        onMomentumScrollEnd={handleMomentumScrollEnd}
        onMomentumScrollBegin={handleMomentumScrollBegin}
        refreshControl={<RefreshControl refreshing={false} onRefresh={onRefresh} />}
        contentContainerStyle={{ paddingTop: space['5'] }}
      />

      <Box px={space['2']}>
        <PrimaryButton
          onPress={() => navigateToReplyToComment({ commentId: commentOrReplyOrPost?.id })}
          text="Reply"
          disabled={isFetchComment && isFetchComment}
        />
      </Box>
    </DefaultBackground>
  );
};
