import React, { FC, useEffect, useMemo } from 'react';
import { useTabView } from 'hooks/index';
import { CustomTabView } from 'components/custom-tab-view';
import { DefaultBackgroundElements } from 'components/default-background';
import { Center } from 'native-base';
import { useDimensions } from '@react-native-community/hooks';
import { useSelector } from 'react-redux';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { selectTabViewType } from 'store/app/selectors';

export const AuthScreen: FC = () => {
  const {
    screen: { height },
  } = useDimensions();
  const { tabs, tabRoutes } = useTabView({ tabType: 'Auth' });

  const tabViewType = useSelector(selectTabViewType);
  const idx = useMemo(() => (tabViewType === 'register' ? 1 : 0), [tabViewType]);

  useEffect(() => {
    if (tabViewType === 'register') {
      NavigationService.navigate({ routeName: ROUTES.SignUpStart });
    }
  }, [tabViewType]);

  return (
    <DefaultBackgroundElements withPadding={true} withPaddingVertical={true}>
      <Center flex={1} pt={height / 8}>
        <CustomTabView tabs={tabs} tabRoutes={tabRoutes} idx={idx} />
      </Center>
    </DefaultBackgroundElements>
  );
};
