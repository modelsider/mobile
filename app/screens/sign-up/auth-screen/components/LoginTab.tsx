import React, { FC, useEffect } from 'react';
import { VStack } from 'native-base';
import { useAppBackHandler, useAuth } from 'hooks/index';
import { TextInput } from 'components/inputs';
import { PrimaryButton, UnderlineWordButton } from 'components/buttons';
import { useIsFocused } from '@react-navigation/native';

export const LoginTab: FC = () => {
  const isFocused = useIsFocused();

  const {
    handleClear,
    handleLogin,
    handleResetPassword,
    handleChange,
    handleReset,
    isLoading,
    errorEmail,
    errorPassword,
    email,
    password,
  } = useAuth();

  const isDisabled = !email || !password;

  useEffect(() => {
    if (isFocused) {
      handleReset();
    }
  }, [handleReset, isFocused]);

  useAppBackHandler();

  return (
    <VStack alignItems="center" justifyContent="space-between" height="100%">
      <VStack space="2xl" mt="10">
        <TextInput
          placeholder="Email"
          value={email}
          onChangeValue={(text) => handleChange({ text, type: 'email' })}
          onClear={() => handleClear({ type: 'email' })}
          error={errorEmail}
          isDisabled={isLoading}
        />

        <TextInput
          placeholder="Password"
          value={password}
          onChangeValue={(text) => handleChange({ text, type: 'password' })}
          onClear={() => handleClear({ type: 'password' })}
          type="password"
          error={errorPassword}
          isDisabled={isLoading}
        />

        <UnderlineWordButton onPress={handleResetPassword} text="Forgot password?" disabled={isLoading} />
      </VStack>
      <PrimaryButton onPress={handleLogin} text="Login" disabled={isDisabled} isLoading={isLoading} />
    </VStack>
  );
};
