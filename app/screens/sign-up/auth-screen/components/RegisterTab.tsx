import React, { FC, useEffect } from 'react';
import { VStack } from 'native-base';
import { useAppBackHandler, useAuth } from 'hooks/index';
import { TextInput } from 'components/inputs';
import { Agreement } from 'components/agreement';
import { PrimaryButton } from 'components/buttons';
import { useIsFocused } from '@react-navigation/native';

export const RegisterTab: FC = () => {
  const isFocused = useIsFocused();
  const {
    handleRegister,
    handleChange,
    handleClear,
    errorEmail,
    email,
    isLoading,
    password,
    errorConfirmPassword,
    setAgree,
    agree,
    confirmPassword,
    handleReset,
  } = useAuth();

  const isDisabled = !email || !password || !confirmPassword || agree;

  useEffect(() => {
    if (isFocused) {
      handleReset();
    }
  }, [handleReset, isFocused]);

  useAppBackHandler();

  return (
    <VStack justifyContent="space-between" height="100%">
      <VStack space="2xl" mt="10">
        <TextInput
          placeholder="Email"
          value={email}
          onChangeValue={(text) => handleChange({ text, type: 'email' })}
          onClear={() => handleClear({ type: 'email' })}
          error={errorEmail}
          isDisabled={isLoading}
        />
        <TextInput
          placeholder="Password"
          value={password}
          onChangeValue={(text) => handleChange({ text, type: 'password' })}
          onClear={() => handleClear({ type: 'password' })}
          type="password"
          isDisabled={isLoading}
        />
        <TextInput
          placeholder="Confirm Password"
          value={confirmPassword}
          onChangeValue={(text) => handleChange({ text, type: 'confirmPassword' })}
          onClear={() => handleClear({ type: 'confirmPassword' })}
          type="password"
          error={errorConfirmPassword}
          isDisabled={isLoading}
        />
        <Agreement isAgreed={!agree} setIsAgreed={() => setAgree(!agree)} isDisabled={isLoading} />
      </VStack>
      <PrimaryButton onPress={handleRegister} text="Next" disabled={isDisabled} isLoading={isLoading} />
    </VStack>
  );
};
