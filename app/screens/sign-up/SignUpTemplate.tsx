import React, { FC, ReactNode, useCallback } from 'react';
import { LogoIcon } from 'components/logo-icon/LogoIcon';
import { SecondaryButton } from 'components/buttons';
import { DefaultBackgroundElements } from 'components/default-background';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { Box, Center, VStack } from 'native-base';
import { useAppBackHandler } from 'hooks/index';

interface IProps {
  children: ReactNode;
}
export const SignUpTemplate: FC<IProps> = ({ children }) => {
  const handleNext = useCallback(async ({ answer }: { answer: 'no' | 'yes' }) => {
    const routeName = answer === 'yes' ? ROUTES.Auth : ROUTES.SignUpError;
    NavigationService.navigate({ routeName });
  }, []);

  useAppBackHandler();

  return (
    <DefaultBackgroundElements withPadding={true} withPaddingVertical={true} type="withGhostLeft">
      <Center justifyContent="space-between" height="100%">
        <LogoIcon />

        <Box>{children}</Box>

        <VStack space="4" width="100%">
          <SecondaryButton onPress={() => handleNext({ answer: 'yes' })} text="Yes 👍" disabled={false} />
          <SecondaryButton onPress={() => handleNext({ answer: 'no' })} text="No 👎" disabled={false} />
        </VStack>
      </Center>
    </DefaultBackgroundElements>
  );
};
