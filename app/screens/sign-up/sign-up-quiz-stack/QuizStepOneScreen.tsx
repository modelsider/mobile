import React, { FC, useCallback, useState } from 'react';
import { Input } from 'components/inputs';
import CustomImage from 'components/image/CustomImage';
import { useAppDispatch, useImagePicker } from 'hooks/index';
import { setName, setProfilePictureUrl } from 'store/auth/reducer';
import SignUpQuizTemplate from '../SignUpQuizTemplate';
import { Text, VStack } from 'native-base';
import NavigationService from 'navigator/NavigationService';

export const QuizStepOneScreen: FC = () => {
  const dispatch = useAppDispatch();

  const [photo, setPhoto] = useState<string>();
  const [value, setValue] = useState<string>('');

  const { getImage } = useImagePicker();

  const handleSetPhoto = useCallback(async () => {
    const [url] = await getImage();
    setPhoto(url);
    dispatch(setProfilePictureUrl({ url }));
  }, [dispatch, getImage]);

  const handlePressNext = useCallback(() => {
    if (value) {
      dispatch(setName({ name: value }));
    }

    return NavigationService.navigateWithParams({
      routeName: 'QuizStepTwo',
      params: { name: value },
    });
  }, [dispatch, value]);

  return (
    <SignUpQuizTemplate type="one" onPressNext={handlePressNext} disabled={!value}>
      <VStack space="5" mb="40">
        <CustomImage
          image={photo}
          type="signUp"
          resizeMode="cover"
          profileImageSize={140}
          onPress={handleSetPhoto}
          withLoading={true}
          loaderSize="large"
        />
        <Text textAlign="center" fontFamily="heading" fontWeight="600" fontStyle="normal" fontSize="xl" color="black">
          What is your Full Name?
        </Text>
        <Input placeholder="Full name" onChangeText={setValue} value={value} onClear={() => setValue('')} />
      </VStack>
    </SignUpQuizTemplate>
  );
};
