import React, { FC, useCallback, useState } from 'react';
import { EMOJIS } from 'components/emoji-icon/emoji';
import { SelectInput } from 'components/select/SelectInput';
import { Text, VStack } from 'native-base';
import SignUpQuizTemplate from '../SignUpQuizTemplate';
import { useAppDispatch } from 'hooks/index';
import { setUserLocation } from 'store/auth/reducer';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import SelectCountries from 'components/select/SelectCountries';
// todo: rename quiz
export const QuizStepThreeScreen: FC = () => {
  const dispatch = useAppDispatch();

  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [value, setValue] = useState<string>('');

  const handleChangeCountry = useCallback((country: string) => {
    setValue(country);
    setIsOpen(false);
  }, []);

  const handlePressNext = useCallback(async () => {
    if (value) {
      await dispatch(setUserLocation({ location: value }));
      NavigationService.navigate({ routeName: ROUTES.InstagramUsername });
    }
  }, [dispatch, value]);

  return (
    <SignUpQuizTemplate type="three" onPressNext={handlePressNext} disabled={!value}>
      <VStack space="2/6" mb="20">
        <Text textAlign="center" fontFamily="mono" fontWeight="400" fontStyle="normal" fontSize="3xl" color="black">
          {`Almost there ${EMOJIS.like}`}
        </Text>
        <VStack space="5">
          <Text textAlign="center" fontFamily="heading" fontWeight="600" fontStyle="normal" fontSize="xl" color="black">
            Where do you live?
          </Text>
          <SelectInput placeholder="Country" onPress={() => setIsOpen(!isOpen)} value={value || 'Country'} />

          <SelectCountries isOpen={isOpen} onChange={handleChangeCountry} />
        </VStack>
      </VStack>
    </SignUpQuizTemplate>
  );
};
