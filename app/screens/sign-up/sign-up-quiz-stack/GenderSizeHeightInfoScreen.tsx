import React, { FC, useCallback, useState } from 'react';
import { useAppDispatch } from 'hooks/index';
import SignUpQuizTemplate from '../SignUpQuizTemplate';
import { Center, HStack, Text, VStack } from 'native-base';
import { EMOJIS } from 'components/emoji-icon/emoji';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { CommentButton } from 'components/comments';
import { userInfoType } from 'types/types';
import { setGenderSizeHeight } from 'store/auth/reducer';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';

const CONFIG = [
  {
    title: 'What is your market?',
    type: 'gender',
    buttons: [
      {
        title: `${EMOJIS.male} Male`,
        type: 'male',
      },
      {
        title: `${EMOJIS.female} Female`,
        type: 'female',
      },
    ],
  },
  {
    title: 'What is your body size?',
    type: 'size',
    buttons: [
      {
        title: `${EMOJIS.straight} Straight`,
        type: 'straight',
      },
      {
        title: `${EMOJIS.plus} Plus`,
        type: 'plus',
      },
    ],
  },
  {
    title: 'What is your height?',
    type: 'height',
    buttons: [
      {
        title: `${EMOJIS.tall} Tall`,
        type: 'tall',
      },
      {
        title: `${EMOJIS.petite} Petite`,
        type: 'petite',
      },
    ],
  },
];

export const GenderSizeHeightInfoScreen: FC = () => {
  const dispatch = useAppDispatch();

  const { name } = useSelector(selectUserInfo);

  const [gender, setGender] = useState<string>('');
  const [size, setSize] = useState<string>('');
  const [height, setHeight] = useState<string>('');

  const handleChooseUserInfo = useCallback(({ prop, value }: { prop: userInfoType; value: string }) => {
    const state = {
      gender: () => setGender(value),
      size: () => setSize(value),
      height: () => setHeight(value),
    };
    state[prop]();
  }, []);

  const handlePressNext = useCallback(() => {
    dispatch(setGenderSizeHeight({ gender, size, height }));
    NavigationService.navigate({ routeName: ROUTES.QuizStepThree });
  }, [dispatch, gender, height, size]);

  return (
    <SignUpQuizTemplate type="three" onPressNext={handlePressNext} disabled={!gender || !size || !height}>
      <VStack mb="20">
        <Text textAlign="center" fontFamily="mono" fontWeight="400" fontStyle="normal" fontSize="2xl" color="black">
          {`Hi ${name} ${EMOJIS.hand}`}
        </Text>
        <VStack mt={20} space="5">
          {CONFIG.map(({ title, type, buttons }) => {
            return (
              <Center key={title}>
                <Text
                  textAlign="center"
                  fontFamily="body"
                  fontWeight="400"
                  fontStyle="normal"
                  fontSize="xl"
                  color="black"
                  mb="3"
                >
                  {title}
                </Text>
                <HStack>
                  {buttons.map(({ title: btnTitle, type: btnType }) => {
                    return (
                      <CommentButton
                        key={btnTitle}
                        title={btnTitle}
                        type={type}
                        onPress={() => handleChooseUserInfo({ prop: type, value: btnType })}
                        isSelect={[gender, size, height].includes(btnType)}
                      />
                    );
                  })}
                </HStack>
              </Center>
            );
          })}
        </VStack>
      </VStack>
    </SignUpQuizTemplate>
  );
};
