import { EMOJIS } from 'components/emoji-icon/emoji';
import SelectDate from 'components/select/select-date/SelectDate';
import { DEFAULT_DATE } from 'components/select/select-date/utils';
import { RouteProp, useRoute } from '@react-navigation/native';
import SignUpQuizTemplate from '../SignUpQuizTemplate';
import { useAppDispatch } from 'hooks/index';
import { setUserDateOfBirth } from 'store/auth/reducer';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import React, { FC, useCallback, useState } from 'react';
import { Text, VStack } from 'native-base';

type Route = {
  params: {
    name: string;
  };
};

export const QuizStepTwoScreen: FC = () => {
  const {
    params: { name },
  } = useRoute<RouteProp<Route>>();

  const dispatch = useAppDispatch();

  const [value, setValue] = useState<string>('');

  const handlePressNext = useCallback(() => {
    if (value) {
      dispatch(setUserDateOfBirth({ dateOfBirth: new Date(value) }));
    }
    return NavigationService.navigate({ routeName: ROUTES.GenderSizeHeightInfo });
  }, [dispatch, value]);

  return (
    <SignUpQuizTemplate type="two" onPressNext={handlePressNext} disabled={!value}>
      <VStack space="2/6" mb="20">
        <Text textAlign="center" fontFamily="mono" fontWeight="400" fontStyle="normal" fontSize="2xl" color="black">
          {`Hi ${name} ${EMOJIS.hand}`}
        </Text>
        <VStack space="5">
          <Text
            textAlign="center"
            fontFamily="heading"
            fontWeight="600"
            fontStyle="normal"
            fontSize="2xl"
            color="black"
          >
            What is your Date of Birth?
          </Text>
          <SelectDate
            date={DEFAULT_DATE as Date}
            placeholder="Date of Birth"
            title="Date of Birth"
            mode="date"
            setData={setValue}
          />
        </VStack>
      </VStack>
    </SignUpQuizTemplate>
  );
};
