import React, { FC, useCallback, useLayoutEffect } from 'react';
import { Box, Center, Image, Text, VStack } from 'native-base';
import { DefaultBackgroundElements } from 'components/default-background';
import { PrimaryButton } from 'components/buttons';
import { icons } from '../../../../assets';
import Clipboard from '@react-native-clipboard/clipboard';
import Toast from 'react-native-toast-message';
import { Linking } from 'react-native';
// @ts-ignore
import { INSTAGRAM_LINK_MODELSIDER } from '@env';
import { ReportBugButton } from 'components/buttons/report-a-bug-button/ReportBugButton';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { useNavigation } from '@react-navigation/native';

export const InstagramVerificationScreen: FC = () => {
  const navigation = useNavigation();

  const { userName } = useSelector(selectUserInfo);

  const handleSendToInstagram = useCallback(async () => {
    await Linking.openURL(INSTAGRAM_LINK_MODELSIDER);
  }, []);

  const handleCopyText = useCallback(() => {
    Clipboard.setString('Hi modelsider');

    Toast.show({
      text1: 'Copied to clipboard!',
    });
  }, []);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: null,
    });
  }, [navigation]);

  return (
    <DefaultBackgroundElements withPaddingVertical={true} withPadding={true}>
      <VStack justifyContent="space-between" height="100%">
        <VStack space="3" alignItems="center" mt="24">
          <Image source={icons.instagram} alt="instagram" />
          <Text textAlign="center" fontFamily="mono" fontWeight="400" fontStyle="normal" fontSize="3xl" color="black">
            Instagram
          </Text>
          <Text textAlign="center" fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black">
            Send
            <Text underline onPress={handleSendToInstagram}>
              {' '}
              @modelsider{' '}
            </Text>
            a direct{'\n'}message{'\n\n'}
            <Text underline onPress={handleCopyText}>
              "Hi modelsider"
            </Text>
            {'\n\n'}from {userName}, so we can{'\n'}confirm this instagram profile{'\n'}
            belongs to you
          </Text>
        </VStack>
        <Center>
          <Box position="absolute" bottom="20">
            <ReportBugButton />
          </Box>
          <PrimaryButton onPress={() => NavigationService.navigate({ routeName: ROUTES.SignUpFinish })} text="Done" />
        </Center>
      </VStack>
    </DefaultBackgroundElements>
  );
};
