import React, { FC, useState } from 'react';
import { Box, Center, Image, Text, VStack } from 'native-base';
import { DefaultBackgroundElements } from 'components/default-background';
import { PrimaryButton } from 'components/buttons';
import { icons } from '../../../../assets';
import { TextInput } from 'components/inputs';
import { useAppDispatch, useAuth } from 'hooks/index';
import { setUserName } from 'store/auth/reducer';
import { ReportBugButton } from 'components/buttons/report-a-bug-button/ReportBugButton';
import { transformUsername } from 'utils/transformUsername';

export const InstagramUsernameScreen: FC = () => {
  const [username, setUsername] = useState<string>('');
  const [next, setNext] = useState<boolean>(false);

  const dispatch = useAppDispatch();
  const { handleFinishSignUp, isLoading } = useAuth();

  return (
    <DefaultBackgroundElements withPaddingVertical={true} withPadding={true}>
      <VStack justifyContent="space-between" height="100%">
        <VStack space="3" alignItems="center" mt="24" mb="40">
          <Image source={icons.instagram} alt="instagram" />
          <Text textAlign="center" fontFamily="mono" fontWeight="400" fontStyle="normal" fontSize="3xl" color="black">
            Instagram
          </Text>
          <Text textAlign="center" fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black">
            {'Enter your Instagram\nusername, so we can confirm\nthat you are a real fashion\nmodel'}
          </Text>
          <TextInput
            placeholder="username"
            onChangeValue={(text) => {
              if (!text.startsWith('@')) {
                text = '@' + text;
              }
              setUsername(text);
            }}
            value={username}
            onClear={() => setUsername('')}
            autoCapitalize="none"
          />
        </VStack>
        <Center>
          <Box position="absolute" bottom="20">
            <ReportBugButton />
          </Box>
          <PrimaryButton
            onPress={() => {
              dispatch(setUserName({ userName: transformUsername(username) }));
              handleFinishSignUp({ username: transformUsername(username) }).then(() => setNext(true));
            }}
            text="Next"
            disabled={!next && !username}
            isLoading={isLoading}
          />
        </Center>
      </VStack>
    </DefaultBackgroundElements>
  );
};
