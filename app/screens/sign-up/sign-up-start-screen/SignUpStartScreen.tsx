import React, { FC } from 'react';
import { AloneCentralMessage } from 'components/alone-central-message/AloneCentralMessage';
import { MAIN_COLOURS } from 'styles/colours';
import { SignUpTemplate } from '../SignUpTemplate';

export const SignUpStartScreen: FC = () => {
  return (
    <SignUpTemplate type="model">
      <AloneCentralMessage color={MAIN_COLOURS['primary-teal']} text="Are you represented by any modelling agency?" />
    </SignUpTemplate>
  );
};
