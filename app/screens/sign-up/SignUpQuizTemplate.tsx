import React, { FC, ReactNode } from 'react';
import { PrimaryButton } from 'components/buttons';
import { DefaultBackgroundElements } from 'components/default-background';
import { Box, Center, Flex, Progress } from 'native-base';
import { useAppBackHandler } from 'hooks/index';
import { ReportBugButton } from 'components/buttons/report-a-bug-button/ReportBugButton';

const QUIZ_SCREENS_CONFIG = {
  one: {
    index: 25,
    bgType: 'withGhostRight',
  },
  two: {
    index: 50,
    bgType: 'templateElements2',
  },
  three: {
    index: 75,
    bgType: 'templateElements2',
  },
  four: {
    index: 100,
    bgType: 'templateElements1',
  },
};

interface IProps {
  type: 'one' | 'two' | 'three' | 'four';
  children: ReactNode;
  onPressNext: () => void;
  disabled: boolean;
  loading?: boolean;
}

const SignUpQuizTemplate: FC<IProps> = ({ children, type, onPressNext, disabled = true, loading = false }) => {
  const { index, bgType } = QUIZ_SCREENS_CONFIG[type];

  useAppBackHandler();

  return (
    <DefaultBackgroundElements type={bgType!} withPadding={true} withPaddingVertical={true}>
      <Flex justifyContent="space-between" height="100%">
        <Progress
          mt="6"
          size="lg"
          value={index}
          _filledTrack={{
            bg: 'black',
          }}
        />
        {children}
        <Center>
          <Box position="absolute" bottom="20">
            <ReportBugButton />
          </Box>
          <PrimaryButton onPress={onPressNext} text="Next" disabled={disabled} isLoading={loading} />
        </Center>
      </Flex>
    </DefaultBackgroundElements>
  );
};

export default SignUpQuizTemplate;
