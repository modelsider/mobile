import React, { FC } from 'react';
import { MAIN_COLOURS } from 'styles/colours';
import { CircleNumber } from 'components/circles/circle-number/CircleNumber';
import { CircleSocial } from 'components/circles/circle-social/CircleSocial';
import { CIRCLE_SOCIAL_PARAMETERS } from 'components/circles/circle-social/circle-social';
import { useAppBackHandler, useAppDispatch, useDidMount } from 'hooks/index';
import { DefaultBackgroundElements } from 'components/default-background';
import { getUser } from 'services/users';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { isEarlyAccessByUserNameFB } from 'services/earlyAccess';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { Center, Text, VStack } from 'native-base';
import { EMOJIS } from 'components/emoji-icon/emoji';
import { clearUserInfoState } from 'store/auth/reducer';
import { clearStorage } from 'services/asyncStorage';
import { setIfRating, setTabView } from 'store/app/reducer';

export const SignUpFinishScreen: FC = () => {
  const dispatch = useAppDispatch();
  const { id, userName } = useSelector(selectUserInfo);

  useDidMount(() => {
    dispatch(setTabView({ type: 'login' }));

    dispatch(clearUserInfoState());
    dispatch(setIfRating({ hasRating: null }));
    clearStorage().then(async () => {
      console.log('The async storage cleared');
    });

    const observable = setInterval(async () => {
      getUser({ id }).then(async ({ status }) => {
        if (status === 'approved') {
          isEarlyAccessByUserNameFB({ userName }).then((isEarlyAccess) => {
            if (isEarlyAccess) {
              NavigationService.navigate({ routeName: ROUTES.Home });
            } else {
              NavigationService.navigateWithParams({
                routeName: ROUTES.SignUpApprovedStatus,
                params: { userId: id, observable },
              });
            }
          });
        }

        if (status === 'refused') {
          NavigationService.navigate({ routeName: ROUTES.SignUpRefusedStatus });
        }
      });
    }, 10000);
  });

  useAppBackHandler();

  return (
    <DefaultBackgroundElements type="templateElements2" withPadding={true} withPaddingVertical={true}>
      <Center justifyContent="space-around" height="100%">
        <Text textAlign="center" fontFamily="mono" fontWeight="400" fontStyle="normal" fontSize="3xl" color="black">
          {`Thank you ${EMOJIS.amen}`}
        </Text>

        <VStack alignItems="center" space="md">
          <CircleNumber color="teal.200" size={8} text="1" />

          <Text textAlign="center" fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black">
            {'Your application will be\nprocessed within the next\n24-48 hours.'}
          </Text>
        </VStack>

        <VStack alignItems="center" space="md">
          <CircleNumber color="teal.200" size={8} text="2" />

          <Text textAlign="center" fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black">
            {'We will send you\na confirmation email, when\nyour profile is approved.'}
          </Text>
        </VStack>

        <CircleSocial
          color={MAIN_COLOURS['primary-teal']}
          size={CIRCLE_SOCIAL_PARAMETERS.circleSmall.size}
          text={CIRCLE_SOCIAL_PARAMETERS.circleSmall.text}
        />
      </Center>
    </DefaultBackgroundElements>
  );
};
