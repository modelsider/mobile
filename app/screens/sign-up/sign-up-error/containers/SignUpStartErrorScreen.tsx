import React, { FC } from 'react';
import { TextHeaderBig } from 'components/typography/text-header/TextHeaderBig';
import { TextBodyNormal } from 'components/typography/text-body/TextBodyNormal';
import { SignUpErrorTemplate } from 'screens/sign-up/sign-up-error/components/SignUpErrorTemplate';
import { FONTS } from 'styles/fonts';
import { GhostSorryIcon } from 'components/UI/ghost/GhostSorryIcon';
import { useAppBackHandler } from 'hooks/index';

interface IAuthStartError {}

export const SignUpStartErrorScreen: FC<IAuthStartError> = () => {
  useAppBackHandler();

  return (
    <SignUpErrorTemplate>
      <GhostSorryIcon />
      <TextHeaderBig text="Sorry" textAlign="center" bottom="5%" top="5%" fontFamily={FONTS.americanTypewriter} />
      <TextBodyNormal
        text="This app is for models who are represented by modelling agencies only."
        textAlign="center"
        width={240}
        fontFamily={FONTS.SFUIText.regular}
      />
    </SignUpErrorTemplate>
  );
};
