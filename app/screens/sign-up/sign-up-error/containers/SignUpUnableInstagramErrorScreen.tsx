import React, { FC } from 'react';
import { SignUpErrorTemplate } from '../components/SignUpErrorTemplate';
import { TextHeaderBig } from 'components/typography/text-header/TextHeaderBig';
import { TextBodyNormal } from 'components/typography/text-body/TextBodyNormal';
import { FONTS } from 'styles/fonts';
import { GhostOopsIcon } from 'components/UI/ghost/GhostOopsIcon';
import { useAppBackHandler } from 'hooks/index';

interface IAuthUnableInstagramError {}

export const SignUpUnableInstagramErrorScreen: FC<IAuthUnableInstagramError> = () => {
  useAppBackHandler();

  return (
    <SignUpErrorTemplate>
      <GhostOopsIcon />
      <TextHeaderBig text="Oops..." textAlign="center" bottom="5%" top="5%" fontFamily={FONTS.americanTypewriter} />

      <TextBodyNormal
        text="We were unable to authorise your Instagram account. Please try again."
        textAlign="center"
        width={230}
        fontFamily={FONTS.SFUIText.regular}
      />
    </SignUpErrorTemplate>
  );
};
