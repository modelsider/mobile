import React, { FC } from 'react';
import { TextHeaderBig } from 'components/typography/text-header/TextHeaderBig';
import { TextBodyNormal } from 'components/typography/text-body/TextBodyNormal';
import { SignUpErrorTemplate } from 'screens/sign-up/sign-up-error/components/SignUpErrorTemplate';
import { FONTS } from 'styles/fonts';
import { GhostSorryIcon } from 'components/UI/ghost/GhostSorryIcon';
import { useAppBackHandler } from 'hooks/index';

interface IAuthNotPublicInstagramError {}

export const SignUpNotPublicInstagramErrorScreen: FC<IAuthNotPublicInstagramError> = () => {
  useAppBackHandler();

  return (
    <SignUpErrorTemplate>
      <GhostSorryIcon />
      <TextHeaderBig text="Sorry" textAlign="center" bottom="5%" top="5%" fontFamily={FONTS.americanTypewriter} />
      <TextBodyNormal
        text="We accept users with public Instagram accounts only."
        textAlign="center"
        width={260}
        bottom="5%"
      />
      <TextBodyNormal
        text="Please change your Instagram account settings and try again."
        textAlign="center"
        width={260}
      />
    </SignUpErrorTemplate>
  );
};
