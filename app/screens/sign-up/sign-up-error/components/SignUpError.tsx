import React, { FC } from 'react';
import { StyleSheet, View } from 'react-native';
import { TextWithEmoji } from 'components/typography/text-with-emoji/TextWithEmoji';
import { EMOJIS } from 'components/emoji-icon/emoji';
import { TextBodySmall } from 'components/typography/text-body/TextBodySmall';
import { ADDITIONAL_COLOURS } from 'styles/colours';

interface IAuthError {}

export const SignUpError: FC<IAuthError> = () => {
  return (
    <View style={styles.container}>
      <TextWithEmoji text="Oops..." emojiName={EMOJIS.oops} color={ADDITIONAL_COLOURS['primary-red']} />
      <TextBodySmall text="Entry is not valid." color={ADDITIONAL_COLOURS['primary-red']} />
      <TextBodySmall text="Please try again." color={ADDITIONAL_COLOURS['primary-red']} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginBottom: '-8.7%',
  },
});
