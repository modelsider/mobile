import React, { FC } from 'react';
import { LogoIcon } from 'components/logo-icon/LogoIcon';
import { MAIN_COLOURS } from 'styles/colours';
import { Signature } from 'components/signature/Signature';
// @ts-ignore
import { SIGNATURE } from '@env';
import { Container } from 'components/layout/splash-register-login/Container';
import { Header } from 'components/layout/splash-register-login/Header';
import { Content } from 'components/layout/splash-register-login/Content';
import { Footer } from 'components/layout/splash-register-login/Footer';
import { CIRCLE_SOCIAL_PARAMETERS } from 'components/circles/circle-social/circle-social';
import { CircleSocial } from 'components/circles/circle-social/CircleSocial';
import { DefaultBackgroundElements } from 'components/default-background';

interface IAuthErrorTemplate {
  children: any;
}

export const SignUpErrorTemplate: FC<IAuthErrorTemplate> = ({ children }) => {
  return (
    <DefaultBackgroundElements type="templateElements2">
      <Container>
        <Header>
          <LogoIcon />
        </Header>
        <Content>{children}</Content>
        <Footer>
          <CircleSocial
            color={MAIN_COLOURS['primary-teal']}
            size={CIRCLE_SOCIAL_PARAMETERS.circleSmall.size}
            text={CIRCLE_SOCIAL_PARAMETERS.circleSmall.text}
          />
          <Signature text={SIGNATURE} />
        </Footer>
      </Container>
    </DefaultBackgroundElements>
  );
};
