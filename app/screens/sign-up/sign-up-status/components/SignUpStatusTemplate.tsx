import React, { FC } from 'react';
import { MAIN_COLOURS } from 'styles/colours';
import { Signature } from 'components/signature/Signature';
// @ts-ignore
import { SIGNATURE } from '@env';
import { CIRCLE_SOCIAL_PARAMETERS } from 'components/circles/circle-social/circle-social';
import { CircleSocial } from 'components/circles/circle-social/CircleSocial';
import { PrimaryButton } from 'components/buttons';
import { DefaultBackgroundElements } from 'components/default-background';
import { Center, Flex, VStack } from 'native-base';
import { statusType } from 'store/auth/reducer';
import { noop } from 'types/types';

interface IProps {
  children: any;
  status?: statusType;
  onPress?: noop;
  isLoading?: boolean;
}

export const SignUpStatusTemplate: FC<IProps> = ({ children, status, onPress, isLoading = false }) => {
  let footerContent = (
    <VStack alignItems="center" space="2xl">
      <CircleSocial
        color={MAIN_COLOURS['primary-teal']}
        size={CIRCLE_SOCIAL_PARAMETERS.circleSmall.size}
        text={CIRCLE_SOCIAL_PARAMETERS.circleSmall.text}
      />
      <Signature text={SIGNATURE} />
    </VStack>
  );

  if (status === 'approved') {
    footerContent = (
      <VStack space="2xl">
        <PrimaryButton onPress={onPress!} text="Rate" isLoading={isLoading} />
      </VStack>
    );
  }

  return (
    <DefaultBackgroundElements type="templateElements2" withPadding={true} withPaddingVertical={true}>
      <Flex flex={1} justifyContent="space-between">
        <Center flex={2}>{children}</Center>
        {footerContent}
      </Flex>
    </DefaultBackgroundElements>
  );
};
