import React, { FC, useCallback } from 'react';
import { SignUpStatusTemplate } from 'screens/sign-up/sign-up-status/components/SignUpStatusTemplate';
import { Ghost } from 'components/UI/ghost/Ghost';
import { useAgencies, useAppBackHandler, useAppDispatch, useAuth, useDidMount } from 'hooks/index';
import { setStatusBar } from 'store/app/reducer';
import { EMOJIS } from 'components/emoji-icon/emoji';
import { RouteProp, useRoute } from '@react-navigation/native';
import { Text, VStack } from 'native-base';

type Route = {
  params: {
    userId: string | null;
    observable: any;
  };
};

export const SignUpApprovedScreen: FC = () => {
  const {
    params: { userId, observable },
  } = useRoute<RouteProp<Route>>();

  const dispatch = useAppDispatch();
  const { handleUpdateIsFirstTimeLoginInfo, isLoading } = useAuth();
  const { handleToggleSearch } = useAgencies();

  const handleRateAgency = useCallback(async () => {
    await handleUpdateIsFirstTimeLoginInfo({ userId });
    handleToggleSearch();
  }, [handleToggleSearch, handleUpdateIsFirstTimeLoginInfo, userId]);

  useDidMount(() => {
    dispatch(setStatusBar({ isStatusBar: true }));
    clearInterval(observable);
  });

  useAppBackHandler();

  return (
    <SignUpStatusTemplate status="approved" onPress={handleRateAgency} isLoading={isLoading}>
      <Ghost rotate={0} type="right" height="95" width="68" />
      <VStack space="sm">
        <Text textAlign="center" fontFamily="mono" fontWeight="400" fontStyle="normal" fontSize="3xl" color="black">
          {`Congrats ${EMOJIS.congrats}`}
        </Text>
        <Text textAlign="center" fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black">
          {
            'Your application is approved.\n\nRate one of the mother or\nbooking agencies you worked\nwith within the last 3 years to get\na full access to the app.\n\nRatings are always anonymous.'
          }
        </Text>
      </VStack>
    </SignUpStatusTemplate>
  );
};
