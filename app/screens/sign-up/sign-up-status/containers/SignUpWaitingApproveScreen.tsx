import React, { FC, useEffect } from 'react';
import { SignUpStatusTemplate } from 'screens/sign-up/sign-up-status/components/SignUpStatusTemplate';
import { Ghost } from 'components/UI/ghost/Ghost';
import { setStatusBar } from 'store/app/reducer';
import { useAppDispatch } from 'hooks/index';
import { Text, VStack } from 'native-base';
import { EMOJIS } from 'components/emoji-icon/emoji';

export const SignUpWaitingApproveScreen: FC = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(setStatusBar({ isStatusBar: false }));
  }, [dispatch]);

  return (
    <SignUpStatusTemplate>
      <Ghost rotate={0} type="right" height="95" width="68" />

      <VStack space="sm">
        <Text textAlign="center" fontFamily="mono" fontWeight="400" fontStyle="normal" fontSize="3xl" color="black">
          {`Please wait ${EMOJIS.clock}`}
        </Text>
        <Text textAlign="center" fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black">
          {'We are still processing your\napplication.'}
        </Text>
        <Text textAlign="center" fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black">
          {'We will ask for further details via\nemail or approve your\napplication soon.'}
        </Text>
      </VStack>
    </SignUpStatusTemplate>
  );
};
