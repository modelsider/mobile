import React, { FC, useEffect } from 'react';
import { SignUpStatusTemplate } from 'screens/sign-up/sign-up-status/components/SignUpStatusTemplate';
import { GhostSorryIcon } from 'components/UI/ghost/GhostSorryIcon';
import { useAppDispatch } from 'hooks/index';
import { setStatusBar } from 'store/app/reducer';
import { Text, VStack } from 'native-base';

export const SignUpRefusedScreen: FC = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(setStatusBar({ isStatusBar: false }));
  }, [dispatch]);

  return (
    <SignUpStatusTemplate>
      <GhostSorryIcon />
      <VStack space="sm">
        <Text textAlign="center" fontFamily="mono" fontWeight="400" fontStyle="normal" fontSize="3xl" color="black">
          Sorry
        </Text>
        <Text textAlign="center" fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black">
          {'We are unable to verify that\nyou are a signed fashion\nmodel.'}
        </Text>
      </VStack>
    </SignUpStatusTemplate>
  );
};
