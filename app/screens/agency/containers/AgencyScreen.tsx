import React, { FC } from 'react';
import { DefaultBackground } from 'components/default-background';
import { CustomTabView } from 'components/custom-tab-view';
import { useAppBackHandler, useTabView } from 'hooks/index';
import { useSelector } from 'react-redux';
import { selectAgencyType } from 'store/agency/selectors';
import { HeaderAgency } from '../components/HeaderAgency';
import { RouteProp, useRoute } from '@react-navigation/native';
import { agencyType } from 'types/types';

type Route = {
  params: {
    type: agencyType;
  };
};

const AgencyScreen: FC = () => {
  const {
    params: { type: typeAgency }, // for deeplink
  } = useRoute<RouteProp<Route>>();

  const type = useSelector(selectAgencyType);
  const idx = typeAgency || type === 'booking' ? 1 : 0;

  const { tabs, tabRoutes } = useTabView({ tabType: 'Agency' });

  useAppBackHandler();

  return (
    <DefaultBackground>
      <HeaderAgency />
      <CustomTabView tabs={tabs} tabRoutes={tabRoutes} idx={idx} />
    </DefaultBackground>
  );
};

export default AgencyScreen;
