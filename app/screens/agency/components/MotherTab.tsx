import React, { FC } from 'react';
import AgencyTab from './AgencyTab';

const MotherTab: FC = () => {
  return <AgencyTab />;
};

export default MotherTab;
