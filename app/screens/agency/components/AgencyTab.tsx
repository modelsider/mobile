import React, { FC, useEffect, useMemo } from 'react';
import TabAgencyList from 'screens/agency/components/TabAgencyList';
import {
  useAppDispatch,
  useComments,
  useAgency,
  useScrollAgencyComments,
  useLeaveComment,
  useSortComments,
  useUsers,
} from 'hooks/index';
import { RouteProp, useIsFocused, useNavigation, useRoute } from '@react-navigation/native';
import { resetRatedAgency } from 'store/rate-agency/reducer';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { selectAgencyType } from 'store/agency/selectors';
import sortBy from 'lodash/sortBy';
import { selectCommentIndex } from 'store/comments/selectors';
import { resetCommentState, setClickedCommentIndex } from 'store/comments/reducer';
import { setIsRatingAgencyNow } from 'store/agency/actions';
import { Box, HStack, Text, useTheme, WarningOutlineIcon } from 'native-base';
import { setStatusBarBg } from 'store/app/reducer';
import { agencyType } from 'types/types';
import isUndefined from 'lodash/isUndefined';

type Route = {
  params: {
    id: number;
    type?: agencyType;
  };
};

const AgencyTab: FC = () => {
  const {
    params: { id, type: typeAgency }, // for deeplink
  } = useRoute<RouteProp<Route>>();

  const { space, colors } = useTheme();

  const navigation = useNavigation();
  const dispatch = useAppDispatch();
  const user = useSelector(selectUserInfo);
  const type = useSelector(selectAgencyType);
  const selectedCommentIndex = useSelector(selectCommentIndex);

  const isFocused = useIsFocused();

  const { handleFetchAgency, isFetchAgency, agencyId, agency, handleRateAgency } = useAgency();

  const {
    comments,
    fetchAgencyComments,
    isFetchComments,
    fetchUploadedComments,
    isMomentumScrollEnd,
    handleMomentumScrollEnd,
    handleMomentumScrollBegin,
  } = useComments(+id);

  const { isCommentButton, onScrollToComments } = useScrollAgencyComments({
    comments,
  });

  const { navigateToLeaveComment } = useLeaveComment();

  const { selectedButtons, handleSortComments } = useSortComments();

  const { handleFetchUsers } = useUsers();

  const sortedByTypes = useMemo(() => {
    return (
      comments
        // @ts-ignore
        .sort((a, b) => {
          if (selectedButtons.includes('likes')) {
            return b.likes_count - a.likes_count;
          }
          return a;
        })
        .filter((comment) => comment.types.some((commentTypeItem) => selectedButtons.includes(commentTypeItem)))
    );
  }, [comments, selectedButtons]);
  const sortedByLikes = useMemo(() => {
    return sortBy(comments, ['likes_count']).reverse();
  }, [comments]);

  useEffect(() => {
    if (isFocused) {
      dispatch(setIsRatingAgencyNow({ isRatingNow: false, docId: agency?.docId! }));

      if (!agency.isClosed) {
        return;
      }

      dispatch(setStatusBarBg({ bg: colors.yellow['100'] }));

      navigation.setOptions({
        title: (
          <HStack alignItems="center" space="xs" paddingTop={2}>
            <WarningOutlineIcon size="md" color="yellow.500" />
            <Text fontFamily="body" fontWeight="500" fontStyle="normal" fontSize="lg">
              Closed
            </Text>
          </HStack>
        ),
        headerStyle: {
          backgroundColor: colors.yellow['100'],
          borderTopColor: colors.yellow['100'],
          shadowColor: colors.yellow['100'],
        },
      });

      return () => {
        dispatch(setStatusBarBg({ bg: '' }));
      };
    }
  }, [agency?.docId, agency.isClosed, colors.yellow, dispatch, isFocused, navigation]);

  useEffect(() => {
    if (isFocused) {
      handleFetchAgency(+id || +agencyId);
      handleFetchUsers(+id || +agencyId);
      dispatch(resetRatedAgency());
      dispatch(resetCommentState());
      dispatch(setClickedCommentIndex({ commentIndex: 0 }));
    }
  }, [dispatch, handleFetchAgency, agencyId, user.id, isFocused, handleFetchUsers, id]);

  useEffect(() => {
    if (isFocused && !selectedCommentIndex) {
      fetchAgencyComments(id);
    }
  }, [dispatch, fetchAgencyComments, id, isFocused, selectedCommentIndex]);

  return (
    <Box mb={space['2']}>
      <TabAgencyList
        comments={
          selectedButtons.includes('likes') && selectedButtons.length === 1
            ? sortedByLikes
            : selectedButtons.length
            ? sortedByTypes
            : comments
        }
        isFetchAgency={isFetchAgency}
        isFetchComments={isFetchComments}
        isCommentButton={isCommentButton}
        onRefresh={() => {
          handleFetchAgency(+id || +agencyId);
          fetchAgencyComments();
        }}
        type={type}
        isMomentumScrollEnd={isMomentumScrollEnd}
        onMomentumScrollBegin={handleMomentumScrollBegin}
        onMomentumScrollEnd={handleMomentumScrollEnd}
        fetchUploadedComments={fetchUploadedComments}
        onScrollToComments={onScrollToComments}
        navigateToLeaveComment={navigateToLeaveComment}
        onSortComments={handleSortComments}
        selectedButtons={selectedButtons}
        agency={agency}
        onRateAgency={handleRateAgency}
        selectedCommentIndex={selectedCommentIndex}
        isDeepLink={!isUndefined(typeAgency)}
      />
    </Box>
  );
};

export default AgencyTab;
