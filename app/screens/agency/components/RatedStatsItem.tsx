import React, { FC } from 'react';
import { RowRating } from 'components/rating';
import { calculateRatedStatsColor } from '../../../functions/rating-agency/calculateStarRatingColor';
import { Text, useTheme } from 'native-base';

interface IMotherRatedStatsItem {
  title: string;
  rating: number;
  isRated: boolean;
}

export const RatedStatsItem: FC<IMotherRatedStatsItem> = ({ title, rating, isRated }) => {
  const { colors } = useTheme();

  const color = isRated ? colors.black : colors.gray['300'];

  const ratingColor = isRated ? calculateRatedStatsColor({ rating }) : colors.gray['100'];

  return (
    <>
      <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="md" color={color}>
        {title}
      </Text>
      <RowRating rating={rating} color={ratingColor} isRated={isRated} />
    </>
  );
};
