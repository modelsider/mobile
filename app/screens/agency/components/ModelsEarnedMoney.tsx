import React, { FC } from 'react';
import { EMOJIS } from 'components/emoji-icon/emoji';
import { Text, useTheme } from 'native-base';

interface IModelsEarnedMoney {
  count: number;
  isRated: boolean;
}

export const ModelsEarnedMoney: FC<IModelsEarnedMoney> = ({ count = 0, isRated }) => {
  const { colors } = useTheme();

  const color = isRated ? colors.black : colors.gray['400'];

  return (
    <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="md" color={color}>
      {`${EMOJIS.money} ${count} models earned money`}
    </Text>
  );
};
