import React, { FC } from 'react';
import { useSelector } from 'react-redux';
import { selectAgency, selectAgencyLoading } from 'store/agency/selectors';
import { AgenciesBookmarkSkeleton } from 'components/skeleton';
import BookmarkAgencyItem from 'components/bookmark-agency-item/BookmarkAgencyItem';
import { Box, useTheme } from 'native-base';

interface IHeaderAgency {
  isBookmark?: boolean;
}

export const HeaderAgency: FC<IHeaderAgency> = () => {
  const { space } = useTheme();

  const agency = useSelector(selectAgency);
  const isLoadingAgency = useSelector(selectAgencyLoading);

  const location = `${agency?.city}, ${agency?.country}`;

  return (
    <>
      {isLoadingAgency ? (
        <Box my={space['1.5']}>
          <AgenciesBookmarkSkeleton items={1} />
        </Box>
      ) : (
        <BookmarkAgencyItem
          title={agency?.agency_name!}
          location={location}
          agencyId={agency?.id!}
          isBookmark={agency?.isBookmark!}
          isHeader={true}
        />
      )}
    </>
  );
};
