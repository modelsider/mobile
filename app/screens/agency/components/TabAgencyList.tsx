import { FlatList, NativeScrollEvent, NativeSyntheticEvent, RefreshControl } from 'react-native';
import React, { FC, memo, useEffect, useRef } from 'react';
import { Comment, CommentsHeader } from 'components/comments';
import { LinksModels } from 'components/links-models/LinksModels';
import { RatedHeader } from 'screens/agency/components/RatedHeader';
import { PrimaryButton, SecondaryButton } from 'components/buttons';
import { AgencySkeleton, CommentItemSkeleton } from 'components/skeleton';
import { agencyType, commentType, IAgency, IComment, noop } from 'types/types';
import BottomActivityIndicator from 'components/bottom-activity-indicator/BottomActivityIndicator';
import { RatedStats } from './RatedStats';
import { getScreenDimensions } from 'utils/layoutUtils';
import { ModelsEarnedMoney } from './ModelsEarnedMoney';
import { Box, Divider, Text, useTheme } from 'native-base';

interface IMotherTabAgencyList {
  comments: IComment[];
  isFetchAgency: boolean;
  isFetchComments: boolean;
  isCommentButton: boolean;
  onRefresh: noop;
  type: agencyType;
  isMomentumScrollEnd: boolean;
  onMomentumScrollBegin: noop;
  onMomentumScrollEnd: noop;
  fetchUploadedComments: noop;
  onScrollToComments: ({
    nativeEvent: { layoutMeasurement, contentOffset, contentSize },
  }: NativeSyntheticEvent<NativeScrollEvent>) => void;
  navigateToLeaveComment: noop;
  onSortComments: ({ type }: { type: commentType }) => void;
  selectedButtons: string[];
  agency: IAgency;
  onRateAgency: ({ isRefetchAgency, id }: { isRefetchAgency?: boolean; id?: number }) => void;
  selectedCommentIndex: number;
  isDeepLink: boolean;
}

const TabAgencyList: FC<IMotherTabAgencyList> = ({
  comments,
  agency,
  onScrollToComments,
  navigateToLeaveComment,
  fetchUploadedComments,
  isMomentumScrollEnd,
  onMomentumScrollBegin,
  onMomentumScrollEnd,
  isFetchComments,
  isFetchAgency,
  isCommentButton,
  onRefresh,
  type,
  onSortComments,
  selectedButtons,
  onRateAgency,
  selectedCommentIndex,
  isDeepLink = false,
}) => {
  const { space } = useTheme();

  const flatListRef = useRef<FlatList>();

  const { isRatedByCurrentUser, isClosed } = agency;

  const { is_rated, rate_count, total_rating, statistics, models_earned_money = 0 } = agency?.[type] || ({} as IAgency);

  const buttonTitle = isRatedByCurrentUser ? 'Update' : 'Rate';

  // scroll only when deeplink
  useEffect(() => {
    if (!isDeepLink) {
      return;
    }

    if (!isFetchComments && !isFetchAgency && comments.length) {
      flatListRef.current?.scrollToIndex({ animated: false, index: 2, viewOffset: 20 });
    }
  }, [comments.length, isDeepLink, isFetchAgency, isFetchComments]);

  const renderItem = ({ item, index }: { item: IComment; index: number }) => {
    return (
      <Box mt={space['1']} pb={space['1']}>
        <Comment comment={item} commentIndex={index} onRefresh={onRefresh} />
      </Box>
    );
  };

  const listEmptyComponent = () =>
    isFetchComments ? (
      <CommentItemSkeleton />
    ) : (
      <Box my={space['1']}>
        <Text fontFamily="heading" fontWeight="600" fontStyle="normal" fontSize="2xl" color="gray.300">
          No comments yet 🙈
        </Text>

        <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="sm" color="gray.300">
          Be the first one to comment!
        </Text>
      </Box>
    );

  const listHeaderComponent = () =>
    isFetchAgency ? (
      <>
        <AgencySkeleton type={type} />
        <CommentsHeader onSortComments={onSortComments} selectedButtons={selectedButtons} />
      </>
    ) : (
      <>
        <RatedHeader isRated={is_rated} rateCount={rate_count} totalRating={total_rating} />
        {type === 'booking' && (
          <>
            <Divider my={space['0.5']} />
            <ModelsEarnedMoney count={models_earned_money} isRated={is_rated} />
          </>
        )}
        {/* @ts-ignore todo: fix interface */}
        <RatedStats isRated={is_rated} statistics={statistics} type={type} />
        <LinksModels />
        <CommentsHeader onSortComments={onSortComments} selectedButtons={selectedButtons} />
      </>
    );

  const ListFooterComponent = () => (isMomentumScrollEnd || !comments.length ? null : <BottomActivityIndicator />);

  const getItemLayout = (data: IComment[] | null | undefined, index: number) => {
    return {
      length: getScreenDimensions().width,
      offset: getScreenDimensions().width * index,
      index,
    };
  };

  return (
    <>
      <FlatList
        // @ts-ignore
        ref={flatListRef}
        data={comments}
        renderItem={renderItem}
        initialScrollIndex={selectedCommentIndex}
        getItemLayout={getItemLayout}
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={listHeaderComponent}
        ListFooterComponent={ListFooterComponent}
        ListEmptyComponent={listEmptyComponent}
        onScroll={onScrollToComments}
        onEndReached={fetchUploadedComments}
        onEndReachedThreshold={0.5}
        onMomentumScrollEnd={onMomentumScrollEnd}
        onMomentumScrollBegin={onMomentumScrollBegin}
        refreshControl={<RefreshControl refreshing={false} onRefresh={onRefresh} />}
        contentContainerStyle={{
          paddingBottom: space['16'],
          paddingTop: space['3'],
        }}
      />
      {/* todo: duplicate */}
      {!isClosed && (
        <Box bottom={space['3']}>
          {isCommentButton ? (
            <SecondaryButton
              onPress={navigateToLeaveComment}
              text="Comment"
              disabled={isFetchAgency}
              isLoading={isFetchAgency}
            />
          ) : (
            <PrimaryButton
              onPress={() => onRateAgency({})}
              text={buttonTitle}
              disabled={isFetchAgency}
              isLoading={isFetchAgency}
            />
          )}
        </Box>
      )}
    </>
  );
};

export default memo(TabAgencyList);
