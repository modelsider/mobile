import React, { FC, useMemo } from 'react';
import { RATED_INFO } from 'constants/index';
import NavigationService from 'navigator/NavigationService';
import { Pressable, Text } from 'native-base';
import { useUsers } from 'hooks/index';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import AvatarGroup from 'components/avatar/AvatarGroup';
import { ROUTES } from 'navigator/constants/routes';
import { selectAgency } from "../../../store/agency/selectors";

const LIMIT_OF_RATES_COUNT = 10;

interface IMotherRatedInfo {
  isRated: boolean;
  rateCount: number;
}

const RatedInfo: FC<IMotherRatedInfo> = ({ isRated, rateCount }) => {
  const currentUser = useSelector(selectUserInfo);
  const { isRatedByCurrentUser } = useSelector(selectAgency);

  const { users } = useUsers();
  const usersProfilePhoto = useMemo(
    () => users.filter((user) => user.id !== currentUser.id).map((user) => user.profilePictureUrl),
    [],
  );

  const isDisabled = rateCount < LIMIT_OF_RATES_COUNT;

  const text = useMemo(
    () =>
      !isRated
        ? RATED_INFO.default
        : rateCount < RATED_INFO.defaultRatedCount
        ? RATED_INFO.limitation
        : `Rated by${isRatedByCurrentUser ? ' You +' : ''} ${rateCount} models`,
    [isRated, rateCount],
  );

  const ratedImage = !isRated || rateCount <= RATED_INFO.defaultRatedCount || (
    <AvatarGroup images={usersProfilePhoto} />
  );

  return (
    <Pressable
      disabled={isDisabled}
      flexDirection="row"
      alignItems="center"
      onPress={() => NavigationService.navigate({ routeName: ROUTES.WhoRatedAgency })}
    >
      {ratedImage}

      <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="sm" color="gray.300">
        {text}
      </Text>
    </Pressable>
  );
};

export default RatedInfo;
