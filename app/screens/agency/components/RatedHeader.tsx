import React, { FC } from 'react';
import RatedInfo from 'screens/agency/components/RatedInfo';
import { StarRating } from 'components/rating';
import { Text, useTheme } from 'native-base';

const SIZE = 26;

interface IMotherRatedHeader {
  isRated: boolean;
  totalRating: number;
  rateCount: number;
}

export const RatedHeader: FC<IMotherRatedHeader> = ({ isRated, totalRating, rateCount }) => {
  const { colors } = useTheme();

  const text = isRated ? 'Total rating' : 'No rating yet 🙈';
  const currentRating = isRated ? totalRating?.toFixed(1) : 0;
  const color = isRated ? colors.black : colors.gray['300'];

  return (
    <>
      <Text fontFamily="heading" fontWeight="600" fontStyle="normal" fontSize="2xl" color={color}>
        {text}
      </Text>

      <StarRating rating={currentRating} starSize={SIZE} ratingNumberSize={SIZE} isRated={isRated} />
      <RatedInfo isRated={isRated} rateCount={rateCount} />
    </>
  );
};
