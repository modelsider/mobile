import React, { FC, Fragment, memo } from 'react';
import { RatedStatsItem } from 'screens/agency/components/RatedStatsItem';
import sample from 'lodash/sample';
import { ratedStatisticsType } from 'constants';
import { agencyType, IBookingAgencyStatistics, IMotherAgencyStatistics, IStatisticItemProps } from 'types/types';
import { Spacer, useTheme } from 'native-base';
import { getStatisticTitle } from 'utils/getStatisticTitle';

interface IMotherRatedStats {
  isRated: boolean;
  statistics: (IStatisticItemProps & (IMotherAgencyStatistics | IBookingAgencyStatistics))[];
  type: agencyType;
}

export const RatedStats: FC<IMotherRatedStats> = memo(({ isRated, statistics, type }) => {
  const { space } = useTheme();

  return (
    <>
      {statistics?.map((item, index) => {
        const statisticType: ratedStatisticsType = item.type;
        const rating = item[statisticType];

        if (rating || rating !== 0 || !isRated) {
          return (
            <Fragment key={index}>
              <Spacer size={space['1']} />
              <RatedStatsItem
                title={getStatisticTitle({ type, statisticType })}
                rating={rating || sample([10, 20, 30, 40, 50, 60, 70, 80, 90, 100])}
                isRated={isRated}
              />
            </Fragment>
          );
        }
      })}
    </>
  );
});
