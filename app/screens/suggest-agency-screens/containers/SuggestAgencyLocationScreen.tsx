import React, { FC } from 'react';
import SuggestAgencyTemplate from './SuggestAgencyTemplate';

export const SuggestAgencyLocationScreen: FC = () => {
  return <SuggestAgencyTemplate type="country" />;
};
