import React, { FC } from 'react';
import SuggestAgencyTemplate from './SuggestAgencyTemplate';

export const SuggestAgencyWebsiteScreen: FC = () => {
  return <SuggestAgencyTemplate type="web" />;
};
