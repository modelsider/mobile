import React, { FC } from 'react';
import SuggestAgencyTemplate from './SuggestAgencyTemplate';

export const SuggestAgencyNameScreen: FC = () => {
  return <SuggestAgencyTemplate type="agency_name" />;
};
