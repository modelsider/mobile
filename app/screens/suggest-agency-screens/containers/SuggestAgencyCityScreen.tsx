import React, { FC } from 'react';
import SuggestAgencyTemplate from './SuggestAgencyTemplate';

export const SuggestAgencyCityScreen: FC = () => {
  return <SuggestAgencyTemplate type="city" />;
};
