import React, { FC } from 'react';
import SuggestAgencyTemplate from './SuggestAgencyTemplate';

export const SuggestAgencyInstagramScreen: FC = () => {
  return <SuggestAgencyTemplate type="instagram" />;
};
