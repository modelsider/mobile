import React, { FC, useState } from 'react';
import { PrimaryButton } from 'components/buttons';
import { DefaultBackgroundElements } from 'components/default-background';
import { Input } from 'components/inputs';
import NavigationService from 'navigator/NavigationService';
import { Flex, Progress, Text, VStack } from 'native-base';
import { SUGGESTED_AGENCY_CONFIG } from '../config/config';
import { useAppDispatch, useAppBackHandler } from 'hooks/index';
import { resetAgencyState, setSuggestAgency } from 'store/agency/reducer';
import { suggestAgencyType } from 'types/types';
import { Keyboard } from 'react-native';
import { addSuggestAgencyFB } from 'services/agencies';
import Toast from 'react-native-toast-message';
import { useSelector } from 'react-redux';
import { selectSuggestAgency } from 'store/agency/selectors';
import { selectUserInfo } from 'store/auth/selectors';

interface IProps {
  type: suggestAgencyType;
}

const SuggestAgencyTemplate: FC<IProps> = ({ type }) => {
  const dispatch = useAppDispatch();

  const suggestAgency = useSelector(selectSuggestAgency);
  const currentUser = useSelector(selectUserInfo);

  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [value, setValue] = useState<string>('');

  const { label, placeholder, routeName, index } = SUGGESTED_AGENCY_CONFIG[type];
  const isSubmit = type === 'instagram';

  const handleNext = async () => {
    if (isSubmit) {
      setIsLoading(true);

      const response = await addSuggestAgencyFB({
        suggestedAgency: {
          ...suggestAgency,
          comment: value,
          user_id: currentUser.id,
          created_at: new Date(),
        },
      });

      if (!response) {
        Toast.show({
          text1: 'The agency already exists',
          type: 'warning',
        });
        setIsLoading(false);
        return;
      }

      setIsLoading(false);
      dispatch(resetAgencyState());
      Toast.show({
        text1: 'The agency added successfully',
      });

      return NavigationService.navigate({
        routeName,
      });
    }

    dispatch(setSuggestAgency({ agency: { [type]: value } }));
    return NavigationService.navigate({
      routeName,
    });
  };

  useAppBackHandler();

  return (
    <DefaultBackgroundElements type="withGhostLeft" withPaddingVertical={true} withPadding={true}>
      <Flex justifyContent="space-between" height="100%" pt="5">
        <Progress
          size="lg"
          value={index}
          _filledTrack={{
            bg: 'black',
          }}
        />
        <VStack space="2" mb="32">
          <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="xl" textAlign="center">
            {label}
          </Text>
          <Input
            placeholder={placeholder}
            onChangeText={setValue}
            value={value}
            onClear={() => setValue('')}
            onTouchEnd={() => Keyboard.dismiss()}
          />
        </VStack>
        <PrimaryButton onPress={handleNext} text={isSubmit ? 'Submit' : 'Next'} isLoading={isLoading} />
      </Flex>
    </DefaultBackgroundElements>
  );
};

export default SuggestAgencyTemplate;
