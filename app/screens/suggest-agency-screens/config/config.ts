import { ROUTES } from 'navigator/constants/routes';

const SUGGESTED_AGENCY_CONFIG = {
  agency_name: {
    label: 'What is the agency name?',
    placeholder: 'Agency name',
    routeName: ROUTES.SuggestAgencyLocation,
    index: 20,
  },
  country: {
    label: 'What is the agency location?',
    placeholder: 'Country',
    routeName: ROUTES.SuggestAgencyCityScreen,
    index: 40,
  },
  city: {
    label: 'What is the agency city?',
    placeholder: 'City',
    routeName: ROUTES.SuggestAgencyWebsite,
    index: 60,
  },
  web: {
    label: 'What is the agency website?',
    placeholder: 'Agency website',
    routeName: ROUTES.SuggestAgencyInstagram,
    index: 80,
  },
  instagram: {
    label: 'What is the agency Instagram?',
    placeholder: 'Agency Instagram',
    routeName: ROUTES.SearchAgency,
    index: 100,
  },
};

export { SUGGESTED_AGENCY_CONFIG };
