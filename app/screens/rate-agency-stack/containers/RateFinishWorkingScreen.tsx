import React, { FC, useEffect, useState } from 'react';
import { RateDatePicker } from '../components/RateDatePicker';
import { IWhoRatedAgency } from 'types/types';
import { ErrorWorkWithAgencyLongTimeAgo } from 'components/errors/ErrorWorkWithAgencyLongTimeAgo';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { useAppDispatch } from 'hooks/index';
import { DEFAULT_DATE } from 'components/select/select-date/utils';
import { selectAgencyById, selectAgencyType } from 'store/agency/selectors';
import { setWorkingTime } from 'store/rate-agency/reducer';
import { useSelector } from 'react-redux';
import { selectIsUpdateDates, selectLastRatedAgency } from 'store/rate-agency/selectors';
import { getMappedDate } from 'utils/getMappedDate';
import { updateWhoRatedAgency } from 'store/agency/actions';
import { getDifferenceInYears } from 'utils/getDifferenceInYears';

export const RateFinishWorkingScreen: FC = () => {
  const dispatch = useAppDispatch();

  const type = useSelector(selectAgencyType);
  const isUpdateDates = useSelector(selectIsUpdateDates);
  const selectedAgency = useSelector(selectAgencyById);
  const ratedAgency = useSelector(selectLastRatedAgency({ type }));

  const prevFinishWorkingAt = getMappedDate(selectedAgency?.finish_working_at);

  const [next, setNext] = useState(true);
  const [date, setDate] = useState(prevFinishWorkingAt || '');
  const [isVisible, setIsVisible] = useState(false);

  const onSelectDate = (value: string) => {
    const isPresent = new Date().toLocaleDateString() === value;
    setDate(isPresent ? 'Present' : value);
  };

  const onNext = async () => {
    if (date !== 'Present') {
      const diffInYears = getDifferenceInYears(date);
      if (diffInYears >= 3) {
        setIsVisible(true);
        return;
      }
    }

    const [day, month, year] = date.split('/');
    const formattedDate = `${year}-${month}-${day}`;
    const dateObject = new Date(formattedDate);
    const outputDateString = dateObject.toDateString();
    const outputTimeString = dateObject.toTimeString();
    const outputDate = `${outputDateString} ${outputTimeString}`;
    const finish_working_at = date === 'Present' ? date : new Date(outputDate);

    dispatch(
      setWorkingTime({
        agency: { finish_working_at } as IWhoRatedAgency,
      }),
    );

    if (isUpdateDates) {
      const data = {
        start_working_at: ratedAgency?.start_working_at,
        finish_working_at,
      };

      dispatch(
        updateWhoRatedAgency({
          docId: selectedAgency?.whoRatedAgencyDocId!,
          data,
        }),
      );
      return NavigationService.resetToHome();
    }

    return NavigationService.navigate({
      routeName: ROUTES.ProfessionalScreen,
    });
  };

  useEffect(() => {
    if (date) {
      setNext(false);
    }
  }, [date]);

  return (
    <>
      <RateDatePicker
        next={next}
        onSelectDate={onSelectDate}
        onNext={onNext}
        index={2}
        isFinish={true}
        isUpdateDates={isUpdateDates}
        date={date || DEFAULT_DATE}
        minimumDate={ratedAgency.start_working_at}
      />
      <ErrorWorkWithAgencyLongTimeAgo isVisible={isVisible} setIsVisible={setIsVisible} />
    </>
  );
};
