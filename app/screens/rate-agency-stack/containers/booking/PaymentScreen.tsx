import React, { FC } from 'react';
import { useQuestionaryData } from 'hooks/index';
import { SideQuestionary } from '../../components/SideQuestionary';

export const PaymentScreen: FC = () => {
  const { data } = useQuestionaryData({ statisticType: 'models_earned_money' });

  return (
    <SideQuestionary
      description={data?.description}
      questionIndex={data?.index}
      onYes={data?.navigateToYes!}
      onNo={data?.navigateToNo!}
      statisticType={data?.statisticType!}
    />
  );
};
