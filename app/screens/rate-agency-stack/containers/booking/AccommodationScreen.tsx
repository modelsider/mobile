import React, { FC } from 'react';
import { SideQuestionary } from '../../components/SideQuestionary';
import { useQuestionaryData } from 'hooks/index';

export const AccommodationScreen: FC = () => {
  const { data } = useQuestionaryData({ statisticType: 'accommodation' });

  return (
    <SideQuestionary
      description={data?.description}
      questionIndex={data?.index}
      onYes={data?.navigateToYes!}
      onNo={data?.navigateToNo!}
    />
  );
};
