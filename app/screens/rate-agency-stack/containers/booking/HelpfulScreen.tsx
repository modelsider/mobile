import React, { FC } from 'react';
import { Questionary } from '../../components/Questionary';
import { useQuestionaryData } from 'hooks/index';

export const HelpfulScreen: FC = () => {
  const { data } = useQuestionaryData({ statisticType: 'portfolio_work' });

  return (
    <Questionary
      description={data?.description}
      advantages={data?.advantages!}
      disadvantages={data?.disadvantages!}
      onNext={data?.navigateToNext!}
      questionIndex={data?.index}
      statisticType={data?.statisticType!}
    />
  );
};
