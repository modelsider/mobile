import React, { FC } from 'react';
import { Questionary } from '../../components/Questionary';
import { useQuestionaryData } from 'hooks/index';

export const FairScreen: FC = () => {
  const { data } = useQuestionaryData({ statisticType: 'fair_expenses' });

  return (
    <Questionary
      description={data?.description}
      advantages={data?.advantages!}
      disadvantages={data?.disadvantages!}
      onNext={data?.navigateToNext!}
      questionIndex={data?.index}
      statisticType={data?.statisticType!}
    />
  );
};
