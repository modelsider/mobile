import React, { FC } from 'react';
import { SideQuestionary } from '../../components/SideQuestionary';
import { useQuestionaryData } from 'hooks/index';

export const PortfolioScreen: FC = () => {
  const { data } = useQuestionaryData({ statisticType: 'portfolio_work_side' });

  return (
    <SideQuestionary
      description={data?.description}
      questionIndex={data?.index}
      onYes={data?.navigateToYes!}
      onNo={data?.navigateToNo!}
    />
  );
};
