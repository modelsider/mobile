import React, { FC, useEffect, useState } from 'react';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { RateDatePicker } from '../components/RateDatePicker';
import { IWhoRatedAgency } from 'types/types';
import { useSelector } from 'react-redux';
import { selectAgency, selectAgencyType } from 'store/agency/selectors';
import { DEFAULT_DATE } from 'components/select/select-date/utils';
import { setWorkingTime } from 'store/rate-agency/reducer';
import { useAppDispatch } from 'hooks/index';
import { selectLastRatedAgency } from 'store/rate-agency/selectors';
import { getMappedDate } from '../../../utils/getMappedDate';

export const RateStartWorkingScreen: FC = () => {
  const dispatch = useAppDispatch();
  const type = useSelector(selectAgencyType);
  const selectedAgencyByType = useSelector(selectLastRatedAgency({ type }));
  const selectedAgency = useSelector(selectAgency);

  const prevStartWorkingAt =
    selectedAgencyByType?.start_working_at || selectedAgency?.agencyRatingFromCurrentUser?.start_working_at;

  const [next, setNext] = useState(true);
  const [date, setDate] = useState(getMappedDate(prevStartWorkingAt) || (DEFAULT_DATE as Date));

  const onSelectDate = (value: string) => {
    setDate(value);
  };

  const onNext = () => {
    dispatch(
      setWorkingTime({
        agency: {
          start_working_at: new Date(date?.split('/').reverse().join('-')),
        } as IWhoRatedAgency,
      }),
    );

    return NavigationService.navigate({
      routeName: ROUTES.RateFinishWorking,
    });
  };

  useEffect(() => {
    if (date !== (DEFAULT_DATE as Date)) {
      setNext(false);
    }
  }, [date]);

  return <RateDatePicker date={date} next={next} onSelectDate={onSelectDate} onNext={onNext} index={1} />;
};
