import React, { FC } from 'react';
import { SideQuestionary } from '../../components/SideQuestionary';
import { useQuestionaryData } from 'hooks/index';

export const PayScreen: FC = () => {
  const { data } = useQuestionaryData({ statisticType: 'pay' });

  return (
    <SideQuestionary
      description={data?.description}
      questionIndex={data?.index}
      onYes={data?.navigateToYes!}
      onNo={data?.navigateToNo!}
      statisticType={data?.statisticType!}
    />
  );
};
