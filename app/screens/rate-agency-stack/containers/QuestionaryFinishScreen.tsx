import React, { FC, useCallback, useEffect } from 'react';
import { Container } from 'components/layout/splash-register-login/Container';
import { Content } from 'components/layout/splash-register-login/Content';
import { FONTS } from 'styles/fonts';
import { TextBodyNormal } from 'components/typography/text-body/TextBodyNormal';
import { Footer } from 'components/layout/splash-register-login/Footer';
import GhostEmpty from 'components/ghost-empty/GhostEmpty';
import { DefaultBackgroundElements } from 'components/default-background';
import { useAgencyRating, useAppDispatch } from 'hooks/index';
import { ActivityIndicator, StyleSheet } from 'react-native';
import NavigationService from '../../../navigator/NavigationService';
import { useSelector } from 'react-redux';
import { selectAgency } from '../../../store/agency/selectors';
import { setIsRatingAgencyNow } from '../../../store/agency/actions';
import { useFocusEffect } from '@react-navigation/native';
import { setIfRating } from 'store/app/reducer';

const SIZES = {
  25: 25,
  240: 240,
};
const TIMER = 3000;

export const QuestionaryFinishScreen: FC = () => {
  const dispatch = useAppDispatch();
  const agency = useSelector(selectAgency);

  const { updateCollections } = useAgencyRating();

  useFocusEffect(
    useCallback(() => {
      dispatch(setIfRating({ hasRating: true }));
    }, [dispatch]),
  );

  useEffect(() => {
    updateCollections()
      .then((isUpdated) => {
        if (isUpdated) {
          setTimeout(() => {
            return NavigationService.resetToAgency({ id: agency?.id! });
          }, TIMER);
        } else {
          dispatch(setIsRatingAgencyNow({ isRatingNow: false, docId: agency?.docId! }));
          return NavigationService.navigateBack();
        }
      })
      .catch(() => {
        dispatch(setIsRatingAgencyNow({ isRatingNow: false, docId: agency?.docId! }));
        return NavigationService.navigateBack();
      });
  }, []);

  return (
    <DefaultBackgroundElements type="templateElements1">
      <Container innerStyles={styles.container}>
        <Content>
          <GhostEmpty title="Thank you 🙏" />
          <TextBodyNormal
            text="We received your rating"
            top={SIZES['25']}
            width={SIZES['240']}
            textAlign="center"
            lineHeight={SIZES['25']}
            fontFamily={FONTS.SFUIText.regular}
            bottom={5}
          />
          <ActivityIndicator size="large" />
        </Content>
        <Footer />
      </Container>
    </DefaultBackgroundElements>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
  },
});
