import React, { FC, useCallback, useLayoutEffect, useMemo, useState } from 'react';
import { PrimaryButton } from 'components/buttons';
import { Progress } from 'components/progress';
import { DefaultBackground } from 'components/default-background';
import { HintIcon } from 'components/UI/svg/HintIcon';
import { calculateRateAgencyEmoji } from 'functions/rating-agency/calculateStarRatingColor';
import RateAgencyStar from 'components/rate-agency-star/RateAgencyStar';
import { RateAgencyHint } from 'components/rate-agency-hint/RateAgencyHint';
import { numberOfQuestionBooking, numberOfQuestionMother, ratedStatisticsType } from 'constants/index';
import { useQuestionary, useAppDispatch, useRatingAgencyNow, useAgency, useAgencies } from 'hooks/index';
import { getProgressState } from 'utils/getProgressState';
import { useSelector } from 'react-redux';
import { selectAgency, selectAgencyType } from 'store/agency/selectors';
import { setIsRatingAgencyNow } from 'store/agency/actions';
import { setAgenciesRatingInfoFB } from 'services/agencies';
import { RateAgencyWaiting } from 'components/rate-agency-waiting/RateAgencyWaiting';
import max from 'lodash/max';
import { Flex, Pressable, Text, VStack } from 'native-base';

interface IQuestionary {
  onNext: () => void;
  questionIndex: number;
  advantages: string[];
  disadvantages: string[];
  description: string;
  statisticType: ratedStatisticsType;
  withHint?: boolean;
}

export const Questionary: FC<IQuestionary> = ({
  onNext,
  questionIndex,
  advantages,
  disadvantages,
  description,
  withHint = true,
  statisticType,
}) => {
  const dispatch = useAppDispatch();
  const type = useSelector(selectAgencyType);
  const agency = useSelector(selectAgency);

  const [isRatingNow, setIsRatingNow] = useState<boolean>(false);
  const buttonTitle = useMemo(() => (statisticType === 'happy_to_work_with' ? 'Submit' : 'Next'), [statisticType]);

  const {
    handleShowHint,
    handleToggleStar,
    isNext,
    isVisible,
    setIsVisible,
    rateColor,
    rateItem,
    handleShowLastRatedAgencyAnswer,
  } = useQuestionary();

  const { handleFetchAgency, isFetchAgency } = useAgency();
  const { fetchAgencies, isFetchAgencies } = useAgencies();
  const { checkIfRatingAgencyNow, isRatingNowLoading } = useRatingAgencyNow();

  const onPress = useCallback(() => {
    if (statisticType === 'happy_to_work_with') {
      checkIfRatingAgencyNow().then(async (isRating) => {
        await handleFetchAgency();
        await fetchAgencies();

        if (isRating) {
          setIsRatingNow(isRating);
          await setAgenciesRatingInfoFB();
          return;
        }

        dispatch(setIsRatingAgencyNow({ isRatingNow: true, docId: agency?.docId! }));
        onNext();
      });
    } else {
      onNext();
    }
  }, [agency?.docId, checkIfRatingAgencyNow, dispatch, fetchAgencies, handleFetchAgency, onNext, statisticType]);

  const progressIndicatorLength = useMemo(
    () =>
      type === 'mother' ? max(Object.values(numberOfQuestionMother)) : max(Object.values(numberOfQuestionBooking)),
    [type],
  );

  useLayoutEffect(() => {
    handleShowLastRatedAgencyAnswer({ statisticType });
  }, [handleShowLastRatedAgencyAnswer, statisticType]);

  return (
    <DefaultBackground withPaddingVertical={true}>
      {isRatingNow ? (
        <RateAgencyWaiting setIsRatingNow={setIsRatingNow} />
      ) : (
        <Flex justifyContent="space-between" height="100%" pt="5">
          <Progress
            steps={getProgressState({
              index: questionIndex,
              length: progressIndicatorLength!,
            })}
          />

          <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="xl" textAlign="center">
            {description}
          </Text>

          {/* todo: as separated component */}
          {withHint && (
            <Pressable onPress={handleShowHint} flexDirection="row" alignItems="center" justifyContent="center">
              <Text
                fontFamily="body"
                fontWeight="600"
                fontStyle="normal"
                fontSize="md"
                textAlign="center"
                color="gray.300"
              >
                Hint
              </Text>
              <HintIcon />
            </Pressable>
          )}
          <RateAgencyHint
            isVisible={isVisible}
            setIsVisible={setIsVisible}
            advantages={advantages}
            disadvantages={disadvantages}
          />
          <VStack space="8">
            <Text fontSize="7xl" textAlign="center">
              {calculateRateAgencyEmoji(rateItem)}
            </Text>
            <RateAgencyStar
              onStar={handleToggleStar}
              rateColor={rateColor}
              rateItem={rateItem}
              statisticType={statisticType}
            />
            <PrimaryButton
              onPress={onPress}
              text={buttonTitle}
              disabled={isNext}
              isLoading={isRatingNowLoading || isFetchAgency || isFetchAgencies}
            />
          </VStack>
        </Flex>
      )}
    </DefaultBackground>
  );
};
