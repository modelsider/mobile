import React, { FC, useMemo } from 'react';
import { PrimaryButton } from 'components/buttons';
import { Progress } from 'components/progress';
import { noop } from 'types/types';
import { DefaultBackground } from 'components/default-background';
import { useQuestionary } from 'hooks/index';
import { numberOfQuestionBooking, numberOfQuestionMother, ratedStatisticsType } from 'constants/index';
import { getProgressState } from 'utils/getProgressState';
import { useSelector } from 'react-redux';
import { selectAgencyType } from 'store/agency/selectors';
import { Flex, Text, VStack } from 'native-base';
import max from 'lodash/max';

interface ISideQuestionary {
  onYes: noop;
  onNo: noop;
  questionIndex: number;
  description: string;
  statisticType?: ratedStatisticsType;
}

export const SideQuestionary: FC<ISideQuestionary> = ({ onNo, questionIndex, onYes, description, statisticType }) => {
  const type = useSelector(selectAgencyType);
  const { handleToggleSideQuestion } = useQuestionary();

  const progressIndicatorLength = useMemo(
    () =>
      type === 'mother' ? max(Object.values(numberOfQuestionMother)) : max(Object.values(numberOfQuestionBooking)),
    [type],
  );

  return (
    <DefaultBackground withPaddingVertical={true}>
      <Flex justifyContent="space-between" height="100%" pt="5">
        <Progress
          steps={getProgressState({
            index: questionIndex,
            length: progressIndicatorLength!,
          })}
        />

        <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="xl" textAlign="center">
          {description}
        </Text>

        <VStack flexDirection="column" space="5">
          <PrimaryButton
            onPress={() => {
              if (statisticType === 'models_earned_money') {
                handleToggleSideQuestion({
                  answer: 'yes',
                });
              }
              onYes();
            }}
            text="Yes"
            disabled={false}
          />
          <PrimaryButton
            onPress={() => {
              if (statisticType === 'models_earned_money') {
                handleToggleSideQuestion({
                  answer: 'no',
                });
              }
              onNo();
            }}
            text="No"
            disabled={false}
          />
        </VStack>
      </Flex>
    </DefaultBackground>
  );
};
