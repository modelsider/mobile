import React, { FC } from 'react';
import { PrimaryButton } from 'components/buttons';
import { Progress } from 'components/progress';
import SelectDate from 'components/select/select-date/SelectDate';
import { useAppBackHandler } from 'hooks/index';
import { DefaultBackground } from 'components/default-background';
import { useSelector } from 'react-redux';
import { selectAgency } from 'store/agency/selectors';
import { noop } from 'types/types';
import { Flex, Text, VStack } from 'native-base';
import { getProgressState } from 'utils/getProgressState';

interface IRateDatePicker {
  date?: any;
  next: boolean;
  onSelectDate: (value: string) => void;
  onNext: noop;
  index: number;
  isFinish?: boolean;
  isUpdateDates?: boolean;
  minimumDate?: any;
}

export const RateDatePicker: FC<IRateDatePicker> = ({
  date,
  onSelectDate,
  next,
  onNext,
  index,
  isFinish = false,
  isUpdateDates = false,
  minimumDate,
}) => {
  const agency = useSelector(selectAgency);

  const title = `When did you ${isFinish ? 'finish' : 'start'} working with "${agency?.agency_name}"?`;

  const buttonText = isUpdateDates ? 'Finish' : isFinish ? 'Go to Questionary' : 'Next';

  useAppBackHandler();

  return (
    <DefaultBackground withPaddingVertical={true}>
      <Flex justifyContent="space-between" height="100%" pt="5">
        <Progress steps={getProgressState({ index, length: 2 })} />
        <VStack space="2" mb="32">
          <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="xl" textAlign="center">
            {title}
          </Text>
          <SelectDate
            placeholder="Please select"
            title="Please select"
            mode="date"
            setData={onSelectDate}
            withStillThereButton={isFinish}
            date={date}
            minimumDate={minimumDate}
          />
        </VStack>
        <PrimaryButton onPress={onNext} text={buttonText} disabled={next} />
      </Flex>
    </DefaultBackground>
  );
};
