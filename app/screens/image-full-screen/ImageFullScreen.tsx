import React, { FC } from 'react';
import { getScreenDimensions } from 'utils/layoutUtils';
import { useImageFullScreen } from 'hooks/index';
import ImageZoom from 'react-native-image-pan-zoom';
import CustomImage from 'components/image/CustomImage';
import { RouteProp, useRoute } from '@react-navigation/native';
import { useTheme } from 'native-base';

const PADDING_HORIZONTAL = 10;
const PADDING_VERTICAL = 70;
const MAX_OVERFLOW = 0;
const MIN_SCALE = 0.1;
const MAX_SCALE = 100;
const SCREEN_WIDTH = getScreenDimensions().width;
const SCREEN_HEIGHT = getScreenDimensions().height;

type Route = {
  params: {
    uri: string;
  };
};

export const ImageFullScreen: FC = () => {
  const {
    params: { uri },
  } = useRoute<RouteProp<Route>>();

  const { colors } = useTheme();

  const { handleClose } = useImageFullScreen({ uri });

  return (
    <ImageZoom
      style={{
        backgroundColor: colors.teal['50'],
      }}
      cropWidth={SCREEN_WIDTH}
      cropHeight={SCREEN_HEIGHT - PADDING_VERTICAL}
      imageWidth={SCREEN_WIDTH - PADDING_HORIZONTAL}
      imageHeight={SCREEN_HEIGHT}
      maxOverflow={MAX_OVERFLOW}
      panToMove={true}
      pinchToZoom={true}
      minScale={MIN_SCALE}
      maxScale={MAX_SCALE}
      useNativeDriver={true}
      onMove={handleClose}
    >
      <CustomImage image={uri} type="fullscreen" />
    </ImageZoom>
  );
};
