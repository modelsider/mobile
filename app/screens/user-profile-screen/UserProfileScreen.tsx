import React, { FC, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import { DefaultBackground } from 'components/default-background';
import { useUserProfile, useAppDispatch, useTabView } from 'hooks/index';
import CustomImage from 'components/image/CustomImage';
import { TextBodyNormal } from 'components/typography/text-body/TextBodyNormal';
import CircleUserInstagram from 'components/circles/circle-user-instagram/CircleUserInstagram';
import { CustomTabView } from 'components/custom-tab-view';
import { icons } from '../../../assets';
import { resetUserProfileStore } from 'store/users/reducer';
import { setStatusBarBg } from 'store/app/reducer';

const { anonymous, defaultUser } = icons;

export const PADDING_TOP = 30;
export const PROFILE_IMAGE_SIZE = 68;

const UserProfileScreen: FC = () => {
  const dispatch = useAppDispatch();

  const { userProfile } = useUserProfile();

  const { tabs, tabRoutes } = useTabView({ tabType: 'UserProfile' });

  useEffect(() => {
    dispatch(setStatusBarBg({ bg: '' }));
    return () => {
      dispatch(resetUserProfileStore());
    };
  }, [dispatch]);

  const UserHeader = () => {
    const userProfilePicture = userProfile?.is_anonymous ? anonymous : userProfile?.profilePictureUrl || defaultUser;

    return (
      <View style={styles.headerContainer}>
        <CustomImage
          image={userProfilePicture}
          type="profile"
          resizeMode="cover"
          profileImageSize={PROFILE_IMAGE_SIZE}
        />
        <TextBodyNormal text={userProfile?.name} fontSize={18} lineHeight={25} top={10} bottom={10} />
        <CircleUserInstagram text={userProfile?.userName} />
      </View>
    );
  };

  return (
    <DefaultBackground>
      <UserHeader />
      <CustomTabView tabs={tabs} tabRoutes={tabRoutes} />
    </DefaultBackground>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    paddingTop: PADDING_TOP,
    alignItems: 'center',
  },
});

export default UserProfileScreen;
