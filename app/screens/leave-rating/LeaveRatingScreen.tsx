import React, { FC } from 'react';
import { DefaultBackgroundElements } from 'components/default-background';
import { Center, Text, VStack } from 'native-base';
import { PrimaryButton, SecondaryButton } from 'components/buttons';
import GhostEmpty from 'components/ghost-empty/GhostEmpty';
import { useAgencies } from 'hooks/index';
import NavigationService from 'navigator/NavigationService';
import { useSelector } from 'react-redux';
import { selectHasRating } from 'store/app/selectors';
import isNull from 'lodash/isNull';
import { ActivityIndicator } from 'react-native';

export const LeaveRatingScreen: FC = (): JSX.Element => {
  const hasRating = useSelector(selectHasRating);

  const { handleToggleSearch } = useAgencies();

  return (
    <DefaultBackgroundElements withPaddingVertical withPadding>
      <Center flex={1}>
        {isNull(hasRating) ? (
          <ActivityIndicator size="large" />
        ) : (
          <>
            <VStack space="2">
              <GhostEmpty title="Rate agency" />

              <Text
                fontFamily="body"
                fontWeight="400"
                fontStyle="normal"
                fontSize="md"
                color="black"
                textAlign="center"
              >
                {'Rate a mother or booking\nagency you worked with to\nsee ratings of other agencies.'}
              </Text>
            </VStack>

            <VStack space="5" w="100%" mt="10">
              <PrimaryButton onPress={handleToggleSearch} text="Rate" />
            </VStack>
          </>
        )}
      </Center>
    </DefaultBackgroundElements>
  );
};
