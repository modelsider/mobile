import React, { FC } from 'react';
import { DefaultBackgroundElements } from 'components/default-background';
import { GhostOopsIcon } from 'components/UI/ghost/GhostOopsIcon';
import { Center, Text } from 'native-base';
import { PrimaryButton } from 'components/buttons';
import { RouteProp, useRoute } from '@react-navigation/native';
import { Linking } from 'react-native';

type Route = {
  params: {
    storeUrl: string;
  };
};

export const VersionCheckScreen: FC = (): JSX.Element => {
  const {
    params: { storeUrl },
  } = useRoute<RouteProp<Route>>();

  return (
    <DefaultBackgroundElements withPaddingVertical withPadding>
      <Center flex={1}>
        <GhostOopsIcon />
        <Text fontFamily="mono" fontWeight="400" fontStyle="normal" fontSize="3xl" textAlign="center">
          I need update
        </Text>
        <Text
          fontFamily="body"
          fontWeight="400"
          fontStyle="normal"
          fontSize="md"
          color="black"
          textAlign="center"
          mt="4"
        >
          {'We have made a critical\nupdate to the app.\n\nIt might include bug fixes and\nexperience improvements.'}
        </Text>
      </Center>
      <PrimaryButton onPress={() => Linking.openURL(storeUrl)} text="Update" />
    </DefaultBackgroundElements>
  );
};
