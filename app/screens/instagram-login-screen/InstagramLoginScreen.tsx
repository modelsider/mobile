import React, { FC } from 'react';
import { WebView } from 'react-native-webview';
import { useInstagramVerification } from 'hooks/index';
import { Box, CloseIcon, IconButton } from 'native-base';
import NavigationService from 'navigator/NavigationService';
import { uriAuthInstagram } from 'constants/index';

export const InstagramLoginScreen: FC = () => {
  const { handleNavigationStateChange } = useInstagramVerification();

  return (
    <Box flex={1} bg="teal.50">
      <IconButton
        onPress={() => NavigationService.navigateBack()}
        icon={<CloseIcon />}
        alignSelf="flex-end"
        right={5}
        _icon={{ color: 'black' }}
      />
      <Box flex={1}>
        <WebView
          source={{ uri: uriAuthInstagram }}
          startInLoadingState={true}
          onNavigationStateChange={handleNavigationStateChange}
        />
      </Box>
    </Box>
  );
};
