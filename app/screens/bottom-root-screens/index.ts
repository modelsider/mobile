export { default as BookmarksScreen } from '../bottom-root-screens/bookmarks-screen/containers/BookmarksScreen';
export { default as MyAgenciesScreen } from '../bottom-root-screens/my-agencies-screen/containers/MyAgenciesScreen';
export { default as NewsScreen } from '../bottom-root-screens/news-screen/containers/NewsScreen';
export { default as HomeScreen } from '../bottom-root-screens/home-screen/containers/HomeScreen';
