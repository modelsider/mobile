import React, { FC, useEffect } from 'react';
import { DefaultBackground } from 'components/default-background';
import { CustomTabView } from 'components/custom-tab-view';
import { useAppBackHandler, useAppDispatch, useTabView } from 'hooks/index';
import { useIsFocused } from '@react-navigation/native';
import { setNewsCounter } from 'store/news/actions';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { setIsRatingAgencyNow } from 'store/agency/actions';
import { selectAgency, selectAgencyType } from 'store/agency/selectors';
import isNull from 'lodash/isNull';

const MyAgenciesScreen: FC = () => {
  const isFocused = useIsFocused();
  const dispatch = useAppDispatch();
  const user = useSelector(selectUserInfo);
  const agency = useSelector(selectAgency);
  const type = useSelector(selectAgencyType);

  const { tabs, tabRoutes } = useTabView({ tabType: 'MyAgencies' });
  const idx = type === 'booking' ? 1 : 0;

  useEffect(() => {
    if (isFocused && !isNull(agency)) {
      dispatch(setIsRatingAgencyNow({ isRatingNow: false, docId: agency?.docId! }));
    }
  }, [agency, agency?.docId, dispatch, isFocused]);

  useEffect(() => {
    if (isFocused) {
      dispatch(setNewsCounter({ userId: user?.id }));
    }
  }, [dispatch, isFocused, user?.id]);

  useEffect(() => {
    // addNewAgencyToDB()
    // addNewCountriesToDB()
  }, []);

  useAppBackHandler();

  return (
    <DefaultBackground>
      <CustomTabView tabs={tabs} tabRoutes={tabRoutes} idx={idx} />
    </DefaultBackground>
  );
};

export default MyAgenciesScreen;
