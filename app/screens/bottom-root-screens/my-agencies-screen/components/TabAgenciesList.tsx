import React, { FC } from 'react';
import AgencyItem from 'components/agency-item/AgencyItem';
import GhostEmpty from 'components/ghost-empty/GhostEmpty';
import { RefreshControl } from 'react-native';
import { useAppScrollToTop } from 'hooks/index';
import { AgenciesSkeleton } from 'components/skeleton';
import { PrimaryButton } from 'components/buttons';
import { IAgency, noop } from 'types/types';
import BottomActivityIndicator from 'components/bottom-activity-indicator/BottomActivityIndicator';
import { Box, Text, useTheme, VStack } from 'native-base';
import { useSelector } from 'react-redux';
import { selectUserProfile } from 'store/users/selectors';
import { selectUserInfo } from 'store/auth/selectors';
import { FlashList } from '@shopify/flash-list';

interface ITabAgenciesList {
  agencies: IAgency[];
  isFetchAgencies: boolean;
  onRefresh?: () => void;
  type: 'mother' | 'booking';
  onShowActionSheet: ({ agencyId, innerOptions }: { agencyId: number; innerOptions?: any[] }) => void;
  onToggleSearch: noop;
  onNavigateToAgency: ({ agencyId }: { agencyId: number }) => void;
  isUserProfile?: boolean;
  fetchUploadedAgencies?: noop;
  isMomentumScrollEnd: boolean;
  handleMomentumScrollBegin: noop;
  handleMomentumScrollEnd: noop;
}

const TabAgenciesList: FC<ITabAgenciesList> = ({
  agencies,
  isFetchAgencies,
  onRefresh,
  type,
  onShowActionSheet,
  onToggleSearch,
  onNavigateToAgency,
  isUserProfile,
  fetchUploadedAgencies,
  isMomentumScrollEnd,
  handleMomentumScrollEnd,
}) => {
  const { space } = useTheme();

  const { ref } = useAppScrollToTop();

  const currentUser = useSelector(selectUserInfo);
  const userProfile = useSelector(selectUserProfile);

  const renderItem = ({ item }: { item: IAgency }) => {
    const { id: agencyId, isClosed } = item;

    return (
      <Box py={space['1']}>
        <AgencyItem
          agency={item}
          type={type}
          isRated={true}
          onPress={() => onNavigateToAgency({ agencyId })}
          onLongPress={() => {
            if (isClosed || (isUserProfile && currentUser.id !== userProfile.id)) {
              return;
            }
            onShowActionSheet({ agencyId });
          }}
        />
      </Box>
    );
  };

  const listEmptyComponent = () =>
    isFetchAgencies ? (
      <AgenciesSkeleton />
    ) : isUserProfile ? (
      <Text
        fontFamily="body"
        fontWeight="400"
        fontStyle="normal"
        fontSize="3xl"
        color="gray.300"
        top={space['5']}
        textAlign="center"
      >
        Nothing to show
      </Text>
    ) : (
      <VStack mt={space['6']} space={space['2']}>
        <GhostEmpty title="Rate agency" />

        <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black" textAlign="center">
          {`Rate a ${type} agency you\n worked with to see ratings of\n other agencies.`}
        </Text>

        <PrimaryButton onPress={onToggleSearch} text="Rate" />
      </VStack>
    );

  const ListFooterComponent = () => (isMomentumScrollEnd && isFetchAgencies ? <BottomActivityIndicator /> : null);

  return (
    <FlashList
      estimatedItemSize={200}
      ref={ref}
      data={agencies}
      renderItem={renderItem}
      ListEmptyComponent={listEmptyComponent}
      showsVerticalScrollIndicator={false}
      refreshControl={<RefreshControl refreshing={false} onRefresh={onRefresh} />}
      ListFooterComponent={ListFooterComponent}
      // onEndReached={fetchUploadedAgencies}
      onMomentumScrollBegin={fetchUploadedAgencies}
      onEndReachedThreshold={0.5}
      // onMomentumScrollBegin={handleMomentumScrollBegin}
      onMomentumScrollEnd={handleMomentumScrollEnd}
    />
  );
};

export default TabAgenciesList;
