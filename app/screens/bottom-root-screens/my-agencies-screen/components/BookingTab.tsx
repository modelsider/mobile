import React, { FC } from 'react';
import AgenciesTab from './AgenciesTab';

interface IBookingTab {
  isUserProfile?: boolean;
}

const BookingTab: FC<IBookingTab> = ({ isUserProfile = false }) => {
  return <AgenciesTab isUserProfile={isUserProfile} />;
};

export default BookingTab;
