import React, { FC, useEffect } from 'react';
import TabAgenciesList from './TabAgenciesList';
import { useAgencies, useAppDispatch, useBottomSheet } from 'hooks/index';
import { resetAgencyState } from 'store/agency/reducer';
import { resetCommentsState } from 'store/comments/reducer';
import { resetRatedAgency } from 'store/rate-agency/reducer';

interface IAgenciesTab {
  isUserProfile?: boolean;
}

const AgenciesTab: FC<IAgenciesTab> = ({ isUserProfile = false }) => {
  const dispatch = useAppDispatch();

  const {
    agencies,
    type,
    isFetchAgencies,
    fetchAgencies,
    handleNavigateToAgency,
    handleToggleSearch,
    fetchUploadedAgencies,
    handleMomentumScrollBegin,
    isMomentumScrollEnd,
    handleMomentumScrollEnd,
  } = useAgencies();

  const { handleAgencySheet } = useBottomSheet();

  useEffect(() => {
    if (!isUserProfile) {
      dispatch(resetRatedAgency());
      dispatch(resetAgencyState());
      dispatch(resetCommentsState());
    }

    fetchAgencies();
    dispatch(resetAgencyState());
  }, [dispatch, fetchAgencies, isUserProfile]);

  return (
    <TabAgenciesList
      agencies={agencies.length ? agencies : []}
      isFetchAgencies={isFetchAgencies}
      onRefresh={fetchAgencies}
      type={type}
      onShowActionSheet={handleAgencySheet}
      onToggleSearch={handleToggleSearch}
      onNavigateToAgency={handleNavigateToAgency}
      isUserProfile={isUserProfile}
      fetchUploadedAgencies={fetchUploadedAgencies}
      isMomentumScrollEnd={isMomentumScrollEnd}
      handleMomentumScrollBegin={handleMomentumScrollBegin}
      handleMomentumScrollEnd={handleMomentumScrollEnd}
    />
  );
};

export default AgenciesTab;
