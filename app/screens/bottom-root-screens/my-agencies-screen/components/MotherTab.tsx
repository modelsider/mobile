import React, { FC } from 'react';
import AgenciesTab from './AgenciesTab';

interface IMotherTab {
  isUserProfile?: boolean;
}

const MotherTab: FC<IMotherTab> = ({ isUserProfile = false }) => {
  return <AgenciesTab isUserProfile={isUserProfile} />;
};

export default MotherTab;
