import React, { FC, useEffect } from 'react';
import { CustomTabView } from 'components/custom-tab-view';
import { DefaultBackground } from 'components/default-background';
import { useAppBackHandler, useAppDispatch, useTabView } from 'hooks/index';
import { useIsFocused } from '@react-navigation/native';
import { setNewsCounter } from 'store/news/actions';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';

const BookmarksScreen: FC = () => {
  const isFocused = useIsFocused();
  const dispatch = useAppDispatch();
  const user = useSelector(selectUserInfo);

  const { tabs, tabRoutes } = useTabView({ tabType: 'Bookmarks' });

  useEffect(() => {
    if (isFocused) {
      dispatch(setNewsCounter({ userId: user?.id }));
    }
  }, [dispatch, isFocused, user?.id]);

  useAppBackHandler();

  return (
    <DefaultBackground>
      <CustomTabView tabs={tabs} tabRoutes={tabRoutes} />
    </DefaultBackground>
  );
};

export default BookmarksScreen;
