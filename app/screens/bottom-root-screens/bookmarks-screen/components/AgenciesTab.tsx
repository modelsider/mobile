import React, { FC, useEffect } from 'react';
import { RefreshControl } from 'react-native';
import { getScreenDimensions } from 'utils/layoutUtils';
import GhostEmpty from 'components/ghost-empty/GhostEmpty';
import { useAppScrollToTop, useBookmarks } from 'hooks/index';
import BookmarkAgencyItem from 'components/bookmark-agency-item/BookmarkAgencyItem';
import { AgenciesBookmarkSkeleton } from 'components/skeleton';
import BottomActivityIndicator from 'components/bottom-activity-indicator/BottomActivityIndicator';
import { Center } from 'native-base';
import { FlashList } from '@shopify/flash-list';

const AgenciesTab: FC = () => {
  const { ref } = useAppScrollToTop();

  const {
    loading,
    fetchBookmarkedAgencies,
    bookmarkedAgencies,
    fetchUploadedBookmarkedAgencies,
    isMomentumScrollEnd,
    handleMomentumScrollEnd,
    handleMomentumScrollBegin,
  } = useBookmarks();

  const data = bookmarkedAgencies.length ? bookmarkedAgencies : [];

  useEffect(() => {
    fetchBookmarkedAgencies();
  }, [fetchBookmarkedAgencies]);

  const renderItem = ({ item }: any) => {
    return (
      <BookmarkAgencyItem
        title={item.agency_name}
        location={`${item.city}, ${item.country}`}
        agencyId={item.id}
        isBookmark={item.isBookmark}
      />
    );
  };

  const listEmptyComponent = () =>
    loading ? (
      <AgenciesBookmarkSkeleton />
    ) : (
      //  todo: fix
      <Center height={getScreenDimensions().height / 1.3}>
        <GhostEmpty title="No agencies bookmarked" />
      </Center>
    );

  const ListFooterComponent = () => (isMomentumScrollEnd && loading ? <BottomActivityIndicator /> : null);

  return (
    <FlashList
      estimatedItemSize={200}
      ref={ref}
      showsVerticalScrollIndicator={false}
      data={data}
      renderItem={renderItem}
      ListEmptyComponent={listEmptyComponent}
      ListFooterComponent={ListFooterComponent}
      onEndReached={fetchUploadedBookmarkedAgencies}
      onEndReachedThreshold={0.5}
      onMomentumScrollEnd={handleMomentumScrollEnd}
      onMomentumScrollBegin={handleMomentumScrollBegin}
      refreshControl={<RefreshControl refreshing={false} onRefresh={fetchBookmarkedAgencies} />}
    />
  );
};

export default AgenciesTab;
