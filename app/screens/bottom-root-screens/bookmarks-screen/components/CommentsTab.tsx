import React, { FC, useEffect } from 'react';
import { RefreshControl } from 'react-native';
import { getScreenDimensions } from 'utils/layoutUtils';
import GhostEmpty from 'components/ghost-empty/GhostEmpty';
import { Comment } from 'components/comments';
import { useAppScrollToTop, useBookmarks } from 'hooks/index';
import { CommentItemSkeleton } from 'components/skeleton';
import BottomActivityIndicator from 'components/bottom-activity-indicator/BottomActivityIndicator';
import { Box, Center, useTheme } from 'native-base';
import { FlashList } from '@shopify/flash-list';

interface ICommentsTab {}

const CommentsTab: FC<ICommentsTab> = () => {
  const { space } = useTheme();

  const { ref } = useAppScrollToTop();

  const {
    loading,
    fetchBookmarkedComments,
    bookmarkedComments,
    fetchUploadedBookmarkedComments,
    isMomentumScrollEnd,
    handleMomentumScrollBegin,
    handleMomentumScrollEnd,
  } = useBookmarks();

  const data = bookmarkedComments.length ? bookmarkedComments : [];

  useEffect(() => {
    fetchBookmarkedComments();
  }, [fetchBookmarkedComments]);

  // todo: create render item duplicate
  const renderItem = ({ item }: any) => {
    return (
      <Box mt={space['0.5']} pb={space['1']}>
        <Comment comment={item} isNavigateToAgency={false} isPost={item?.type === 'Post'} />
      </Box>
    );
  };

  const listEmptyComponent = () =>
    loading ? (
      <CommentItemSkeleton />
    ) : (
      //  todo: duplicate
      <Center height={getScreenDimensions().height / 1.3}>
        <GhostEmpty title="No comments bookmarked" />
      </Center>
    );

  const ListFooterComponent = () => (isMomentumScrollEnd && loading ? <BottomActivityIndicator /> : null);

  return (
    <FlashList
      ref={ref}
      estimatedItemSize={200}
      showsVerticalScrollIndicator={false}
      data={data}
      renderItem={renderItem}
      ListEmptyComponent={listEmptyComponent}
      ListFooterComponent={ListFooterComponent}
      onEndReached={fetchUploadedBookmarkedComments}
      onEndReachedThreshold={0.5}
      onMomentumScrollEnd={handleMomentumScrollEnd}
      onMomentumScrollBegin={handleMomentumScrollBegin}
      refreshControl={<RefreshControl refreshing={false} onRefresh={fetchBookmarkedComments} />}
    />
  );
};

export default CommentsTab;
