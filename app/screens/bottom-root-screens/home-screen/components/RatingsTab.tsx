import React, { FC, useCallback } from 'react';
import { useAppDispatch } from 'hooks/index';
import { useSelector } from 'react-redux';
import { selectRatings } from 'store/feed/selectors';
import { setRatings } from 'store/feed/actions';
import FeedList from './FeedList';

const RatingsTab: FC = () => {
  const dispatch = useAppDispatch();

  const ratings = useSelector(selectRatings);

  const handleRefresh = useCallback(() => {
    dispatch(setRatings({ agencies: ratings }));
  }, [dispatch, ratings]);

  return <FeedList data={ratings} onRefresh={handleRefresh} />;
};

export default RatingsTab;
