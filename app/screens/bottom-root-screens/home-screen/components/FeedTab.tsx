import React, { FC, useCallback } from 'react';
import { useAppDispatch } from 'hooks/index';
import { setFeed } from 'store/feed/actions';
import { useSelector } from 'react-redux';
import { selectFeed } from 'store/feed/selectors';
import FeedList from './FeedList';
import { Box, useTheme } from 'native-base';
import { IconButton } from 'components/buttons/icon-button/IconButton';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { IPost } from 'types/types';
import { icons } from '../../../../../assets';

const FeedTab: FC = () => {
  const { space } = useTheme();

  const dispatch = useAppDispatch();

  const feed = useSelector(selectFeed);

  const handleRefresh = useCallback(() => {
    dispatch(setFeed({ feed }));
  }, [dispatch, feed]);

  return (
    <>
      <FeedList data={feed} onRefresh={handleRefresh} />

      <Box position="absolute" bottom={space['1']} right={0}>
        <IconButton
          onPress={() =>
            NavigationService.navigateWithParams({
              routeName: ROUTES.LeavePost,
              params: { isEdit: false, post: {} as IPost },
            })
          }
          icon={icons.plus}
        />
      </Box>
    </>
  );
};

export default FeedTab;
