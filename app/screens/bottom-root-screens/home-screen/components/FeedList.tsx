import { RefreshControl } from 'react-native';
import React, { FC } from 'react';
import { useAgencies, useAppDispatch, useAppScrollToTop } from 'hooks/index';
import { Box, HStack, useTheme, VStack } from 'native-base';
import AgencyItem from 'components/agency-item/AgencyItem';
import { Comment } from 'components/comments';
import { IAgency, IComment, IPost, IWhoRatedAgency, noop } from 'types/types';
import { FeedItemLabel } from 'components/feed/FeedItemLabel';
import { FlashList } from '@shopify/flash-list';
import isUndefined from 'lodash/isUndefined';
import { AgenciesSkeleton, CommentItemSkeleton } from 'components/skeleton';
import { LabelDate } from 'components/label/LabelDate';
import { formatDate } from 'utils/getMonth';
import { setRatings } from 'store/feed/actions';
import { resetFeed, resetRatings } from 'store/feed/reducer';

interface IProps {
  data: (IAgency | IComment | IPost)[];
  onRefresh?: noop;
}

const FeedList: FC<IProps> = ({ data, onRefresh }) => {
  const dispatch = useAppDispatch();

  const { ref } = useAppScrollToTop();
  const { space } = useTheme();

  const { handleNavigateToAgency } = useAgencies();

  const renderItem = ({ item, index }: { item: IComment | IWhoRatedAgency; index: number }) => {
    if ((item?.type === 'booking' || item?.type === 'mother') && isUndefined(item?.agency_name)) {
      return null;
    }

    if (item?.type === 'Comment' || item?.type === 'Post') {
      return (
        <Box mt={space['1']} pb={space['1']}>
          <Comment
            comment={item as IComment}
            commentIndex={index}
            onRefresh={onRefresh}
            isFeed={item?.type === 'Comment'}
            isPost={item?.type === 'Post'}
          />
        </Box>
      );
    }

    return (
      <Box py={space['1.5']}>
        <HStack justifyContent="space-between" pb="1">
          <FeedItemLabel text={`${item?.type} rating update`} />
          <LabelDate date={formatDate(item?.created_at, true)} />
        </HStack>
        <AgencyItem
          // @ts-ignore
          agency={item}
          type={item?.type}
          isRated={true}
          onPress={() => handleNavigateToAgency({ agencyId: item?.agency_id })}
          withDate={false}
        />
      </Box>
    );
  };

  const listEmptyComponent = () => {
    return data?.length ? null : (
      <>
        <AgenciesSkeleton items={1} withDate={false} />
        <CommentItemSkeleton items={1} />
        <AgenciesSkeleton items={1} withDate={false} />
        <CommentItemSkeleton items={1} />
        <AgenciesSkeleton items={1} withDate={false} />
        <CommentItemSkeleton items={1} />
        <AgenciesSkeleton items={1} withDate={false} />
        <CommentItemSkeleton items={1} />
      </>
    );
  };

  return (
    <FlashList
      estimatedItemSize={300}
      ref={ref}
      data={data}
      renderItem={renderItem}
      ListEmptyComponent={listEmptyComponent}
      showsVerticalScrollIndicator={false}
      refreshControl={
        <RefreshControl
          refreshing={false}
          onRefresh={() => {
            dispatch(resetFeed());
            dispatch(resetRatings());
            dispatch(setRatings({ agencies: [] }));
          }}
        />
      }
      onEndReached={onRefresh}
      onEndReachedThreshold={0.5}
      ListFooterComponent={
        <VStack marginBottom={30}>
          <AgenciesSkeleton items={1} />
        </VStack>
      }
    />
  );
};

export default FeedList;
