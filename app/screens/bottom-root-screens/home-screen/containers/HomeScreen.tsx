import React, { FC } from 'react';
import { DefaultBackground } from 'components/default-background';
import { useAppBackHandler, useTabView } from 'hooks/index';
import { useSelector } from 'react-redux';
import { Center } from 'native-base';
import isNull from 'lodash/isNull';
import { ActivityIndicator } from 'react-native';
import { selectHasRating } from 'store/app/selectors';
import { CustomTabView } from 'components/custom-tab-view';

const HomeScreen: FC = () => {
  const hasRating = useSelector(selectHasRating);

  const { tabs, tabRoutes } = useTabView({ tabType: 'Home' });

  useAppBackHandler();

  if (isNull(hasRating)) {
    return (
      <DefaultBackground>
        <Center flex="1">
          <ActivityIndicator size="large" />
        </Center>
      </DefaultBackground>
    );
  }

  return (
    <DefaultBackground>
      <CustomTabView tabs={tabs} tabRoutes={tabRoutes} />
    </DefaultBackground>
  );
};

export default HomeScreen;
