import React, { FC } from 'react';
import { FlatList, RefreshControl } from 'react-native';
import NewsItem, { INewsItem } from 'components/news/NewsItem';
import GhostEmpty from 'components/ghost-empty/GhostEmpty';
import { getScreenDimensions } from 'utils/layoutUtils';
import { useAppScrollToTop } from 'hooks/index';
import { INews, noop } from 'types/types';
import { NewsItemSkeleton } from 'components/skeleton';
import { Box, Center, useTheme } from 'native-base';

interface INewsList {
  news: INews[];
  isFetch: boolean;
  handleFetchNews: noop;
}

const NewsList: FC<INewsList> = ({ news, isFetch, handleFetchNews }) => {
  const { ref } = useAppScrollToTop();
  const { space } = useTheme();

  const renderItem = ({ item }: INewsItem) => {
    return (
      <Box mb={space['1']}>
        <NewsItem item={item} />
      </Box>
    );
  };

  // todo: create common component
  const listEmptyComponent = () =>
    isFetch ? (
      <NewsItemSkeleton />
    ) : (
      //  todo: duplicate
      <Center height={getScreenDimensions().height / 1.3}>
        <GhostEmpty title="No news just yet" />
      </Center>
    );

  return (
    <Box flex={1}>
      <FlatList
        ref={ref}
        data={news}
        renderItem={renderItem}
        ListEmptyComponent={listEmptyComponent}
        refreshControl={<RefreshControl refreshing={false} onRefresh={handleFetchNews} />}
      />
    </Box>
  );
};

export default NewsList;
