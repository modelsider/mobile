import React, { FC, useEffect } from 'react';
import { DefaultBackground } from 'components/default-background';
import NewsList from 'screens/bottom-root-screens/news-screen/components/NewsList';
import { useAppBackHandler, useNews, useAppDispatch } from 'hooks/index';
import { useIsFocused } from '@react-navigation/native';
import { removeNewsCounter } from 'store/news/actions';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';

const TIMER = 1000;

const NewsScreen: FC = () => {
  useAppBackHandler();

  const { news, handleFetchNews, loading } = useNews();

  const data = news.length ? news : [];
  const isFocused = useIsFocused();
  const dispatch = useAppDispatch();
  const user = useSelector(selectUserInfo);

  useEffect(() => {
    if (isFocused) {
      handleFetchNews();
      setTimeout(async () => {
        await dispatch(removeNewsCounter({ userId: user?.id }));
      }, TIMER);
    }
  }, [dispatch, handleFetchNews, isFocused, user?.id]);

  return (
    <DefaultBackground withPadding={false}>
      <NewsList news={data} isFetch={loading} handleFetchNews={handleFetchNews} />
    </DefaultBackground>
  );
};

export default NewsScreen;
