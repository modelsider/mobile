import React, { FC, useLayoutEffect } from 'react';
import { DefaultBackground } from 'components/default-background';
import ReactionsList from 'screens/bottom-root-screens/reactions-screen/components/ReactionsList';
import { useAppBackHandler } from 'hooks/index';
import { useNavigation } from '@react-navigation/native';

interface IReactionsScreen {}

const fakeData = [
  {
    icon: require('../../../../../assets/comment-icon.png'),
    name: 'Hanna Smirnova',
    description:
      'Nice apartments in the town center, short walk to all amenities. Every model has its own room. Clean bathroom and kitchen.',
    date: '15 Aug 20',
    subComments: [
      {
        id: 1,
        title: 'Comment 1',
      },
      {
        id: 2,
        title: 'Comment 2',
      },
    ],
    likes: 2,
    isBookmark: false,
    commentReactions: 2,
    likesReactions: 2,
  },
  {
    icon: require('../../../../../assets/comment-icon.png'),
    name: 'Gabe Johnson',
    description: 'Nice apartments',
    date: '15 Aug 20',
    subComments: [
      {
        id: 1,
        title: 'Comment 1',
      },
      {
        id: 2,
        title: 'Comment 2',
      },
    ],
    likes: 2,
    isBookmark: false,
    commentReactions: 2,
    likesReactions: 1,
  },
  {
    icon: null,
    name: '',
    description:
      'Nice apartments in the town center, short walk to all amenities. Every model has its own room. Clean bathroom and kitchen.',
    date: '15 Aug 20',
    subComments: [
      {
        id: 1,
        title: 'Comment 1',
      },
      {
        id: 2,
        title: 'Comment 2',
      },
    ],
    likes: 2,
    isBookmark: false,
    commentReactions: 0,
    likesReactions: 0,
  },
  {
    icon: null,
    name: '',
    description:
      'Nice apartments in the town center, short walk to all amenities. Every model has its own room. Clean bathroom and kitchen.',
    date: '15 Aug 20',
    subComments: [
      {
        id: 1,
        title: 'Comment 1',
      },
      {
        id: 2,
        title: 'Comment 2',
      },
    ],
    likes: 2,
    isBookmark: false,
    commentReactions: 0,
    likesReactions: 0,
  },
  {
    icon: null,
    name: '',
    description:
      'Nice apartments in the town center, short walk to all amenities. Every model has its own room. Clean bathroom and kitchen.',
    date: '15 Aug 20',
    subComments: [
      {
        id: 1,
        title: 'Comment 1',
      },
      {
        id: 2,
        title: 'Comment 2',
      },
    ],
    likes: 2,
    isBookmark: false,
    commentReactions: 0,
    likesReactions: 0,
  },
  {
    icon: null,
    name: '',
    description:
      'Nice apartments in the town center, short walk to all amenities. Every model has its own room. Clean bathroom and kitchen.',
    date: '15 Aug 20',
    subComments: [
      {
        id: 1,
        title: 'Comment 1',
      },
      {
        id: 2,
        title: 'Comment 2',
      },
    ],
    likes: 2,
    isBookmark: false,
    commentReactions: 0,
    likesReactions: 0,
  },
];

const ReactionsScreen: FC<IReactionsScreen> = () => {
  useAppBackHandler();

  const navigation = useNavigation();

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Reactions',
    });
  }, [navigation]);

  return (
    <DefaultBackground>
      <ReactionsList fakeData={fakeData} />
    </DefaultBackground>
  );
};

export default ReactionsScreen;
