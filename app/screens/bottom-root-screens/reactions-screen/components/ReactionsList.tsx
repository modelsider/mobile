import { FlatList, StyleSheet, View } from 'react-native';
import React, { FC } from 'react';
import { convertPtToResponsive } from 'utils/layoutUtils';
import GhostEmpty from 'components/ghost-empty/GhostEmpty';
import Comment from 'components/comments/comment/Comment';
import { ADDITIONAL_COLOURS } from 'styles/colours';
import { useAppScrollToTop } from 'hooks/index';

interface IReactionsList {
  fakeData: any[];
}

const ReactionsList: FC<IReactionsList> = ({ fakeData }) => {
  const { ref } = useAppScrollToTop();

  const renderItem = ({ item }: any) => (
    <View style={styles.commentContainer}>
      <Comment
        name={item?.name}
        icon={item?.icon}
        description={item?.description}
        date={item?.date}
        subComments={item?.subComments}
        likes={item?.likes}
        isBookmark={item?.isBookmark}
        commentReactions={item?.commentReactions}
        likesReactions={item?.likesReactions}
      />
    </View>
  );

  const listEmptyComponent = () => (
    <View style={styles.listEmptyComponent}>
      <GhostEmpty title="Nothing here yet" />
    </View>
  );

  return (
    <FlatList
      ref={ref}
      showsVerticalScrollIndicator={false}
      style={styles.container}
      data={fakeData}
      renderItem={renderItem}
      ListEmptyComponent={listEmptyComponent}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'center',
  },
  commentContainer: {
    borderBottomWidth: 0.5,
    borderBottomColor: ADDITIONAL_COLOURS['primary-grey'],
    marginTop: convertPtToResponsive(20),
    paddingBottom: convertPtToResponsive(20),
  },
  listEmptyComponent: {
    top: convertPtToResponsive(180),
  },
});

export default ReactionsList;
