import React, { FC, useCallback, useLayoutEffect } from 'react';
import { Input } from 'components/inputs';
import { PrimaryButton } from 'components/buttons';
import { useSearchAgency, useAppBackHandler, useAppDispatch } from 'hooks/index';
import { DefaultBackgroundElements } from 'components/default-background';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { CloseIcon, Flex, Pressable, Text, VStack } from 'native-base';
import { EMOJIS } from 'components/emoji-icon/emoji';
import { useKeyboard } from '@react-native-community/hooks';
import SearchIndicator from '../components/SearchIndicator';
import SearchData from 'components/search-data/SearchData';
import { resetAgencyState } from 'store/agency/reducer';
import { AgencyListNames } from '../../agency-list/components/AgencyListNames';
import { setAllAgencies } from 'store/agency/actions';
import { useSelector } from 'react-redux';
import { selectHasRating } from 'store/app/selectors';
import NavigationService from 'navigator/NavigationService';
import { ArrowBackIcon } from 'components/UI/svg/ArrowBackIcon';

const SearchAgencyScreen: FC = () => {
  const navigation = useNavigation();
  const dispatch = useAppDispatch();
  const { keyboardShown } = useKeyboard();

  const hasRating = useSelector(selectHasRating);

  const {
    searchAgency,
    suggestedAgencies,
    handleNavigateToAgency,
    handleChangeAgency,
    handleSuggestAgency,
    handleClear,
    searchAgenciesStatus,
  } = useSearchAgency();

  useFocusEffect(
    useCallback(() => {
      dispatch(resetAgencyState());
      dispatch(setAllAgencies());
    }, [dispatch]),
  );

  useLayoutEffect(() => {
    navigation.setOptions({
      headerBackImage: () => {
        return (
          hasRating && (
            <Pressable onPress={() => NavigationService.navigateBack()}>
              <ArrowBackIcon />
            </Pressable>
          )
        );
      },
      headerRight: () => {
        return (
          <Pressable onPress={() => NavigationService.resetToHome()}>
            <CloseIcon color="black" />
          </Pressable>
        );
      },
    });
  }, [hasRating, navigation]);

  useAppBackHandler();

  return (
    <DefaultBackgroundElements type="withGhostRight" withPadding={true} withPaddingVertical={true}>
      <Flex justifyContent="space-between" height="100%">
        <VStack space="md">
          <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="xl" textAlign="center">
            {`Find an agency ${EMOJIS.find}`}
          </Text>
          <Input
            placeholder="Name, city or country"
            onChangeText={handleChangeAgency}
            value={searchAgency}
            onClear={handleClear}
            autoCapitalize="words"
          />
          <SearchIndicator searchAgenciesStatus={searchAgenciesStatus} onSuggestAgency={handleSuggestAgency} />
          <SearchData data={suggestedAgencies} onPress={handleNavigateToAgency} />
        </VStack>

        {!suggestedAgencies.length && <AgencyListNames />}

        {!keyboardShown && (
          <VStack space="md">
            <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="md" textAlign="center">
              Cannot find an agency?
            </Text>
            <PrimaryButton onPress={handleSuggestAgency} text="Suggest an agency" />
          </VStack>
        )}
      </Flex>
    </DefaultBackgroundElements>
  );
};

export default SearchAgencyScreen;
