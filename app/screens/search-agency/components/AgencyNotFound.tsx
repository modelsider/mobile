import React, { FC } from 'react';
import { HStack, Text, useTheme } from 'native-base';
import { NotFoundIcon } from 'components/UI/svg/NotFoundIcon';
import { noop } from 'types/types';

interface IProps {
  onSuggestAgency: noop;
}

const AgencyNotFound: FC<IProps> = ({ onSuggestAgency }) => {
  const { space } = useTheme();

  return (
    <HStack
      space="5"
      flexDirection="row"
      alignItems="center"
      borderWidth={2}
      borderColor="gray.300"
      borderRadius={space['2.5']}
      pl="6"
      p="3"
      bg="white"
    >
      <NotFoundIcon />
      <Text fontFamily="body" fontWeight="300" fontStyle="normal" fontSize="md" color="black">
        {'Nothing found.\nPlease '}
        <Text
          fontFamily="body"
          fontWeight="300"
          fontStyle="normal"
          fontSize="md"
          color="black"
          underline={true}
          onPress={onSuggestAgency}
        >
          suggest an agency
        </Text>
      </Text>
    </HStack>
  );
};

export default AgencyNotFound;
