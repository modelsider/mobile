import React, { FC } from 'react';
import { noop, searchAgenciesStatusType } from 'types/types';
import { ActivityIndicator } from 'react-native';
import AgencyNotFound from './AgencyNotFound';

interface IProps {
  searchAgenciesStatus: searchAgenciesStatusType;
  onSuggestAgency: noop;
}

const SearchIndicator: FC<IProps> = ({ searchAgenciesStatus, onSuggestAgency }) => {
  return searchAgenciesStatus === 'default' ? (
    <></>
  ) : searchAgenciesStatus === 'notFound' ? (
    <AgencyNotFound onSuggestAgency={onSuggestAgency} />
  ) : searchAgenciesStatus === 'searching' ? (
    <ActivityIndicator />
  ) : (
    <></>
  );
};

export default SearchIndicator;
