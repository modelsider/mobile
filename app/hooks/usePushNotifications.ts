import { useCallback, useEffect } from 'react';
import messaging from '@react-native-firebase/messaging';
import { getFCMToken, setFCMToken } from 'services/asyncStorage';
import { saveDeviceToken } from 'services/notifications';
import { useAppDispatch } from 'hooks/index';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { Linking } from 'react-native';
import isUndefined from 'lodash/isUndefined';

type Response = {
  requestUserPermission: ({ userId }: { userId: string }) => void;
};

const usePushNotifications = (): Response => {
  const dispatch = useAppDispatch();
  const user = useSelector(selectUserInfo);

  const saveFCMToken = useCallback(async ({ userId }: { userId: string }) => {
    let fcmToken = await getFCMToken();

    if (!fcmToken) {
      try {
        let fcmToken = await messaging().getToken();
        if (fcmToken) {
          await saveDeviceToken({
            userId,
            deviceToken: fcmToken,
          });
          await setFCMToken(fcmToken);
        }
      } catch (e) {
        console.log('Error in FCM token: ', e);
      }
    }
  }, []);

  const requestUserPermission = useCallback(
    async ({ userId }: { userId: string }) => {
      try {
        const authStatus = await messaging().requestPermission();

        const enabled =
          authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
          authStatus === messaging.AuthorizationStatus.PROVISIONAL;

        if (enabled) {
          setTimeout(async () => await saveFCMToken({ userId }), 1000);
        }
      } catch (e) {
        console.log('requestUserPermission error: ', e);
      }
    },
    [saveFCMToken],
  );

  useEffect(() => {
    messaging().onNotificationOpenedApp(async (remoteMessage) => {
      const deeplink = remoteMessage?.data?.deeplink as string;

      const isCanOpen = await Linking.canOpenURL(deeplink);

      if (isCanOpen) {
        await Linking.openURL(deeplink);
      }
    });

    messaging()
      .getInitialNotification()
      .then(async (remoteMessage) => {
        const deeplink = remoteMessage?.data?.deeplink as string;

        if (isUndefined(deeplink)) {
          return;
        }

        const isCanOpen = await Linking.canOpenURL(deeplink);

        if (isCanOpen) {
          await Linking.openURL(deeplink);
        }
      })
      .catch((e) => console.error('getInitialNotification error: ', e));
  }, [dispatch, user?.id]);

  return {
    requestUserPermission,
  };
};

export { usePushNotifications };
