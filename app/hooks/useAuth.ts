import { useCallback, useState } from 'react';
import auth from '@react-native-firebase/auth';
import { AUTH_ERRORS } from '../constants';
// @ts-ignore
import isEmail from 'validator/lib/isEmail';
import { useAppDispatch } from 'hooks/index';
import { IUserInfo, setUserEmail, setUserId } from 'store/auth/reducer';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { deleteUsernameFromEarlyAccessFB, isEarlyAccessByUserNameFB } from 'services/earlyAccess';
import { createUser, getUser, updateUser } from 'services/users';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import isNull from 'lodash/isNull';
import { setUpdateUserInfo } from 'store/auth/actions';
import Toast from 'react-native-toast-message';
import { setGlobalLoader } from 'store/app/reducer';

const useAuth = () => {
  // login
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [errorEmail, setErrorEmail] = useState<string>('');
  const [errorPassword, setErrorPassword] = useState<string>('');
  const [isLoading, setIsLoading] = useState<boolean>(false);
  // register
  const [confirmPassword, setConfirmPassword] = useState<string>('');
  const [errorConfirmPassword, setErrorConfirmPassword] = useState<string>('');
  const [agree, setAgree] = useState<boolean>(true);
  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState<boolean>(true);
  const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);

  const dispatch = useAppDispatch();
  const userInfo = useSelector(selectUserInfo);

  // Handle user state changes
  const handleAuthStateChanged = useCallback(
    async (user: any) => {
      // todo: find user interface
      setIsLoggedIn(!isNull(user));

      if (isNull(user)) {
        setIsLoggedIn(false);
      } else {
        await dispatch(setUpdateUserInfo({ userId: user?.uid }));
        setIsLoggedIn(true);

        const { status, is_first_time_login } = await getUser({ id: user?.uid });

        if (status === 'waiting') {
          return NavigationService.navigate({ routeName: ROUTES.SignUpWaitingApproveStatus });
        }
        if (status === 'refused') {
          return NavigationService.navigate({ routeName: ROUTES.SignUpRefusedStatus });
        }
        if (status === 'approved' && is_first_time_login) {
          return NavigationService.navigate({ routeName: ROUTES.SignUpApprovedStatus });
        }
      }

      if (initializing) {
        setInitializing(false);
      }
    },
    [dispatch, initializing],
  );

  /*  isNull(userId)
      1. without reloading the app after sign up
      2. after reloading the app
      after reloading userId is not available from SignUpFinishScreen
  */
  const handleUpdateIsFirstTimeLoginInfo = useCallback(
    async ({ userId }: { userId: string | null }) => {
      setIsLoading(true);

      const loggedInUserId = await auth().currentUser?.uid;
      const id = isNull(userId) ? loggedInUserId : userId;
      const { docId, is_first_time_login } = await getUser({ id: id! });

      if (is_first_time_login || isNull(is_first_time_login)) {
        await updateUser({ docId, data: { is_first_time_login: false } });
        await dispatch(setUpdateUserInfo({ userId: id! }));
      }

      setIsLoading(false);

      if (id) {
        NavigationService.navigate({ routeName: ROUTES.Home });
      }
    },
    [dispatch],
  );

  const handleApproveLogin = useCallback(async () => {
    return NavigationService.navigate({ routeName: ROUTES.Auth });
  }, []);

  const handleLogin = useCallback(async () => {
    try {
      setIsLoading(true);
      const { user } = await auth().signInWithEmailAndPassword(email, password);
      const { uid: id } = user;

      const currentUser = await getUser({ id });
      await dispatch(setUserEmail({ email }));
      await dispatch(setUserId({ id }));

      if (!currentUser.id) {
        setIsLoading(false);
        return NavigationService.navigate({ routeName: ROUTES.QuizStepOne });
      }

      return NavigationService.resetToHome();
    } catch (e) {
      setIsLoading(false);
      // @ts-ignore
      const message = AUTH_ERRORS[e.code];
      return message.includes('password') ? setErrorPassword(message) : setErrorEmail(message);
    }
  }, [dispatch, email, password]);

  const handleRegister = useCallback(async () => {
    try {
      setIsLoading(true);

      if (!isEmail(email)) {
        setErrorEmail('The email address is invalid');
        return;
      }

      if (password !== confirmPassword) {
        setErrorConfirmPassword('Password does not match');
        return;
      }

      const { user } = await auth().createUserWithEmailAndPassword(email, password);
      const { uid: id } = user;
      dispatch(setUserId({ id }));
      dispatch(setUserEmail({ email }));
      // await auth().signOut();

      NavigationService.navigate({ routeName: ROUTES.QuizStepOne });
    } catch (e) {
      console.log('e: ', e);
      // @ts-ignore
      const message = AUTH_ERRORS[e.code];
      return message.includes('password') ? setErrorConfirmPassword(message) : setErrorEmail(message);
    } finally {
      setIsLoading(false);
    }
  }, [confirmPassword, dispatch, email, password]);

  const handleResetPassword = useCallback(async () => {
    try {
      dispatch(setGlobalLoader({ isLoading: true }));
      await auth().sendPasswordResetEmail(email);
      dispatch(setGlobalLoader({ isLoading: false }));
      Toast.show({
        text1: 'Check email to reset password',
      });
    } catch (e) {
      // @ts-ignore
      const message = AUTH_ERRORS[e.code];
      setErrorEmail(message);
    } finally {
      dispatch(setGlobalLoader({ isLoading: false }));
    }
  }, [dispatch, email]);

  const handleFinishSignUp = useCallback(
    async ({ username }: { username: string }) => {
      setIsLoading(true);

      // if user has early access
      const isEarlyAccess = await isEarlyAccessByUserNameFB({
        userName: username,
      });

      const userData: IUserInfo = {
        ...userInfo,
        status: isEarlyAccess ? 'approved' : 'waiting',
        is_anonymous: false,
        notifications: {
          bookmarksAgencies: true,
          bookmarksComments: true,
          likes: true,
          replies: true,
          news: true,
        },
        created_at: new Date(),
        is_first_time_login: !isEarlyAccess,
        userName: username,
      };
      const isCreated = await createUser({ userInfo: userData });

      if (!isCreated) {
        setIsLoading(false);
        Toast.show({
          text1: 'Failed to create user. Try again later.',
          type: 'warning',
        });
        return;
      }

      if (isEarlyAccess) {
        await deleteUsernameFromEarlyAccessFB({ userName: username });
      }

      setIsLoading(false);

      return isEarlyAccess
        ? NavigationService.resetToHome()
        : NavigationService.navigate({ routeName: ROUTES.InstagramVerification });
    },
    [userInfo],
  );

  const handleChange = useCallback(
    ({ type, text }: { type: 'password' | 'email' | 'confirmPassword'; text: string }) => {
      const state = {
        email: () => {
          setEmail(text);
          setErrorEmail('');
        },
        password: () => {
          setPassword(text);
          setErrorPassword('');
        },
        confirmPassword: () => {
          setConfirmPassword(text);
          setErrorConfirmPassword('');
        },
      };

      state[type]();
    },
    [],
  );

  const handleClear = useCallback(({ type }: { type: 'password' | 'email' | 'confirmPassword' }) => {
    const state = {
      email: () => {
        setEmail('');
        setErrorEmail('');
      },
      password: () => {
        setPassword('');
        setErrorPassword('');
      },
      confirmPassword: () => {
        setConfirmPassword('');
        setErrorConfirmPassword('');
      },
    };

    state[type]();
  }, []);

  const handleReset = useCallback(() => {
    setEmail('');
    setPassword('');
    setErrorEmail('');
    setErrorPassword('');
    setConfirmPassword('');
    setErrorConfirmPassword('');
    setAgree(true);
  }, []);

  return {
    handleApproveLogin,
    handleLogin,
    handleRegister,
    handleResetPassword,
    handleFinishSignUp,
    handleAuthStateChanged,
    handleUpdateIsFirstTimeLoginInfo,
    handleClear,
    handleChange,
    handleReset,
    isLoading,
    email,
    password,
    errorEmail,
    errorPassword,
    setAgree,
    agree,
    errorConfirmPassword,
    confirmPassword,
    initializing,
    isLoggedIn,
  };
};

export { useAuth };
