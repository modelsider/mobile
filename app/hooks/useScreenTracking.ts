import { useCallback, useRef } from 'react';
import analytics from '@react-native-firebase/analytics';
import { navigationRef } from 'navigator/NavigationService';

const useScreenTracking = () => {
  const routeNameRef = useRef<string>();

  const handleReady = useCallback(() => {
    routeNameRef.current = navigationRef.getCurrentRoute()?.name;
  }, []);

  const handleStateChange = useCallback(async () => {
    const previousRouteName = routeNameRef.current;
    const currentRouteName = navigationRef.getCurrentRoute()?.name;

    if (previousRouteName !== currentRouteName) {
      await analytics().logScreenView({
        screen_name: currentRouteName,
        screen_class: currentRouteName,
      });
    }
    routeNameRef.current = currentRouteName;
  }, []);

  return {
    handleReady,
    handleStateChange,
  };
};

export { useScreenTracking };
