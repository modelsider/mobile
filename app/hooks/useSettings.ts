import { useAppDispatch } from 'hooks/index';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { useCallback, useState } from 'react';
import { noop } from 'types/types';
import { useImagePicker } from './useImagePicker';
import { updateUser } from '../services/users';
import { setUpdateUserInfo } from '../store/auth/actions';
import { prompt } from 'components/alerts';
// @ts-ignore
import isEmail from 'validator/lib/isEmail';
import { icons } from '../../assets';

type Response = {
  profilePicture: string;
  country: string;
  isErrorEmail: boolean;
  isOpenCountry: boolean;
  setIsOpenCountry: (isOpen: boolean) => void;
  handleSetProfilePicture: noop;
  handlePressFullName: noop;
  handlePressEmail: noop;
  handleChangeCountry: (value: string) => void;
};

const useSettings = (): Response => {
  const dispatch = useAppDispatch();
  const { id, name: userName, profilePictureUrl, location, email: userEmail, docId } = useSelector(selectUserInfo);

  const { getImage } = useImagePicker();

  const [name, setName] = useState<string>(userName);
  // todo: use icons.defaultUserPicture in one place
  const [profilePicture, setProfilePicture] = useState<string>(profilePictureUrl || icons.defaultUser);
  const [isOpenCountry, setIsOpenCountry] = useState<boolean>(false);
  const [country, setCountry] = useState<string>(location); // change location => country
  const [email, setEmail] = useState<string>(userEmail);
  const [isErrorEmail, setIsErrorEmail] = useState<boolean>(false);

  const handleSetProfilePicture = useCallback(async () => {
    const [url] = await getImage();
    setProfilePicture(url);

    await updateUser({
      docId: docId!,
      data: { profilePictureUrl: url },
    });
    dispatch(setUpdateUserInfo({ userId: id }));
  }, [dispatch, getImage, docId, id]);

  const handlePressFullName = useCallback(() => {
    const onConfirm = async (value: string) => {
      setName(value);

      await updateUser({
        docId: docId!,
        data: { name: value },
      });
      dispatch(setUpdateUserInfo({ userId: id }));
    };

    return prompt({
      title: 'Name',
      message: 'Please update your name',
      onConfirm,
      defaultValue: name,
    });
  }, [dispatch, name, docId, id]);

  const handlePressEmail = useCallback(() => {
    const onConfirm = async (value: string) => {
      if (!isEmail(value)) {
        setEmail(value);
        setIsErrorEmail(true);
        return;
      }
      setEmail(value);
      setIsErrorEmail(false);

      await updateUser({
        docId: docId!,
        data: { email: value },
      });
      dispatch(setUpdateUserInfo({ userId: id }));
    };

    return prompt({
      title: 'Email',
      message: 'Please update your email',
      onConfirm,
      defaultValue: email,
      keyboardType: 'email-address',
    });
  }, [dispatch, email, docId, id]);

  const handleChangeCountry = useCallback(
    async (value: string) => {
      setCountry(value);
      setIsOpenCountry(!isOpenCountry);

      await updateUser({
        docId: docId!,
        data: { location: value },
      });
      dispatch(setUpdateUserInfo({ userId: id }));
    },
    [dispatch, docId, id, isOpenCountry],
  );

  return {
    profilePicture,
    country,
    isOpenCountry,
    setIsOpenCountry,
    isErrorEmail,
    handleSetProfilePicture,
    handlePressFullName,
    handlePressEmail,
    handleChangeCountry,
  };
};

export { useSettings };
