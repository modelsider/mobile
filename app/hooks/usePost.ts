import { IPost } from 'types/types';
import { useSelector } from 'react-redux';
import { useCallback, useState } from 'react';
import { useAppDispatch, useImagePicker } from 'hooks/index';
import NavigationService from 'navigator/NavigationService';
import { selectUserInfo } from 'store/auth/selectors';
import { generateId } from 'utils/generateId';
import { MAX_SYMBOLS_TO_LEAVE_COMMENT } from 'constants/index';
import { addComment, updateComment } from 'store/comments/actions';
import Toast from 'react-native-toast-message';

const usePost = (isEdit = false, post?: IPost) => {
  const dispatch = useAppDispatch();

  const user = useSelector(selectUserInfo);

  const { getImage } = useImagePicker();

  const [postValue, setPostValue] = useState<string | undefined>(isEdit ? post?.text : '');
  const [attachments, setAttachments] = useState<string[]>(isEdit ? post?.attachments! : []);
  const [isAnonymous, setIsAnonymous] = useState<boolean>(isEdit ? post?.is_anonymous! : false);
  const [loading, setLoading] = useState<boolean>(false);

  const postValueCounter = MAX_SYMBOLS_TO_LEAVE_COMMENT - postValue!.trim().length;
  const isDisabledSubmitButton = !postValue!.trim().length;

  const handleToggleIsAnonymous = useCallback(() => {
    setIsAnonymous(!isAnonymous);
  }, [isAnonymous]);

  const handleAddAttachments = useCallback(async () => {
    const images = await getImage();
    setAttachments(images);
  }, [getImage]);

  const handleCreatePost = async () => {
    setLoading(true);

    const newPost: IPost = {
      id: generateId(),
      created_at: new Date(),
      replies_count: 0,
      likes_count: 0,
      text: postValue!,
      user_id: user.id,
      attachments,
      is_anonymous: isAnonymous,
      type: 'Post',
      types: [],
      agency_type: '',
      agency_id: null,
      agency_name: '',
    };

    const updatedPost = {
      text: postValue!,
      attachments,
      is_anonymous: isAnonymous,
    };

    // use the same collection Comments for posts
    if (isEdit) {
      // @ts-ignore
      await dispatch(updateComment({ docId: post?.docId!, comment: updatedPost }));
    } else {
      await dispatch(addComment({ comment: newPost }));
    }

    Toast.show({
      text1: `Thank you! Your post is ${isEdit ? 'updated' : 'published'}.`,
    });

    NavigationService.navigateBack();
  };

  return {
    postValue,
    setPostValue,
    isAnonymous,
    handleToggleIsAnonymous,
    loading,
    attachments,
    handleAddAttachments,
    postValueCounter,
    isDisabledSubmitButton,
    handleCreatePost,
  };
};

export { usePost };
