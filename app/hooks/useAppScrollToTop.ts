import { useRef } from 'react';
import { useScrollToTop } from '@react-navigation/native';

export const useAppScrollToTop = () => {
  const ref = useRef(null);
  useScrollToTop(ref);

  return {
    ref,
  };
};
