import { useCallback } from 'react';
import { useAppDispatch } from 'hooks/index';
import { setIsUpdateInstagramData, setUserName } from 'store/auth/reducer';
import { useSelector } from 'react-redux';
import { selectIsUpdateInstagramData } from 'store/auth/selectors';
// @ts-ignore
import { APP_SECRET, REDIRECT_URL } from '@env';
import { getFormattedCode, getResponseAuthInstagram } from 'utils/instagramLogin';
import { ROUTES } from 'navigator/constants/routes';
import NavigationService from 'navigator/NavigationService';
import { authByInstagram, getUserInfo } from 'services/instagram';
import { noop } from '../types/types';

const ERROR_TYPE = 'error_type';

type Response = {
  handleNavigateToInstagramLogin: noop;
  handleNavigationStateChange: ({ url }: { url: string }) => void;
  handleUpdateInstagramData: noop;
};

export const useInstagramVerification = (): Response => {
  const dispatch = useAppDispatch();
  const isUpdate = useSelector(selectIsUpdateInstagramData);

  const handleNavigateToInstagramLogin = useCallback(() => {
    NavigationService.navigate({ routeName: ROUTES.InstagramLogin });
  }, []);

  const handleLoginSuccess = useCallback(
    async ({ access_token, user_id }: { access_token?: string; user_id?: string }) => {
      NavigationService.navigateBack();

      const userName = await getUserInfo(access_token!);
      dispatch(setUserName({ userName }));
      if (isUpdate) {
        dispatch(setIsUpdateInstagramData({ isUpdate: false }));
        return;
      }

      NavigationService.navigate({ routeName: ROUTES.QuizStepOne });
    },
    [dispatch, isUpdate],
  );

  const handleLoginFailure = () => {
    return NavigationService.navigate({
      routeName: ROUTES.SignUpUnableInstagramError,
    });
  };

  const handleNavigationStateChange = async ({ url }: { url: string }) => {
    if (url && url.startsWith(REDIRECT_URL)) {
      const response = getResponseAuthInstagram({ url });
      const { access_token, user_id, code } = response;

      const formattedCode = getFormattedCode({ code: code! });

      if (access_token || (code && !APP_SECRET)) {
        await handleLoginSuccess({
          access_token,
          user_id,
        });
        return;
      }

      const responseAuth = await authByInstagram({ code: formattedCode });
      if (responseAuth?.hasOwnProperty(ERROR_TYPE)) {
        handleLoginFailure();
      } else {
        await handleLoginSuccess({
          access_token: responseAuth.access_token,
          user_id: responseAuth.user_id,
        });
      }
    }
  };

  // my-agencies tab to update instagram data
  const handleUpdateInstagramData = useCallback(() => {
    handleNavigateToInstagramLogin();
    dispatch(setIsUpdateInstagramData({ isUpdate: true }));
  }, [dispatch, handleNavigateToInstagramLogin]);

  return {
    handleNavigateToInstagramLogin,
    handleNavigationStateChange,
    handleUpdateInstagramData,
  };
};
