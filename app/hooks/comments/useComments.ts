import { useAppDispatch } from 'hooks/index';
import { useSelector } from 'react-redux';
import { selectAgencyComments, selectComment } from 'store/comments/selectors';
import { useCallback, useMemo, useState } from 'react';
import { setCommentById, setCommentsByAgencyId, setUploadedComments, toggleCommentLike } from 'store/comments/actions';
import { selectAgencyType } from 'store/agency/selectors';
import { IComment, IReply, noop } from 'types/types';
import { setClickedCommentIndex } from 'store/comments/reducer';
import { useBottomSheet } from '../useBottomSheet';
import auth from '@react-native-firebase/auth';

type Response = {
  comments: IComment[];
  comment: IComment;
  isFetchComments: boolean;
  isFetchComment: boolean;
  fetchAgencyComments: (id?: number) => void;
  fetchComment: ({ commentId }: { commentId: number }) => void;
  handleCommentLike: ({
    commentId,
    isLikeLocal,
    likesCountLocal,
  }: {
    commentId: number;
    isLikeLocal?: boolean;
    likesCountLocal?: number;
  }) => void;
  handleCommentSheet: ({
    comment,
    isNavigateBack,
    onRefresh,
  }: {
    comment: IComment | IReply;
    isNavigateBack?: boolean;
    onRefresh?: noop;
  }) => void;
  handleReportCommentSheet: ({
    comment,
    onRefresh,
    isNavigateBack,
  }: {
    comment: IComment | IReply;
    onRefresh?: noop;
    isNavigateBack?: boolean;
  }) => void;
  fetchUploadedComments: noop;
  handleMomentumScrollEnd: noop;
  handleMomentumScrollBegin: noop;
  isMomentumScrollEnd: boolean;
  handeClickedCommentIndex: ({ commentIndex }: { commentIndex: number }) => void;
};

const useComments = (agencyId?: number): Response => {
  const dispatch = useAppDispatch();

  // const { id: userId } = useSelector(selectUserInfo);
  // const agency = useSelector(selectAgency);
  const userId = auth().currentUser?.uid!;
  const agencyType = useSelector(selectAgencyType);
  const comments = useSelector(selectAgencyComments);
  const comment = useSelector(selectComment);

  const { handleCommentSheet, handleReportCommentSheet } = useBottomSheet();

  const [isFetchComments, setIsFetchComments] = useState<boolean>(false);
  const [isFetchComment, setIsFetchComment] = useState<boolean>(false);
  const [isMomentumScrollEnd, setIsMomentumScrollEnd] = useState<boolean>(false);

  const fetchAgencyComments = useCallback(
    async (id = agencyId) => {
      setIsFetchComments(true);
      if (+id) {
        await dispatch(setCommentsByAgencyId({ agencyId: +id, userId, agencyType }));
      }
      setIsFetchComments(false);
    },
    [agencyId, agencyType, dispatch, userId],
  );

  const fetchComment = useCallback(
    async ({ commentId }: { commentId: number }) => {
      setIsFetchComment(true);
      await dispatch(setCommentById({ commentId, userId }));
      setIsFetchComment(false);
    },
    [dispatch, userId],
  );

  const handleCommentLike = useCallback(
    async ({
      commentId,
      isLikeLocal,
      likesCountLocal,
    }: {
      commentId: number;
      isLikeLocal?: boolean;
      likesCountLocal?: number;
    }) => {
      dispatch(
        toggleCommentLike({
          userId,
          commentId,
          isLikeLocal,
          likesCountLocal,
        }),
      );
    },
    [dispatch, userId],
  );

  const commentsByType = useMemo(
    () => comments.filter(({ agency_type }) => agency_type === agencyType),
    [agencyType, comments],
  );

  const fetchUploadedComments = useCallback(async () => {
    await dispatch(
      setUploadedComments({
        agencyId: agencyId!,
        userId,
        comments,
        agencyType,
      }),
    );
  }, [agencyId, agencyType, comments, dispatch, userId]);

  const handleMomentumScrollEnd = useCallback(() => {
    setIsMomentumScrollEnd(true);
  }, []);

  const handleMomentumScrollBegin = useCallback(() => {
    setIsMomentumScrollEnd(false);
  }, []);

  const handeClickedCommentIndex = useCallback(
    ({ commentIndex }: { commentIndex: number }) => {
      dispatch(setClickedCommentIndex({ commentIndex }));
    },
    [dispatch],
  );

  return {
    // comments
    comments: commentsByType,
    isFetchComments,
    fetchAgencyComments,
    handleCommentLike,
    // comment
    isFetchComment,
    fetchComment,
    comment,
    handleCommentSheet,
    handleReportCommentSheet,
    handeClickedCommentIndex,
    // upload
    fetchUploadedComments,
    handleMomentumScrollBegin,
    isMomentumScrollEnd,
    handleMomentumScrollEnd,
  };
};

export { useComments };
