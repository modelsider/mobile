import { useCallback, useMemo, useState } from 'react';
import { NativeScrollEvent, NativeSyntheticEvent } from 'react-native';
import { IComment } from 'types/types';
import { isBottomToComments } from 'utils/isBottomToComments';

type Arguments = {
  comments: IComment[];
};

type Response = {
  isCommentButton: boolean;
  onScrollToComments: ({
    nativeEvent: { layoutMeasurement, contentOffset, contentSize },
  }: NativeSyntheticEvent<NativeScrollEvent>) => void;
  setIsCommentButton: (flag: boolean) => void;
};

const useScrollAgencyComments = ({ comments }: Arguments): Response => {
  const [isCommentButton, setIsCommentButton] = useState<boolean>(false);

  const commentsLength = useMemo(
    () => (comments?.length === 0 ? 1 : comments?.length),
    [comments?.length]
  );

  const onScrollToComments = useCallback(
    ({
      nativeEvent: { layoutMeasurement, contentOffset, contentSize },
    }: NativeSyntheticEvent<NativeScrollEvent>) => {
      const isScrollDownEnough = isBottomToComments({
        layoutMeasurement,
        contentOffset,
        contentSize,
        commentsLength
      });

      if (isScrollDownEnough) {
        setIsCommentButton(true);
      } else {
        setIsCommentButton(false);
      }
    },
    [commentsLength]
  );
  return {
    isCommentButton,
    onScrollToComments,
    setIsCommentButton,
  };
};

export { useScrollAgencyComments };
