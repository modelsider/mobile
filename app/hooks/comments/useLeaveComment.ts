import { useAppDispatch, useImagePicker } from 'hooks/index';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { useCallback, useMemo, useState } from 'react';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { commentType, IComment, noop } from 'types/types';
import { generateId } from 'utils/generateId';
import { selectAgencyType, selectAgency } from 'store/agency/selectors';
import { addComment, updateComment } from 'store/comments/actions';
import { selectComment } from 'store/comments/selectors';
import { setClickedCommentIndex } from 'store/comments/reducer';
import { MAX_SYMBOLS_TO_LEAVE_COMMENT } from 'constants/index';

type Response = {
  navigateToLeaveComment: noop;
  handleCreateComment: ({ types }: { types: commentType[] }) => void;
  handleUpdateComment: ({ types }: { types: commentType[] }) => void;
  commentValue: string;
  setCommentValue: (value: string) => void;
  commentValueCounter: number;
  isAnonymous: boolean;
  handleToggleIsAnonymous: noop;
  isDisabledCommentButton: boolean;
  isAddedComment: boolean;
  handleAddAttachments: noop;
  attachments: string[];
  handleRemoveAttachment: ({ uri }: { uri: string }) => void;
};

const useLeaveComment = (isEdit = false): Response => {
  const dispatch = useAppDispatch();

  const user = useSelector(selectUserInfo);
  const agency = useSelector(selectAgency);
  const agencyType = useSelector(selectAgencyType);
  const comment = useSelector(selectComment);

  const { getImage } = useImagePicker();

  const [commentValue, setCommentValue] = useState<string>(
    isEdit ? comment?.text : ''
  );
  const [attachments, setAttachments] = useState<string[]>(
    isEdit ? comment?.attachments : []
  );
  const [isAnonymous, setIsAnonymous] = useState<boolean>(
    isEdit ? comment?.is_anonymous : false
  );
  const [isAddedComment, setIsAddedComment] = useState<boolean>(false);

  const handleToggleIsAnonymous = useCallback(() => {
    setIsAnonymous(!isAnonymous);
  }, [isAnonymous]);

  const commentValueCounter = useMemo(
    () => MAX_SYMBOLS_TO_LEAVE_COMMENT - commentValue?.trim()?.length,
    [commentValue]
  );

  const isDisabledCommentButton = useMemo(
    () => !commentValue?.trim().length,
    [commentValue]
  );

  const handleRemoveAttachment = ({ uri }: { uri: string }) => {};

  const navigateToLeaveComment = useCallback(() => {
    NavigationService.navigateWithParams({
      routeName: ROUTES.LeaveCommentScreen,
      params: { isEdit: false }
    });
  }, []);

  const handleAddAttachments = useCallback(async () => {
    const images = await getImage();
    setAttachments(images);
  }, [getImage]);

  const handleCreateComment = useCallback(
    async ({ types }: { types: commentType[] }) => {
      setIsAddedComment(true);
      dispatch(setClickedCommentIndex({ commentIndex: 0 }));
      const comment: IComment = {
        id: generateId(),
        created_at: new Date(),
        types,
        replies_count: 0,
        likes_count: 0,
        text: commentValue,
        user_id: user.id,
        attachments,
        agency_id: agency?.id!,
        is_anonymous: isAnonymous,
        agency_type: agencyType,
        type: 'Comment',
        agency_name: agency?.agency_name,
      };

      await dispatch(addComment({ comment }));

      // todo: enhancer
      NavigationService.navigateWithParams({
        routeName: ROUTES.Agency,
        params: { id: agency?.id }
      });
    },
    [
      agency?.id,
      agencyType,
      attachments,
      commentValue,
      dispatch,
      isAnonymous,
      user.id
    ]
  );

  const handleUpdateComment = useCallback(
    async ({ types }: { types: commentType[] }) => {
      setIsAddedComment(true);
      dispatch(setClickedCommentIndex({ commentIndex: 0 }));
      const updatedComment = {
        text: commentValue,
        attachments,
        is_anonymous: isAnonymous,
        types: types.filter(Boolean)
      };

      await dispatch(
        updateComment({ docId: comment?.docId!, comment: updatedComment })
      );

      NavigationService.navigateWithParams({
        routeName: ROUTES.CommentScreen,
        params: { commentId: comment?.id }
      });
    },
    [
      attachments,
      comment?.docId,
      comment?.id,
      commentValue,
      dispatch,
      isAnonymous
    ]
  );

  return {
    commentValue,
    setCommentValue,
    commentValueCounter,
    isAnonymous,
    handleToggleIsAnonymous,
    isDisabledCommentButton,
    isAddedComment,
    navigateToLeaveComment,
    handleCreateComment,
    handleAddAttachments,
    attachments,
    handleUpdateComment,
    handleRemoveAttachment
  };
};

export { useLeaveComment };
