import { commentType, IComment } from 'types/types';
import { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import {
  selectComment,
  selectFilteredCommentsByType,
  selectSortedAgencyCommentsByLikes,
} from 'store/comments/selectors';

type Response = {
  handleSortComments: ({ type }: { type: commentType }) => void;
  selectedButtons: commentType[];
  sortedComments: IComment[];
};

const useSortComments = (isEdit = false): Response => {
  const comment = useSelector(selectComment);

  const sortedCommentsByLikes = useSelector(selectSortedAgencyCommentsByLikes);
  const filteredCommentsByApartment = useSelector(
    selectFilteredCommentsByType({ type: 'apartment' }),
  );
  const filteredCommentsByComplaints = useSelector(
    selectFilteredCommentsByType({ type: 'complaints' }),
  );
  const filteredCommentsByOther = useSelector(
    selectFilteredCommentsByType({ type: 'bravo' }),
  );

  const commentsState = {
    likes: sortedCommentsByLikes,
    apartment: filteredCommentsByApartment,
    complaints: filteredCommentsByComplaints,
    other: filteredCommentsByOther,
  };

  const [selectedButtons, setSelectedButton] = useState<commentType[]>(
    isEdit ? comment?.types : [],
  );

  const handleSortComments = useCallback(
    ({ type }: { type: commentType }) => {
      if (selectedButtons.includes(type)) {
        setSelectedButton(prevState =>
          prevState.filter(prevType => prevType !== type),
        );
      } else {
        setSelectedButton(prevState => [...prevState, type]);
      }
    },
    [selectedButtons],
  );

  // todo: not finish yet
  const sortedComments = useMemo(() => {
    return selectedButtons.map(type => {
      return commentsState[type];
    });
  }, [commentsState, selectedButtons]);

  return {
    handleSortComments,
    selectedButtons,
    sortedComments,
  };
};

export { useSortComments };
