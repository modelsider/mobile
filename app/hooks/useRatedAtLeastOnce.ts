import { useCallback } from 'react';
import { getRatedAgenciesFB } from 'services/agencies';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { useAppDispatch } from './useAppDispatch';
import { setIfRating } from 'store/app/reducer';
import { useFocusEffect } from '@react-navigation/native';

const useRatedAtLeastOnce = () => {
  const dispatch = useAppDispatch();
  const user = useSelector(selectUserInfo);

  const handleCheckIsRated = useCallback(async () => {
    const [agenciesMother, agenciesBooking] = await Promise.all([
      getRatedAgenciesFB({
        userId: user.id,
        type: 'mother',
      }),
      getRatedAgenciesFB({
        userId: user.id,
        type: 'booking',
      }),
    ]);

    const isRated = agenciesMother.mappedAgencies.length > 0 || agenciesBooking.mappedAgencies.length > 0;
    dispatch(setIfRating({ hasRating: isRated }));
  }, [dispatch, user.id]);

  useFocusEffect(
    useCallback(() => {
      console.log('handleCheckIsRated: ');
      handleCheckIsRated();
    }, [handleCheckIsRated]),
  );
};

export { useRatedAtLeastOnce };
