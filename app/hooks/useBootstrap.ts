import { useAppDispatch } from 'hooks/index';
import { useCallback } from 'react';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { selectAgencyType } from 'store/agency/reducer';
import { setSelectedUserProfileId } from 'store/users/reducer';
import { setCountries } from 'store/countries/actions';
import { setUpdateUserInfo } from 'store/auth/actions';
import { setStatusBarBg, setTabView } from 'store/app/reducer';

const useBootstrap = () => {
  const dispatch = useAppDispatch();

  const { is_first_time_login, status, id } = useSelector(selectUserInfo);

  const runBootstrap = useCallback(async () => {
    if (id) {
      dispatch(setUpdateUserInfo({ userId: id }));
      dispatch(setSelectedUserProfileId({ userId: null }));
    }
    dispatch(setStatusBarBg({ bg: '' }));
    dispatch(setTabView({ type: 'login' }));
    dispatch(selectAgencyType({ type: 'mother' }));
    await dispatch(setCountries());
  }, [dispatch, id]);

  return {
    runBootstrap,
    isFirstTimeLogin: is_first_time_login,
    status,
  };
};

export { useBootstrap };
