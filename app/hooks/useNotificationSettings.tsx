import { useCallback, useState } from 'react';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { useAppDispatch } from 'hooks/index';
import { updateUser } from 'services/users';
import { setUpdateUserInfo } from 'store/auth/actions';
import { SETTINGS_NOTIFICATIONS_ITEMS } from '../screens/settings-screen/config';
import { ISettingsNotificationsItem, notificationType } from 'types/types';

export const useNotificationSettings = () => {
  const dispatch = useAppDispatch();

  const { notifications, docId, id } = useSelector(selectUserInfo);

  const initialNotifications = SETTINGS_NOTIFICATIONS_ITEMS?.map((notification) => ({
    ...notification,
    isEnabled: notifications[notification?.value],
  }));

  const [notificationsState, setNotificationsState] = useState<ISettingsNotificationsItem[]>(initialNotifications);

  const handleToggleSwitch = useCallback(
    async (value: notificationType) => {
      const updatedNotifications = {
        ...notifications,
        [value]: !notifications[value],
      };

      const updatedNotificationsState = notificationsState?.map((notification) => {
        if (notification?.value === value) {
          return {
            ...notification,
            isEnabled: !notification?.isEnabled,
          };
        }
        return notification;
      });

      await updateUser({
        docId,
        data: { notifications: updatedNotifications },
      });
      dispatch(setUpdateUserInfo({ userId: id }));

      setNotificationsState(updatedNotificationsState);
    },
    [notificationsState, dispatch, docId, id, notifications],
  );

  return {
    notificationsState,
    handleToggleSwitch,
  };
};
