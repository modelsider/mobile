import { useAppDispatch } from 'hooks/index';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { selectAgencyType, selectRatedAgencies, selectWhoRatedAgencies } from 'store/agency/selectors';
import { useCallback, useState } from 'react';
import { setRatedAgencies, setUploadedAgencies } from 'store/agency/actions';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { agencyType, IAgency, noop } from 'types/types';
import { setAgencyId } from '../store/agency/reducer';
import { selectUserProfileId } from '../store/users/selectors';

type Response = {
  agencies: IAgency[];
  type: agencyType;
  isFetchAgencies: boolean;
  fetchAgencies: noop;
  handleToggleSearch: noop;
  handleNavigateToAgency: ({ agencyId }: { agencyId: number }) => void;
  fetchUploadedAgencies: noop;
  handleMomentumScrollEnd: noop;
  handleMomentumScrollBegin: noop;
  isMomentumScrollEnd: boolean;
};

const useAgencies = (): Response => {
  const dispatch = useAppDispatch();

  const { id: userId } = useSelector(selectUserInfo);
  const selectedProfileId = useSelector(selectUserProfileId);
  const ratedAgencies = useSelector(selectRatedAgencies);
  const whoRatedAgencies = useSelector(selectWhoRatedAgencies);
  const type = useSelector(selectAgencyType);

  const [isFetchAgencies, setIsFetchAgencies] = useState<boolean>(false);
  const [isMomentumScrollEnd, setIsMomentumScrollEnd] = useState<boolean>(false);

  const agencies = isFetchAgencies ? [] : ratedAgencies;

  const handleMomentumScrollEnd = useCallback(() => {
    setIsMomentumScrollEnd(true);
  }, []);

  const handleMomentumScrollBegin = useCallback(() => {
    setIsMomentumScrollEnd(false);
  }, []);

  const handleToggleSearch = useCallback(() => {
    NavigationService.navigate({ routeName: ROUTES.SearchAgency });
  }, []);

  // todo: enhancer
  const handleNavigateToAgency = useCallback(
    ({ agencyId }: { agencyId: number }) => {
      dispatch(setAgencyId({ agencyId }));
      return NavigationService.push({
        routeName: ROUTES.Agency,
        params: {
          id: agencyId,
        },
      });
    },
    [dispatch],
  );

  const fetchAgencies = useCallback(async () => {
    setIsFetchAgencies(true);
    await dispatch(setRatedAgencies({ userId: selectedProfileId || userId, type }));
    setIsFetchAgencies(false);
  }, [dispatch, selectedProfileId, userId, type]);

  const fetchUploadedAgencies = useCallback(async () => {
    await dispatch(setUploadedAgencies({ agencies: whoRatedAgencies, type, userId: selectedProfileId || userId }));
  }, [dispatch, selectedProfileId, type, userId, whoRatedAgencies]);

  return {
    agencies,
    type,
    isFetchAgencies,
    fetchAgencies,
    handleNavigateToAgency,
    handleToggleSearch,
    fetchUploadedAgencies,
    isMomentumScrollEnd,
    handleMomentumScrollEnd,
    handleMomentumScrollBegin,
  };
};

export { useAgencies };
