import { IReply, noop } from 'types/types';
import { useSelector } from 'react-redux';
import { selectComment, selectCommentReplies } from 'store/comments/selectors';
import { useCallback, useMemo, useState } from 'react';
import { addReply, setCommentById, setRepliesByCommentDocId, setUploadedReplies } from 'store/comments/actions';
import { useAppDispatch, useImagePicker } from 'hooks/index';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { selectUserInfo } from 'store/auth/selectors';
import { generateId } from 'utils/generateId';
import { selectAgency, selectAgencyType } from 'store/agency/selectors';
import { MAX_SYMBOLS_TO_LEAVE_COMMENT } from 'constants/index';
import { updateReplyFB } from 'services/replies';

type Response = {
  isFetchReplies: boolean;
  fetchReplies: ({ commentDocId }: { commentDocId: string }) => void;
  replies: IReply[];
  navigateToReplyToComment: ({ commentId, isPost }: { commentId: number; isPost?: boolean }) => void;
  replyValue: string;
  setReplyValue: (value: string) => void;
  replyValueCounter: number;
  isAnonymous: boolean;
  handleToggleIsAnonymous: noop;
  isDisabledReplyButton: boolean;
  handleCreateReply: noop;
  isAddedReply: boolean;
  attachments: string[];
  handleAddAttachments: noop;
  fetchUploadedReplies: noop;
  handleMomentumScrollEnd: noop;
  handleMomentumScrollBegin: noop;
  isMomentumScrollEnd: boolean;
  handleRemoveAttachment: ({ uri }: { uri: string }) => void;
};
const useReplies = (reply?: IReply): Response => {
  const dispatch = useAppDispatch();

  const user = useSelector(selectUserInfo);
  const comment = useSelector(selectComment);
  const replies = useSelector(selectCommentReplies);
  const agency = useSelector(selectAgency);
  const agencyType = useSelector(selectAgencyType);

  const { getImage } = useImagePicker();

  const [replyValue, setReplyValue] = useState<string>(reply?.text || '');
  const [attachments, setAttachments] = useState<string[]>([]);
  const [isAnonymous, setIsAnonymous] = useState<boolean>(reply?.is_anonymous || false);
  const [isFetchReplies, setIsFetchReplies] = useState<boolean>(false);
  const [isAddedReply, setIsAddedReply] = useState<boolean>(false);
  const [isMomentumScrollEnd, setIsMomentumScrollEnd] = useState<boolean>(false);

  const handleToggleIsAnonymous = useCallback(() => {
    setIsAnonymous(!isAnonymous);
  }, [isAnonymous]);

  const replyValueCounter = useMemo(() => MAX_SYMBOLS_TO_LEAVE_COMMENT - replyValue.trim().length, [replyValue]);

  const isDisabledReplyButton = useMemo(() => !replyValue.trim().length, [replyValue]);

  //  need commentDocId because comment not received yet
  const fetchReplies = useCallback(
    async ({ commentDocId }: { commentDocId: string }) => {
      setIsFetchReplies(true);
      await dispatch(setRepliesByCommentDocId({ docId: commentDocId }));
      setIsFetchReplies(false);
    },
    [dispatch],
  );

  const fetchUploadedReplies = useCallback(async () => {
    await dispatch(
      setUploadedReplies({
        commentDocId: comment?.docId!,
        replies,
      }),
    );
  }, [comment?.docId, dispatch, replies]);

  const handleMomentumScrollEnd = useCallback(() => {
    setIsMomentumScrollEnd(true);
  }, []);

  const handleMomentumScrollBegin = useCallback(() => {
    setIsMomentumScrollEnd(false);
  }, []);

  const navigateToReplyToComment = useCallback(
    ({ commentId, isPost = false }: { commentId: number; isPost?: boolean }) => {
      dispatch(setCommentById({ commentId, userId: user.id }));

      NavigationService.navigateWithParams({
        routeName: ROUTES.LeaveReplyScreen,
        params: {
          commentId,
          isPost,
        },
      });
    },
    [dispatch, user.id],
  );

  const handleAddAttachments = useCallback(async () => {
    const images = await getImage();
    setAttachments(images);
  }, [getImage]);

  //  comment already received
  const handleCreateReply = useCallback(async () => {
    setIsAddedReply(true);

    await dispatch(
      setCommentById({
        commentId: comment.id,
        userId: user.id,
      }),
    );

    const reply: IReply = {
      id: generateId(),
      created_at: new Date(),
      attachments,
      text: replyValue,
      user_id: user.id,
      is_anonymous: isAnonymous,
      agency_id: agency?.id! || null,
      agency_type: agencyType,
      type: 'Reply',
    };

    await dispatch(
      addReply({
        commentDocId: comment?.docId!,
        reply,
        repliesCount: comment?.replies_count,
      }),
    );

    NavigationService.navigateBack();
  }, [
    agency?.id,
    agencyType,
    attachments,
    comment?.docId,
    comment?.id,
    comment?.replies_count,
    dispatch,
    isAnonymous,
    replyValue,
    user.id,
  ]);

  const handleUpdateReply = async () => {
    setIsAddedReply(true);

    const updatedReply = {
      text: replyValue,
      attachments,
      is_anonymous: isAnonymous,
    };
    await updateReplyFB({ commentDocId: comment?.docId!, replyDocId: reply?.docId!, reply: updatedReply });

    NavigationService.navigateBack();

    setIsAddedReply(false);
  };

  const handleRemoveAttachment = ({ uri }: { uri: string }) => {};

  return {
    isFetchReplies,
    fetchReplies,
    replies,
    navigateToReplyToComment,
    replyValue,
    setReplyValue,
    replyValueCounter,
    isAnonymous,
    handleToggleIsAnonymous,
    isDisabledReplyButton,
    handleCreateReply,
    isAddedReply,
    attachments,
    handleAddAttachments,
    fetchUploadedReplies,
    handleMomentumScrollEnd,
    handleMomentumScrollBegin,
    isMomentumScrollEnd,
    handleRemoveAttachment,
    handleUpdateReply,
  };
};

export { useReplies };
