import VersionCheck from 'react-native-version-check';
import { useCallback, useEffect } from 'react';
import NavigationService from '../navigator/NavigationService';
import { ROUTES } from '../navigator/constants/routes';

const useVersionCheck = () => {
  const checkUpdateNeeded = useCallback(async () => {
    const updateNeeded = await VersionCheck.needUpdate();

    if (!__DEV__) {
      if (updateNeeded.isNeeded) {
        return NavigationService.navigateWithParams({
          routeName: ROUTES.VersionCheck,
          params: { storeUrl: updateNeeded.storeUrl },
        });
      }
    }
  }, []);

  useEffect(() => {
    checkUpdateNeeded();
  }, [checkUpdateNeeded]);

  return {};
};

export { useVersionCheck };
