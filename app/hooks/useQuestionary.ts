import { noop } from '../types/types';
import { useAppDispatch } from './useAppDispatch';
import { useSelector } from 'react-redux';
import { useCallback, useEffect, useState } from 'react';
import { ratedStatisticsType, TO_PERCENT } from '../constants';
import {
  calculateRateAgencyColor,
  calculateRatedItem,
  calculateRatedStatsColor,
} from '../functions/rating-agency/calculateStarRatingColor';
import {
  selectAgency,
  selectAgencyByIdAndType,
  selectAgencyType,
  selectWhoRatedAgency,
} from '../store/agency/selectors';
import { setModelsEarnedMoney, setRatedAgency } from '../store/rate-agency/reducer';
import { selectIsUpdateOnlyRating, selectLastRatedAgency } from '../store/rate-agency/selectors';
import { selectUserInfo } from '../store/auth/selectors';
import isUndefined from 'lodash/isUndefined';

type Response = {
  handleShowHint: noop;
  isVisible: boolean;
  setIsVisible: (flag: boolean) => void;
  rateItem: number;
  rateColor: string;
  isNext: boolean;
  handleToggleStar: ({ item, statisticType }: { item: number; statisticType: ratedStatisticsType }) => void;
  handleShowLastRatedAgencyAnswer: ({ statisticType }: { statisticType: ratedStatisticsType }) => void;
  handleToggleSideQuestion: ({ answer }: { answer: 'yes' | 'no' }) => void;
};

const useQuestionary = (): Response => {
  const dispatch = useAppDispatch();

  const user = useSelector(selectUserInfo);
  const type = useSelector(selectAgencyType);
  const ratedAgency = useSelector(selectAgencyByIdAndType);
  const agency = useSelector(selectAgency);
  const whoRatedAgency = useSelector(selectWhoRatedAgency({ userId: user.id }));
  const isUpdateOnlyRating = useSelector(selectIsUpdateOnlyRating);
  const lastRatedAgency = useSelector(selectLastRatedAgency({ type }));

  const [isNext, setNext] = useState<boolean>(false);
  const [rateItem, setRateItem] = useState<number>(0);
  const [rateColor, setRateColor] = useState<string>('');
  const [isVisible, setIsVisible] = useState<boolean>(false);

  const handleShowHint = useCallback(() => {
    setIsVisible(true);
  }, []);

  const handleToggleStar = useCallback(
    ({ item, statisticType }: { item: number; statisticType: ratedStatisticsType }) => {
      const color = calculateRateAgencyColor(item);
      setRateColor(color);
      setRateItem(item);

      const findRateCount = agency?.[type]?.statistics?.find((item) => item.type === statisticType)?.rate_count;

      const statistic = {
        [statisticType]: TO_PERCENT[item],
        rate_count:
          findRateCount < agency?.[type]?.rate_count || agency?.[type]?.rate_count === 0
            ? findRateCount + 1
            : findRateCount,
        type: statisticType, // need to use uniqueBy in reducer
      };
      dispatch(setRatedAgency({ type, statistic }));
    },
    [agency, dispatch, type],
  );

  const handleToggleSideQuestion = useCallback(
    ({ answer }: { answer: 'yes' | 'no'; statisticType: ratedStatisticsType }) => {
      const modelsEarnedMoneyCount = answer === 'yes' ? 1 : 0;

      // if not update
      const count = agency?.booking?.models_earned_money! + modelsEarnedMoneyCount;
      // console.log('count: ', count);
      // update if user already rated
      console.log('whoRatedAgency: ', whoRatedAgency);
      console.log(
        'res: ',
        whoRatedAgency?.models_earned_money === agency?.booking?.models_earned_money && answer === 'yes',
      );
      const updatedCount =
        answer === 'yes'
          ? agency?.booking?.models_earned_money! +
            (isUndefined(whoRatedAgency) ? 1 : whoRatedAgency.models_earned_money === 0 ? 1 : 0)
          : agency?.booking?.models_earned_money! -
            (isUndefined(whoRatedAgency) ? 0 : whoRatedAgency.models_earned_money === 0 ? 0 : 1);
      console.log('agency?.isRatedByCurrentUser: ', agency?.isRatedByCurrentUser);

      console.log('updatedCount: ', updatedCount);
      dispatch(setModelsEarnedMoney({ count: updatedCount! }));
    },
    [agency?.booking?.models_earned_money, agency?.isRatedByCurrentUser, dispatch, whoRatedAgency],
  );

  const handleShowLastRatedAgencyAnswer = useCallback(
    ({ statisticType }: { statisticType: ratedStatisticsType }) => {
      const findRateCount = agency?.[type]?.statistics?.find((item) => item.type === statisticType)?.rate_count;

      const statisticItem = whoRatedAgency?.[type]?.filter((item) => item.type === statisticType)[0];
      const rating = statisticItem?.[statisticType];

      if (rating) {
        setRateColor(calculateRatedStatsColor({ rating }));
        setRateItem(calculateRatedItem({ rating }));

        const statistic = {
          [statisticType]: TO_PERCENT[calculateRatedItem({ rating })],
          rate_count:
            findRateCount < agency?.[type]?.rate_count || agency?.[type]?.rate_count === 0
              ? findRateCount + 1
              : findRateCount,
          type: statisticType, // need to use uniqueBy in reducer
        };
        dispatch(setRatedAgency({ type, statistic }));
        //
        // const statistic = {
        //   [statisticType]: TO_PERCENT[calculateRatedItem({ rating })]
        // };

        // dispatch(setRatedAgency({ type, statistic }));
      }
    },
    [agency, dispatch, type, whoRatedAgency],
  );

  useEffect(() => setNext(!rateItem || !rateColor), [rateItem, rateColor]);

  return {
    handleShowHint,
    handleToggleStar,
    isNext,
    isVisible,
    setIsVisible,
    rateColor,
    rateItem,
    handleShowLastRatedAgencyAnswer,
    handleToggleSideQuestion,
  };
};

export { useQuestionary };
