import { useAppBackHandler } from 'hooks/index';
import { useCallback } from 'react';
import { IOnMove } from 'react-native-image-pan-zoom/src/image-zoom/image-zoom.type';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';

const POSITION_TO_CLOSE = 150;
const STOP_CLOSING = 1;

type Arguments = {
  uri?: string | undefined;
};

type Response = {
  uri: string;
  handleClose: ({ scale, positionY }: IOnMove) => void;
  handleOpenImage: ({ uri }: { uri: string }) => void;
};

export const useImageFullScreen = ({ uri }: Arguments): Response => {
  useAppBackHandler();

  const handleClose = useCallback(({ scale, positionY }: IOnMove) => {
    if (scale > STOP_CLOSING) {
      return;
    }

    if (positionY > POSITION_TO_CLOSE || positionY < -POSITION_TO_CLOSE) {
      NavigationService.navigateBack();
    }
  }, []);

  const handleOpenImage = useCallback(({ uri: uriImage }: { uri: string }) => {
    NavigationService.navigateWithParams({
      routeName: ROUTES.ImageFullScreen,
      params: { uri: uriImage },
    });
  }, []);

  return {
    uri: uri!,
    handleClose,
    handleOpenImage,
  };
};
