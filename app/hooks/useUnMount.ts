import { useEffect } from 'react';
import { noop } from 'types/types';

const useUnMount = (cb: noop) => {
  useEffect(() => {
    return () => {
      cb();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};

export { useUnMount };
