import { ROUTES } from '../navigator/constants/routes';

export const useDeepLinking = () => {
  return {
    config: {
      prefixes: ['modelsider://'],
      config: {
        screens: {
          [ROUTES.Home]: {
            screens: {
              [ROUTES.News]: 'news',
            },
          },
          [ROUTES.CommentScreenDeeplink]: 'comment/:agencyId/:commentId/:type', // liked comment, bookmarked comment
          [ROUTES.CommentScreen]: 'reply/:agencyId/:commentId/:replyId/:type', // liked, left a reply to bookmarked comment
          [ROUTES.Agency]: 'bookmarked_agency_comment/:id/:type', // left a comment to bookmarked agency and scroll to the comment section
          [ROUTES.SignUpApprovedStatus]: 'application_approved',
        },
      },
    },
  };
};
