import { useEffect, useRef } from 'react';
import { Animated } from 'react-native';

export const usePulseAnimation = ({ from = 1, to = 0.3, duration = 750 }) => {
  const animValue = useRef<Animated.Value>(new Animated.Value(from)).current;

  useEffect(() => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(animValue, {
          toValue: to,
          duration,
          useNativeDriver: true,
        }),
        Animated.timing(animValue, {
          toValue: from,
          duration,
          useNativeDriver: true,
        }),
      ]),
    ).start();

    return () => {
      animValue.stopAnimation();
    };
  }, [animValue, duration, from, to]);

  return animValue;
};
