import { useDispatch } from 'react-redux';
import { DispatchType } from 'store/types/store';

export const useAppDispatch = () => useDispatch<DispatchType>();
