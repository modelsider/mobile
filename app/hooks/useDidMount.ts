import { useEffect } from 'react';
import { noop } from '../types/types';

/*
  Runs only on mount
  Does not run ony an subsequent changes to `cb`

  Use it only when you want the effect to run once,
  with the initially "captured" state
*/
const useDidMount = (cb: noop) => {
  useEffect(() => {
    cb();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};

export { useDidMount };
