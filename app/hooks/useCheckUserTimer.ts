import { clearStorage, getUserId } from 'services/asyncStorage';
import NavigationService from 'navigator/NavigationService';
import { useAppDispatch } from './useAppDispatch';
import { useCallback, useEffect, useState } from 'react';
import { signOut } from 'store/auth/reducer';
import isEmpty from 'lodash/isEmpty';
import { getUser } from '../services/users';
import auth from "@react-native-firebase/auth";

const INTERVAL = 10000;

// todo: deprecated
const useCheckUserTimer = () => {
  const dispatch = useAppDispatch();

  const [isClear, setIsClear] = useState<boolean>(false);

  const checkUser = useCallback(() => {
    const checkingIfUserExist = setInterval(async () => {
      const userId = await getUserId();
      const user = await getUser({ id: userId });

      if (isEmpty(user)) {
        setIsClear(true);
        dispatch(signOut());
        await auth().signOut();
        await clearStorage();
        NavigationService.resetToLogin();
      }
    }, INTERVAL);

    if (isClear) {
      return clearInterval(checkingIfUserExist);
    }
    return null;
  }, [dispatch, isClear]);

  useEffect(() => {
    checkUser();
  }, [checkUser]);
};

export { useCheckUserTimer };
