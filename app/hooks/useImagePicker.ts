import ImagePicker from 'react-native-image-crop-picker';
import { useCallback } from 'react';

const config = {
  cropping: false,
  useFrontCamera: false,
  includeBase64: true,
  includeExif: false,
  freeStyleCropEnabled: false,
  multiple: true,
  maxFiles: 6,
  compressImageMaxWidth: 1000,
  compressImageMaxHeight: 1000,
  compressImageQuality: 0.9
};

type Response = {
  getImage: () => Promise<string[]>;
};

export const useImagePicker = (): Response => {
  const getImage = useCallback(async (): Promise<string[]> => {
    try {
      const response: any = await ImagePicker.openPicker(config);

      let images: any[] = []; // todo: add current interface

      for (const image of response) {
        images.push(image.data);
      }

      return images;
    } catch (e) {
      console.log('getImage error: ', e);
      throw e;
    }
  }, []);

  return {
    getImage,
  };
};
