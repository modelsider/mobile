import { useAppDispatch } from 'hooks/index';
import { useSelector } from 'react-redux';
import { useCallback, useEffect, useState } from 'react';
import { noop } from 'types/types';
import { selectUserProfile } from 'store/users/selectors';
import { setUserProfileById } from 'store/users/actions';
import { IUserInfo } from 'store/auth/reducer';
import { RouteProp, useRoute } from '@react-navigation/native';
import { setSelectedUserProfileId } from '../store/users/reducer';

type Response = {
  userProfile: IUserInfo;
  handleFetchUserProfile: noop;
  isFetch: boolean;
};

const useUserProfile = (): Response => {
  const {
    params: { userId },
  } = useRoute<
    RouteProp<{
      params: {
        userId: string;
      };
    }>
  >();

  const dispatch = useAppDispatch();
  const userProfile = useSelector(selectUserProfile);

  const [isFetch, setIsFetch] = useState(false);

  const handleFetchUserProfile = useCallback(async () => {
    setIsFetch(true);
    await dispatch(setUserProfileById({ id: userId }));
    await dispatch(setSelectedUserProfileId({ userId }));
    setIsFetch(false);
  }, [dispatch, userId]);

  useEffect(() => {
    handleFetchUserProfile();
    return () => {
      dispatch(setSelectedUserProfileId({ userId: null }));
    };
  }, [dispatch, handleFetchUserProfile]);

  return {
    userProfile,
    handleFetchUserProfile,
    isFetch
  };
};

export { useUserProfile };
