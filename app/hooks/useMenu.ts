import { clearStorage } from 'services/asyncStorage';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { useAppDispatch } from './useAppDispatch';
import { useCallback } from 'react';
import { noop } from 'types/types';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { clearUserInfoState } from 'store/auth/reducer';
import { alert } from 'components/alerts';
import { deleteUser } from 'services/users';
// @ts-ignore
import { REPORT_A_BUG_LINK } from '@env';
import { InAppBrowser } from 'react-native-inappbrowser-reborn';
import auth from '@react-native-firebase/auth';
import Toast from 'react-native-toast-message';
import { setGlobalLoader, setIfRating } from 'store/app/reducer';
import { clearBookmarks } from 'store/bookmarks/reducer';
import { resetCommentsState } from 'store/comments/reducer';
import { resetFeed, resetRatings } from 'store/feed/reducer';
import { resetRatedAgency } from 'store/rate-agency/reducer';
import { resetUserProfileStore } from 'store/users/reducer';

type Response = {
  handleNavigate: ({ routeName }: { routeName: string }) => void;
  handleSignOut: noop;
  handleDeleteAccount: noop;
};

const useMenu = (): Response => {
  const dispatch = useAppDispatch();

  const { docId } = useSelector(selectUserInfo);

  const handleNavigate = useCallback(async ({ routeName }: { routeName: string }) => {
    if (routeName === ROUTES.ReportBug) {
      if (await InAppBrowser.isAvailable()) {
        await InAppBrowser.open(REPORT_A_BUG_LINK, {
          modalEnabled: false,
        });
      }
    } else {
      NavigationService.navigate({ routeName });
    }
  }, []);

  const handleSignOut = useCallback(() => {
    alert({
      message: 'Are you sure you would like to Sign out?',
      onConfirm: async () => {
        dispatch(resetFeed());
        dispatch(resetRatings());
        dispatch(clearBookmarks());
        dispatch(resetRatedAgency());
        dispatch(resetUserProfileStore());
        dispatch(resetCommentsState());
        dispatch(clearUserInfoState());
        dispatch(setIfRating({ hasRating: null }));
        await auth().signOut();
        await clearStorage();
        return NavigationService.resetToLogin();
      },
    });
  }, [dispatch]);

  const handleDeleteAccount = useCallback(() => {
    alert({
      message:
        'Are you sure you would like to Delete your account? \n\n Please note all your feedback remains. You may delete it manually if needed.',
      onConfirm: async () => {
        dispatch(setGlobalLoader({ isLoading: true }));
        const isDeleted = await deleteUser({ docId });
        if (isDeleted) {
          dispatch(clearUserInfoState());
          dispatch(setIfRating({ hasRating: null }));
          await clearStorage();
          dispatch(setGlobalLoader({ isLoading: false }));
          return NavigationService.resetToLogin();
        } else {
          dispatch(setGlobalLoader({ isLoading: false }));
          Toast.show({
            text1: 'Failed to complete the operation. Please try again later.',
            type: 'warning',
          });
        }
      },
    });
  }, [dispatch, docId]);

  return {
    handleNavigate,
    handleSignOut,
    handleDeleteAccount,
  };
};

export { useMenu };
