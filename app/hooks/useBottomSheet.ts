import { useCallback } from 'react';
import { showActionSheet } from '../utils/showActionSheet';
import { BOTTOM_SHEET_TITLES, bottomSheetConfig } from '../config/bottomSheetConfig';
import { setIsUpdateDates, setIsUpdateOnlyRating } from '../store/rate-agency/reducer';
import NavigationService from '../navigator/NavigationService';
import { ROUTES } from '../navigator/constants/routes';
import { useSelector } from 'react-redux';
import { selectAgencyType } from '../store/agency/selectors';
import { useAppDispatch } from './useAppDispatch';
import { alert } from 'components/alerts';
import { useAgencyRating } from './useAgencyRating';
import { setAgency } from 'store/agency/actions';
import { selectUserInfo } from 'store/auth/selectors';
import { IComment, IPost, IReply, noop } from '../types/types';
import { selectAgencyType as setAgencyType, setAgencyId } from '../store/agency/reducer';
import { setClickedCommentIndex } from 'store/comments/reducer';
import { deleteComment, setReportComment } from 'store/comments/actions';
import { useAgencies } from './useAgencies';
import Toast from 'react-native-toast-message';
import { setGlobalLoader } from 'store/app/reducer';
import { deleteReplyFB } from 'services/replies';
import { selectComment } from 'store/comments/selectors';

const ACTION_SHEET_TITLE = 'Please choose your action';
const {
  agency: { updateRating, updateDates, deleteRating, message },
  comment: { agencyProfile, remove },
  reportComment: { report },
  attachments: { deleteImage },
  post: { edit },
} = BOTTOM_SHEET_TITLES;

const useBottomSheet = () => {
  const dispatch = useAppDispatch();
  const { id } = useSelector(selectUserInfo);
  const type = useSelector(selectAgencyType);
  const user = useSelector(selectUserInfo);
  const currentComment = useSelector(selectComment);

  const { deleteRatingFromCollections } = useAgencyRating();
  const { handleNavigateToAgency } = useAgencies();

  const handleAgencySheet = useCallback(
    async ({ agencyId }: { agencyId: number }) => {
      dispatch(setGlobalLoader({ isLoading: true }));
      await dispatch(setAgencyId({ agencyId }));
      await dispatch(setAgency({ agencyId, userId: id, type }));
      dispatch(setGlobalLoader({ isLoading: false }));

      const options = bottomSheetConfig.agency.map((option) => {
        if (option.text === updateRating) {
          return {
            ...option,
            onPress: () => {
              dispatch(setIsUpdateOnlyRating({ isUpdate: true }));
              NavigationService.navigate({
                routeName: ROUTES.ProfessionalScreen,
              });
            },
          };
        } else if (option.text === updateDates) {
          return {
            ...option,
            onPress: () => {
              dispatch(setIsUpdateDates({ isUpdate: true }));
              NavigationService.navigate({
                routeName: ROUTES.RateStartWorking,
              });
            },
          };
        } else if (option.text === deleteRating) {
          return {
            ...option,
            onPress: () => {
              alert({
                message,
                onConfirm: async () => {
                  const { payload } = await dispatch(setAgency({ userId: user.id!, agencyId, type }));
                  await deleteRatingFromCollections({
                    // todo: fix
                    // @ts-ignore
                    agencyToRemove: payload,
                  });
                },
              });
            },
          };
        }

        return option;
      });

      await showActionSheet({
        title: ACTION_SHEET_TITLE,
        options,
      });
    },
    [deleteRatingFromCollections, dispatch, id, type, user.id],
  );

  const handleCommentSheet = useCallback(
    async ({
      comment,
      parentComment,
      isNavigateBack = false,
      onRefresh = () => {},
    }: {
      comment: IComment | IReply;
      parentComment?: IComment;
      isNavigateBack?: boolean;
      onRefresh?: noop;
    }) => {
      const isReply = comment?.type === 'Reply';
      const config = isReply ? bottomSheetConfig.reply : bottomSheetConfig.comment;

      const options = config.map((option) => {
        if (option.text === agencyProfile) {
          return {
            ...option,
            onPress: async () => {
              await dispatch(setAgencyType({ type: comment?.agency_type! }));
              handleNavigateToAgency({ agencyId: comment?.agency_id! });
            },
          };
        } else if (option.text === remove) {
          return {
            ...option,
            onPress: async () => {
              if (isReply) {
                await deleteReplyFB({
                  commentDocId: parentComment?.docId!,
                  replyDocId: comment?.docId!,
                });
              } else {
                setClickedCommentIndex({ commentIndex: 0 });
                await dispatch(deleteComment({ docId: comment.docId! }));
              }

              await onRefresh();
              if (isNavigateBack) {
                NavigationService.navigateBack();
              }
            },
          };
        } else if (option.text === edit) {
          return {
            ...option,
            onPress: async () => {
              const routeName = isReply ? ROUTES.LeaveReplyScreen : ROUTES.LeaveCommentScreen;
              const params = isReply ? { reply: comment } : {};

              NavigationService.navigateWithParams({
                routeName,
                params: { isEdit: true, ...params },
              });
            },
          };
        }

        return option;
      });

      await showActionSheet({
        title: ACTION_SHEET_TITLE,
        options,
      });
    },
    [dispatch, handleNavigateToAgency],
  );

  const handleReportCommentSheet = useCallback(
    async ({
      comment,
      onRefresh = () => {},
      isNavigateBack = false,
    }: {
      comment: IComment | IReply;
      onRefresh?: noop;
      isNavigateBack?: boolean;
    }) => {
      const isReply = comment?.type === 'Reply';
      const config = isReply ? bottomSheetConfig.reportReply : bottomSheetConfig.reportComment

      const options = config.map((option) => {
        if (option.text === agencyProfile) {
          return {
            ...option,
            onPress: async () => {
              await dispatch(setAgencyType({ type: comment?.agency_type! }));
              handleNavigateToAgency({ agencyId: comment?.agency_id! });
            },
          };
        } else if (option.text === report) {
          return {
            ...option,
            onPress: async () => {
              await dispatch(
                setReportComment({
                  userIdWhoReported: user.id,
                  userId: comment?.user_id!,
                  commentId: comment?.id!,
                }),
              );
              await onRefresh();

              Toast.show({
                text1: 'Thank you! We will investigate that.', // todo: create constants
              });

              if (isNavigateBack) {
                NavigationService.navigateBack();
              }
            },
          };
        }

        return option;
      });

      await showActionSheet({
        title: ACTION_SHEET_TITLE,
        options,
      });
    },
    [dispatch, handleNavigateToAgency, user.id],
  );

  const handleReportPostSheet = useCallback(
    async ({
      post,
      onRefresh = () => {},
      isNavigateBack = false,
    }: {
      post: IPost;
      onRefresh?: noop;
      isNavigateBack?: boolean;
    }) => {
      const options = bottomSheetConfig.reportPost.map((option) => {
        if (option.text === report) {
          return {
            ...option,
            onPress: async () => {
              await dispatch(
                setReportComment({
                  userIdWhoReported: user.id,
                  userId: post?.user_id!,
                  commentId: post?.id!,
                }),
              );
              await onRefresh();

              Toast.show({
                text1: 'Thank you! We will investigate that.', // todo: create constants
              });

              if (isNavigateBack) {
                NavigationService.navigateBack();
              }
            },
          };
        }

        return option;
      });

      await showActionSheet({
        title: ACTION_SHEET_TITLE,
        options,
      });
    },
    [dispatch, user.id],
  );

  const handleAttachmentsSheet = useCallback(async ({ onRemove }: { onRemove: ({ uri }: { uri: string }) => void }) => {
    const options = bottomSheetConfig.attachments.map((option) => {
      if (option.text === deleteImage) {
        return {
          ...option,
          onPress: onRemove,
        };
      }
      return option;
    });

    await showActionSheet({
      title: ACTION_SHEET_TITLE,
      options,
    });
  }, []);

  const handlePostSheet = useCallback(
    async ({
      post,
      onRefresh = () => {},
      isNavigateBack = false,
    }: {
      post: IPost;
      onRefresh?: noop;
      isNavigateBack?: boolean;
    }) => {
      const options = bottomSheetConfig.post.map((option) => {
        if (option.text === remove) {
          return {
            ...option,
            onPress: async () => {
              await dispatch(deleteComment({ docId: post.docId! }));
              if (isNavigateBack) {
                return NavigationService.navigateBack();
              }
              await onRefresh();
            },
          };
        } else if (option.text === edit) {
          return {
            ...option,
            onPress: () => {
              NavigationService.navigateWithParams({
                routeName: ROUTES.LeavePost,
                params: { isEdit: true, post },
              });
            },
          };
        }

        return option;
      });

      await showActionSheet({
        title: ACTION_SHEET_TITLE,
        options,
      });
    },
    [dispatch],
  );

  return {
    handleAgencySheet,
    handleCommentSheet,
    handleReportCommentSheet,
    handleAttachmentsSheet,
    handlePostSheet,
    handleReportPostSheet,
  };
};

export { useBottomSheet };
