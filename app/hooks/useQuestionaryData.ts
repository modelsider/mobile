import { IQuestionaryData } from '../types/types';
import { useSelector } from 'react-redux';
import { selectAgencyType } from '../store/agency/selectors';
import { HINT_DATA, numberOfQuestionBooking, numberOfQuestionMother, ratedStatisticsType } from '../constants';
import NavigationService from '../navigator/NavigationService';
import { ROUTES } from '../navigator/constants/routes';
import { useAppDispatch } from './useAppDispatch';
import { selectDotsStateCount } from '../store/rate-agency/selectors';

type Arguments = {
  statisticType: ratedStatisticsType;
};

type Response = {
  data: IQuestionaryData;
};

const useQuestionaryData = ({ statisticType }: Arguments): Response => {
  const dispatch = useAppDispatch();
  const type = useSelector(selectAgencyType);
  const checkDotsState = useSelector(selectDotsStateCount);

  let data: IQuestionaryData;

  if (type === 'mother') {
    if (statisticType === 'professionalism') {
      data = {
        description: 'How professional and responsible the mother agency work-wise?',
        advantages: HINT_DATA.MOTHER.professionalism.advantages,
        disadvantages: HINT_DATA.MOTHER.professionalism.disadvantages,
        index: numberOfQuestionMother[statisticType],
        statisticType,
        navigateToNext: () => {
          return NavigationService.navigate({
            routeName: ROUTES.RespectfulScreen,
          });
        },
      };
    } else if (statisticType === 'respect_and_care') {
      data = {
        description: 'How respectful and caring the mother agency is?',
        advantages: HINT_DATA.MOTHER.respectful.advantages,
        disadvantages: HINT_DATA.MOTHER.respectful.disadvantages,
        index: numberOfQuestionMother[statisticType],
        statisticType,
        navigateToNext: () => {
          return NavigationService.navigate({
            routeName: ROUTES.SatisfiedScreen,
          });
        },
      };
    } else if (statisticType === 'career_development') {
      data = {
        description: 'How satisfied are you with the career development built by the mother agency?',
        advantages: HINT_DATA.MOTHER.satisfied.advantages,
        disadvantages: HINT_DATA.MOTHER.satisfied.disadvantages,
        index: numberOfQuestionMother[statisticType],
        statisticType,
        navigateToNext: () => {
          return NavigationService.navigate({
            routeName: ROUTES.PayScreen,
          });
        },
      };
    } else if (statisticType === 'pay') {
      data = {
        description: 'Did the mother agency pay you directly for jobs?',
        index: numberOfQuestionMother[statisticType],
        statisticType,
        navigateToYes: () => {
          return NavigationService.navigate({
            routeName: ROUTES.ResponsibleScreen,
          });
        },
        navigateToNo: () => {
          return NavigationService.navigate({
            routeName: ROUTES.PortfolioScreen,
          });
        },
      };
    } else if (statisticType === 'payments') {
      data = {
        description: 'How responsible was the mother agency with payments?',
        advantages: HINT_DATA.MOTHER.responsible.advantages,
        disadvantages: HINT_DATA.MOTHER.responsible.disadvantages,
        index: numberOfQuestionMother[statisticType],
        statisticType,
        navigateToNext: () => {
          return NavigationService.navigate({
            routeName: ROUTES.PortfolioScreen,
          });
        },
      };
    } else if (statisticType === 'portfolio_work_side') {
      data = {
        description: 'Did you need help with building or improving the portfolio?',
        index: numberOfQuestionMother[statisticType],
        statisticType,
        navigateToYes: () => {
          return NavigationService.navigate({
            routeName: ROUTES.HelpfulScreen,
          });
        },
        navigateToNo: () => {
          return NavigationService.navigate({
            routeName: ROUTES.ExpensesScreen,
          });
        },
      };
    } else if (statisticType === 'happy_to_work_with') {
      data = {
        description: 'How likely would you work with the mother agency again?',
        advantages: HINT_DATA.MOTHER.fair.advantages,
        disadvantages: HINT_DATA.MOTHER.fair.disadvantages,
        index: numberOfQuestionMother[statisticType],
        statisticType,
        navigateToNext: () => {
          return NavigationService.navigate({
            routeName: ROUTES.FinishScreen,
          });
        },
      };
    } else if (statisticType === 'portfolio_work') {
      data = {
        description: 'How helpful was the mother agency with building or improving your portfolio?',
        advantages: HINT_DATA.MOTHER.helpful.advantages,
        disadvantages: HINT_DATA.MOTHER.helpful.disadvantages,
        index: numberOfQuestionMother[statisticType],
        statisticType,
        navigateToNext: () => {
          return NavigationService.navigate({
            routeName: ROUTES.ExpensesScreen,
          });
        },
      };
    } else if (statisticType === 'fair_expenses') {
      data = {
        description: 'How fair were those expenses?',
        advantages: HINT_DATA.MOTHER.fair.advantages,
        disadvantages: HINT_DATA.MOTHER.fair.disadvantages,
        index: numberOfQuestionMother[statisticType],
        statisticType,
        navigateToNext: () => {
          return NavigationService.navigate({
            routeName: ROUTES.LikelyScreen,
          });
        },
      };
    } else if (statisticType === 'expenses') {
      data = {
        description: 'Did the mother agency charge you for any expenses?',
        index: numberOfQuestionMother[statisticType],
        navigateToYes: () => {
          return NavigationService.navigate({
            routeName: ROUTES.FairScreen,
          });
        },
        navigateToNo: () => {
          return NavigationService.navigate({
            routeName: ROUTES.LikelyScreen,
          });
        },
      };
    }
  }

  if (type === 'booking') {
    if (statisticType === 'payments') {
      data = {
        description: 'How responsible was the booking agency with payments?',
        advantages: HINT_DATA.BOOKING.responsible.advantages,
        disadvantages: HINT_DATA.BOOKING.responsible.disadvantages,
        index: numberOfQuestionBooking[statisticType],
        statisticType,
        navigateToNext: () => {
          return NavigationService.navigate({
            routeName: ROUTES.PortfolioScreen,
          });
        },
      };
    } else if (statisticType === 'respect_and_care') {
      data = {
        description: 'How respectful and caring the booking agency is?',
        advantages: HINT_DATA.BOOKING.respectful.advantages,
        disadvantages: HINT_DATA.BOOKING.respectful.disadvantages,
        index: numberOfQuestionBooking[statisticType],
        statisticType,
        navigateToNext: () => {
          return NavigationService.navigate({
            routeName: ROUTES.AccommodationScreen,
          });
        },
      };
    } else if (statisticType === 'professionalism') {
      data = {
        description: 'How professional and responsible the booking agency work-wise?',
        advantages: HINT_DATA.BOOKING.professionalism.advantages,
        disadvantages: HINT_DATA.BOOKING.professionalism.disadvantages,
        index: numberOfQuestionBooking[statisticType],
        statisticType,
        navigateToNext: () => {
          return NavigationService.navigate({
            routeName: ROUTES.RespectfulScreen,
          });
        },
      };
    } else if (statisticType === 'portfolio_work_side') {
      data = {
        description: 'Did you need help with building or improving the portfolio?',
        index: numberOfQuestionBooking[statisticType],
        navigateToYes: () => {
          return NavigationService.navigate({
            routeName: ROUTES.HelpfulScreen,
          });
        },
        navigateToNo: () => {
          return NavigationService.navigate({
            routeName: ROUTES.ExpensesScreen,
          });
        },
      };
    } else if (statisticType === 'models_earned_money') {
      data = {
        description: 'Did you earn money excluding guarantee payment?',
        index: numberOfQuestionBooking[statisticType],
        statisticType,
        navigateToYes: () => {
          return NavigationService.navigate({
            routeName: ROUTES.ResponsibleScreen,
          });
        },
        navigateToNo: () => {
          return NavigationService.navigate({
            routeName: ROUTES.PortfolioScreen,
          });
        },
      };
    } else if (statisticType === 'happy_to_work_with') {
      data = {
        description: 'How likely would you work with the booking agency again?',
        advantages: HINT_DATA.BOOKING.fair.advantages,
        disadvantages: HINT_DATA.BOOKING.fair.disadvantages,
        index: numberOfQuestionBooking[statisticType],
        statisticType,
        navigateToNext: () => {
          return NavigationService.navigate({
            routeName: ROUTES.FinishScreen,
          });
        },
      };
    } else if (statisticType === 'portfolio_work') {
      data = {
        description: 'How helpful was the booking agency with building or improving your portfolio?',
        advantages: HINT_DATA.BOOKING.helpful.advantages,
        disadvantages: HINT_DATA.BOOKING.helpful.disadvantages,
        index: numberOfQuestionBooking[statisticType],
        statisticType,
        navigateToNext: () => {
          return NavigationService.navigate({
            routeName: ROUTES.ExpensesScreen,
          });
        },
      };
    } else if (statisticType === 'fair_expenses') {
      data = {
        description: 'How fair were those expenses?',
        advantages: HINT_DATA.BOOKING.fair.advantages,
        disadvantages: HINT_DATA.BOOKING.fair.disadvantages,
        index: numberOfQuestionBooking[statisticType],
        statisticType,
        navigateToNext: () => {
          return NavigationService.navigate({
            routeName: ROUTES.LikelyScreen,
          });
        },
      };
    } else if (statisticType === 'expenses') {
      data = {
        description: 'Did the booking agency charge you for any expenses?',
        index: numberOfQuestionBooking[statisticType],
        navigateToYes: () => {
          return NavigationService.navigate({
            routeName: ROUTES.FairScreen,
          });
        },
        navigateToNo: () => {
          return NavigationService.navigate({
            routeName: ROUTES.LikelyScreen,
          });
        },
      };
    } else if (statisticType === 'apartments_condition') {
      data = {
        description: 'How good were living conditions in the apartment?',
        advantages: HINT_DATA.BOOKING.apartment.advantages,
        disadvantages: HINT_DATA.BOOKING.apartment.disadvantages,
        index: numberOfQuestionBooking[statisticType],
        statisticType,
        navigateToNext: () => {
          return NavigationService.navigate({
            routeName: ROUTES.PaymentScreen,
          });
        },
      };
    } else if (statisticType === 'accommodation') {
      data = {
        description: 'Did the booking agency provide you with accommodation?',
        index: numberOfQuestionBooking[statisticType],
        navigateToYes: () => {
          return NavigationService.navigate({
            routeName: ROUTES.ApartmentScreen,
          });
        },
        navigateToNo: () => {
          return NavigationService.navigate({
            routeName: ROUTES.PaymentScreen,
          });
        },
      };
    }
  }

  return {
    data: data!,
  };
};

export { useQuestionaryData };
