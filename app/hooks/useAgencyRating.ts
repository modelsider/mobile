import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { selectAgency, selectAgencyById, selectAgencyId, selectAgencyType } from 'store/agency/selectors';
import add from 'lodash/add';
import divide from 'lodash/divide';
import isUndefined from 'lodash/isUndefined';
import sumBy from 'lodash/sumBy';
import { selectLastRatedAgency, selectModelsEarnedMoney } from '../store/rate-agency/selectors';
import { IAgency } from '../types/types';
import { useCallback, useMemo } from 'react';
import {
  addWhoRatedAgency,
  removeWhoRatedAgency,
  setRatedAgencies,
  updateRatedAgency,
  updateWhoRatedAgency,
} from '../store/agency/actions';
import { useAppDispatch } from './useAppDispatch';
import { calculateRatedItem } from '../functions/rating-agency/calculateStarRatingColor';
import Toast from 'react-native-toast-message';
import { setGlobalLoader } from '../store/app/reducer';
import { templatesStatistics } from '../constants';

type Response = {
  updateCollections: () => Promise<boolean>;
  deleteRatingFromCollections: ({ agencyToRemove }: { agencyToRemove: IAgency }) => void;
};

const useAgencyRating = (): Response => {
  const dispatch = useAppDispatch();

  const { id } = useSelector(selectUserInfo);
  const type = useSelector(selectAgencyType);
  const agency = useSelector(selectAgency);
  const agencyId = useSelector(selectAgencyId);
  const lastRatedAgency = useSelector(selectLastRatedAgency({ type }));
  const modelsEarnedMoney = useSelector(selectModelsEarnedMoney);
  const agencyById = useSelector(selectAgencyById);

  const agencyRatingFromCurrentUser = useMemo(
    () => agency?.agencyRatingFromCurrentUser,
    [agency?.agencyRatingFromCurrentUser],
  );
  const isRatedOnlyCurrentUser = useMemo(() => {
    return agency?.isRatedByCurrentUser && agency?.[type]?.rate_count === 1;
  }, [agency, type]);
  const rate_count = useMemo(() => {
    return isRatedOnlyCurrentUser || agency?.isRatedByCurrentUser
      ? agency?.[type]?.rate_count
      : agency!?.[type]?.rate_count + 1;
  }, [agency, isRatedOnlyCurrentUser, type]);

  const currentAgencyStatistics = useMemo(() => agency?.[type]?.statistics, [agency, type]);

  const getDataToDeleteAgencyRatingOfCurrentUser = useCallback(
    ({ agencyToRemove }: { agencyToRemove: IAgency }) => {
      const agencyByType = agencyToRemove?.[type];

      if (isUndefined(agencyToRemove)) {
        Toast.show({
          text1: 'Something went wrong. Please try again later.',
          type: 'warning',
        });
        return;
      }

      const currentUserAgencyStatistics = agencyToRemove?.agencyRatingFromCurrentUser?.[type];

      const currentAgencyStatistics = agencyByType?.statistics;
      const statisticKeys = currentAgencyStatistics?.map((statisticItem) => statisticItem.type);

      const currentUserAnswersInStars = currentUserAgencyStatistics?.map((answer) => {
        const rating = answer[answer.type];

        if (rating === 0) {
          return 0;
        }
        return calculateRatedItem({ rating });
      });

      let data;

      // check if only current user rate agency
      if (agencyByType?.rate_count === 1 && currentAgencyStatistics?.length) {
        const statistics =
          type === 'mother'
            ? [
                {
                  career_development: 0,
                  rate_count: 0,
                  type: 'career_development',
                },
                { fair_expenses: 0, rate_count: 0, type: 'fair_expenses' },
                {
                  happy_to_work_with: 0,
                  rate_count: 0,
                  type: 'happy_to_work_with',
                },
                { payments: 0, rate_count: 0, type: 'payments' },
                { portfolio_work: 0, rate_count: 0, type: 'portfolio_work' },
                { professionalism: 0, rate_count: 0, type: 'professionalism' },
                { respect_and_care: 0, rate_count: 0, type: 'respect_and_care' },
              ]
            : [
                {
                  apartments_condition: 0,
                  rate_count: 0,
                  type: 'apartments_condition',
                },
                { fair_expenses: 0, rate_count: 0, type: 'fair_expenses' },
                {
                  happy_to_work_with: 0,
                  rate_count: 0,
                  type: 'happy_to_work_with',
                },
                { payments: 0, rate_count: 0, type: 'payments' },
                { portfolio_work: 0, rate_count: 0, type: 'portfolio_work' },
                { professionalism: 0, rate_count: 0, type: 'professionalism' },
                { respect_and_care: 0, rate_count: 0, type: 'respect_and_care' },
              ]; // todo: add for booking

        const models_earned_money =
          agencyToRemove?.booking?.models_earned_money !== 0
            ? agencyToRemove?.booking?.models_earned_money! - 1
            : agencyToRemove?.booking?.models_earned_money;

        data = {
          [type]: {
            statistics,
            total_rating: 0,
            rate_count: 0,
            is_rated: false,
            models_earned_money: models_earned_money < 0 ? 0 : models_earned_money,
          },
        };
      } else {
        // 68 (current average1) / 20 * 5 (current amount of users) = 17( current sum of all responses)
        const currentSumOfAllAnswers = currentAgencyStatistics.map((answer) => {
          const rating = answer[answer.type];
          return (rating / 20) * answer.rate_count!; // todo: herer
        });
        //
        // 17( current sum of all responses) - 4⭐⭐⭐⭐(current user3 response) = 13 (new sum of all responses)
        const newSumAllAnswers = currentSumOfAllAnswers.map((answer, index) => {
          return answer - currentUserAnswersInStars[index]!;
        });
        //

        // result
        const newCurrentRate = agencyByType?.rate_count! - 1;

        // 13 (new sum of all responses) / 4 (new amount of users) *20 = 66 (new average1)
        const newStatistics = newSumAllAnswers.map((answer, index) => {
          const currentAgencyStatisticItem = currentUserAgencyStatistics?.find(
            ({ type }) => type === statisticKeys[index],
          );

          const currentAgencyRateCount = currentAgencyStatistics.find(
            (agencyItem) => agencyItem.type === statisticKeys[index],
          )?.rate_count;

          const rate_count = currentAgencyStatisticItem?.is_rated ? currentAgencyRateCount - 1 : currentAgencyRateCount;

          return {
            type: currentAgencyStatisticItem.type,
            [currentAgencyStatisticItem.type]: currentUserAgencyStatistics[currentAgencyStatisticItem.type],
            rate_count,
            [statisticKeys[index]]: (answer / newCurrentRate!) * 20,
          };
        });
        //

        const newTotalRating = +(
          sumBy(newStatistics.map((item) => item[item.type])) /
          newStatistics.filter((item) => item[item.type]).length /
          20
        ).toFixed(1);

        const models_earned_money =
          agencyToRemove?.booking?.models_earned_money! -
          agencyToRemove?.agencyRatingFromCurrentUser?.models_earned_money!;

        data = {
          [type]: {
            statistics: newStatistics.map((item) => ({
              ...item,
              [item.type]: +item[item.type].toFixed(0),
            })),
            total_rating: newTotalRating,
            rate_count: newCurrentRate,
            is_rated: agencyToRemove?.[type]?.is_rated || true,
            models_earned_money: models_earned_money < 0 ? 0 : models_earned_money,
          },
        };
      }
      console.log('data: ', data);
      return data;
    },
    [type],
  );

  const updateCollections = useCallback(async (): Promise<boolean | undefined> => {
    // if only first user rated agency
    if (!agency?.[type].is_rated || isRatedOnlyCurrentUser) {
      console.log('if only one user rated agency');

      const statistics = currentAgencyStatistics?.map((statisticItem) => {
        const rated = lastRatedAgency?.[type].find((item) => item.type === statisticItem.type);

        if (isUndefined(rated)) {
          const template = templatesStatistics[type].find((item) => item.type === statisticItem.type);
          return template;
        }

        return rated;
      });
      // total rating calculation
      const statisticsSum = sumBy(statistics?.map((answer) => answer[answer.type]));
      const statisticsLength = statistics?.filter((item) => item.rate_count).length;

      const total_rating = +(statisticsSum / statisticsLength! / 20).toFixed(1);
      // total rating calculation

      const updatedAgencyRating = {
        [type]: {
          statistics,
          total_rating,
          rate_count,
          is_rated: agency?.[type]?.is_rated || true,
          models_earned_money: modelsEarnedMoney < 0 ? 0 : modelsEarnedMoney,
        },
      };

      const updatedWhoRatedAgency = {
        ...lastRatedAgency,
        [type]: statistics?.map((item) => {
          return {
            [item.type]: +item[item.type].toFixed(0),
            is_rated: item[item.type] > 0,
            type: item.type,
          };
        }),
        user_id: id!,
        agency_id: agencyId,
        created_at: new Date(),
        type,
        models_earned_money:
          isRatedOnlyCurrentUser && modelsEarnedMoney === agency?.[type]?.models_earned_money!
            ? agency?.[type]?.models_earned_money! < 0
              ? 0
              : agency?.[type]?.models_earned_money!
            : modelsEarnedMoney - agency?.[type]?.models_earned_money! < 0
            ? 0
            : modelsEarnedMoney - agency?.[type]?.models_earned_money!,
      };

      let isWhoRatedAgencyUpdated: boolean = false;

      if (agency?.isRatedByCurrentUser) {
        const { payload } = await dispatch(
          updateWhoRatedAgency({
            docId: agencyById?.whoRatedAgencyDocId!,
            data: updatedWhoRatedAgency,
          }),
        );
        isWhoRatedAgencyUpdated = payload as boolean;
      } else {
        const { payload } = await dispatch(
          addWhoRatedAgency({
            whoRatedAgency: updatedWhoRatedAgency,
          }),
        );
        isWhoRatedAgencyUpdated = payload as boolean;
      }

      if (isWhoRatedAgencyUpdated) {
        const { payload } = await dispatch(
          updateRatedAgency({
            docId: agency?.docId!,
            data: updatedAgencyRating,
          }),
        );
        return payload as boolean;
      } else {
        return false;
      }
    }
    // if only first user rated agency

    // if current user rate agency first time
    if (agency?.[type]?.is_rated && !agency?.isRatedByCurrentUser) {
      console.log('if current user rate agency');

      const whoRatedStatistics = currentAgencyStatistics?.map((statisticItem) => {
        const rated = lastRatedAgency?.[type].find((item) => item.type === statisticItem.type);

        if (isUndefined(rated)) {
          const template = templatesStatistics[type].find((item) => item.type === statisticItem.type);
          return template;
        }

        return rated;
      });

      const statistics = currentAgencyStatistics?.map((statisticItem) => {
        const rated = lastRatedAgency?.[type].find((item) => item.type === statisticItem.type);
        console.log('statisticItem; ', statisticItem);
        const rate_count = isUndefined(rated) ? statisticItem.rate_count : statisticItem.rate_count + 1;

        const result = +divide(
          add(statisticItem[statisticItem.type] * statisticItem.rate_count, rated?.[rated?.type]),
          rate_count,
        ).toFixed(0);
        console.log('statisticItem[statisticItem.type]: ', statisticItem[statisticItem.type]);
        console.log('rated?.[rated?.type]: ', rated?.[rated?.type]);
        console.log('rate_count: ', rate_count);
        console.log('result: ', result);
        return {
          ...(rated || statisticItem),
          rate_count,
          [statisticItem.type]: isNaN(result) ? 0 : result,
        };
      });

      // total rating calculation
      const statisticsSum = sumBy(statistics?.map((answer) => answer[answer.type]));
      const statisticsLength = statistics?.filter((item) => item.rate_count !== 0).length;
      const total_rating = +(statisticsSum / statisticsLength! / 20).toFixed(1);
      // total rating calculation

      const updatedAgencyRating = {
        [type]: {
          statistics,
          total_rating,
          rate_count,
          is_rated: agency?.[type]?.is_rated || true,
          models_earned_money: modelsEarnedMoney < 0 ? 0 : modelsEarnedMoney,
        },
      };

      const updatedWhoRatedAgency = {
        ...lastRatedAgency,
        [type]: whoRatedStatistics?.map((item) => {
          return {
            [item.type]: +item[item.type].toFixed(0),
            is_rated: item[item.type] > 0,
            type: item.type,
          };
        }),
        user_id: id!,
        agency_id: agencyId,
        created_at: new Date(),
        type,
        models_earned_money:
          modelsEarnedMoney - agency?.[type]?.models_earned_money! < 0
            ? 0
            : modelsEarnedMoney - agency?.[type]?.models_earned_money!,
      };

      let isWhoRatedAgencyUpdated: boolean = false;

      if (agency?.isRatedByCurrentUser) {
        const { payload } = await dispatch(
          updateWhoRatedAgency({
            docId: agencyById?.whoRatedAgencyDocId!,
            data: updatedWhoRatedAgency,
          }),
        );
        isWhoRatedAgencyUpdated = payload as boolean;
      } else {
        const { payload } = await dispatch(
          addWhoRatedAgency({
            whoRatedAgency: updatedWhoRatedAgency,
          }),
        );
        isWhoRatedAgencyUpdated = payload as boolean;
      }

      if (isWhoRatedAgencyUpdated) {
        const { payload } = await dispatch(
          updateRatedAgency({
            docId: agency?.docId!,
            data: updatedAgencyRating,
          }),
        );
        return payload as boolean;
      } else {
        return false;
      }
    }
    // if current user rate agency first time

    // if current user update only his rating
    if (agency?.[type]?.is_rated && agency?.isRatedByCurrentUser && agency?.[type].rate_count > 1) {
      console.log('if current user update only his rating');
      const currentUserAgencyStatistics = lastRatedAgency?.[type];
      const previousUserAgencyStatistics = agencyRatingFromCurrentUser?.[type];
      const currentAgencyStatistics = agency?.[type]?.statistics;

      const currentUserAnswersInStars = currentUserAgencyStatistics.map((answer) => {
        const rating = answer[answer.type];

        if (rating === 0) {
          return {
            [answer.type]: 0,
            type: answer.type,
          };
        }
        return {
          [answer.type]: calculateRatedItem({ rating }),
          type: answer.type,
        };
      });

      const previousUserAnswersInStars = previousUserAgencyStatistics?.map((answer) => {
        const rating = answer[answer.type];

        if (rating === 0) {
          return {
            [answer.type]: 0,
            type: answer.type,
          };
        }
        return {
          [answer.type]: calculateRatedItem({ rating }),
          type: answer.type,
        };
      });

      // 68 (current average1) / 20 * 5 (current amount of users) = 17( current sum of all responses)
      const currentSumOfAllAnswers = currentAgencyStatistics?.map((answer) => {
        const rating = answer[answer.type];
        return {
          [answer.type]: (rating / 20) * answer.rate_count!,
          type: answer.type,
        }; // todo: here
      });
      // 68 (current average1) / 20 * 5 (current amount of users) = 17( current sum of all responses)

      // 17  (current sum of all responses) - 4⭐⭐⭐⭐(Prev response) + 2⭐⭐(New response) = 15 (new sum of all responses)
      const newSumAllAnswers = currentSumOfAllAnswers.map((answer) => {
        const currentSumAnswers = answer[answer.type];
        console.log('currentSumAnswers: ', currentSumAnswers);
        const prevResponse = previousUserAnswersInStars.find((item) => item.type === answer.type)?.[answer.type];
        console.log('prevResponse: ', prevResponse);
        const currentResponse = currentUserAnswersInStars.find((item) => item.type === answer.type)?.[answer.type] || 0;
        console.log('currentResponse: ', currentResponse);
        const newSumOfAllResponses = currentSumAnswers - prevResponse + currentResponse;
        console.log('newSumOfAllResponses: ', newSumOfAllResponses);
        return {
          [answer.type]: newSumOfAllResponses,
          type: answer.type,
          rate_count: currentResponse === 0 || currentSumAnswers === 0 ? rate_count - 1 : rate_count,
        };
      });
      // 17  (current sum of all responses) - 4⭐⭐⭐⭐(Prev response) + 2⭐⭐(New response) = 15 (new sum of all responses)
      console.log('newSumAllAnswers: ', newSumAllAnswers);
      // 13 (new sum of all responses) / 4 (new amount of users) *20 = 66 (new average1)
      const statistics = newSumAllAnswers.map((answer) => {
        const newSumOfAllResponses = answer[answer.type];
        console.log('newSumOfAllResponses: ', newSumOfAllResponses);
        console.log('answer.rate_count: ', answer.rate_count);
        console.log('res: ', (newSumOfAllResponses / answer.rate_count!) * 20);

        return {
          [answer.type]: +((newSumOfAllResponses / answer.rate_count!) * 20).toFixed(0),
          type: answer.type,
          rate_count: newSumOfAllResponses === 0 ? 0 : answer.rate_count,
        };
      });
      // 13 (new sum of all responses) / 4 (new amount of users) *20 = 66 (new average1)

      const whoRatedStatistics = currentAgencyStatistics?.map((statisticItem) => {
        const rated = lastRatedAgency?.[type].find((item) => item.type === statisticItem.type);

        if (isUndefined(rated)) {
          const template = templatesStatistics[type].find((item) => item.type === statisticItem.type);
          return template;
        }

        return rated;
      });

      // total rating calculation
      const statisticsSum = sumBy(statistics?.map((answer) => answer[answer.type]));
      const statisticsLength = statistics?.filter((item) => item.rate_count !== 0).length;
      const total_rating = +(statisticsSum / statisticsLength! / 20).toFixed(1);
      // total rating calculation

      const updatedAgencyRating = {
        [type]: {
          statistics,
          total_rating,
          rate_count,
          is_rated: agency?.[type]?.is_rated || true,
          models_earned_money: modelsEarnedMoney < 0 ? 0 : modelsEarnedMoney,
        },
      };

      const updatedWhoRatedAgency = {
        ...lastRatedAgency,
        [type]: whoRatedStatistics?.map((item) => {
          return {
            [item.type]: +item[item.type].toFixed(0),
            is_rated: item[item.type] > 0,
            type: item.type,
          };
        }),
        user_id: id!,
        agency_id: agencyId,
        created_at: new Date(),
        type,
        models_earned_money: modelsEarnedMoney - agency?.[type]?.models_earned_money < 0 ? 0 : 1,
      };

      console.log('updatedAgencyRating: ', updatedAgencyRating);
      console.log('updatedWhoRatedAgency: ', updatedWhoRatedAgency);

      let isWhoRatedAgencyUpdated: boolean = false;

      if (agency?.isRatedByCurrentUser) {
        const { payload } = await dispatch(
          updateWhoRatedAgency({
            docId: agencyById?.whoRatedAgencyDocId!,
            data: updatedWhoRatedAgency,
          }),
        );
        isWhoRatedAgencyUpdated = payload as boolean;
      } else {
        const { payload } = await dispatch(
          addWhoRatedAgency({
            whoRatedAgency: updatedWhoRatedAgency,
          }),
        );
        isWhoRatedAgencyUpdated = payload as boolean;
      }
      console.log('isWhoRatedAgencyUpdated: ', isWhoRatedAgencyUpdated);
      if (isWhoRatedAgencyUpdated) {
        const { payload } = await dispatch(
          updateRatedAgency({
            docId: agency?.docId!,
            data: updatedAgencyRating,
          }),
        );
        return payload as boolean;
      } else {
        return false;
      }
    }
    // if current user update only his rating
  }, [
    agency,
    agencyById?.whoRatedAgencyDocId,
    agencyId,
    agencyRatingFromCurrentUser,
    currentAgencyStatistics,
    dispatch,
    id,
    isRatedOnlyCurrentUser,
    lastRatedAgency,
    modelsEarnedMoney,
    rate_count,
    type,
  ]);

  const deleteRatingFromCollections = useCallback(
    async ({ agencyToRemove }: { agencyToRemove: IAgency }) => {
      try {
        await dispatch(setGlobalLoader({ isLoading: true }));

        await dispatch(
          updateRatedAgency({
            docId: agencyToRemove?.docId!,
            data: getDataToDeleteAgencyRatingOfCurrentUser({ agencyToRemove }),
          }),
        );

        await dispatch(
          removeWhoRatedAgency({
            docId: agencyToRemove?.agencyRatingFromCurrentUser?.docId!,
          }),
        );

        await dispatch(setRatedAgencies({ userId: id, type }));
      } finally {
        await dispatch(setGlobalLoader({ isLoading: false }));
      }
    },
    [dispatch, getDataToDeleteAgencyRatingOfCurrentUser, id, type],
  );

  return {
    updateCollections,
    deleteRatingFromCollections,
  };
};

export { useAgencyRating };
