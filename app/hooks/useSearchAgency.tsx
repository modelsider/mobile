import { useMemo, useState } from 'react';
import { Keyboard } from 'react-native';
import NavigationService from 'navigator/NavigationService';
import { DEFAULT_NUMBERS_OF_LETTERS_FOR_START_SEARCH } from 'constants/index';
import { IAgency, searchAgenciesStatusType } from 'types/types';
import { useAppDispatch, useAgency } from 'hooks/index';
import { setAgencyId } from 'store/agency/reducer';
import { ROUTES } from 'navigator/constants/routes';
import { useSelector } from 'react-redux';
import { selectAgencies } from 'store/agency/selectors';
import { selectHasRating } from 'store/app/selectors';

export const useSearchAgency = () => {
  const dispatch = useAppDispatch();

  const allAgencies = useSelector(selectAgencies);
  const hasRating = useSelector(selectHasRating);

  const [searchAgency, setSearchAgency] = useState<string>('');
  const [isNotFoundAgencies, setIsNotFoundAgencies] = useState<boolean>(false);
  const [suggestedAgencies, setSuggestedAgencies] = useState<IAgency[]>([]);

  const { handleRateAgency } = useAgency();

  const handleClear = () => {
    setSearchAgency('');
    setSuggestedAgencies([]);
  };

  const handleSuggestAgency = () => {
    handleClear();
    return NavigationService.navigate({ routeName: ROUTES.SuggestAgencyName });
  };

  const handleNavigateToAgency = async ({ agency }: { agency: IAgency }) => {
    const { agency_name, city, country, id } = agency;

    await dispatch(setAgencyId({ agencyId: id }));

    const withCity = `${agency_name}, ${city}, ${country}`;
    const withoutCity = `${agency_name}, ${country}`;

    const foundAgency = city ? withCity : withoutCity;

    Keyboard.dismiss();
    setSearchAgency(foundAgency);
    setSuggestedAgencies([]);

    // show agency profile if user rated at least one agency
    if (hasRating) {
      return NavigationService.navigateWithParams({
        routeName: ROUTES.Agency,
        params: { id },
      });
    }
    return handleRateAgency({ id, isRefetchAgency: true });
  };

  const handleChangeAgency = async (inputAgency: string) => {
    setSearchAgency(inputAgency);
    setIsNotFoundAgencies(false);

    if (!inputAgency) {
      setSuggestedAgencies([]);
    }

    if (inputAgency?.length > DEFAULT_NUMBERS_OF_LETTERS_FOR_START_SEARCH) {
      // const agencies = await getSearchingAgenciesFB({
      //   searchWord: inputAgency,
      // });
      const agencies = allAgencies.filter(
        (agency) =>
          agency.agency_name.toLowerCase().includes(inputAgency.toLowerCase()) ||
          agency.city.toLowerCase().includes(inputAgency.toLowerCase()) ||
          agency.country.toLowerCase().includes(inputAgency.toLowerCase()),
      );
      setSuggestedAgencies(agencies);
      setIsNotFoundAgencies(!agencies.length);
    }
  };

  const searchAgenciesStatus: searchAgenciesStatusType = useMemo(
    () =>
      searchAgency.length === 0 && suggestedAgencies.length === 0
        ? 'default'
        : isNotFoundAgencies
        ? 'notFound'
        : searchAgency.length > 0 && suggestedAgencies.length === 0
        ? 'searching'
        : 'default',
    [isNotFoundAgencies, searchAgency.length, suggestedAgencies.length],
  );

  return {
    searchAgency,
    suggestedAgencies,
    handleSuggestAgency,
    handleNavigateToAgency,
    handleChangeAgency,
    handleClear,
    searchAgenciesStatus,
  };
};
