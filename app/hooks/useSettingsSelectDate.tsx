import { useState } from 'react';
import { DEFAULT_DATE, parsedDate, parsedSettingsDate } from 'components/select/select-date/utils';
import { resolveConditionHandler } from 'utils/resolveConditionHandler';
import { useAppDispatch } from 'hooks/index';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { updateUser } from 'services/users';
import { setUpdateUserInfo } from 'store/auth/actions';
import { getMappedDate } from 'utils/getMappedDate';

export const useSettingsSelectDate = (userDateOfBirth: string) => {
  const dispatch = useAppDispatch();
  const userInfo = useSelector(selectUserInfo);

  const [date, setDate] = useState(getMappedDate(userDateOfBirth) as Date);
  const [open, setOpen] = useState(false);

  const onOpen = () => {
    setOpen(true);
  };
  const onCancel = () => setOpen(false);

  const onConfirm = async (selectDate: any) => {
    setOpen(false);
    setDate(selectDate);

    await updateUser({
      docId: userInfo?.docId,
      data: { dateOfBirth: selectDate },
    });
    dispatch(setUpdateUserInfo({ userId: userInfo?.id }));
  };

  const value = resolveConditionHandler(
    parsedDate(DEFAULT_DATE) === parsedDate(date),
    parsedSettingsDate(DEFAULT_DATE),
    parsedSettingsDate(date),
  );

  return {
    date,
    open,
    onOpen,
    onCancel,
    onConfirm,
    value,
  };
};
