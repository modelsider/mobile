import { useBackHandler } from '@react-native-community/hooks';
import { useRoute } from '@react-navigation/native';
import NavigationService from 'navigator/NavigationService';

const SCREENS_WITHOUT_BACK_HANDLER: string | any[] = [
  'SignUpStartError',
  'QuizStepOne',
  'SignUpFinish',
  'SignUpUnableInstagramError',
  'SignUpNotPublicInstagramError',
];

export const useAppBackHandler = () => {
  const { name } = useRoute();

  useBackHandler(() => {
    if (SCREENS_WITHOUT_BACK_HANDLER.includes(name)) {
      return true;
    }

    NavigationService.navigateBack();
    return false;
  });
};
