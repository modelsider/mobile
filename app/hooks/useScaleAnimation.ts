import { useCallback, useRef } from 'react';
import { Animated } from 'react-native';

export const useScaleAnimation = (from = 1, to = 0.9) => {
  const animation = useRef(new Animated.Value(from)).current;

  const handlePressIn = useCallback(() => {
    Animated.spring(animation, {
      toValue: to,
      useNativeDriver: true,
    }).start();
  }, [animation, to]);

  const handlePressOut = useCallback(() => {
    Animated.spring(animation, {
      toValue: from,
      friction: 4,
      tension: 30,
      useNativeDriver: true,
    }).start();
  }, [animation, from]);

  const transform = [{ scale: animation }];

  return {
    handlePressIn,
    handlePressOut,
    transform,
  };
};
