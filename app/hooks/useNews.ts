import { useAppDispatch } from 'hooks/index';
import { useSelector } from 'react-redux';
import { selectNews, selectNewsCounter } from 'store/news/selectors';
import { useCallback, useState } from 'react';
import { setNews } from 'store/news/actions';
import { selectUserInfo } from 'store/auth/selectors';

const useNews = () => {
  const dispatch = useAppDispatch();
  const user = useSelector(selectUserInfo);
  const news = useSelector(selectNews);
  const newsCounter = useSelector(selectNewsCounter);

  const [loading, setLoading] = useState(false);

  const handleFetchNews = useCallback(async () => {
    setLoading(true);
    await dispatch(setNews({ userId: user?.id }));
    setLoading(false);
  }, [dispatch, user?.id]);

  return {
    news,
    handleFetchNews,
    loading,
    newsBadge: newsCounter,
  };
};

export { useNews };
