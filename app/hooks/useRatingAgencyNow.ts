import { useAppDispatch } from 'hooks/index';
import { useSelector } from 'react-redux';
import { selectAgency } from 'store/agency/selectors';
import { useCallback, useState } from 'react';
import { setIsRatingAgencyNow } from 'store/agency/actions';
import { isRatingAgencyNowFB } from '../services/agencies';

type Response = {
  checkIfRatingAgencyNow: () => Promise<boolean>;
  isRatingNowLoading: boolean;
};

const useRatingAgencyNow = (): Response => {
  const dispatch = useAppDispatch();
  const agency = useSelector(selectAgency);

  const [isRatingNowLoading, setIsRatingNowLoading] = useState<boolean>(false);

  const checkIfRatingAgencyNow = useCallback(async (): Promise<boolean> => {
    setIsRatingNowLoading(true);
    const isRatingNow = await isRatingAgencyNowFB({ docId: agency?.docId! });
    setIsRatingNowLoading(false);
    console.log('isRatingNow: ', isRatingNow);
    if (isRatingNow) {
      return isRatingNow;
    }

    dispatch(setIsRatingAgencyNow({ isRatingNow: true, docId: agency?.docId! }));
    return isRatingNow;
  }, [agency?.docId, dispatch]);

  return { checkIfRatingAgencyNow, isRatingNowLoading };
};

export { useRatingAgencyNow };
