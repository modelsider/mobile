import { useAppDispatch } from 'hooks/index';
import { useSelector } from 'react-redux';
import { useCallback, useState } from 'react';
import { selectUsers } from 'store/users/selectors';
import { setUsersByAgencyId } from 'store/users/actions';
import { IUserInfo } from 'store/auth/reducer';
import { selectAgencyType, selectAgency } from 'store/agency/selectors';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { setSelectedUserProfileId } from 'store/users/reducer';

type Response = {
  users: IUserInfo[];
  handleFetchUsers: (agencyId?: number) => void;
  isFetch: boolean;
  navigateToUserProfile: ({ userId }: { userId: string }) => void;
};

const useUsers = (): Response => {
  const dispatch = useAppDispatch();
  const users = useSelector(selectUsers);
  const agency = useSelector(selectAgency);
  const type = useSelector(selectAgencyType);

  const [isFetch, setIsFetch] = useState(false);

  const handleFetchUsers = useCallback(
    async (agencyId = agency?.id) => {
      setIsFetch(true);
      await dispatch(setUsersByAgencyId({ agencyId, type }));
      setIsFetch(false);
    },
    [agency?.id, dispatch, type],
  );

  const navigateToUserProfile = useCallback(
    ({ userId }: { userId: string }) => {
      dispatch(setSelectedUserProfileId({ userId }));
      return NavigationService.navigateWithParams({
        routeName: ROUTES.UserProfile,
        params: { userId },
      });
    },
    [dispatch],
  );

  return {
    users,
    handleFetchUsers,
    isFetch,
    navigateToUserProfile,
  };
};

export { useUsers };
