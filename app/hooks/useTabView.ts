import { useSelector } from 'react-redux';
import { selectAgencyType } from 'store/agency/selectors';
import { SCREEN_TABS, SCREEN_TABS_ROUTES } from 'components/custom-tab-view/config';

const useTabView = ({
  tabType,
}: {
  tabType: 'MyAgencies' | 'Bookmarks' | 'Settings' | 'Agency' | 'UserProfile' | 'Auth' | 'AgenciesByCountry' | 'Home';
}) => {
  const type = useSelector(selectAgencyType);

  const tabRoutes = type === 'mother' ? SCREEN_TABS_ROUTES[tabType].left : SCREEN_TABS_ROUTES[tabType].right;

  return {
    tabs: SCREEN_TABS[tabType],
    tabRoutes: tabType === 'Home' ? SCREEN_TABS_ROUTES[tabType].left : tabRoutes,
  };
};

export { useTabView };
