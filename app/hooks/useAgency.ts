import { useAppDispatch } from 'hooks/index';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { selectAgency, selectAgencyId, selectAgencyType } from 'store/agency/selectors';
import { useCallback, useState } from 'react';
import { setAgency, setRatedAgencies } from 'store/agency/actions';
import { IAgency } from 'types/types';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { setCheckDotsStateCount, setIsUpdateDates, setIsUpdateOnlyRating } from 'store/rate-agency/reducer';
import { setGlobalLoader } from 'store/app/reducer';

type Response = {
  handleFetchAgency: (id?: number) => void;
  agencyId: number;
  isFetchAgency: boolean;
  agency: IAgency;
  handleRateAgency: ({ isRefetchAgency, id }: { isRefetchAgency?: boolean; id?: number }) => void;
};

const useAgency = (): Response => {
  const dispatch = useAppDispatch();
  const user = useSelector(selectUserInfo);
  const agency = useSelector(selectAgency);
  const type = useSelector(selectAgencyType);
  const agencyId = useSelector(selectAgencyId);

  const isRated = agency?.isRatedByCurrentUser;

  const [isFetchAgency, setIsFetchAgency] = useState<boolean>(false);

  const handleFetchAgency = useCallback(
    async (id?: number) => {
      // id for push + deeplink
      setIsFetchAgency(true);
      await dispatch(setAgency({ userId: user.id!, agencyId: id || agencyId, type }));
      await dispatch(setRatedAgencies({ userId: user.id!, type }));
      setIsFetchAgency(false);
    },
    [dispatch, agencyId, type, user.id],
  );

  const handleRateAgency = useCallback(
    async ({
      id = agencyId, // if redirect to without any rating for current user
      isRefetchAgency,
    }: {
      isRefetchAgency?: boolean;
      id?: number;
    }) => {
      dispatch(setIsUpdateDates({ isUpdate: false }));
      dispatch(setIsUpdateOnlyRating({ isUpdate: false }));
      dispatch(setCheckDotsStateCount({ count: type === 'booking' ? 11 : 10 })); // add constants

      if (isRefetchAgency) {
        await dispatch(setGlobalLoader({ isLoading: true }));
        await handleFetchAgency(id);
        await dispatch(setGlobalLoader({ isLoading: false }));
      } else {
        handleFetchAgency(id);
      }

      if (isRated) {
        dispatch(setIsUpdateOnlyRating({ isUpdate: true }));

        return NavigationService.navigate({
          routeName: ROUTES.ProfessionalScreen,
        });
      }

      return NavigationService.navigate({
        routeName: ROUTES.ChooseAgencyType,
      });
    },
    [agencyId, dispatch, handleFetchAgency, isRated, type],
  );

  return {
    handleFetchAgency,
    agencyId,
    isFetchAgency,
    agency: agency!,
    handleRateAgency,
  };
};

export { useAgency };
