import { useCallback, useState } from 'react';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { useSelector } from 'react-redux';
import { selectFeed } from '../store/feed/selectors';
import { useAppDispatch } from './useAppDispatch';
import { updateFeed } from '../store/feed/reducer';

type Arguments = {
  isLike?: boolean;
  likesCount?: number;
  isBookmark?: boolean;
  isBookmarkAgency?: boolean;
  handleCommentLike?: ({
    commentId,
    isLikeLocal,
    likesCountLocal,
  }: {
    commentId: number;
    isLikeLocal: boolean;
    likesCountLocal: number;
  }) => void;
  onBookmarkCommentByCommentId?: ({
    commentId,
    isBookmarkLocal,
  }: {
    commentId: number;
    isBookmarkLocal: boolean;
  }) => void;
  onBookmarkAgencyByAgencyId?: ({ agencyId, isBookmarkLocal }: { agencyId: number; isBookmarkLocal: boolean }) => void;
  commentId: string | number;
};

type Response = {
  isLikeLocal: boolean | null;
  likesCountLocal: number;
  isBookmarkLocal: boolean | null;
  isBookmarkAgencyLocal: boolean;
  handleToggleLikeComment: ({ commentId, isLikeLocal }: { commentId: number; isLikeLocal: boolean }) => void;
  handleToggleBookmarkComment: ({
    commentId,
    isBookmarkLocal,
  }: {
    commentId: number;
    isBookmarkLocal: boolean;
  }) => void;
  navigateToComment: ({ commentId, isNavigateToAgency }: { commentId: number; isNavigateToAgency?: boolean }) => void;
  handleToggleBookmarkAgency: ({ agencyId }: { agencyId: number }) => void;
};

const useLocalStateActions = ({
  isLike = false,
  likesCount = 0,
  isBookmark = false,
  isBookmarkAgency = false,
  onBookmarkCommentByCommentId,
  handleCommentLike,
  onBookmarkAgencyByAgencyId,
  commentId,
}: Arguments): Response => {
  const dispatch = useAppDispatch();
  const feed = useSelector(selectFeed);

  const [isLikeLocal, setIsLikeLocal] = useState<boolean | null>(null);
  const [likesCountLocal, setLikesCountLocal] = useState<number | null>(null);
  const [isBookmarkLocal, setIsBookmarkLocal] = useState<boolean | null>(null);
  const [isBookmarkAgencyLocal, setIsBookmarkAgencyLocal] = useState<boolean>(isBookmarkAgency);

  const handleToggleLikeComment = useCallback(
    ({ commentId, isLikeLocal }: { commentId: number; isLikeLocal: boolean }) => {
      handleCommentLike!({
        commentId,
        isLikeLocal: !isLikeLocal,
        likesCountLocal: likesCountLocal === null ? 0 : likesCountLocal,
      });

      setIsLikeLocal(!isLikeLocal);

      const selectedFeed = feed.find(({ id }) => id === commentId);

      if (!isLikeLocal) {
        dispatch(
          updateFeed({ ...selectedFeed, likes_count: likesCountLocal === null ? likesCount + 1 : likesCountLocal + 1 }),
        );
        setLikesCountLocal((prevLikesCount) => (prevLikesCount === null ? likesCount + 1 : prevLikesCount + 1));
      } else {
        if (likesCountLocal !== 0) {
          dispatch(
            updateFeed({
              ...selectedFeed,
              likes_count: likesCountLocal === null ? likesCount - 1 : likesCountLocal - 1,
            }),
          );
          setLikesCountLocal((prevLikesCount) => (prevLikesCount === null ? likesCount - 1 : prevLikesCount - 1));
        }
      }
    },
    [handleCommentLike, likesCountLocal],
  );

  const handleToggleBookmarkComment = useCallback(
    ({ commentId, isBookmarkLocal }: { commentId: number; isBookmarkLocal: boolean }) => {
      setIsBookmarkLocal(!isBookmarkLocal);
      onBookmarkCommentByCommentId!({
        commentId,
        isBookmarkLocal: !isBookmarkLocal,
      });
    },
    [onBookmarkCommentByCommentId],
  );
  // todo: already exist
  const navigateToComment = ({
    commentId,
    isNavigateToAgency = false,
  }: {
    commentId: number;
    isNavigateToAgency?: boolean;
  }) => {
    return NavigationService.navigateWithParams({
      routeName: ROUTES.CommentScreen,
      params: { commentId, isNavigateToAgency },
    });
  };

  const handleToggleBookmarkAgency = useCallback(
    ({ agencyId }: { agencyId: number }) => {
      setIsBookmarkAgencyLocal(!isBookmarkAgencyLocal);
      onBookmarkAgencyByAgencyId!({
        agencyId,
        isBookmarkLocal: !isBookmarkAgencyLocal,
      });
    },
    [isBookmarkAgencyLocal, onBookmarkAgencyByAgencyId],
  );

  return {
    isLikeLocal,
    isBookmarkLocal,
    likesCountLocal,
    handleToggleLikeComment,
    handleToggleBookmarkComment,
    navigateToComment,
    isBookmarkAgencyLocal,
    handleToggleBookmarkAgency,
  };
};

export { useLocalStateActions };
