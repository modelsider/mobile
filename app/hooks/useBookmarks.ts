import { useAppDispatch } from './useAppDispatch';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { useCallback, useState } from 'react';
import {
  setBookmarkedAgencies,
  setBookmarkedComments,
  setUploadedBookmarkedAgencies,
  setUploadedBookmarkedComments,
  toggleBookmark,
} from 'store/bookmarks/actions';
import { IAgency, IComment, noop } from 'types/types';
import { selectBookmarkedAgencies, selectBookmarkedComments } from 'store/bookmarks/selectors';

type Response = {
  onBookmarkAgencyByAgencyId: ({ agencyId, isBookmarkLocal }: { agencyId: number; isBookmarkLocal: boolean }) => void;
  onBookmarkCommentByCommentId: ({
    commentId,
    isBookmarkLocal,
  }: {
    commentId: number;
    isBookmarkLocal: boolean;
  }) => void;
  loading: boolean;
  fetchBookmarkedAgencies: noop;
  bookmarkedAgencies: IAgency[];
  fetchBookmarkedComments: noop;
  bookmarkedComments: IComment[];
  fetchUploadedBookmarkedComments: noop;
  fetchUploadedBookmarkedAgencies: noop;
  handleMomentumScrollEnd: noop;
  handleMomentumScrollBegin: noop;
  isMomentumScrollEnd: boolean;
};

const useBookmarks = (): Response => {
  const dispatch = useAppDispatch();

  const currentUser = useSelector(selectUserInfo);
  const bookmarkedAgencies = useSelector(selectBookmarkedAgencies);
  const bookmarkedComments = useSelector(selectBookmarkedComments);

  const [loading, setLoading] = useState<boolean>(false);
  const [isMomentumScrollEnd, setIsMomentumScrollEnd] = useState<boolean>(false);

  // from bookmark screen
  const onBookmarkAgencyByAgencyId = useCallback(
    ({ agencyId, isBookmarkLocal }: { agencyId: number; isBookmarkLocal: boolean }) => {
      dispatch(
        toggleBookmark({
          userId: currentUser.id!,
          bookmarkId: agencyId,
          type: 'agencies',
          isBookmarkLocal,
          created_at: new Date(),
        }),
      );
    },
    [currentUser.id, dispatch],
  );

  const onBookmarkCommentByCommentId = useCallback(
    ({ commentId, isBookmarkLocal }: { commentId: number; isBookmarkLocal: boolean }) => {
      dispatch(
        toggleBookmark({
          userId: currentUser.id!,
          bookmarkId: commentId,
          type: 'comments',
          isBookmarkLocal,
          created_at: new Date(),
        }),
      );
    },
    [currentUser.id, dispatch],
  );

  const fetchBookmarkedAgencies = useCallback(async () => {
    setLoading(true);
    await dispatch(setBookmarkedAgencies({ userId: currentUser.id! }));
    setLoading(false);
  }, [currentUser.id, dispatch]);

  const fetchBookmarkedComments = useCallback(async () => {
    setLoading(true);
    await dispatch(setBookmarkedComments({ userId: currentUser.id! }));
    setLoading(false);
  }, [currentUser.id, dispatch]);

  const fetchUploadedBookmarkedComments = useCallback(async () => {
    await dispatch(
      setUploadedBookmarkedComments({
        userId: currentUser?.id,
        bookmarkedComments,
      }),
    );
  }, [bookmarkedComments, currentUser?.id, dispatch]);

  const fetchUploadedBookmarkedAgencies = useCallback(async () => {
    await dispatch(
      setUploadedBookmarkedAgencies({
        userId: currentUser?.id,
        bookmarkedAgencies,
      }),
    );
  }, [bookmarkedAgencies, currentUser?.id, dispatch]);

  const handleMomentumScrollEnd = useCallback(() => {
    setIsMomentumScrollEnd(true);
  }, []);

  const handleMomentumScrollBegin = useCallback(() => {
    setIsMomentumScrollEnd(false);
  }, []);

  return {
    loading,
    fetchBookmarkedAgencies,
    bookmarkedAgencies,
    onBookmarkAgencyByAgencyId,
    onBookmarkCommentByCommentId,
    fetchBookmarkedComments,
    bookmarkedComments,
    fetchUploadedBookmarkedComments,
    handleMomentumScrollEnd,
    handleMomentumScrollBegin,
    isMomentumScrollEnd,
    fetchUploadedBookmarkedAgencies,
  };
};

export { useBookmarks };
