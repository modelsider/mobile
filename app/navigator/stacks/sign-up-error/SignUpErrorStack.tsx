import React from 'react';
import { TransitionPresets, createStackNavigator } from '@react-navigation/stack';
import { ROUTES } from 'navigator/constants/routes';
import {
  SignUpUnableInstagramErrorScreen,
  SignUpNotPublicInstagramErrorScreen,
  SignUpStartErrorScreen,
} from 'screens/index';

const { Navigator, Screen } = createStackNavigator();

export const SignUpErrorStack = () => {
  return (
    <Navigator
      screenOptions={{
        headerShown: false,
        ...TransitionPresets.ModalFadeTransition,
        gestureEnabled: false,
      }}
    >
      <Screen name={ROUTES.SignUpStartError} component={SignUpStartErrorScreen} options={{ gestureEnabled: false }} />
      <Screen
        name={ROUTES.SignUpUnableInstagramError}
        component={SignUpUnableInstagramErrorScreen}
        options={{ gestureEnabled: false }}
      />
      <Screen
        name={ROUTES.SignUpNotPublicInstagramError}
        component={SignUpNotPublicInstagramErrorScreen}
        options={{ gestureEnabled: false }}
      />
    </Navigator>
  );
};
