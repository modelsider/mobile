import React from 'react';
import { TransitionPresets, createStackNavigator } from '@react-navigation/stack';
import { ROUTES } from 'navigator/constants/routes';
import { HomeScreen } from 'screens/index';
import { LeaveRatingScreen } from 'screens/leave-rating/LeaveRatingScreen';
import { useSelector } from 'react-redux';
import { selectHasRating } from 'store/app/selectors';
import { useRatedAtLeastOnce } from 'hooks/index';
import isNull from 'lodash/isNull';

const { Navigator, Screen } = createStackNavigator();

export const HomeStack = () => {
  const hasRating = useSelector(selectHasRating);

  useRatedAtLeastOnce();

  return (
    <Navigator
      screenOptions={{
        headerShown: false,
        ...TransitionPresets.ModalFadeTransition,
        gestureEnabled: false,
        title: 'Feed',
      }}
    >
      {hasRating || isNull(hasRating) ? (
        <Screen name={ROUTES.Home} component={HomeScreen} />
      ) : (
        <Screen name={ROUTES.LeaveRating} component={LeaveRatingScreen} />
      )}
    </Navigator>
  );
};
