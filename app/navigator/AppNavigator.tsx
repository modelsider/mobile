import React, { useEffect } from 'react';
import { useAuth, useBootstrap } from '../hooks';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { ROUTES } from 'navigator/constants/routes';
import { BottomTabNavigation } from 'navigator/navigation/bottom-tab/BottomTabNavigation';
import { useHeaderStyle } from 'navigator/hooks/useHeaderStyle';
import { SignUpErrorStack } from 'navigator/stacks/sign-up-error/SignUpErrorStack';
import noop from 'lodash/noop';

import {
  AgencyScreen,
  CommentScreen,
  LeaveCommentScreen,
  LeaveReplyScreen,
  MenuScreen,
  ImageFullScreen,
  RespectfulScreen,
  RateFinishWorkingScreen,
  SatisfiedScreen,
  QuestionaryFinishScreen,
  AccommodationScreen,
  ProfessionalScreen,
  RateStartWorkingScreen,
  PaymentScreen,
  LikelyScreen,
  PortfolioScreen,
  ExpensesScreen,
  ResponsibleScreen,
  HelpfulScreen,
  FairScreen,
  ApartmentScreen,
  PayScreen,
  SearchAgencyScreen,
  SettingsScreen,
  SuggestAgencyLocationScreen,
  SuggestAgencyWebsiteScreen,
  SuggestAgencyInstagramScreen,
  SuggestAgencyNameScreen,
  SuggestAgencyCityScreen,
  SupportScreen,
  UserProfileScreen,
  WelcomeCheckRatingsScreen,
  WelcomeRateAgencyScreen,
  WelcomeModelsiderScreen,
  WelcomeScreen,
  WhoRatedAgency,
  QuizStepTwoScreen,
  QuizStepThreeScreen,
  QuizStepOneScreen,
  GenderSizeHeightInfoScreen,
  SignUpUnableInstagramErrorScreen,
  SignUpNotPublicInstagramErrorScreen,
  SignUpStartErrorScreen,
  SignUpApprovedScreen,
  SignUpWaitingApproveScreen,
  SignUpFinishScreen,
  SignUpStartScreen,
  SignUpRefusedScreen,
  InstagramLoginScreen,
  AuthScreen,
  InstagramVerificationScreen,
  AllRatedAgenciesScreen,
  AgenciesByCountryScreen,
  LeavePostScreen,
} from 'screens/index';
import auth from '@react-native-firebase/auth';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { InstagramUsernameScreen } from 'screens/sign-up/instagram-verification-screen/InstagramUsernameScreen';
import { ChooseAgencyTypeScreen } from 'screens/choose-agency-type-screen/ChooseAgencyTypeScreen';
import { VersionCheckScreen } from 'screens/version-check/VersionCheckScreen';
import { LeaveRatingScreen } from 'screens/leave-rating/LeaveRatingScreen';

const { Navigator, Screen, Group } = createStackNavigator();

export const AppNavigation = () => {
  const { isFirstTimeLogin, status, runBootstrap } = useBootstrap();
  const { defaultStyle, questionaryRatingAgency, signUpQuestions, comments, agency, suggestAgency } = useHeaderStyle();
  const { handleAuthStateChanged, initializing, isLoggedIn } = useAuth();
  const { created_at } = useSelector(selectUserInfo);

  useEffect(() => {
    runBootstrap();
  }, []);

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(handleAuthStateChanged);
    return subscriber;
  }, []);

  // if (initializing) {
  //   return null;
  // }

  return (
    <Navigator>
      {isLoggedIn && status && created_at ? (
        <Group
          screenOptions={{
            headerShown: false,
            gestureEnabled: false,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        >
          {status === 'waiting' && (
            <Screen name={ROUTES.SignUpWaitingApproveStatus} component={SignUpWaitingApproveScreen} />
          )}
          {status === 'refused' && <Screen name={ROUTES.SignUpRefusedStatus} component={SignUpRefusedScreen} />}
          {status === 'approved' && isFirstTimeLogin && (
            <Screen
              name={ROUTES.SignUpApprovedStatus}
              component={SignUpApprovedScreen}
              initialParams={{ userId: null, observable: null }}
            />
          )}
        </Group>
      ) : (
        <Group
          screenOptions={{
            gestureEnabled: false,
            ...TransitionPresets.ModalFadeTransition,
          }}
        >
          <Group screenOptions={{ headerShown: false }}>
            <Screen name={ROUTES.Welcome} component={WelcomeScreen} />
            <Screen name={ROUTES.WelcomeRateAgency} component={WelcomeRateAgencyScreen} />
            <Screen name={ROUTES.WelcomeCheckRatings} component={WelcomeCheckRatingsScreen} />
            <Screen name={ROUTES.WelcomeModelsider} component={WelcomeModelsiderScreen} />
          </Group>

          <Group
            // @ts-ignore
            screenOptions={{
              ...signUpQuestions,
            }}
          >
            <Screen
              name={ROUTES.SignUpStart}
              component={SignUpStartScreen}
              options={{ headerShown: false, ...TransitionPresets.ModalSlideFromBottomIOS }}
            />
            <Screen name={ROUTES.InstagramUsername} component={InstagramUsernameScreen} />
            <Screen name={ROUTES.InstagramVerification} component={InstagramVerificationScreen} />
            <Screen name={ROUTES.SignUpError} component={SignUpErrorStack} options={{ headerShown: false }} />
            <Screen name={ROUTES.QuizStepOne} component={QuizStepOneScreen} options={{ headerShown: false }} />
            <Screen name={ROUTES.QuizStepTwo} component={QuizStepTwoScreen} />
            <Screen name={ROUTES.QuizStepThree} component={QuizStepThreeScreen} />
            <Screen name={ROUTES.GenderSizeHeightInfo} component={GenderSizeHeightInfoScreen} />
            <Screen
              name={ROUTES.SignUpFinish}
              component={SignUpFinishScreen}
              options={{ gestureEnabled: false, headerShown: false }}
            />
            <Screen
              name={ROUTES.SignUpApprovedStatus}
              component={SignUpApprovedScreen}
              options={{ headerShown: false }}
              initialParams={{ userId: null, observable: null }}
            />
            <Screen
              name={ROUTES.SignUpRefusedStatus}
              component={SignUpRefusedScreen}
              options={{ headerShown: false }}
            />
          </Group>

          <Group screenOptions={{ headerShown: false }}>
            <Screen
              name={ROUTES.SignUpStartError}
              component={SignUpStartErrorScreen}
              options={{ gestureEnabled: false }}
            />
            <Screen
              name={ROUTES.SignUpUnableInstagramError}
              component={SignUpUnableInstagramErrorScreen}
              options={{ gestureEnabled: false }}
            />
            <Screen
              name={ROUTES.SignUpNotPublicInstagramError}
              component={SignUpNotPublicInstagramErrorScreen}
              options={{ gestureEnabled: false }}
            />
          </Group>
        </Group>
      )}

      {/* Common modal screens */}
      <Group
        // @ts-ignore
        screenOptions={{
          ...TransitionPresets.SlideFromRightIOS,
          ...defaultStyle,
        }}
      >
        <Group screenOptions={{ headerShown: false, gestureEnabled: false }}>
          <Screen name={ROUTES.Home} component={BottomTabNavigation} />
        </Group>
        {/*@ts-ignore*/}
        <Group screenOptions={{ ...agency }}>
          <Screen name={ROUTES.Agency} component={AgencyScreen} options={{ title: 'Agency' }} />
          <Screen name={ROUTES.WhoRatedAgency} component={WhoRatedAgency} options={{ title: 'Who rated agency' }} />
          <Screen name={ROUTES.UserProfile} component={UserProfileScreen} options={{ title: 'Profile' }} />
        </Group>
        <Screen name={ROUTES.MenuScreen} component={MenuScreen} options={{ title: 'Menu' }} />
        <Screen name={ROUTES.Settings} component={SettingsScreen} options={{ title: 'Settings' }} />
        <Screen name={ROUTES.Support} component={SupportScreen} options={{ title: 'Support' }} />

        <Screen name={ROUTES.SearchAgency} component={SearchAgencyScreen} options={{ title: 'Search' }} />
        {/*@ts-ignore*/}
        <Group screenOptions={{ title: 'Suggest an agency', ...suggestAgency }}>
          <Screen name={ROUTES.SuggestAgencyName} component={SuggestAgencyNameScreen} />
          <Screen name={ROUTES.SuggestAgencyLocation} component={SuggestAgencyLocationScreen} />
          <Screen name={ROUTES.SuggestAgencyCityScreen} component={SuggestAgencyCityScreen} />
          <Screen name={ROUTES.SuggestAgencyWebsite} component={SuggestAgencyWebsiteScreen} />
          <Screen name={ROUTES.SuggestAgencyInstagram} component={SuggestAgencyInstagramScreen} />
        </Group>
        {/*@ts-ignore*/}
        <Group screenOptions={{ ...comments }}>
          <Screen name={ROUTES.CommentScreen} component={CommentScreen} options={{ title: 'Comment' }} />
          <Screen name={ROUTES.CommentScreenDeeplink} component={CommentScreen} options={{ title: 'Comment' }} />
          <Screen name={ROUTES.LeaveReplyScreen} component={LeaveReplyScreen} options={{ title: 'Reply' }} />
          <Screen name={ROUTES.LeaveCommentScreen} component={LeaveCommentScreen} />
          <Screen name={ROUTES.ImageFullScreen} component={ImageFullScreen} options={{ title: '' }} />
        </Group>

        {/* mother rating */}
        <Group
          // @ts-ignore
          screenOptions={{
            ...TransitionPresets.ModalFadeTransition,
            ...questionaryRatingAgency,
            title: 'Questionary',
          }}
        >
          <Screen name={ROUTES.SatisfiedScreen} component={SatisfiedScreen} />
          <Screen name={ROUTES.PayScreen} component={PayScreen} />
        </Group>

        {/* booking rating */}
        <Group
          // @ts-ignore
          screenOptions={{
            ...TransitionPresets.ModalFadeTransition,
            ...questionaryRatingAgency,
            title: 'Questionary',
          }}
        >
          <Screen name={ROUTES.AccommodationScreen} component={AccommodationScreen} />
          <Screen name={ROUTES.ApartmentScreen} component={ApartmentScreen} />
          <Screen name={ROUTES.PaymentScreen} component={PaymentScreen} />
        </Group>

        {/* common rating screens */}
        <Screen name={ROUTES.Auth} component={AuthScreen} options={{ headerShown: false }} />
        <Screen
          options={{
            ...TransitionPresets.ModalFadeTransition,
          }}
          name={ROUTES.LeavePost}
          component={LeavePostScreen}
        />
        <Group
          // @ts-ignore
          screenOptions={{
            ...TransitionPresets.ModalFadeTransition,
            ...questionaryRatingAgency,
            title: 'Questionary',
          }}
        >
          <Screen name={ROUTES.ChooseAgencyType} component={ChooseAgencyTypeScreen} />
          <Screen name={ROUTES.RateStartWorking} component={RateStartWorkingScreen} />
          <Screen name={ROUTES.RateFinishWorking} component={RateFinishWorkingScreen} />
          <Screen name={ROUTES.ProfessionalScreen} component={ProfessionalScreen} />
          <Screen name={ROUTES.RespectfulScreen} component={RespectfulScreen} />
          <Screen name={ROUTES.ResponsibleScreen} component={ResponsibleScreen} />
          <Screen name={ROUTES.HelpfulScreen} component={HelpfulScreen} />
          <Screen name={ROUTES.FairScreen} component={FairScreen} />
          <Screen name={ROUTES.LikelyScreen} component={LikelyScreen} />
          <Screen name={ROUTES.PortfolioScreen} component={PortfolioScreen} />
          <Screen name={ROUTES.ExpensesScreen} component={ExpensesScreen} />
          <Screen
            name={ROUTES.FinishScreen}
            component={QuestionaryFinishScreen}
            // @ts-ignore
            options={{ headerLeft: noop, headerRight: noop }}
          />
        </Group>

        <Group
          screenOptions={{
            headerShown: false,
            ...TransitionPresets.ModalFadeTransition,
          }}
        >
          <Screen name={ROUTES.InstagramLogin} component={InstagramLoginScreen} />
        </Group>

        {/* agency-list */}
        <Group>
          <Screen name={ROUTES.AllRatedAgencies} component={AllRatedAgenciesScreen} options={{ title: 'All rated' }} />
          <Screen name={ROUTES.AgenciesByCountry} component={AgenciesByCountryScreen} />
        </Group>

        <Group
          screenOptions={{
            headerShown: false,
            gestureEnabled: false,
            ...TransitionPresets.BottomSheetAndroid,
          }}
        >
          <Screen name={ROUTES.VersionCheck} component={VersionCheckScreen} />
          <Screen name={ROUTES.LeaveRating} component={LeaveRatingScreen} />
        </Group>
      </Group>
    </Navigator>
  );
};
