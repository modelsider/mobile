import React from 'react';

import NavigationService from 'navigator/NavigationService';
import { ArrowBackIcon } from 'components/UI/svg/ArrowBackIcon';
import { Pressable, useTheme, CloseIcon, ThreeDotsIcon } from 'native-base';
import { ROUTES } from 'navigator/constants/routes';
import { HIT_SLOP } from 'constants/index';
import { TransitionPresets } from '@react-navigation/stack';

const useHeaderStyle = () => {
  const { space, sizes, colors, fontSizes } = useTheme();

  const defaultStyle = {
    headerStatusBarHeight: 0,
    headerLeftContainerStyle: {
      paddingLeft: space['7'],
    },
    headerRightContainerStyle: {
      paddingRight: space['7'],
    },
    headerStyle: {
      backgroundColor: colors.teal['50'],
      borderTopColor: colors.teal['50'],
      shadowColor: colors.teal['50'],
      shadowOpacity: 1,
      shadowRadius: 3,
      shadowOffset: {
        height: 4,
        width: 0,
      },
    },
    headerTitleStyle: {
      fontSize: fontSizes.lg,
    },
    headerBackTitleVisible: false,
    headerTitleAlign: 'center',
    headerBackImage: () => (
      <Pressable onPress={() => NavigationService.navigateBack()}>
        <ArrowBackIcon />
      </Pressable>
    ),
  };

  const questionaryRatingAgency = {
    ...defaultStyle,
    headerBackImage: () => (
      <Pressable
        hitSlop={HIT_SLOP}
        onPress={() => {
          NavigationService.navigateBack();
        }}
      >
        <ArrowBackIcon />
      </Pressable>
    ),
    headerRight: () => (
      <Pressable onPress={() => NavigationService.resetToHome()}>
        <CloseIcon color="black" />
      </Pressable>
    ),
  };

  const signUpQuestions = {
    ...defaultStyle,
    headerTransparent: true,
    title: '',
    headerBackImage: () => (
      <Pressable
        hitSlop={HIT_SLOP}
        onPress={() => {
          NavigationService.navigateBack();
        }}
      >
        <ArrowBackIcon />
      </Pressable>
    ),
  };

  const defaultNavigation = {
    ...defaultStyle,
    headerRight: () => (
      <Pressable onPress={() => NavigationService.resetToHome()}>
        <CloseIcon color="black" />
      </Pressable>
    ),
    headerLeft: () => (
      <Pressable hitSlop={HIT_SLOP} onPress={() => NavigationService.resetToHome()}>
        <ArrowBackIcon />
      </Pressable>
    ),
  };

  const agency = {
    ...defaultStyle,
    headerRight: () => (
      <Pressable onPress={() => NavigationService.resetToHome()}>
        <CloseIcon color="black" />
      </Pressable>
    ),
    headerLeft: () => (
      <Pressable
        hitSlop={HIT_SLOP}
        onPress={() =>
          NavigationService.canNavigateBack() ? NavigationService.navigateBack() : NavigationService.resetToHome()
        }
      >
        <ArrowBackIcon />
      </Pressable>
    ),
  };

  const suggestAgency = {
    ...defaultStyle,
    ...TransitionPresets.ModalFadeTransition,
    gestureEnabled: false,
    headerRight: () => (
      <Pressable onPress={() => NavigationService.resetToHome()}>
        <CloseIcon color="black" />
      </Pressable>
    ),
    headerLeft: () => (
      <Pressable hitSlop={HIT_SLOP} onPress={() => NavigationService.navigateBack()}>
        <ArrowBackIcon />
      </Pressable>
    ),
  };

  const comments = {
    ...defaultStyle,
    headerRight: () => (
      <Pressable onPress={() => NavigationService.resetToHome()}>
        <CloseIcon color="black" />
      </Pressable>
    ),
  };

  const bottomTab = {
    ...defaultStyle,
    headerRight: () => (
      <Pressable onPress={() => NavigationService.navigate({ routeName: ROUTES.MenuScreen })}>
        <ThreeDotsIcon color="black" size={sizes['1.5']} />
      </Pressable>
    ),
  };

  return {
    defaultStyle,
    questionaryRatingAgency,
    signUpQuestions,
    defaultNavigation,
    comments,
    bottomTab,
    agency,
    suggestAgency,
  };
};

export { useHeaderStyle };
