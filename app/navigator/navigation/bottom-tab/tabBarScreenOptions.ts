import { ADDITIONAL_COLOURS, MAIN_COLOURS } from 'styles/colours';

export const tabBarScreenOptions = {
  tabBarActiveTintColor: MAIN_COLOURS['primary-black'],
  tabBarInactiveTintColor: ADDITIONAL_COLOURS['primary-grey'],
  tabBarLabelStyle: {},
  tabBarShowLabel: false,
  tabBarStyle: {
    backgroundColor: MAIN_COLOURS['tertiary-teal'],
    borderTopColor: MAIN_COLOURS['tertiary-teal'],
    shadowColor: MAIN_COLOURS['tertiary-teal'],
    shadowOpacity: 1,
    shadowRadius: 3,
  },
};
