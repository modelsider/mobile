import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CustomTabBar from 'navigator/navigation/bottom-tab/CustomTabBar';
import { tabBarScreenOptions } from 'navigator/navigation/bottom-tab/tabBarScreenOptions';
import { ROUTES } from 'navigator/constants/routes';
import { useNews } from 'hooks/index';
import { NewsScreen, BookmarksScreen, MyAgenciesScreen } from 'screens/index';
import { useHeaderStyle } from 'navigator/hooks/useHeaderStyle';
import { Pressable, SearchIcon, useTheme } from 'native-base';
import NavigationService from '../../NavigationService';
import { useSelector } from 'react-redux';
import { selectHasRating } from 'store/app/selectors';
import { HomeStack } from '../../stacks/home/HomeStack';

const { Navigator, Screen } = createBottomTabNavigator();

export const BottomTabNavigation = () => {
  const { sizes } = useTheme();

  const hasRating = useSelector(selectHasRating);

  const { bottomTab } = useHeaderStyle();
  const { newsBadge } = useNews();

  const handleSearch = () => {
    if (hasRating) {
      NavigationService.navigate({ routeName: ROUTES.SearchAgency });
    } else {
      NavigationService.navigate({ routeName: ROUTES.LeaveRating });
    }
  };

  return (
    <Navigator
      screenOptions={({ route: { name } }) => ({
        tabBarIcon: ({ color, focused }) => <CustomTabBar color={color} focused={focused} name={name} />,
        ...tabBarScreenOptions,
        ...bottomTab,
        headerLeft: () => {
          return (
            <Pressable onPress={handleSearch}>
              <SearchIcon color="black" size={sizes['1.5']} />
            </Pressable>
          );
        },
      })}
    >
      <Screen name={ROUTES.Home} component={HomeStack} />
      <Screen name={ROUTES.MyAgencies} component={MyAgenciesScreen} options={{ title: 'My agencies' }} />
      <Screen name={ROUTES.Bookmarks} component={BookmarksScreen} options={{ title: 'Bookmarks' }} />
      <Screen
        name={ROUTES.News}
        component={NewsScreen}
        options={newsBadge ? { tabBarBadge: newsBadge, title: 'News' } : {}}
      />
    </Navigator>
  );
};
