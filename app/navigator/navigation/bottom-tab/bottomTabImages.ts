import { icons } from '../../../../assets';

export type bottomTabImagesType = 'Home' | 'MyAgencies' | 'Bookmarks' | 'News';

export const bottomTabImages = {
  Home: icons.bottomTab.home,
  MyAgencies: icons.bottomTab.agencies,
  Bookmarks: icons.bottomTab.bookmarks,
  News: icons.bottomTab.news,
};
