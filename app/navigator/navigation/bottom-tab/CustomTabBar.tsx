import React, { FC } from 'react';
import { Image } from 'react-native';
import { bottomTabImages, bottomTabImagesType } from 'navigator/navigation/bottom-tab/bottomTabImages';
import { Text, VStack } from 'native-base';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

interface IProps {
  color: string;
  focused: boolean;
  name: string;
}

const CustomTabBar: FC<IProps> = ({ color, name, focused }) => {
  const insets = useSafeAreaInsets();

  return (
    <VStack alignItems="center" space="2xs" height={insets.bottom || '100%'} top={insets.bottom ? 0 : 1}>
      <Image source={bottomTabImages[name as bottomTabImagesType]} style={{ tintColor: color }} />
      <Text
        textAlign="center"
        fontFamily="body"
        fontWeight="400"
        fontStyle="normal"
        fontSize="xs"
        color={focused ? 'black' : 'gray.500'}
      >
        {name}
      </Text>
    </VStack>
  );
};

export default CustomTabBar;
