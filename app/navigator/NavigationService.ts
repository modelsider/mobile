import {} from 'react-navigation'; // todo: ???
import debounce from 'lodash/debounce';
import { createNavigationContainerRef } from '@react-navigation/native';
import { CommonActions, StackActions } from '@react-navigation/native';
import { ROUTES } from './constants/routes';

export const navigationRef = createNavigationContainerRef();

const navigate = ({ routeName }: { routeName: string }) => {
  navigationRef.dispatch(CommonActions.navigate({ name: routeName }));
};

const navigateWithParams = ({ routeName, params }: { routeName: string; params: any }) => {
  navigationRef.dispatch(
    CommonActions.navigate({
      name: routeName,
      params,
    }),
  );
};

const backHandler = () => {
  navigationRef.dispatch(CommonActions.goBack());
};

const navigateBack = debounce(backHandler, 300, {
  trailing: false,
  leading: true,
});

const canNavigateBack = () => navigationRef.canGoBack();

function resetToHome() {
  return navigationRef.dispatch(
    CommonActions.reset({
      index: 0,
      routes: [{ name: ROUTES.Home }],
    }),
  );
}

function resetToAgency(params?: any) {
  return navigationRef.dispatch(
    CommonActions.reset({
      index: 0,
      routes: [{ name: ROUTES.Agency, params }],
    }),
  );
}

function resetToLogin() {
  return navigationRef.dispatch(
    CommonActions.reset({
      index: 0,
      routes: [{ name: ROUTES.Auth }],
    }),
  );
}

const navigateToTop = () => {
  navigationRef.dispatch(StackActions.popToTop());
};

const pop = (n = 1) => navigationRef.dispatch(StackActions.pop(n));

const push = ({ routeName, params = {} }: { routeName: string; params?: any }) => {
  navigationRef.dispatch(StackActions.push(routeName, params));
};

export default {
  navigationRef,
  navigate,
  navigateWithParams,
  navigateBack,
  canNavigateBack,
  navigateToTop,
  resetToHome,
  resetToLogin,
  resetToAgency,
  pop,
  push,
};
