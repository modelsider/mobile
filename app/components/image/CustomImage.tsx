import React, { FC, useMemo, useState } from 'react';
import { ActivityIndicator, StyleSheet, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { noop } from 'types/types';
import { MAIN_COLOURS } from '../../styles/colours';
import { TextHeaderMedium } from '../typography/text-header/TextHeaderMedium';

const IMAGE_SIZE = 80;
const IMAGE_MARGIN = 8;
const PROFILE_IMAGE_SIZE = 40;
const BORDER_RADIUS = 100;

interface ICustomImage {
  image: any;
  loaderSize?: 'small' | 'large';
  withLoading?: boolean;
  resizeMode?: 'contain' | 'cover';
  type?: 'profile' | 'attachment' | 'fullscreen' | 'signUp' | 'settings';
  profileImageSize?: number;
  isDisabledProfileImage?: boolean;
  onPress?: noop;
  onLongPress?: noop;
  isLoading?: boolean;
}

const CustomImage: FC<ICustomImage> = ({
  image,
  loaderSize = 'large',
  withLoading = true,
  resizeMode = 'contain',
  type = 'attachment',
  profileImageSize = PROFILE_IMAGE_SIZE,
  isDisabledProfileImage = false,
  onPress,
  onLongPress,
  isLoading,
}) => {
  const [loaded, setLoaded] = useState<boolean>(false);

  const img = useMemo(() => `data:image/jpeg;base64,${image}`, [image]);

  const profileImage = useMemo(
    () => (typeof image === 'string' ? { uri: img, priority: FastImage.priority.high } : image),
    [image, img],
  );

  if (type === 'settings') {
    return (
      <TouchableOpacity onPress={onPress} disabled={isDisabledProfileImage} onLongPress={onLongPress}>
        <FastImage
          style={[styles.settingsImage, { width: profileImageSize, height: profileImageSize }]}
          source={profileImage}
          resizeMode={resizeMode}
        />
      </TouchableOpacity>
    );
  }

  if (type === 'profile') {
    return (
      <TouchableOpacity onPress={onPress} disabled={isDisabledProfileImage}>
        <FastImage
          style={[styles.profileImage, { width: profileImageSize, height: profileImageSize }]}
          source={profileImage}
          resizeMode={resizeMode}
        />
      </TouchableOpacity>
    );
  }

  if (type === 'signUp') {
    return (
      <TouchableOpacity
        onPress={onPress}
        disabled={isDisabledProfileImage}
        style={{ justifyContent: 'center', alignItems: 'center' }}
      >
        <FastImage
          style={[
            styles.profileImage,
            {
              width: profileImageSize,
              height: profileImageSize,
              backgroundColor: MAIN_COLOURS['primary-black'],
              alignSelf: 'center',
            },
          ]}
          source={profileImage}
          resizeMode={resizeMode}
        />
        {withLoading && isLoading && <ActivityIndicator style={styles.loader} size={loaderSize} />}
        {!isLoading && !profileImage && (
          <View style={{ position: 'absolute' }}>
            <TextHeaderMedium fontSize={18} text="Add photo" color={MAIN_COLOURS['primary-white']} />
          </View>
        )}
      </TouchableOpacity>
    );
  }

  return (
    <>
      <FastImage
        style={type === 'fullscreen' ? styles.fullscreenImage : styles.attachmentImage}
        onLoad={() => setLoaded(true)}
        source={{ uri: img, priority: FastImage.priority.normal }}
        resizeMode={resizeMode}
      />
      {withLoading && !loaded && <ActivityIndicator style={styles.loader} size={loaderSize} />}
    </>
  );
};

const styles = StyleSheet.create({
  attachmentImage: {
    marginRight: IMAGE_MARGIN,
    marginBottom: IMAGE_MARGIN,
    height: IMAGE_SIZE,
    width: IMAGE_SIZE,
  },
  fullscreenImage: {
    width: '100%',
    height: '100%',
  },
  profileImage: {
    borderRadius: BORDER_RADIUS,
  },
  settingsImage: {
    borderRadius: BORDER_RADIUS,
    borderColor: MAIN_COLOURS['primary-teal'],
    borderWidth: 1,
  },
  loader: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default CustomImage;
