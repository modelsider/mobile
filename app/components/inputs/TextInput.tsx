import React, { FC } from 'react';
import { Box, Input, Pressable, CloseIcon, FormControl, Text, WarningOutlineIcon } from 'native-base';
import { noop } from '../../types/types';
import { IInputProps } from 'native-base/src/components/primitives/Input/types';
interface IProps {
  placeholder: string;
  onClear?: noop;
  value: string;
  onChangeValue: (value: string) => void;
  type?: IInputProps['type'];
  error?: string;
  isDisabled?: boolean;
  autoCapitalize?: 'none' | 'sentences' | 'words' | 'characters' | undefined;
}

export const TextInput: FC<IProps> = ({
  placeholder,
  onClear,
  value,
  onChangeValue,
  type = 'text',
  error = '',
  isDisabled = false,
  autoCapitalize = 'words',
}) => {
  const isError = !!error;

  return (
    <FormControl isInvalid={isError}>
      <Input
        w="100%"
        value={value}
        onChangeText={onChangeValue}
        placeholder={placeholder}
        type={type}
        padding="3"
        borderRadius="10"
        borderColor={isError ? 'red.500' : 'gray.500'}
        color={isError ? 'red.500' : 'black'}
        _focus={{ bg: 'white', borderColor: 'gray.500' }}
        _input={{ fontSize: 'md' }}
        isDisabled={isDisabled}
        bg="white"
        autoCapitalize={autoCapitalize}
        // @ts-ignore
        InputRightElement={
          isError ? (
            <Box paddingRight="3">
              <WarningOutlineIcon size="lg" color="red.500" />
            </Box>
          ) : (
            !!value && (
              <Pressable onPress={onClear} paddingRight="3">
                <CloseIcon />
              </Pressable>
            )
          )
        }
      />
      <FormControl.ErrorMessage>
        <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md">
          {error}
        </Text>
      </FormControl.ErrorMessage>
    </FormControl>
  );
};
