import React, { FC } from 'react';
import { ADDITIONAL_COLOURS } from 'styles/colours';
import { IconSvg } from 'components/iconography/IconSvg';
import { ICONS } from 'components/iconography/iconography-params';
import { Box, Pressable, useTheme, Input } from 'native-base';
import { MAX_SYMBOLS_TO_LEAVE_COMMENT } from 'constants/index';
import { Keyboard } from 'react-native';

interface IMultilineInput {
  placeholder: string;
  onChangeText: (e: any) => void;
  value: string;
  editable?: boolean;
  isError?: boolean;
  width?: number;
  onClear?: () => void;
  autoCapitalize?: 'none' | 'sentences' | 'words' | 'characters';
  autoFocus?: boolean;
  maxLength?: number;
  returnKeyType?: 'done' | 'go' | 'next' | 'search' | 'send';
}

export const MultilineInput: FC<IMultilineInput> = ({
  placeholder,
  onChangeText,
  value,
  editable = true,
  onClear,
  autoCapitalize = 'sentences',
  autoFocus = false,
  maxLength = MAX_SYMBOLS_TO_LEAVE_COMMENT,
  returnKeyType = 'done',
}) => {
  const { space, colors } = useTheme();

  const clearButton = !!value && (
    <Pressable onPress={onClear} position="absolute" right={space['0.5']} top={space['0.5']}>
      <IconSvg // TODO: separate all svg icons as icon component
        name={ICONS.close}
        width={22}
        height={22}
        color={ADDITIONAL_COLOURS['primary-grey']}
        transform="translate(14.142) rotate(45)"
        viewBox="0 0 28.284 28.285"
      />
    </Pressable>
  );

  return (
    <Box alignItems="center">
      <Input
        placeholder={placeholder}
        placeholderTextColor={colors.gray['300']}
        onChangeText={onChangeText}
        editable={editable}
        value={value}
        autoCapitalize={autoCapitalize}
        autoFocus={autoFocus}
        maxLength={maxLength}
        fontSize="sm"
        pr="7"
        height="32"
        multiline={true}
        textAlignVertical="top"
        returnKeyType={returnKeyType}
        onSubmitEditing={() => Keyboard.dismiss()}
      />
      {clearButton}
    </Box>
  );
};
