import React, { FC } from 'react';
import { StyleSheet, TextInput, TouchableOpacity, View } from 'react-native';
import { convertPtToResponsive } from 'utils/layoutUtils';
import { ADDITIONAL_COLOURS, MAIN_COLOURS } from 'styles/colours';
import { resolveConditionHandler } from 'utils/resolveConditionHandler';
import {
  getValidateBackgroundColor,
  getValidateBorderColor,
  getValidateInputColor,
  getValidateShadowColor,
  getValidateShadowOpacity,
} from 'utils/input';
import { IconSvg } from 'components/iconography/IconSvg';
import { ICONS } from 'components/iconography/iconography-params';
import { FONTS } from 'styles/fonts';
interface IInput {
  placeholder: string;
  onChangeText: (e: any) => void;
  backgroundColor?: string;
  value: string;
  color?: string;
  borderColor?: string;
  editable?: boolean;
  isError?: boolean;
  isSuccess?: boolean;
  width?: number;
  height?: number;
  onClear?: () => void;
  onFocus?: () => void;
  onTouchEnd?: () => void;
  multiline?: boolean;
  numberOfLines?: number;
  autoCapitalize?: 'none' | 'sentences' | 'words' | 'characters';
  autoFocus?: boolean;
}

export const Input: FC<IInput> = ({
  placeholder,
  onChangeText,
  color = MAIN_COLOURS['primary-black'],
  borderColor = ADDITIONAL_COLOURS['primary-grey'],
  value,
  backgroundColor = MAIN_COLOURS['primary-white'],
  editable = true,
  isError = false,
  isSuccess = false,
  width = 310,
  height = 40,
  onClear,
  onFocus,
  onTouchEnd,
  multiline = false,
  numberOfLines = 1,
  autoCapitalize = 'words',
  autoFocus = false,
}) => {
  const innerStyles = {
    container: {
      color: getValidateInputColor(isError, isSuccess, color),
      borderColor: getValidateBorderColor(isError, isSuccess, editable, borderColor),
      backgroundColor: getValidateBackgroundColor(editable, backgroundColor),
      shadowColor: getValidateShadowColor(isError, isSuccess),
      shadowOpacity: getValidateShadowOpacity(isError, isSuccess),
      width: convertPtToResponsive(width),
      height: multiline ? 140 : convertPtToResponsive(height),
      alignItems: multiline ? 'flex-start' : 'center',
      paddingTop: multiline ? convertPtToResponsive(10) : 0,
    },
    clearButton: {
      top: multiline ? convertPtToResponsive(10) : null,
    },
  };

  const clearButton = resolveConditionHandler(
    !!value,
    <TouchableOpacity onPress={onClear} style={[styles.clearButton, innerStyles.clearButton]}>
      <IconSvg // TODO: separate all svg icons as icon component
        name={ICONS.close}
        width={22}
        height={22}
        color={ADDITIONAL_COLOURS['primary-grey']}
        transform="translate(14.142) rotate(45)"
        viewBox="0 0 28.284 28.285"
      />
    </TouchableOpacity>,
    <View />,
  );

  return (
    <View style={[styles.container, innerStyles.container]}>
      <TextInput
        placeholder={placeholder}
        placeholderTextColor={ADDITIONAL_COLOURS['primary-grey']}
        onChangeText={onChangeText}
        editable={editable}
        value={value}
        style={[styles.textInput, { height }]}
        multiline={multiline}
        numberOfLines={numberOfLines}
        autoCapitalize={autoCapitalize}
        onFocus={onFocus}
        onSubmitEditing={onTouchEnd}
        autoFocus={autoFocus}
        returnKeyType="done"
      />
      {clearButton}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
    borderWidth: convertPtToResponsive(1),
    borderRadius: convertPtToResponsive(10),
    paddingHorizontal: convertPtToResponsive(15),
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
  textInput: {
    width: '100%',
    fontSize: convertPtToResponsive(15),
    fontFamily: FONTS.SFUIText.regular,
    paddingRight: convertPtToResponsive(15),
    color: MAIN_COLOURS['primary-black'],
    textAlignVertical: 'top',
  },
  clearButton: {
    position: 'absolute',
    right: 10,
  },
});
