import React, { FC, useEffect } from 'react';
import { StyleSheet } from 'react-native';
import { useAppDispatch } from 'hooks/index';
import { setLogoAppLoader, setStatusBar } from 'store/app/reducer';
import { Center, Text } from 'native-base';
import { LogoIcon } from '../logo-icon/LogoIcon';

const TIMEOUT_MS = 3000;

export const LogoLoader: FC = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(setStatusBar({ isStatusBar: true }));
    const timeout = setTimeout(() => dispatch(setLogoAppLoader({ isLoading: false })), TIMEOUT_MS);

    return () => {
      clearTimeout(timeout);
    };
  }, [dispatch]);

  return (
    <Center {...StyleSheet.absoluteFillObject} bg="white">
      <LogoIcon size="56" animated={true} />
      <Text textAlign="center" fontFamily="mono" fontWeight="400" fontStyle="normal" fontSize="3xl" color="black">
        Modelsider
      </Text>
    </Center>
  );
};
