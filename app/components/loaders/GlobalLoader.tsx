import React, { FC } from 'react';
import { ActivityIndicator, StyleSheet } from 'react-native';
import { Center } from 'native-base';

interface IProps {
  isLoading: boolean;
}

export const GlobalLoader: FC<IProps> = ({ isLoading = false }) => {
  return (
    <>
      {isLoading && (
        <Center {...StyleSheet.absoluteFillObject} bg="gray.100" opacity={0.5}>
          <ActivityIndicator size="large" color="black" />
        </Center>
      )}
    </>
  );
};
