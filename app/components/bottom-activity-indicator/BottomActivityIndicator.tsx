import React, { FC } from 'react';
import { ActivityIndicator } from 'react-native';

const BottomActivityIndicator: FC = () => {
  return <ActivityIndicator size="large" />;
};

export default BottomActivityIndicator;
