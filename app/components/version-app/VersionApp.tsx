import React, { FC } from 'react';
import { Text } from 'native-base';

export const VersionApp: FC = () => {
  const version = `version 1.8.3`;
  return (
    <Text fontFamily="mono" fontWeight="400" fontStyle="normal" fontSize="sm" color="black">
      {version}
    </Text>
  );
};
