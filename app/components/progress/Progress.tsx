import React, { FC } from 'react';
import { HStack, Circle, CheckIcon } from 'native-base';

export interface ISteps {
  border?: boolean;
  check: boolean;
}

export interface ICheckDotsProps {
  steps: ISteps[];
}

export const Progress: FC<ICheckDotsProps> = ({ steps }) => {
  return (
    <HStack space="2" alignSelf="center">
      {steps?.map(({ border, check }, index) => {
        return (
          <Circle key={index} height="5" width="5" borderColor={border ? 'black' : 'gray.300'} borderWidth="2">
            {check && <CheckIcon color="black" size="3" />}
          </Circle>
        );
      })}
    </HStack>
  );
};
