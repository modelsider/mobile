import React, { FC } from 'react';
import { Circle } from 'components/circles/circle/Circle';
import { HStack, useTheme } from 'native-base';

const progressSteps = [{ index: 1 }, { index: 2 }, { index: 3 }, { index: 4 }];

interface IProps {
  currentIndex: number;
}

export const ProgressWelcome: FC<IProps> = ({ currentIndex }) => {
  const { colors } = useTheme();

  return (
    <HStack space="2">
      {progressSteps.map(({ index }) => {
        const color = index === currentIndex ? colors.teal['100'] : colors.gray['100'];

        return <Circle key={index} color={color} size={5} />;
      })}
    </HStack>
  );
};
