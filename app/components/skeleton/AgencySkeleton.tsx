import React from 'react';
import { StyleSheet, View } from 'react-native';
import { SkeletonElement } from './SkeletonElement';
import SkeletonWrapper from './SkeletonWrapper';
import { RATED_STATISTICS_TITLES } from '../../constants';
import { TextBodyNormal } from 'components/typography/text-body/TextBodyNormal';
import { ADDITIONAL_COLOURS, MAIN_COLOURS } from 'styles/colours';
import { agencyType } from 'types/types';

const MARGIN_TOP_10 = 10;
const MARGIN_TOP_15 = 15;
const MARGIN_VERTICAL = 45;
const PADDING = 20;

const RatedItemStats = ({ title }: { title: string }) => (
  <View style={styles.ratedItemStats}>
    <TextBodyNormal text={title} bottom={5} left={3} color={ADDITIONAL_COLOURS['primary-grey']} />
    <SkeletonElement height={16} width="100%" />
  </View>
);

const Item = ({ type }: { type: agencyType }) => (
  <View>
    <View style={styles.header}>
      <SkeletonElement top={10} />
      <SkeletonElement width={200} top={5} />
      <SkeletonElement width={200} top={10} height={15} />
    </View>
    <View style={styles.stats}>
      {RATED_STATISTICS_TITLES[type].map(({ title }, index) => {
        return <RatedItemStats key={index} title={title} />;
      })}
    </View>
    <View style={styles.links}>
      <View style={styles.link}>
        <SkeletonElement width="100%" />
      </View>
      <View style={styles.link}>
        <SkeletonElement width="100%" />
      </View>
    </View>
  </View>
);

export const AgencySkeleton = ({ type }: { type: agencyType }) => {
  return (
    <SkeletonWrapper>
      <Item type={type} />
    </SkeletonWrapper>
  );
};

const styles = StyleSheet.create({
  header: {},
  stats: {
    marginTop: MARGIN_TOP_10,
  },
  ratedItemStats: {
    marginTop: MARGIN_TOP_15,
  },
  links: {
    marginVertical: MARGIN_VERTICAL,
    paddingTop: PADDING,
    backgroundColor: MAIN_COLOURS['tertiary-teal'],
  },
  link: {
    paddingHorizontal: PADDING,
    paddingBottom: PADDING,
  },
});
