import React from 'react';
import { StyleSheet, View } from 'react-native';
import { SkeletonElement } from './SkeletonElement';
import SkeletonWrapper from './SkeletonWrapper';
import { getScreenDimensions } from 'utils/layoutUtils';

const DEFAULT_ITEMS = 3;
const ONE_ITEM = 1;

const Item = () => (
  <View style={styles.container}>
    <SkeletonElement width={40} height={40} borderRadius={100} />

    <View style={styles.section}>
      <View style={styles.header}>
        <SkeletonElement width={150} height={20} />
        <SkeletonElement width={60} height={15} />
      </View>

      <SkeletonElement width="100%" height={100} top={10} />

      <View style={styles.footer}>
        <SkeletonElement width={30} />
        <SkeletonElement width={30} />
        <SkeletonElement width={30} />
      </View>
    </View>
  </View>
);

export const CommentItemSkeleton = ({ items = DEFAULT_ITEMS }) => {
  return (
    <SkeletonWrapper>
      {items === ONE_ITEM ? (
        <Item />
      ) : (
        <>
          <Item />
          <Item />
          <Item />
        </>
      )}
    </SkeletonWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 10,
    paddingBottom: 10,
  },
  section: {
    paddingLeft: 5,
    width: getScreenDimensions().width - 35 * 2 - 40, // todo: fix!!!!
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 30,
  },
});
