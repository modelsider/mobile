import React from 'react';
import { StyleSheet, View } from 'react-native';
import { SkeletonElement } from './SkeletonElement';
import SkeletonWrapper from './SkeletonWrapper';

const Item = () => (
  <View style={styles.container}>
    <SkeletonElement width={50} height={10} />
    <SkeletonElement width="100%" height={20} top={10} />
    <SkeletonElement width="100%" height={200} top={30} />
  </View>
);

export const NewsItemSkeleton = () => {
  return (
    <SkeletonWrapper>
      <Item />
      <Item />
      <Item />
    </SkeletonWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
});
