import React from 'react';
import { StyleSheet, View } from 'react-native';
import { SkeletonElement } from './SkeletonElement';
import SkeletonWrapper from './SkeletonWrapper';
import { PADDING_LEFT } from 'components/user-item/UserItem';
import { SEPARATOR_HEIGHT } from 'screens/who-rated-agency-screen/WhoRatedAgency';

const Item = () => (
  <View style={styles.container}>
    <SkeletonElement width={40} height={40} borderRadius={100} />

    <View style={styles.text}>
      <SkeletonElement height={15} />
      <SkeletonElement height={10} top={10} width={40} />
    </View>
  </View>
);

export const UserItemSkeleton = () => {
  return (
    <SkeletonWrapper>
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
    </SkeletonWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingBottom: SEPARATOR_HEIGHT,
  },
  text: {
    paddingLeft: PADDING_LEFT,
  },
});
