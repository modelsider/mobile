import React, { FC } from 'react';
import { StyleSheet, View } from 'react-native';
import { ADDITIONAL_COLOURS } from 'styles/colours';

const BORDER_RADIUS = 10;
const WIDTH = 140;
const HEIGHT = 25;

interface ISkeletonElement {
  width?: number | string;
  height?: number | string;
  top?: number | string;
  bottom?: number | string;
  right?: number | string;
  left?: number | string;
  borderRadius?: number;
}

export const SkeletonElement: FC<ISkeletonElement> = ({
  width = WIDTH,
  height = HEIGHT,
  top,
  bottom,
  left,
  right,
  borderRadius = BORDER_RADIUS,
}) => {
  return (
    <View
      style={[
        styles.line,
        {
          width,
          height,
          marginTop: top,
          marginBottom: bottom,
          marginLeft: left,
          marginRight: right,
          borderRadius,
        },
      ]}
    />
  );
};

const styles = StyleSheet.create({
  line: {
    backgroundColor: ADDITIONAL_COLOURS['secondary-grey'],
    borderRadius: BORDER_RADIUS,
  },
});
