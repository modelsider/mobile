import React, { FC, ReactNode } from 'react';
import { Animated } from 'react-native';
import { usePulseAnimation } from 'hooks/index';

interface ISkeletonWrapper {
  children: ReactNode;
}

const SkeletonWrapper: FC<ISkeletonWrapper> = ({ children }) => {
  const animValue = usePulseAnimation({});

  return <Animated.View style={{ opacity: animValue }}>{children}</Animated.View>;
};

export default SkeletonWrapper;
