import React, { FC } from 'react';
import { StyleSheet, View } from 'react-native';
import { SkeletonElement } from './SkeletonElement';
import SkeletonWrapper from './SkeletonWrapper';

const PADDING = 20;
const ONE_ITEM = 1;

interface IProps {
  withDate?: boolean;
  items?: number;
}

const Item: FC<IProps> = ({ withDate = true }) => (
  <View style={styles.container}>
    <View style={styles.section1}>
      <SkeletonElement />
      <SkeletonElement top={10} height={20} />
    </View>
    <View style={styles.section2}>
      <SkeletonElement />
      {withDate && <SkeletonElement top={10} height={20} />}
    </View>
  </View>
);

export const AgenciesSkeleton: FC<IProps> = ({ withDate = true, items }) => {
  return (
    <SkeletonWrapper>
      {items === ONE_ITEM ? (
        <Item withDate={withDate} />
      ) : (
        <>
          <Item withDate={withDate} />
          <Item withDate={withDate} />
          <Item withDate={withDate} />
          <Item withDate={withDate} />
          <Item withDate={withDate} />
          <Item withDate={withDate} />
          <Item withDate={withDate} />
          <Item withDate={withDate} />
          <Item withDate={withDate} />
        </>
      )}
    </SkeletonWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: PADDING,
  },
  section1: {
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  section2: {
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
});
