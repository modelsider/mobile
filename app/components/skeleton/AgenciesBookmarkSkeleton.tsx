import React from 'react';
import { StyleSheet, View } from 'react-native';
import { SkeletonElement } from './SkeletonElement';
import SkeletonWrapper from './SkeletonWrapper';

const DEFAULT_ITEMS = 3;
const ONE_ITEM = 1;
const PADDING = 20;

const Item = ({ isAgencyHeader = false }) => {
  const top = isAgencyHeader ? 7 : 10;
  const pv = isAgencyHeader ? 0 : PADDING;

  const innerStyles = {
    paddingVertical: pv,
  };

  return (
    <View style={[styles.container, innerStyles]}>
      <View style={styles.section}>
        <SkeletonElement height={17} />
        <SkeletonElement top={top} height={15} />
      </View>
      <SkeletonElement borderRadius={5} width={20} height={30} />
    </View>
  );
};

export const AgenciesBookmarkSkeleton = ({ items = DEFAULT_ITEMS }) => {
  return (
    <SkeletonWrapper>
      {items === ONE_ITEM ? (
        <Item isAgencyHeader={true} />
      ) : (
        <>
          <Item />
          <Item />
          <Item />
        </>
      )}
    </SkeletonWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  section: {
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
});
