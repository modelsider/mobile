import React, { FC } from 'react';
import { IAgency } from 'types/types';
import { Box, Divider, Pressable, Text, FlatList } from 'native-base';
import { keyExtractor } from 'utils/keyExtractor';

interface ISearchDataList {
  data: IAgency[];
  onPress: ({ agency }: { agency: IAgency }) => void;
}

const SearchData: FC<ISearchDataList> = ({ data, onPress }) => {
  const renderItem = ({ item }: { item: IAgency }) => {
    const { agency_name, city, country } = item;

    const withCity = `${agency_name}, ${city}, ${country}`;
    const withoutCity = `${agency_name}, ${country}`;

    return (
      <Pressable onPress={() => onPress({ agency: item })} py="3" bg="teal.50" pl="5%">
        <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="xs" color="black">
          {city ? withCity : withoutCity}
        </Text>
      </Pressable>
    );
  };

  return (
    <Box maxHeight="96">
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        scrollEnabled={true}
        showsVerticalScrollIndicator={true}
        keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps="handled"
        ItemSeparatorComponent={() => <Divider width="90%" alignSelf="center" color="black" />}
        borderTopLeftRadius="md"
        borderTopRightRadius="md"
        borderBottomLeftRadius="md"
        borderBottomRightRadius="md"
      />
    </Box>
  );
};

export default SearchData;
