import React, { FC } from 'react';
import { Pressable, useTheme, Text } from 'native-base';

interface ICustomTabViewButton {
  isActive: boolean;
  onIndexChange: (i: number) => void;
  idx: number;
  title: string;
}

export const CustomTabViewButton: FC<ICustomTabViewButton> = ({ isActive, title, idx, onIndexChange }) => {
  const { colors, space } = useTheme();

  const color = isActive ? colors.black : colors.gray['300'];
  const borderBottomColor = isActive ? colors.black : colors.gray['100'];

  return (
    <Pressable
      flex={1}
      pt={space['1']}
      pb={space['0.5']}
      alignItems="center"
      borderBottomWidth={space['1.5']}
      borderBottomColor={borderBottomColor}
      onPress={() => onIndexChange(idx)}
    >
      <Text fontFamily="heading" fontWeight="600" fontStyle="normal" fontSize="sm" color={color}>
        {title}
      </Text>
    </Pressable>
  );
};
