import * as React from 'react';
import { FC, useCallback, useState } from 'react';
import { NavigationState, SceneRendererProps } from 'react-native-tab-view/lib/typescript/types';
import { SceneMap, TabView } from 'react-native-tab-view';
import { CustomTabViewButton } from 'components/custom-tab-view';
import { AGENCY_TABS } from 'constants/index';
import { useAppDispatch, useDidMount } from 'hooks/index';
import { selectAgencyType } from 'store/agency/reducer';
import { ITabViewRoute } from 'types/types';
import { Box } from 'native-base';
import { setTabView } from 'store/app/reducer';
import { SCREEN_TABS_ROUTES } from './config';
import { ROUTES } from '../../navigator/constants/routes';
import analytics from '@react-native-firebase/analytics';

interface ICustomTabView {
  tabs: any;
  tabRoutes: any;
  idx?: 1 | 0;
}

export const CustomTabView: FC<ICustomTabView> = ({ tabs, tabRoutes, idx = 0 }) => {
  const dispatch = useAppDispatch();

  const [index, setIndex] = useState<number>(idx);
  const [routes, setRoutes] = useState(tabRoutes);

  const onIndexChange = useCallback(
    async (i: number) => {
      const key = routes[i]?.key;

      if (key === SCREEN_TABS_ROUTES[ROUTES.Home].left[0].key) {
        await analytics().logEvent('feed');
      }

      if (key === SCREEN_TABS_ROUTES[ROUTES.Home].right[0].key) {
        await analytics().logEvent('ratings');
      }

      if (AGENCY_TABS.includes(key)) {
        dispatch(selectAgencyType({ type: key }));
      } else {
        dispatch(setTabView({ type: key }));
      }

      setRoutes(
        routes.map((route: { key: any }) =>
          route.key === key ? { ...route, isActive: true } : { ...route, isActive: false },
        ),
      );

      setIndex(i);
    },
    [dispatch, routes],
  );

  useDidMount(() => {
    onIndexChange(index);
  });

  const renderTabBar = ({
    navigationState,
  }: SceneRendererProps & {
    navigationState: NavigationState<any>;
  }) => {
    return (
      <Box alignSelf="center" flexDirection="row">
        {navigationState.routes.map((route: ITabViewRoute, idx: number) => {
          const { isActive, title } = route;

          return (
            <CustomTabViewButton
              key={`${idx}${title}`}
              idx={idx}
              title={title}
              isActive={isActive}
              onIndexChange={onIndexChange}
            />
          );
        })}
      </Box>
    );
  };

  const renderScene = SceneMap(tabs);

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      renderTabBar={renderTabBar}
      onIndexChange={onIndexChange}
    />
  );
};
