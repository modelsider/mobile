import MotherTab from 'screens/bottom-root-screens/my-agencies-screen/components/MotherTab';
import BookingTab from 'screens/bottom-root-screens/my-agencies-screen/components/BookingTab';
import { ROUTES } from 'navigator/constants/routes';
import AgenciesTab from 'screens/bottom-root-screens/bookmarks-screen/components/AgenciesTab';
import CommentsTab from 'screens/bottom-root-screens/bookmarks-screen/components/CommentsTab';
import ProfileTab from 'screens/settings-screen/containers/ProfileTab';
import NotificationsTab from 'screens/settings-screen/containers/NotificationsTab';
import MotherTabAgency from 'screens/agency/components/MotherTab';
import BookingTabAgency from 'screens/agency/components/BookingTab';
import React from 'react';
import { LoginTab } from 'screens/sign-up/auth-screen/components/LoginTab';
import { RegisterTab } from 'screens/sign-up/auth-screen/components/RegisterTab';
import { AllAgenciesByCountryTab } from 'screens/agency-list/components/AllAgenciesByCountryTab';
import { RatedOnlyAgenciesByCountryTab } from 'screens/agency-list/components/RatedOnlyAgenciesByCountryTab';
import FeedTab from 'screens/bottom-root-screens/home-screen/components/FeedTab';
import RatingsTab from 'screens/bottom-root-screens/home-screen/components/RatingsTab';

const SCREEN_TABS_ROUTES = {
  [ROUTES.MyAgencies]: {
    left: [
      { key: 'mother', title: 'Mother', isActive: true },
      { key: 'booking', title: 'Booking', isActive: false },
    ],
    right: [
      { key: 'mother', title: 'Mother', isActive: false },
      { key: 'booking', title: 'Booking', isActive: true },
    ],
  },
  [ROUTES.Bookmarks]: {
    left: [
      { key: 'agencies', title: 'Agencies', isActive: true },
      { key: 'comments', title: 'Comments', isActive: false },
    ],
    right: [
      { key: 'agencies', title: 'Agencies', isActive: true },
      { key: 'comments', title: 'Comments', isActive: false },
    ],
  },
  [ROUTES.Settings]: {
    left: [
      { key: 'profile', title: 'Profile', isActive: true },
      { key: 'notifications', title: 'Notifications', isActive: false },
    ],
    right: [
      { key: 'profile', title: 'Profile', isActive: true },
      { key: 'notifications', title: 'Notifications', isActive: false },
    ],
  },
  [ROUTES.Agency]: {
    left: [
      { key: 'mother', title: 'Mother', isActive: true },
      { key: 'booking', title: 'Booking', isActive: false },
    ],
    right: [
      { key: 'mother', title: 'Mother', isActive: false },
      { key: 'booking', title: 'Booking', isActive: true },
    ],
  },
  [ROUTES.UserProfile]: {
    left: [
      { key: 'mother', title: 'Mother', isActive: true },
      { key: 'booking', title: 'Booking', isActive: false },
    ],
    right: [
      { key: 'mother', title: 'Mother', isActive: false },
      { key: 'booking', title: 'Booking', isActive: true },
    ],
  },
  [ROUTES.Auth]: {
    left: [
      { key: 'login', title: 'Login', isActive: true },
      { key: 'register', title: 'Register', isActive: false },
    ],
    right: [
      { key: 'login', title: 'Login', isActive: false },
      { key: 'register', title: 'Register', isActive: true },
    ],
  },
  [ROUTES.AgenciesByCountry]: {
    left: [
      { key: 'all', title: 'All', isActive: true },
      { key: 'rated', title: 'Rated only', isActive: false },
    ],
    right: [
      { key: 'all', title: 'All', isActive: false },
      { key: 'rated', title: 'Rated only', isActive: true },
    ],
  },
  [ROUTES.Home]: {
    left: [
      { key: 'feed', title: 'Feed', isActive: true },
      { key: 'ratings', title: 'Ratings', isActive: false },
    ],
    right: [
      { key: 'ratings', title: 'Ratings', isActive: false },
      { key: 'feed', title: 'Feed', isActive: true },
    ],
  },
};
const SCREEN_TABS = {
  [ROUTES.MyAgencies]: {
    mother: MotherTab,
    booking: BookingTab,
  },
  [ROUTES.Bookmarks]: {
    agencies: AgenciesTab,
    comments: CommentsTab,
  },
  [ROUTES.Settings]: {
    profile: ProfileTab,
    notifications: NotificationsTab,
  },
  [ROUTES.Agency]: {
    mother: MotherTabAgency,
    booking: BookingTabAgency,
  },
  [ROUTES.UserProfile]: {
    mother: () => <MotherTab isUserProfile={true} />,
    booking: () => <BookingTab isUserProfile={true} />,
  },
  [ROUTES.Auth]: {
    login: () => <LoginTab />,
    register: () => <RegisterTab />,
  },
  [ROUTES.AgenciesByCountry]: {
    all: () => <AllAgenciesByCountryTab />,
    rated: () => <RatedOnlyAgenciesByCountryTab />,
  },
  [ROUTES.Home]: {
    feed: () => <FeedTab />,
    ratings: () => <RatingsTab />,
  },
};

export { SCREEN_TABS_ROUTES, SCREEN_TABS };
