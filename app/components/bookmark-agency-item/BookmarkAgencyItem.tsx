import React, { FC, useMemo } from 'react';
import { BookmarksIcon } from 'components/UI/svg/BookmarksIcon';
import { useLocalStateActions, useBookmarks, useAgencies } from 'hooks/index';
import { Pressable, Text, useTheme, VStack } from 'native-base';

const BOOKMARK_ICON_SIZE = {
  width: 18,
  height: 28,
};

interface IBookmarkAgencyItem {
  title: string;
  agencyId: number;
  location: string;
  isBookmark: boolean;
  isHeader?: boolean;
}

const BookmarkAgencyItem: FC<IBookmarkAgencyItem> = ({ title, agencyId, location, isBookmark, isHeader = false }) => {
  const { space, fontSizes } = useTheme();

  const { handleNavigateToAgency } = useAgencies();

  const { onBookmarkAgencyByAgencyId } = useBookmarks();

  const { isBookmarkAgencyLocal, handleToggleBookmarkAgency } = useLocalStateActions({
    isBookmarkAgency: isBookmark,
    onBookmarkAgencyByAgencyId,
  });

  const titleFontSize = useMemo(() => (isHeader ? fontSizes.xl : fontSizes.lg), [fontSizes.lg, fontSizes.xl, isHeader]);

  return (
    <Pressable
      flexDirection="row"
      justifyContent="space-between"
      alignItems="center"
      py={space['1']}
      onPress={() => handleNavigateToAgency({ agencyId })}
    >
      <VStack space="2xs">
        <Text fontFamily="heading" fontWeight="600" fontStyle="normal" fontSize={titleFontSize} color="black">
          {title}
        </Text>

        <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="sm" color="gray.300">
          {location}
        </Text>
      </VStack>

      {/* todo: create component */}
      <Pressable onPress={() => handleToggleBookmarkAgency({ agencyId })}>
        <BookmarksIcon
          isBookmark={isBookmarkAgencyLocal}
          withSign={false}
          width={BOOKMARK_ICON_SIZE.width}
          height={BOOKMARK_ICON_SIZE.height}
        />
      </Pressable>
    </Pressable>
  );
};

export default BookmarkAgencyItem;
