import { Avatar, Box } from 'native-base';
import { icons } from '../../../assets';
import React, { FC } from 'react';

export const NUMBER_OF_AVATARS_TO_SHOW = 3;

interface IProps {
  images: string[];
}

const AvatarGroup: FC<IProps> = ({ images }) => {
  return (
    <Box pl="4">
      <Avatar.Group
        _avatar={{
          size: 'sm',
        }}
        max={NUMBER_OF_AVATARS_TO_SHOW}
      >
        {images.map((image: string, index: number) => {
          const source = image.length ? { uri: `data:image/jpeg;base64,${image}` } : icons.defaultUser;
          return <Avatar key={index} source={source} />;
        })}
      </Avatar.Group>
    </Box>
  );
};

export default AvatarGroup;
