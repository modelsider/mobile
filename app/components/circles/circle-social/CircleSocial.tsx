import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { IconSvg } from 'components/iconography/IconSvg';
import { ICONS } from 'components/iconography/iconography-params';
import { TextBodyNormal } from 'components/typography/text-body/TextBodyNormal';
import { Circle } from 'components/circles/circle/Circle';
import { FONTS } from 'styles/fonts';
import { INSTAGRAM_LINK_MODELSIDER } from '@env';
import { InAppBrowser } from 'react-native-inappbrowser-reborn';
export interface ICircleSocial {
  color: string;
  size: number;
  text: string;
  left?: number;
}

export const CircleSocial: FC<ICircleSocial> = ({ color, size, text, left = 15 }) => {
  const onInstagram = async () => {
    if (await InAppBrowser.isAvailable()) {
      await InAppBrowser.open(INSTAGRAM_LINK_MODELSIDER, {
        modalEnabled: false,
      });
    }
  };

  return (
    <TouchableOpacity onPress={onInstagram} style={{ left }}>
      <View style={styles.container}>
        <View style={styles.socialContainer}>
          <View style={styles.icon}>
            <IconSvg name={ICONS.instagram} />
          </View>
          <TextBodyNormal text={text} fontFamily={FONTS.SFUIText.semibold} />
        </View>
        <View style={styles.circle}>
          <Circle color={color} size={size} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
  socialContainer: {
    alignItems: 'center',
    position: 'absolute',
    zIndex: 1,
    right: '1%',
  },
  icon: {
    paddingBottom: '3%',
  },
  circle: {
    zIndex: 0,
  },
});
