export const CIRCLE_PARAMETERS = {
  large: {
    size: 97,
  },
  middle: {
    size: 64,
  },
  small: {
    size: 48,
  },
  smaller: {
    size: 16,
  },
};
