import React, { FC, ReactNode } from 'react';
import { Box } from 'native-base';

interface IProps {
  color: string;
  size: number;
  children?: ReactNode;
}

export const Circle: FC<IProps> = ({ color, size, children }) => {
  return (
    <Box bg={color} w={size} h={size} borderRadius={500} justifyContent="center">
      {children}
    </Box>
  );
};
