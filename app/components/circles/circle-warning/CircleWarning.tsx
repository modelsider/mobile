import React, { FC } from 'react';
import { StyleSheet, View } from 'react-native';
import { ADDITIONAL_COLOURS, MAIN_COLOURS } from 'styles/colours';
import { IconSvg } from 'components/iconography/IconSvg';
import { ICONS } from 'components/iconography/iconography-params';
import { TextBodySmall } from 'components/typography/text-body/TextBodySmall';
import { Circle } from 'components/circles/circle/Circle';
import { CIRCLE_PARAMETERS } from 'components/circles/circle/circle-params';

interface ICircleWarning {
  text: string;
}

export const CircleWarning: FC<ICircleWarning> = ({ text }) => {
  return (
    <View style={styles.container}>
      <View style={styles.warningDescription}>
        <IconSvg name={ICONS.warning} transform="translate(-0.96 -1.931)" />
        <View style={styles.warningText}>
          <TextBodySmall text={text} color={ADDITIONAL_COLOURS['primary-grey']} />
        </View>
      </View>
      <Circle color={MAIN_COLOURS['primary-teal']} size={CIRCLE_PARAMETERS.middle.size} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  warningDescription: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 2,
    top: '35%',
    right: '16.5%',
  },
  warningText: {
    width: '85%',
    left: '65%',
  },
});
