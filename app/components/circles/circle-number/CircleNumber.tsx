import React, { FC } from 'react';
import { Circle } from 'components/circles/circle/Circle';
import { Text } from 'native-base';

export interface ICircleNumber {
  color: string;
  size: number;
  text: string;
}

export const CircleNumber: FC<ICircleNumber> = ({ color, size, text }) => {
  return (
    <Circle color={color} size={size}>
      <Text textAlign="center" fontFamily="heading" fontWeight="600" fontStyle="normal" fontSize="md" color="black">
        {text}
      </Text>
    </Circle>
  );
};
