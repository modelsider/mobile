import React, { FC, useCallback } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { IconSvg } from 'components/iconography/IconSvg';
import { ICONS } from 'components/iconography/iconography-params';
import { TextBodyNormal } from 'components/typography/text-body/TextBodyNormal';
import { Circle } from 'components/circles/circle/Circle';
import { FONTS } from 'styles/fonts';
import { ADDITIONAL_COLOURS, MAIN_COLOURS } from 'styles/colours';
import { InAppBrowser } from 'react-native-inappbrowser-reborn';
import { URL_INSTAGRAM } from '@env';

const CIRCLE_SIZE = 31;
const INSTAGRAM_POSITION = {
  top: 3,
  left: -3,
};

export interface ICircleUserInstagram {
  color?: string;
  size?: number;
  text?: string;
}

const CircleUserInstagram: FC<ICircleUserInstagram> = ({
  color = MAIN_COLOURS['primary-teal'],
  size = CIRCLE_SIZE,
  text = '',
}) => {
  const handleInstagram = useCallback(async () => {
    if (await InAppBrowser.isAvailable()) {
      await InAppBrowser.open(`${URL_INSTAGRAM}${text}`, {
        modalEnabled: false,
      });
    }
  }, [text]);

  return (
    <TouchableOpacity onPress={handleInstagram} style={styles.container}>
      <Circle color={color} size={size}>
        <View style={styles.instagram}>
          <IconSvg name={ICONS.instagram} />
          <TextBodyNormal
            text={`@${text}`}
            fontFamily={FONTS.SFUIText.regular}
            fontSize={14}
            color={ADDITIONAL_COLOURS['primary-grey']}
            left={5}
            width="100%"
          />
        </View>
      </Circle>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '30%',
  },
  instagram: {
    flexDirection: 'row',
    position: 'absolute',
    alignSelf: 'center',
    top: INSTAGRAM_POSITION.top,
    left: INSTAGRAM_POSITION.left,
  },
});

export default CircleUserInstagram;
