import React, { FC } from 'react';
import { Ghost } from 'components/UI/ghost/Ghost';
import { Text, VStack } from 'native-base';

interface IGhostEmpty {
  title: string;
  textColor?: string;
}

const GhostEmpty: FC<IGhostEmpty> = ({ title, textColor = 'gray.300' }) => {
  return (
    <VStack alignItems="center">
      <Ghost rotate={0} type="right" height="95" width="68" />
      <Text fontFamily="mono" fontWeight="400" fontStyle="normal" fontSize="3xl" color={textColor} textAlign="center">
        {title}
      </Text>
    </VStack>
  );
};

export default GhostEmpty;
