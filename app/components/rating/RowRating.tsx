import React, { FC } from 'react';
import { Box, Flex, Text, useTheme } from 'native-base';

interface IRowRating {
  rating: number;
  color: string;
  isRated: boolean;
}

export const RowRating: FC<IRowRating> = ({ rating = 0.0, color, isRated }) => {
  const { space } = useTheme();

  const text = isRated ? `${rating}%` : '';
  const width = `${rating - rating / 5}%`;

  return (
    <Flex direction="row" alignItems="center" justifyContent="space-between">
      <Box height={space['1']} width={width} borderRadius={space['3']} bg={color} />

      <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="lg" color="gray.300">
        {text}
      </Text>
    </Flex>
  );
};
