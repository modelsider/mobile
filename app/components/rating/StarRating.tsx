import React, { FC } from 'react';
import { Rating } from 'react-native-ratings';
import { calculateStarRatingColor } from 'functions/rating-agency/calculateStarRatingColor';
import { HStack, Text, useTheme } from 'native-base';

const TOTAL_RATING_EMPTY = '??';
const RATING_COUNT = 5;
const FRACTIONS = 1;
const MIN_VALUE = 0;
const SIZES = {
  21: 21,
  17: 17,
  10: 10,
};

interface IProps {
  rating: string;
  ratingNumberSize?: number;
  isStarsNumber?: boolean;
  starSize?: number;
  isRated: boolean;
  isClosed?: boolean;
}

export const StarRating: FC<IProps> = ({
  isRated,
  rating,
  ratingNumberSize = 18,
  isStarsNumber = true,
  starSize = SIZES['17'],
  isClosed = false,
}) => {
  const { colors, space } = useTheme();

  const currentRating = rating === 0 || rating === undefined ? TOTAL_RATING_EMPTY : rating?.toString();

  const ratingColor = isRated ? calculateStarRatingColor(rating) : colors.gray['100'];

  const textColor = currentRating === TOTAL_RATING_EMPTY ? colors.gray['100'] : colors.black;

  return (
    <HStack space={space['0.5']} flexDirection="row" alignItems="center" justifyContent="flex-start">
      <Rating
        type="custom"
        ratingCount={RATING_COUNT}
        ratingColor={isClosed ? colors.gray['300'] : ratingColor}
        ratingBackgroundColor={colors.gray['100']}
        imageSize={starSize}
        readonly={true}
        fractions={FRACTIONS}
        minValue={MIN_VALUE}
        startingValue={rating}
        tintColor={colors.white}
      />

      {isStarsNumber && (
        <Text
          fontFamily="heading"
          fontWeight="600"
          fontStyle="normal"
          fontSize={ratingNumberSize}
          color={isClosed ? 'gray.300' : textColor}
        >
          {currentRating}
        </Text>
      )}
    </HStack>
  );
};
