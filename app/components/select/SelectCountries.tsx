import React, { FC, useCallback } from 'react';
import { Box, Select, useTheme } from 'native-base';
import { useSelector } from 'react-redux';
import { selectCountries } from 'store/countries/selectors';
import { useFocusEffect } from '@react-navigation/native';
import { setCountries } from 'store/countries/actions';
import { useAppDispatch } from 'hooks/index';

interface IProps {
  isOpen: boolean;
  onChange: (country: string) => void;
}

const SelectCountries: FC<IProps> = ({ onChange, isOpen }): JSX.Element => {
  const { radii } = useTheme();

  const dispatch = useAppDispatch();
  const countries = useSelector(selectCountries);

  const handleValueChange = useCallback(
    (selectValue: string) => {
      onChange(selectValue);
    },
    [onChange],
  );

  useFocusEffect(
    useCallback(() => {
      dispatch(setCountries());
    }, [dispatch]),
  );

  return (
    <Select
      color="white"
      variant="unstyled"
      onValueChange={handleValueChange}
      fontSize="md"
      dropdownIcon={<Box />}
      _actionSheet={{
        isOpen,
      }}
      _selectedItem={{
        bg: 'gray.100',
        borderRadius: radii.full,
      }}
    >
      {countries?.map(({ id, name }) => {
        return <Select.Item key={id} label={name} value={name} />;
      })}
    </Select>
  );
};

export default SelectCountries;
