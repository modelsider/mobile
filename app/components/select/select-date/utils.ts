import moment from 'moment';

export type modeType = 'date' | 'time' | 'datetime';

export const DEFAULT_DATE = new Date(1546300800000); // 01.01.2019
const FORMAT_DATE = 'DD-MM-YYYY';
const FORMAT_SETTINGS_DATE = 'DD/MM/YYYY';

export const parsedDate = (date: Date) => moment(date).format(FORMAT_DATE);
export const parsedSettingsDate = (date: Date) =>
  moment(date).format(FORMAT_SETTINGS_DATE);

export const isError = (date: Date) => moment(date).isBefore(DEFAULT_DATE);
