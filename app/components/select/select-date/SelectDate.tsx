import React, { FC, useEffect, useState } from 'react';
import DatePicker from 'react-native-date-picker';
import { resolveConditionHandler } from 'utils/resolveConditionHandler';
import { DEFAULT_DATE, modeType, parsedSettingsDate } from 'components/select/select-date/utils';
import { SelectInput } from 'components/select/SelectInput';
import { StyleSheet, View } from 'react-native';
import { SecondaryButton } from 'components/buttons';

interface ISelectDate {
  date: any;
  title: string;
  mode: modeType;
  placeholder: string;
  setData: (data: string) => void; // remove after adding configureStore configureStore
  top?: string;
  withStillThereButton?: boolean;
  minimumDate?: any;
}

const SelectDate: FC<ISelectDate> = ({
  date: innerDate,
  title,
  mode,
  placeholder,
  setData,
  top,
  withStillThereButton = false,
  minimumDate,
}) => {
  const isPresent = innerDate === 'Present';

  const [date, setDate] = useState(isPresent ? (DEFAULT_DATE as Date) : innerDate);
  const [currentDate, setCurrentDate] = useState<string>(isPresent ? 'Present' : '');
  const [open, setOpen] = useState(false);

  const onOpen = () => {
    setOpen(true);
  };
  const onCancel = () => setOpen(false);

  const onConfirm = (selectDate: any) => {
    setCurrentDate('');
    setOpen(false);
    setDate(selectDate);
  };

  const onStillThere = () => {
    setCurrentDate('Present');
  };

  const value = resolveConditionHandler(
    parsedSettingsDate(DEFAULT_DATE) === parsedSettingsDate(date),
    placeholder,
    parsedSettingsDate(date),
  );

  useEffect(() => {
    if ((currentDate || value) !== placeholder) {
      setData(currentDate || value);
    }
  }, [value, placeholder, setData, currentDate]);

  return (
    <View style={{ top, backgroundColor: 'white' }}>
      <SelectInput placeholder={placeholder} onPress={onOpen} value={currentDate || value} />
      <DatePicker
        minimumDate={minimumDate}
        mode={mode as modeType}
        modal
        open={open}
        date={date}
        onConfirm={(selectDate) => onConfirm(selectDate)}
        onCancel={onCancel}
        title={title}
      />
      {withStillThereButton && (
        <View style={styles.stillThereButton}>
          <SecondaryButton onPress={onStillThere} text="Still there" withIcon={!!currentDate.length} />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  stillThereButton: {
    position: 'relative',
    top: '35%',
  },
});

export default SelectDate;
