import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { convertPtToResponsive } from 'utils/layoutUtils';
import { ADDITIONAL_COLOURS, MAIN_COLOURS } from 'styles/colours';
import { TextBodyNormal } from 'components/typography/text-body/TextBodyNormal';
import { resolveConditionHandler } from 'utils/resolveConditionHandler';
import { SelectArrowIcon } from 'components/UI/svg/SelectArrowIcon';
import { FONTS } from 'styles/fonts';

interface ISelect {
  placeholder: string;
  onPress: () => void;
  value: string;
}

export const SelectInput: FC<ISelect> = ({ placeholder, onPress, value }) => {
  const textColor = resolveConditionHandler(
    value !== placeholder,
    MAIN_COLOURS['primary-black'],
    ADDITIONAL_COLOURS['primary-grey'],
  );

  return (
    <View style={stylesSelect.container}>
      <TouchableOpacity onPress={onPress} style={stylesSelect.select}>
        <TextBodyNormal
          text={placeholder && value}
          color={textColor}
          fontFamily={FONTS.SFUIText.regular}
          fontSize={16}
        />
        <SelectArrowIcon />
      </TouchableOpacity>
    </View>
  );
};

export const stylesSelect = StyleSheet.create({
  container: {
    borderWidth: convertPtToResponsive(1),
    borderRadius: convertPtToResponsive(10),
    borderColor: ADDITIONAL_COLOURS['primary-grey'],
  },
  select: {
    padding: convertPtToResponsive(10),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
