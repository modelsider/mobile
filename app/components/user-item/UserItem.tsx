import React, { FC } from 'react';
import { ADDITIONAL_COLOURS } from 'styles/colours';
import { TextHeaderMedium } from 'components/typography/text-header/TextHeaderMedium';
import { IUserInfo } from 'store/auth/reducer';
import CustomImage from 'components/image/CustomImage';
import { timeDifference } from 'utils/timeDifference';
import { TextBodySmall } from 'components/typography/text-body/TextBodySmall';
import { noop } from 'types/types';
import { icons } from '../../../assets';
import { HStack, Pressable, VStack } from 'native-base';

const { anonymous, defaultUser } = icons;
export const PADDING_LEFT = 15;

interface IUserItem {
  user: IUserInfo;
  onPress: noop;
}

const UserItem: FC<IUserItem> = ({ user, onPress }) => {
  const { profilePictureUrl, is_anonymous, name, createdAt } = user;

  const userProfilePicture = is_anonymous ? anonymous : profilePictureUrl || defaultUser;

  return (
    <Pressable onPress={onPress} flexDirection="row">
      <HStack space="md">
        <CustomImage image={userProfilePicture} type="profile" resizeMode="cover" />
        <VStack>
          <TextHeaderMedium text={name} fontSize={16} />
          <TextBodySmall text={timeDifference(createdAt)} color={ADDITIONAL_COLOURS['primary-grey']} />
        </VStack>
      </HStack>
    </Pressable>
  );
};

export default UserItem;
