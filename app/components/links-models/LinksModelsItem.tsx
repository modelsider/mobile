import React, { FC } from 'react';
import { IconSvg } from 'components/iconography/IconSvg';
import { agencyLinkTypes } from 'types/types';
import { HStack, Pressable, Text, useTheme } from 'native-base';

interface ILinksModelsItem {
  text: string;
  icon: any;
  type: agencyLinkTypes;
  onPress: (type: agencyLinkTypes) => void;
}

export const LinksModelsItem: FC<ILinksModelsItem> = ({ text, icon, type, onPress }) => {
  const { space } = useTheme();

  return (
    <Pressable onPress={() => onPress(type)} flexDirection="row">
      <HStack space={space['0.5']}>
        {/* todo: change icon svg */}
        <IconSvg name={icon} />

        <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black" underline={true}>
          {text}
        </Text>
      </HStack>
    </Pressable>
  );
};
