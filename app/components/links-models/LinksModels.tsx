import React, { FC, useCallback } from 'react';
import { LinksModelsItem } from 'components/links-models/LinksModelsItem';
import { ICONS } from 'components/iconography/iconography-params';
import { agencyLinkTypes } from 'types/types';
import { useSelector } from 'react-redux';
import { selectAgency } from 'store/agency/selectors';
import { getInstagramText, getWebText } from 'utils/links';
import { Box, useTheme, VStack } from 'native-base';
import { InAppBrowser } from 'react-native-inappbrowser-reborn';

export const LinksModels: FC = () => {
  const { space } = useTheme();

  const { web, instagram } = useSelector(selectAgency);

  const onLink = useCallback(
    async (type: agencyLinkTypes) => {
      const link = type === 'web' ? web : instagram;
      if (await InAppBrowser.isAvailable()) {
        await InAppBrowser.open(link, {
          modalEnabled: false,
        });
      }
    },
    [instagram, web],
  );

  return (
    <VStack space={space['0.5']} my={space['1.5']} bg="teal.50" borderRadius="2xl" padding={space['0.5']}>
      {!!web && (
        <Box p={space['0.5']}>
          <LinksModelsItem
            text={getWebText(web)}
            icon={ICONS.share} // todo: change icon
            type="web"
            onPress={onLink}
          />
        </Box>
      )}

      {!!instagram && (
        <Box p={space['0.5']}>
          <LinksModelsItem
            text={getInstagramText(instagram)}
            icon={ICONS.instagram} // todo: change icon
            type="instagram"
            onPress={onLink}
          />
        </Box>
      )}
    </VStack>
  );
};
