import React, { FC } from 'react';
import { Text } from 'native-base';

interface IProps {
  text: string;
}

const FeedItemLabel: FC<IProps> = ({ text }) => {
  return (
    <Text fontFamily="body" fontWeight="500" fontStyle="normal" fontSize="xs">
      {text}
    </Text>
  );
};

export { FeedItemLabel };
