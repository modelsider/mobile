import React, { FC } from 'react';
import { icons } from '../../../assets';
import { Image } from 'native-base';
import { Animated } from 'react-native';
import { usePulseAnimation } from 'hooks/index';
import { noop } from '../../types/types';

interface IProps {
  size?: string | number;
  animated?: boolean;
  onLoad?: noop;
}
export const LogoIcon: FC<IProps> = ({ size = '32', animated = false, onLoad }) => {
  const animValue = usePulseAnimation({ duration: 800, to: 0.1 });

  return (
    <>
      {animated ? (
        <Animated.View style={{ opacity: animValue }}>
          <Image source={icons.logo} h={size} w={size} alt="logo" onLoad={onLoad} />
        </Animated.View>
      ) : (
        <Image source={icons.logo} h={size} w={size} alt="logo" />
      )}
    </>
  );
};
