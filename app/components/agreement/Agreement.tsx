import React, { FC, useCallback } from 'react';
import { AgreementIcon } from 'components/UI/svg/AgreementIcon';
import { Circle } from 'components/circles/circle/Circle';
import { InAppBrowser } from 'react-native-inappbrowser-reborn';
// @ts-ignore
import { TERMS_AND_CONDITIONS_LINK } from '@env';
import { HStack, Pressable, Text } from 'native-base';

interface IProps {
  isAgreed: boolean;
  setIsAgreed: () => void;
  isDisabled?: boolean;
}

export const Agreement: FC<IProps> = ({ isAgreed, setIsAgreed, isDisabled = false }) => {
  const handleOpenTerms = useCallback(async () => {
    if (await InAppBrowser.isAvailable()) {
      await InAppBrowser.open(TERMS_AND_CONDITIONS_LINK, {
        modalEnabled: false,
      });
    }
  }, []);

  const color = isDisabled || !isAgreed ? 'gray.200' : 'black';

  return (
    <HStack alignItems="center" space="2">
      <Pressable onPress={setIsAgreed} disabled={isDisabled}>
        {isAgreed ? <AgreementIcon /> : <Circle color="gray.200" size={4} />}
      </Pressable>

      <HStack space="1">
        <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="xs" color={color}>
          I agree to
        </Text>
        <Pressable onPress={handleOpenTerms} disabled={isDisabled}>
          <Text underline={true} fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="xs" color={color}>
            Terms of Use
          </Text>
        </Pressable>
      </HStack>
    </HStack>
  );
};
