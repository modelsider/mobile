import React, { FC } from 'react';
import { AgreementIcon } from 'components/UI/svg/AgreementIcon';
import { ADDITIONAL_COLOURS } from 'styles/colours';
import { Circle } from 'components/circles/circle/Circle';
import { noop } from 'types/types';
import { Spacer, Pressable, Text } from 'native-base';
import { HIT_SLOP } from 'constants/index';

interface ISymbolsCounter {
  isAnonymous: boolean;
  handleIsAnonymous: noop;
}

export const AnonymousAgreement: FC<ISymbolsCounter> = ({ isAnonymous, handleIsAnonymous }) => {
  return (
    <Pressable onPress={handleIsAnonymous} hitSlop={HIT_SLOP} flexDirection="row" alignItems="center">
      {isAnonymous ? <AgreementIcon /> : <Circle color={ADDITIONAL_COLOURS['secondary-grey']} size={15} />}
      <Spacer size="2" />
      <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="sm" color="gray.300">
        Anonymous
      </Text>
    </Pressable>
  );
};
