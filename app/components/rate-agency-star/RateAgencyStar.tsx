import React, { FC } from 'react';
import { StarIcon } from 'components/UI/svg/StarIcon';
import { ratedStatisticsType } from '../../constants';
import { Flex, Pressable, useTheme } from 'native-base';
import { useScaleAnimation } from 'hooks/index';

interface IProps {
  rateItem: number;
  rateColor: string;
  statisticType: ratedStatisticsType;
  onStar: ({ item, statisticType }: { item: number; statisticType: ratedStatisticsType }) => void;
}

const RateAgencyStar: FC<IProps> = ({ rateItem, rateColor, onStar, statisticType }) => {
  const { colors } = useTheme();

  return (
    <Flex direction="row" justifyContent="space-between">
      {[1, 2, 3, 4, 5].map((item, index) => {
        const color = rateItem >= item ? rateColor : colors.gray['100'];

        // eslint-disable-next-line react-hooks/rules-of-hooks
        const { handlePressOut, handlePressIn, transform } = useScaleAnimation(1, 0.5);

        return (
          <Pressable
            key={index}
            onPress={() => onStar({ item, statisticType })}
            onPressIn={handlePressIn}
            onPressOut={handlePressOut}
            // @ts-ignore
            withOpacity={false}
          >
            <StarIcon color={color} transform={transform} />
          </Pressable>
        );
      })}
    </Flex>
  );
};

export default RateAgencyStar;
