import React, { FC, useEffect, useState } from 'react';
import { Divider, Pressable, Text, ChevronDownIcon } from 'native-base';
import { ISupportContent, noop } from '../../types/types';
import { useDimensions } from '@react-native-community/hooks';

interface IProps {
  item: ISupportContent;
  onUpdateLayout: noop;
}

export const ExpandableText: FC<IProps> = ({ item, onUpdateLayout }) => {
  const {
    screen: { width },
  } = useDimensions();
  const [layoutHeight, setLayoutHeight] = useState<number | null>(0);

  const { isExpanded, title, description } = item;

  const rotate = isExpanded ? '180deg' : '0deg';

  useEffect(() => {
    if (isExpanded) {
      setLayoutHeight(null);
    } else {
      setLayoutHeight(0);
    }
  }, [isExpanded]);

  return (
    <>
      <Pressable onPress={onUpdateLayout} flexDirection="row" justifyContent="space-between" alignItems="center">
        <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="lg" color="black" width={width / 1.3}>
          {title}
        </Text>
        <ChevronDownIcon style={{ transform: [{ rotate }] }} />
      </Pressable>

      <Text height={layoutHeight} fontFamily="body" fontWeight="300" fontStyle="normal" fontSize="md" color="black">
        {description}
      </Text>

      <Divider my="5" />
    </>
  );
};
