import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';
import { ICONOGRAPHY_PARAMS } from 'components/iconography/iconography-params';

interface IconSvgProps {
  name: string;
  color?: string;
  width?: number | string;
  height?: number | string;
  viewBox?: string;
  fillRule?: string;
  transform?: string;
}

export const IconSvg: FC<IconSvgProps> = ({
  name,
  color = ICONOGRAPHY_PARAMS.color,
  width = ICONOGRAPHY_PARAMS.width,
  height = ICONOGRAPHY_PARAMS.height,
  viewBox = ICONOGRAPHY_PARAMS.viewBox,
  fillRule = ICONOGRAPHY_PARAMS.fillRule,
  transform = ICONOGRAPHY_PARAMS.transform,
}) => {
  return (
    <Svg width={width} height={height} viewBox={viewBox}>
      <Path d={name} fill={color} fillRule={fillRule} transform={transform} />
    </Svg>
  );
};
