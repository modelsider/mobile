import React, { FC } from 'react';
import { ActivityIndicator, Animated, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { HStack, Text } from 'native-base';
import { noop } from '../../types/types';

interface IProps {
  onPress?: noop;
  onPressIn?: noop;
  onPressOut?: noop;
  text: string;
  bg: string;
  borderColor: string;
  color: string;
  disabled: boolean;
  isLoading: boolean;
  rightIcon?: any;
  transform?: any; // array
}

export const BaseButton: FC<IProps> = ({
  onPress,
  onPressIn,
  onPressOut,
  text,
  disabled,
  color,
  borderColor,
  bg,
  isLoading,
  rightIcon = null,
  transform = null,
}) => {
  return (
    <TouchableWithoutFeedback
      onPress={onPress}
      onPressIn={onPressIn}
      onPressOut={onPressOut}
      disabled={disabled || isLoading}
    >
      <Animated.View
        style={{
          ...styles.button,
          backgroundColor: bg,
          borderColor,
          transform,
        }}
      >
        {isLoading ? (
          <HStack justifyContent="center" space="xs">
            <Text textAlign="center" fontFamily="body" fontWeight="700" fontStyle="normal" fontSize="md" color={color}>
              {text} {rightIcon && rightIcon}
            </Text>
            <ActivityIndicator size="small" />
          </HStack>
        ) : (
          <Text textAlign="center" fontFamily="body" fontWeight="700" fontStyle="normal" fontSize="md" color={color}>
            {text} {rightIcon && rightIcon}
          </Text>
        )}
      </Animated.View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  button: {
    borderRadius: 1000,
    padding: 5,
    borderWidth: 3,
    width: '100%',
  },
});
