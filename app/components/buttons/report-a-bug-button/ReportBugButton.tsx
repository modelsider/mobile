import React, { FC, useCallback } from 'react';
import { Center, Pressable, Text, useTheme } from 'native-base';
import { BugIcon } from '../../UI/svg/BugIcon';
import { InAppBrowser } from 'react-native-inappbrowser-reborn';
// @ts-ignore
import { REPORT_A_BUG_LINK } from '@env';

export const ReportBugButton: FC = (): JSX.Element => {
  const { space, colors } = useTheme();

  const handleReportBug = useCallback(async () => {
    if (await InAppBrowser.isAvailable()) {
      await InAppBrowser.open(REPORT_A_BUG_LINK, {
        modalEnabled: false,
      });
    }
  }, []);

  return (
    <Pressable
      flexDirection="row"
      justifyContent="space-between"
      alignItems="center"
      py={space['1.5']}
      onPress={handleReportBug}
    >
      <Center flexDirection="row">
        <BugIcon color={colors.gray['300']} />

        <Text
          fontFamily="heading"
          fontWeight="600"
          fontStyle="normal"
          fontSize="md"
          left={space['0.5']}
          color="gray.300"
        >
          Report a bug
        </Text>
      </Center>
    </Pressable>
  );
};
