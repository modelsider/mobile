import React, { FC } from 'react';
import { noop } from '../../../types/types';
import { Pressable, Text } from 'native-base';

interface IProps {
  onPress: noop;
  text: string;
  disabled?: boolean;
}

export const UnderlineWordButton: FC<IProps> = ({ onPress, text, disabled = false }) => {
  return (
    <Pressable onPress={onPress} disabled={disabled}>
      <Text
        underline={true}
        textAlign="center"
        fontFamily="body"
        fontWeight="400"
        fontStyle="normal"
        fontSize="md"
        color={disabled ? 'gray.500' : 'black'}
      >
        {text}
      </Text>
    </Pressable>
  );
};
