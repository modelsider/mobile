import React, { FC } from 'react';
import { BaseButton } from '../BaseButton';
import { StillThereIcon } from 'components/UI/svg/StillThereIcon';
import { useScaleAnimation } from 'hooks/index';

interface IPrimaryButton {
  onPress: () => void;
  text: string;
  disabled?: boolean;
  isLoading?: boolean;
  withIcon?: boolean;
}

export const SecondaryButton: FC<IPrimaryButton> = ({
  onPress,
  text,
  disabled = false,
  isLoading = false,
  withIcon = false,
}) => {
  const { handlePressOut, handlePressIn, transform } = useScaleAnimation();
  return (
    <BaseButton
      onPress={onPress}
      onPressIn={handlePressIn}
      onPressOut={handlePressOut}
      transform={transform}
      text={text}
      disabled={disabled}
      color="black"
      bg="white"
      borderColor="black"
      isLoading={isLoading}
      rightIcon={withIcon ? <StillThereIcon /> : null}
    />
  );
};
