import React, { FC } from 'react';
import { noop } from '../../../types/types';
import { BaseButton } from '../BaseButton';
import { useTheme } from 'native-base';
import { useScaleAnimation } from 'hooks/index';

interface IProps {
  onPress: noop;
  text: string;
  disabled?: boolean;
  isLoading?: boolean;
}

export const PrimaryButton: FC<IProps> = ({ onPress, text, disabled = false, isLoading = false }) => {
  const { colors } = useTheme();

  const { handlePressOut, handlePressIn, transform } = useScaleAnimation();

  return (
    <BaseButton
      onPress={onPress}
      onPressIn={handlePressIn}
      onPressOut={handlePressOut}
      transform={transform}
      text={text}
      disabled={disabled}
      color={disabled ? colors.gray['200'] : 'white'}
      bg={disabled ? colors.gray['100'] : 'black'}
      borderColor={disabled ? colors.gray['100'] : 'black'}
      isLoading={isLoading}
    />
  );
};
