import React, { FC } from 'react';
import { noop } from 'types/types';
import { Image } from 'native-base';
import { useScaleAnimation } from 'hooks/index';
import { Animated, ImageURISource, TouchableWithoutFeedback } from 'react-native';

interface IProps {
  onPress: noop;
  icon: ImageURISource;
}

export const IconButton: FC<IProps> = ({ onPress, icon }) => {
  const { handlePressOut, handlePressIn, transform } = useScaleAnimation();

  return (
    <TouchableWithoutFeedback onPress={onPress} onPressIn={handlePressIn} onPressOut={handlePressOut}>
      <Animated.View
        style={{
          transform,
        }}
      >
        <Image source={icon} alt="addPostButton" />
      </Animated.View>
    </TouchableWithoutFeedback>
  );
};
