import React, { FC } from 'react';
import { Text } from 'native-base';

interface IProps {
  counter: number;
}

export const SymbolsCounter: FC<IProps> = ({ counter }) => {
  return (
    <Text fontFamily="body" fontWeight="300" fontStyle="normal" fontSize="sm" color="gray.200">
      {counter.toString()}
    </Text>
  );
};
