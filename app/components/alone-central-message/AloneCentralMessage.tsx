import React, { FC } from 'react';
import { StyleSheet, View } from 'react-native';
import { Circle } from 'components/circles/circle/Circle';
import { CIRCLE_PARAMETERS } from 'components/circles/circle/circle-params';
import { TextHeaderMedium } from 'components/typography/text-header/TextHeaderMedium';
import { FONTS } from 'styles/fonts';
import { useTheme } from 'native-base';

const SIZES = {
  265: 265,
  20: 20,
  15: 15,
};

interface IAloneCentralMessage {
  color: string;
  text: string;
}

export const AloneCentralMessage: FC<IAloneCentralMessage> = ({ color, text }) => {
  const { space } = useTheme();

  return (
    <View style={styles.container}>
      <View style={styles.circleContainer}>
        <Circle color={color} size={CIRCLE_PARAMETERS.large.size} />
      </View>
      <View style={styles.textContainer}>
        <TextHeaderMedium text={text} fontFamily={FONTS.SFUIText.semibold} textAlign="center" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: SIZES['265'],
  },
  circleContainer: {
    position: 'absolute',
  },
  textContainer: {
    left: SIZES['20'],
    top: SIZES['15'],
  },
});
