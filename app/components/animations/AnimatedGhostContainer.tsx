import React, { FC, useCallback, useEffect, useRef } from 'react';
import { Animated, Easing } from 'react-native';

const DEFAULT_DELAY = 300;
const DEFAULT_ZINDEX = 1;
const DURATION = 200;
const SIZES = {
  90: 90,
  200: 200,
};

interface IAnimatedGhostContainer {
  top?: number | string;
  bottom?: number | string;
  left?: number | string;
  right?: number | string;
  leftAnimation?: boolean;
  rightAnimation?: boolean;
  topAnimation?: boolean;
  bottomAnimation?: boolean;
  translateAnimationX?: boolean;
  translateAnimationY?: boolean;
  delay?: number;
  zIndex?: number;
  children: any;
}

export const AnimatedGhostContainer: FC<IAnimatedGhostContainer> = ({
  top,
  left,
  right,
  bottom,
  leftAnimation = false,
  rightAnimation = false,
  topAnimation = false,
  bottomAnimation = false,
  translateAnimationX = false,
  translateAnimationY = false,
  delay = DEFAULT_DELAY,
  zIndex = DEFAULT_ZINDEX,
  children,
}) => {
  const animatedValue = useRef(new Animated.Value(0)).current;

  const startAnimation = useCallback(
    (toValue: 0 | 1) => {
      Animated.timing(animatedValue, {
        toValue,
        duration: DURATION,
        delay: delay,
        easing: Easing.linear,
        useNativeDriver: true,
      }).start();
    },
    [animatedValue, delay],
  );

  useEffect(() => {
    startAnimation(1);
  }, [startAnimation]);

  const translateX = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [leftAnimation ? -SIZES['90'] : 0, rightAnimation ? -SIZES['90'] : 0],
    extrapolate: 'clamp',
  });

  const translateY = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [topAnimation ? 0 : SIZES['200'], bottomAnimation ? SIZES['90'] : 0],
    extrapolate: 'clamp',
  });

  const translateAnimation = (translateAnimationX && { translateX }) || (translateAnimationY && { translateY });

  const validateStyles = {
    position: 'absolute',
    top: top,
    left: left,
    bottom: bottom,
    right: right,
    transform: [translateAnimation],
    zIndex: zIndex,
  };

  return <Animated.View style={validateStyles}>{children}</Animated.View>;
};
