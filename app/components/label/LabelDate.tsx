import React, { FC } from 'react';
import { Text } from 'native-base';

interface ILabelDate {
  date: string;
}

export const LabelDate: FC<ILabelDate> = ({ date }) => {
  return (
    <Text flexBasis="30%" textAlign="right" fontFamily="heading" fontWeight="400" fontStyle="normal" fontSize="xs" color="gray.300">
      {date}
    </Text>
  );
};
