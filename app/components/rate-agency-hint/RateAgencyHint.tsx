import React, { FC } from 'react';
import { HintCloseButton } from 'components/UI/svg/HintCloseButton';
import { Divider, HStack, Modal, Pressable, Text } from 'native-base';
import { EMOJIS } from '../emoji-icon/emoji';

interface IProps {
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  advantages: string[];
  disadvantages: string[];
}

export const RateAgencyHint: FC<IProps> = ({ isVisible, setIsVisible, advantages, disadvantages }) => {
  return (
    <>
      <Modal isOpen={isVisible} animationPreset="slide" bg="gray.50">
        <Modal.Content width="100%" height="100%">
          <Modal.Body>
            <Text fontSize="3xl">{EMOJIS.like}</Text>
            {advantages?.map((advantage, index) => (
              <HStack key={index} space="3" py="2" alignItems="center" justifyContent="flex-start">
                <Text fontSize="2xl">{EMOJIS.done}</Text>
                <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="xl" lineHeight="sm">
                  {advantage}
                </Text>
              </HStack>
            ))}

            <Divider my="10" />

            <Text fontSize="3xl">{EMOJIS.dislike}</Text>
            {disadvantages?.map((disadvantage, index) => (
              <HStack key={index} space="3" py="2" alignItems="center" justifyContent="flex-start">
                <Text fontSize="2xl">{EMOJIS.error}</Text>
                <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="xl" lineHeight="sm">
                  {disadvantage}
                </Text>
              </HStack>
            ))}
          </Modal.Body>
          <Modal.Footer justifyContent="center">
            <Pressable onPress={() => setIsVisible(false)}>
              <HintCloseButton />
            </Pressable>
          </Modal.Footer>
        </Modal.Content>
      </Modal>
    </>
  );
};
