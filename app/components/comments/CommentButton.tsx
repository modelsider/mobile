import React, { FC } from 'react';
import { commentType, userInfoType } from 'types/types';
import { Pressable, Text, useTheme } from 'native-base';

interface ICommentButton {
  title: string;
  type: commentType | userInfoType;
  isSelect?: boolean;
  onPress: (type: commentType & userInfoType) => void;
}
// todo: use BaseButton
export const CommentButton: FC<ICommentButton> = ({ title, isSelect, onPress, type }) => {
  const { colors, space } = useTheme();

  const color = isSelect ? colors.white : colors.black;
  const backgroundColor = isSelect ? colors.blue['500'] : colors.white;
  const borderColor = isSelect ? colors.blue['500'] : colors.gray['300'];

  return (
    <Pressable
      onPress={() => onPress(type as commentType)}
      backgroundColor={backgroundColor}
      borderColor={borderColor}
      alignItems="center"
      borderRadius="3xl"
      borderWidth={1}
      p={space['0.5']}
      mr={space['0.5']}
      mb={space['0.5']}
    >
      <Text fontFamily="heading" fontWeight="600" fontStyle="normal" fontSize="md" color={color}>
        {title}
      </Text>
    </Pressable>
  );
};
