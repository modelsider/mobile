import React, { FC } from 'react';
import CustomImage from 'components/image/CustomImage';
import { noop } from 'types/types';
import { Pressable } from 'native-base';

interface ICommentAttachedImagesItem {
  image: string;
  onPress: ({ uri }: { uri: string }) => void;
  onLongPress: noop;
}

export const CommentAttachedImagesItem: FC<ICommentAttachedImagesItem> = ({ image, onPress, onLongPress }) => {
  return (
    <Pressable onPress={() => onPress({ uri: image })} onLongPress={onLongPress}>
      <CustomImage image={image} withLoading={false} resizeMode="cover" />
    </Pressable>
  );
};
