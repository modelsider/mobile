import React, { FC } from 'react';
import { LabelDate } from 'components/label/LabelDate';
import { Box, HStack, Text } from 'native-base';

interface ICommentHeader {
  name: string;
  date: string;
  isCurrentUserComment?: boolean;
}

export const CommentHeader: FC<ICommentHeader> = ({ name, date, isCurrentUserComment }) => {
  return (
    <Box flexDirection="row" alignItems="flex-start" justifyContent="space-between">
      <HStack flexBasis="70%" alignItems="center" space="xs">
        <Text numberOfLines={2} adjustsFontSizeToFit fontFamily="heading" fontWeight="600" fontStyle="normal" fontSize="md" color="black">
          {name}
        </Text>
        {isCurrentUserComment && (
          <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="xs" color="gray.300">
            (yours)
          </Text>
        )}
      </HStack>
      <LabelDate date={date} />
    </Box>
  );
};
