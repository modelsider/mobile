import React, { FC, useMemo } from 'react';
import { CommentHeader, CommentFooter } from 'components/comments';
import { CommentAttachedImages } from 'components/comments/comment/CommentAttachedImages';
import { formatDate } from 'utils/getMonth';
import { IUserInfo } from 'store/auth/reducer';
import { IComment as ICommentGlobal, IPost, IReply, noop } from 'types/types';
import CustomImage from 'components/image/CustomImage';
import { useUsers, useComments, useBottomSheet, useDidMount } from 'hooks/index';
import { useSelector } from 'react-redux';
import { selectUserInfo } from 'store/auth/selectors';
import { icons } from '../../../../assets';
import { Box, HStack, Pressable, Text, useTheme, VStack } from 'native-base';
import isUndefined from 'lodash/isUndefined';
import { FeedItemLabel } from '../../feed/FeedItemLabel';
import NavigationService from 'navigator/NavigationService';
import { ROUTES } from 'navigator/constants/routes';
import { selectComment } from 'store/comments/selectors';

const { anonymous, defaultUser, deletedUser } = icons;

interface IComment {
  comment: ICommentGlobal | IReply | IPost;
  isReply?: boolean;
  isOpenedComment?: boolean; // need to change reply button behavior on CommentScreen link: https://www.notion.so/Phase-4-Comments-7bb2e69e63374366a7bb368c18fc2e4a#9cfa58f0d37b4336ab5d05699d7869e3
  commentIndex?: number;
  isNavigateBack?: boolean;
  onRefresh?: noop;
  isFeed?: boolean;
  isPost?: boolean;
}

export const Comment: FC<IComment> = ({
  comment,
  isReply = false,
  isOpenedComment = false,
  commentIndex = 0,
  isNavigateBack = false,
  onRefresh,
  isFeed = false,
  isPost = false,
}) => {
  const { space } = useTheme();

  const currentUser = useSelector(selectUserInfo);
  const parentComment = useSelector(selectComment);

  const { navigateToUserProfile } = useUsers();
  const { handleReportCommentSheet, handleCommentSheet, handeClickedCommentIndex, fetchComment } = useComments();
  const { handlePostSheet, handleReportPostSheet } = useBottomSheet();

  const user: IUserInfo = comment?.user!;
  const isDeleted = isUndefined(user);
  // todo: move to mapping
  const name = useMemo(() => {
    const deletedName = isDeleted && 'Deleted';
    const anonymousName = comment?.is_anonymous && 'Anonymous';
    return deletedName || anonymousName || user?.name;
  }, [isDeleted, comment.is_anonymous]);
  // todo: move to mapping
  const userProfilePicture = useMemo(() => {
    const deletedIcon = isDeleted && deletedUser;
    const anonymousIcon = comment?.is_anonymous && anonymous;
    const profileIcon = user?.profilePictureUrl || defaultUser;
    return deletedIcon || anonymousIcon || profileIcon;
  }, [isDeleted, deletedUser, comment.is_anonymous, anonymous, defaultUser]);

  const isCurrentUserComment = user?.id === currentUser.id;

  const onLongPress =
    isPost && isCurrentUserComment
      ? () => handlePostSheet({ post: comment as IPost, onRefresh, isNavigateBack })
      : isPost && !isCurrentUserComment
      ? () => handleReportPostSheet({ post: comment as IPost, onRefresh, isNavigateBack })
      : isCurrentUserComment
      ? () => handleCommentSheet({ comment, onRefresh, parentComment })
      : () => handleReportCommentSheet({ comment });

  useDidMount(() => {
    if (!comment.text && !comment.id) {
      fetchComment({ commentId: comment.id });
    }
  });

  return (
    <HStack space={space['1']}>
      <CustomImage
        image={userProfilePicture}
        type="profile"
        resizeMode="cover"
        onPress={() => navigateToUserProfile({ userId: user.id })}
        isDisabledProfileImage={comment?.is_anonymous || isDeleted}
      />

      <Pressable
        width="100%"
        onPress={() => {
          if (isReply) {
            return;
          }
          handeClickedCommentIndex({ commentIndex });
          return NavigationService.navigateWithParams({
            routeName: ROUTES.CommentScreen,
            params: { commentOrReplyOrPost: comment },
          });
        }}
        onLongPress={onLongPress} // todo: add bottom sheet for replies
      >
        {/* todo: 81% fix !!!! */}
        <VStack space={2} width="81%">
          <Box>
            <CommentHeader
              date={formatDate(comment?.created_at, true)}
              name={name}
              isCurrentUserComment={isCurrentUserComment && comment?.is_anonymous}
            />
            {isFeed && <FeedItemLabel text={`new comment for ${comment?.agency_name || 'agency'}`} />}
          </Box>

          <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black">
            {comment?.text?.trim()}
          </Text>

          {!!comment?.attachments && (
            <CommentAttachedImages attachments={comment?.attachments} isCurrentUserComment={isCurrentUserComment} />
          )}

          {!isReply && <CommentFooter comment={comment} isOpenedComment={isOpenedComment} isPost={isPost} />}
        </VStack>
      </Pressable>
    </HStack>
  );
};
