import React, { FC, useCallback, useEffect, useState } from 'react';
import { CommentFooterItem } from 'components/comments';
import { useAppDispatch, useReplies } from 'hooks/index';
import { BookmarksIcon } from 'components/UI/svg/BookmarksIcon';
import { LikeIcon } from 'components/UI/svg/LikeIcon';
import { CommentIcon } from 'components/UI/svg/CommentIcon';
import { Flex } from 'native-base';
import { dislikeCommentFB, likeCommentFB } from 'services/comments';
import { IComment, IPost, IReply } from 'types/types';
import { addBookmarkFB, removeBookmarkFB } from 'services/bookmarks';
import { updateFeed } from 'store/feed/reducer';

const ICON_SIZES = {
  bookmark: {
    width: 14,
    height: 20,
  },
};

interface IProps {
  isOpenedComment?: boolean;
  isPost?: boolean;
  comment: IComment & IReply & IPost;
}

export const CommentFooter: FC<IProps> = ({ isOpenedComment, isPost = false, comment }) => {
  const {
    isLike,
    likes_count,
    isBookmark,
    replies_count,
    id: commentId,
    user_id: commentUserId,
    docId,
    agency_type,
    agency_id,
  } = comment || ({} as IComment & IReply & IPost);

  const dispatch = useAppDispatch();

  const [isLikeLocal, setIsLikeLocal] = useState<boolean>(isLike);
  const [likesCountLocal, setLikesCountLocal] = useState<number>(likes_count);
  const [isBookmarkLocal, setIsBookmarkLocal] = useState<boolean>(isBookmark);

  const { navigateToReplyToComment } = useReplies();

  const handleToggleLikeComment = useCallback(async () => {
    setIsLikeLocal((prevState) => !prevState);

    if (isLikeLocal) {
      dispatch(updateFeed({ id: commentId, isLike: !isLikeLocal, likesCount: likesCountLocal - 1 }));
      setLikesCountLocal((prevState) => prevState - 1);
      await dislikeCommentFB({
        docId,
        commentId,
        likesCount: likesCountLocal <= 0 ? 0 : likesCountLocal - 1,
        commentUserId,
      });
    } else {
      dispatch(updateFeed({ id: commentId, isLike: !isLikeLocal, likesCount: likesCountLocal + 1 }));
      setLikesCountLocal((prevState) => prevState + 1);
      await likeCommentFB({
        docId,
        commentId,
        likesCount: likesCountLocal + 1,
        agencyType: agency_type,
        agencyId: agency_id,
      });
    }
  }, [agency_id, agency_type, commentId, commentUserId, dispatch, docId, isLikeLocal, likesCountLocal]);

  const handleToggleBookmarkComment = useCallback(async () => {
    if (!isBookmarkLocal) {
      await addBookmarkFB({ bookmarkId: commentId, type: 'comments' });
    } else {
      await removeBookmarkFB({ bookmarkId: commentId, type: 'comments' });
    }
    setIsBookmarkLocal((prevState) => !prevState);
    dispatch(updateFeed({ id: commentId, isBookmark: !isBookmarkLocal }));
  }, [commentId, dispatch, isBookmarkLocal]);

  useEffect(() => {
    setIsLikeLocal(isLike);
    setLikesCountLocal(likes_count);
    setIsBookmarkLocal(isBookmark);
  }, [isBookmark, isLike, likes_count]);

  return (
    <Flex direction="row" justifyContent="space-between">
      <CommentFooterItem
        onPress={() => navigateToReplyToComment({ commentId, isPost })}
        text={isOpenedComment ? '' : String(replies_count)}
        icon={<CommentIcon />}
      />
      <CommentFooterItem
        onPress={handleToggleLikeComment}
        text={likesCountLocal?.toString()}
        icon={<LikeIcon isLike={isLikeLocal} />}
      />
      <CommentFooterItem
        onPress={handleToggleBookmarkComment}
        icon={
          <BookmarksIcon
            isBookmark={isBookmarkLocal}
            withSign={false}
            width={ICON_SIZES.bookmark.width}
            height={ICON_SIZES.bookmark.height}
          />
        }
      />
    </Flex>
  );
};
