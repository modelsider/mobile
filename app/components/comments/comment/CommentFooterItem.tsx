import React, { FC } from 'react';
import { TextBodySmall } from 'components/typography/text-body/TextBodySmall';
import { Pressable } from 'native-base';
import { ActivityIndicator } from 'react-native';

interface IProps {
  icon?: any;
  text?: string;
  onPress: () => void;
  loading?: boolean;
}

export const CommentFooterItem: FC<IProps> = ({ icon, text = '', onPress, loading = false }) => {
  const correctText = !!text?.length && <TextBodySmall text={text} left={5} />;

  return loading ? (
    <ActivityIndicator />
  ) : (
    <Pressable flexDirection="row" justifyContent="center" alignItems="center" onPress={onPress}>
      {icon}
      {correctText}
    </Pressable>
  );
};
