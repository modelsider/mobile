import React, { FC } from 'react';
import { CommentAttachedImagesItem } from 'components/comments/comment/CommentAttachedImagesItem';
import { useImageFullScreen, useReplies, useBottomSheet } from 'hooks/index';
import { Flex } from 'native-base';

interface ICommentAttachedImages {
  attachments: string[];
  isCurrentUserComment?: boolean;
}

export const CommentAttachedImages: FC<ICommentAttachedImages> = ({ attachments, isCurrentUserComment }) => {
  const { handleOpenImage } = useImageFullScreen({});

  const { handleRemoveAttachment } = useReplies();
  const { handleAttachmentsSheet } = useBottomSheet();

  return (
    <Flex flexDirection="row" flexWrap="wrap" justifyContent="flex-end" alignItems="flex-end">
      {attachments?.map((attachment, index) => {
        return (
          <CommentAttachedImagesItem
            key={index}
            image={attachment}
            onPress={handleOpenImage}
            onLongPress={() => {}}
            // onLongPress={async () => {
            //   if (isCurrentUserComment) {
            //     await handleAttachmentsSheet({
            //       onRemove: () => handleRemoveAttachment({ uri: attachment }),
            //     });
            //   }
            // }}
          />
        );
      })}
    </Flex>
  );
};
