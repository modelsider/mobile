import React, { FC } from 'react';
import { CommentButton } from 'components/comments';
import { commentType } from 'types/types';
import { Flex, Spacer, Text, VStack } from 'native-base';
import { EMOJIS } from '../emoji-icon/emoji';

interface IProps {
  onSortComments: ({ type }: { type: commentType }) => void;
  selectedButtons: commentType[] | string[];
  isLeaveComment?: boolean;
}

export const CommentsHeader: FC<IProps> = ({ onSortComments, selectedButtons, isLeaveComment = false }) => {
  const currentTitle = isLeaveComment ? (
    <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="xs" color="gray.400">
      Tag you comment
    </Text>
  ) : (
    <Text fontFamily="heading" fontWeight="600" fontStyle="normal" fontSize="lg" color="black">
      Comments
    </Text>
  );

  return (
    <VStack space={1}>
      {currentTitle}
      <Spacer size="2" />
      <Flex direction="row" flexWrap="wrap">
        {!isLeaveComment && (
          <CommentButton
            title={`${EMOJIS.heart} Sort by likes`}
            type="likes"
            isSelect={selectedButtons.includes('likes')}
            onPress={() => onSortComments({ type: 'likes' })}
          />
        )}
        <CommentButton
          title={`${EMOJIS.complaints} Complaints`}
          type="complaints"
          isSelect={selectedButtons.includes('complaints')}
          onPress={() => onSortComments({ type: 'complaints' })}
        />
        {isLeaveComment ? (
          <>
            <CommentButton
              title={`${EMOJIS.bravo} Bravo`}
              type="bravo"
              isSelect={selectedButtons.includes('bravo')}
              onPress={() => onSortComments({ type: 'bravo' })}
            />
            <CommentButton
              title={`${EMOJIS.apartment} Apartment`}
              type="apartment"
              isSelect={selectedButtons.includes('apartment')}
              onPress={() => onSortComments({ type: 'apartment' })}
            />
            <CommentButton
              title={`${EMOJIS.rates} Rates`}
              type="apartment"
              isSelect={selectedButtons.includes('rates')}
              onPress={() => onSortComments({ type: 'rates' })}
            />
            <CommentButton
              title={`${EMOJIS.other} Other`}
              type="bravo"
              isSelect={selectedButtons.includes('other')}
              onPress={() => onSortComments({ type: 'other' })}
            />
          </>
        ) : (
          <>
            <CommentButton
              title={`${EMOJIS.apartment} Apartment`}
              type="apartment"
              isSelect={selectedButtons.includes('apartment')}
              onPress={() => onSortComments({ type: 'apartment' })}
            />
            <CommentButton
              title={`${EMOJIS.rates} Rates`}
              type="apartment"
              isSelect={selectedButtons.includes('rates')}
              onPress={() => onSortComments({ type: 'rates' })}
            />
            <CommentButton
              title={`${EMOJIS.bravo} Bravo`}
              type="bravo"
              isSelect={selectedButtons.includes('bravo')}
              onPress={() => onSortComments({ type: 'bravo' })}
            />
            <CommentButton
              title={`${EMOJIS.other} Other`}
              type="bravo"
              isSelect={selectedButtons.includes('other')}
              onPress={() => onSortComments({ type: 'other' })}
            />
          </>
        )}
      </Flex>
    </VStack>
  );
};
