import React, { FC } from 'react';
import { MultilineInput } from 'components/inputs';
import { SymbolsCounter } from 'components/symbols-counter/SymbolsCounter';
import { AnonymousAgreement } from 'components/agreement';
import { IconSvg } from 'components/iconography/IconSvg';
import { ICONS } from 'components/iconography/iconography-params';
import { noop } from 'types/types';
import { Box, Pressable, VStack } from 'native-base';

const COUNTER_TOP = -20;

interface ICommentInput {
  value: string;
  setValue: (value: string) => void;
  valueCounter: number;
  isAnonymous: boolean;
  handleToggleIsAnonymous: noop;
  handleAddAttachments: noop;
  isCamera?: boolean;
}

export const CommentInput: FC<ICommentInput> = ({
  value,
  setValue,
  valueCounter,
  handleToggleIsAnonymous,
  isAnonymous,
  handleAddAttachments,
  isCamera = true,
}) => {
  return (
    <VStack space="2">
      <Box position="absolute" top={COUNTER_TOP} right={0}>
        <SymbolsCounter counter={valueCounter} />
      </Box>

      <MultilineInput onChangeText={setValue} value={value} placeholder="Type here" onClear={() => setValue('')} />

      <Box flexDirection="row" justifyContent="space-between">
        <AnonymousAgreement isAnonymous={isAnonymous} handleIsAnonymous={handleToggleIsAnonymous} />
        {isCamera && (
          <Pressable onPress={handleAddAttachments}>
            {/* todo: change svg */}
            <IconSvg name={ICONS.camera} />
          </Pressable>
        )}
      </Box>
    </VStack>
  );
};
