import React, { FC, ReactNode, useEffect, useRef, useState } from 'react';
import { Animated, StyleSheet } from 'react-native';
import { useBackHandler } from '@react-native-community/hooks';
import Lottie from 'lottie-react-native';
import { lottie } from '../../../assets';
import { useTheme } from 'native-base';

export const TIME_TO_LOADING_SPLASH_APP = 5500;

interface IProps {
  children: ReactNode;
}

export const SplashScreenApp: FC<IProps> = ({ children }) => {
  const { colors } = useTheme();

  const [completed, setCompleted] = useState<boolean>(false);

  const animationRef = useRef<Lottie>(null);
  const animation = useRef(new Animated.Value(1)).current;

  useEffect(() => {
    Animated.timing(animation, {
      toValue: 0,
      duration: 500,
      delay: TIME_TO_LOADING_SPLASH_APP,
      useNativeDriver: true,
    }).start(() => {
      animationRef?.current?.reset();
      setCompleted(true);
    });
  }, [animation]);

  useBackHandler(() => true);

  return (
    <>
      {children}
      {!completed && (
        <Animated.View
          style={[{ ...StyleSheet.absoluteFillObject, backgroundColor: colors.teal['200'] }, { opacity: animation }]}
        >
          <Lottie ref={animationRef} source={lottie.splash} autoPlay speed={2} />
        </Animated.View>
      )}
    </>
  );
};
