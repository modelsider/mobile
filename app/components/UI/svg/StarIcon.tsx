import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';
import { Animated } from 'react-native';
import { ADDITIONAL_COLOURS } from 'styles/colours';

interface IAgreementIcon {
  size?: number;
  color?: string;
  transform?: any;
}

export const StarIcon: FC<IAgreementIcon> = ({
  size = 48,
  color = ADDITIONAL_COLOURS['secondary-grey'],
  transform,
}) => {
  return (
    <Animated.View style={{ transform }}>
      <Svg width={size} height={size} viewBox="0 0 48 48">
        <Path
          d="M2.087,20.593a2.419,2.419,0,0,1,2.061-1.682l13.637-2.083L23.9,3.852a2.351,2.351,0,0,1,4.314,0L34.323,16.8,47.96,18.911A2.429,2.429,0,0,1,49.9,20.618a2.593,2.593,0,0,1-.6,2.51l-9.9,10.04,2.4,14.256a2.567,2.567,0,0,1-.959,2.51,2.306,2.306,0,0,1-2.517.176l-12.271-6.7L13.831,50.135a2.151,2.151,0,0,1-1.1.3,2.324,2.324,0,0,1-1.414-.477,2.567,2.567,0,0,1-.959-2.51l2.4-14.256-9.9-10.04A2.582,2.582,0,0,1,2.087,20.593Z"
          transform="translate(-2.006 -2.436)"
          fill={color}
        />
      </Svg>
    </Animated.View>
  );
};
