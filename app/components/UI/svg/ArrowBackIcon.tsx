import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';

interface IArrowBackIcon {}

export const ArrowBackIcon: FC<IArrowBackIcon> = () => {
  return (
    <Svg width="20" height="12.516" viewBox="0 0 20 12.516">
      <Path
        d="M20.755,12H6.263L9.138,9.139A1.256,1.256,0,1,0,7.363,7.364l-5,5a1.293,1.293,0,0,0,0,1.776l5,5a1.256,1.256,0,1,0,1.776-1.776L6.263,14.5H20.755a1.25,1.25,0,1,0,0-2.5Z"
        transform="translate(-2.005 -6.996)"
        fill="#000"
      />
    </Svg>
  );
};
