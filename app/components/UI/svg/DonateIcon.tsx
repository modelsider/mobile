import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';

interface IDonateIcon {}

export const DonateIcon: FC<IDonateIcon> = () => {
  return (
    <Svg width="20" height="17.742" viewBox="0 0 20 17.742">
      <Path
        d="M20.165,5A6.292,6.292,0,0,0,12,4.36,6.272,6.272,0,0,0,3.84,13.843l6.212,6.222a2.781,2.781,0,0,0,3.9,0l6.212-6.222a6.272,6.272,0,0,0,0-8.842Z"
        transform="translate(-1.988 -3.121)"
        fill="#f86f6f"
      />
    </Svg>
  );
};
