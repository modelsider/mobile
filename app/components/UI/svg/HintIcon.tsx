import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';

interface IAgreementIcon {}

export const HintIcon: FC<IAgreementIcon> = () => {
  return (
    <Svg width="18.785" height="20.004" viewBox="0 0 18.785 20.004">
      <Path
        d="M3.479,4.57A3.483,3.483,0,0,1,0,1.091V0A6.075,6.075,0,0,0,1.737.84v.251a1.74,1.74,0,1,0,3.479,0V.84A6.091,6.091,0,0,0,6.958,0V1.091A3.483,3.483,0,0,1,3.479,4.57Z"
        transform="translate(5.914 15.434)"
        fill="#999"
      />
      <Path
        d="M.87.87v0h0M.87,0a.87.87,0,0,1,.87.87v.87A.87.87,0,1,1,0,1.739V.87A.87.87,0,0,1,.87,0Z"
        transform="translate(8.523)"
        fill="#999"
      />
      <Path
        d="M.87.87v0h0M.87,0a.87.87,0,0,1,.87.87v.87A.87.87,0,1,1,0,1.739V.87A.87.87,0,0,1,.87,0Z"
        transform="translate(13.742 0.584) rotate(30)"
        fill="#999"
      />
      <Path
        d="M.87.87v0h0M.87,0a.87.87,0,0,1,.87.87v.87A.87.87,0,1,1,0,1.739V.87A.87.87,0,0,1,.87,0Z"
        transform="translate(17.915 3.813) rotate(60)"
        fill="#999"
      />
      <Path
        d="M.87.87v0M.87,0h0a.87.87,0,0,1,.87.87v.87a.87.87,0,0,1-.87.87h0A.87.87,0,0,1,0,1.74V.87A.87.87,0,0,1,.87,0Z"
        transform="translate(3.13 5.044) rotate(120)"
        fill="#999"
      />
      <Path
        d="M.87.87v0h0M.87,0h0a.87.87,0,0,1,.87.87v.87a.87.87,0,0,1-.87.87h0A.87.87,0,0,1,0,1.74V.87A.87.87,0,0,1,.87,0Z"
        transform="translate(6.348 2.869) rotate(150)"
        fill="#999"
      />
      <Path
        d="M6.088,1.739a4.349,4.349,0,1,0,4.349,4.349A4.354,4.354,0,0,0,6.088,1.739M6.088,0A6.088,6.088,0,1,1,0,6.088,6.088,6.088,0,0,1,6.088,0Z"
        transform="translate(3.305 4.349)"
        fill="#999"
      />
    </Svg>
  );
};
