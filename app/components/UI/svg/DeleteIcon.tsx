import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';

interface IDeleteIcon {}

export const DeleteIcon: FC<IDeleteIcon> = () => {
  return (
    <Svg width="16.643" height="16.643" viewBox="0 0 16.643 16.643">
      <Path
        d="M15.026,16.794,8.838,10.607,2.651,16.794A1.25,1.25,0,0,1,.884,15.026L7.07,8.839.884,2.652A1.25,1.25,0,0,1,2.651.884L8.838,7.071,15.026.884a1.25,1.25,0,0,1,1.769,1.768L10.607,8.839l6.188,6.187a1.25,1.25,0,0,1-1.769,1.768Z"
        transform="translate(-0.517 -0.517)"
        fill="#999"
      />
    </Svg>
  );
};
