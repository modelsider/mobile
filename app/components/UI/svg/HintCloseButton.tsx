import React, { FC } from 'react';
import Svg, { Circle, G, Path } from 'react-native-svg';

interface IAgreementIcon {}

export const HintCloseButton: FC<IAgreementIcon> = () => {
  return (
    <Svg width="48" height="48" viewBox="0 0 48 48">
      <G fill="none" stroke="#000101" stroke-width="2">
        <Circle cx="24" cy="24" r="24" stroke="none" />
        <Circle cx="24" cy="24" r="23" fill="none" />
      </G>
      <G transform="translate(15.395 16.075)">
        <Path
          d="M15.026,16.794,8.838,10.607,2.651,16.794A1.25,1.25,0,0,1,.884,15.026L7.07,8.839.884,2.652A1.25,1.25,0,0,1,2.651.884L8.838,7.071,15.026.884a1.25,1.25,0,0,1,1.769,1.768L10.607,8.839l6.188,6.187a1.25,1.25,0,0,1-1.769,1.768Z"
          transform="translate(-0.517 -0.517)"
          fill="#010101"
        />
      </G>
    </Svg>
  );
};
