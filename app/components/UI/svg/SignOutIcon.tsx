import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';

interface ISignOutIcon {}

export const SignOutIcon: FC<ISignOutIcon> = () => {
  return (
    <Svg width="20" height="20" viewBox="0 0 20 20">
      <Path
        d="M15.71,12.71a1.034,1.034,0,0,0,0-1.42l-3-3a1,1,0,0,0-1.42,1.42L12.59,11H9a1,1,0,0,0,0,2h3.59l-1.3,1.29a1,1,0,1,0,1.42,1.42ZM22,12A10,10,0,1,0,12,22,10,10,0,0,0,22,12ZM4,12a8,8,0,1,1,8,8,8,8,0,0,1-8-8Z"
        transform="translate(-2 -2)"
        fill="#999"
      />
    </Svg>
  );
};
