import React, { FC } from 'react';
import Svg, { G, Path } from 'react-native-svg';

export const ArrowListItemIcon: FC = () => {
  return (
    <Svg width="11.676" height="20" viewBox="0 0 11.676 20">
      <G transform="translate(0 20) rotate(-90)">
        <Path
          d="M11.184,8.816,2.857.49A1.672,1.672,0,0,0,.493,2.854L7.653,10,.493,17.143a1.672,1.672,0,1,0,2.365,2.365l8.326-8.326a1.665,1.665,0,0,0,0-2.365Z"
          transform="translate(20) rotate(90)"
          fill="#999"
        />
      </G>
    </Svg>
  );
};
