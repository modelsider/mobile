import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';

interface IToastIcon {
  type?: 'success' | 'error' | 'warning';
}

const ToastIcon: FC<IToastIcon> = ({ type = 'success' }) => {
  if (type === 'success') {
    return (
      <Svg width="24" height="24" viewBox="0 0 24 24">
        <Path
          id="check-circle"
          d="M17.264,10.148l-5.148,5.16-1.98-1.98A1.2,1.2,0,1,0,8.444,15.02l2.82,2.832a1.2,1.2,0,0,0,1.692,0l6-6a1.2,1.2,0,0,0-1.692-1.7ZM14,2A12,12,0,1,0,26,14,12,12,0,0,0,14,2Zm0,21.6A9.6,9.6,0,1,1,23.6,14,9.6,9.6,0,0,1,14,23.6Z"
          transform="translate(-2 -2)"
          fill="#55bf50"
        />
      </Svg>
    );
  }
  if (type === 'warning') {
    return (
      <Svg width="24" height="24" viewBox="0 0 24 24">
        <Path
          id="exclamation-circle"
          d="M14,8a1.2,1.2,0,0,0-1.2,1.2V14a1.2,1.2,0,1,0,2.4,0V9.2A1.2,1.2,0,0,0,14,8Zm1.1,10.344A.912.912,0,0,0,15,18.128l-.144-.18a1.2,1.2,0,0,0-1.308-.252,1.38,1.38,0,0,0-.4.252,1.188,1.188,0,0,0-.252,1.308,1.08,1.08,0,0,0,.648.648,1.128,1.128,0,0,0,.912,0,1.08,1.08,0,0,0,.648-.648,1.2,1.2,0,0,0,.1-.456,1.632,1.632,0,0,0,0-.24.768.768,0,0,0-.1-.216ZM14,2A12,12,0,1,0,26,14,12,12,0,0,0,14,2Zm0,21.6A9.6,9.6,0,1,1,23.6,14,9.6,9.6,0,0,1,14,23.6Z"
          transform="translate(-2 -2)"
          fill="#efb800"
        />
      </Svg>
    );
  }

  return <></>;
};

export { ToastIcon };
