import React, { FC } from 'react';
import Svg, { Circle, G, Path } from 'react-native-svg';
import { DEFAULT_SIZE_AGREEMENT_ICON } from 'constants';
import { useTheme } from 'native-base';

export const AgreementIcon: FC = () => {
  const { colors } = useTheme();

  return (
    <Svg width={DEFAULT_SIZE_AGREEMENT_ICON} height={DEFAULT_SIZE_AGREEMENT_ICON} viewBox="0 0 16 16">
      <G transform="translate(-127 -58)">
        <Circle cx="8" cy="8" r="8" transform="translate(127 58)" fill={colors.blue['500']} />
        <Path
          d="M5.519.187h0A.522.522,0,0,0,5.107,0a.666.666,0,0,0-.375.15L1.845,3.336H1.732l-.75-.862A.484.484,0,0,0,.57,2.287a.564.564,0,0,0-.412.15.515.515,0,0,0-.037.675l1.5,2.062a.462.462,0,0,0,.375.187h.112a.636.636,0,0,0,.6-.337L5.594.862A.52.52,0,0,0,5.519.187Z"
          transform="translate(132.23 63.241)"
          fill={colors.white}
        />
      </G>
    </Svg>
  );
};
