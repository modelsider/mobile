import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';
import { ADDITIONAL_COLOURS } from 'styles/colours';

const SIZE = {
  width: 20,
  height: 17.742,
};

const likeState = {
  default:
    'M20.165,5A6.292,6.292,0,0,0,12,4.36,6.272,6.272,0,0,0,3.84,13.843l6.212,6.222a2.781,2.781,0,0,0,3.9,0l6.212-6.222a6.272,6.272,0,0,0,0-8.842Zm-1.41,7.462-6.212,6.212a.76.76,0,0,1-1.08,0L5.251,12.432a4.244,4.244,0,0,1,6-6,1,1,0,0,0,1.42,0,4.272,4.272,0,0,1,6.082,6Z',
  fill: 'M20.165,5A6.292,6.292,0,0,0,12,4.36,6.272,6.272,0,0,0,3.84,13.843l6.212,6.222a2.781,2.781,0,0,0,3.9,0l6.212-6.222a6.272,6.272,0,0,0,0-8.842Z',
};

interface ILikeIcon {
  isLike?: boolean;
  width?: number;
  height?: number;
}

export const LikeIcon: FC<ILikeIcon> = ({ isLike = false, width = SIZE.width, height = SIZE.height }) => {
  const color = isLike ? ADDITIONAL_COLOURS['primary-red'] : ADDITIONAL_COLOURS['primary-grey'];

  const d = isLike ? likeState.fill : likeState.default;

  return (
    <>
      <Svg width={width} height={height} viewBox="0 0 20 17.742">
        <Path d={d} transform="translate(-1.988 -3.121)" fill={color} />
      </Svg>
    </>
  );
};
