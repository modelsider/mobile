import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';

interface ISelectArrowIcon {}

export const SelectArrowIcon: FC<ISelectArrowIcon> = () => {
  return (
    <Svg width="12.01" height="7.012" viewBox="0 0 12.01 7.012">
      <Path
        d="M20.71,11.29l-5-5a1,1,0,1,0-1.42,1.42L18.59,12l-4.3,4.29a1,1,0,1,0,1.42,1.42l5-5a1,1,0,0,0,0-1.42Z"
        transform="translate(18.006 -13.994) rotate(90)"
        fill="#999"
      />
    </Svg>
  );
};
