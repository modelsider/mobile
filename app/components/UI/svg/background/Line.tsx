import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';
import { FigureBackgroundContainer } from './FigureBackgroundContainer';

interface ILine {
  left?: number;
  right?: number;
  top?: number;
  bottom?: number;
}

export const Line: FC<ILine> = ({ left, right, top, bottom }) => {
  return (
    <FigureBackgroundContainer top={top} bottom={bottom} left={left} right={right}>
      <Svg width="142.176" height="54.407" viewBox="0 0 142.176 54.407">
        <Path d="M135.912,47.2l-138-40L2.088-7.2l138,40Z" transform="translate(3.088 7.203)" fill="#dafffc" />
      </Svg>
    </FigureBackgroundContainer>
  );
};
