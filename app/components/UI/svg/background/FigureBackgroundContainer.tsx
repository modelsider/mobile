import React, { FC } from 'react';
import { View } from 'react-native';

interface IFigureBackgroundContainer {
  children: any;
  left?: number;
  right?: number;
  top?: number;
  bottom?: number;
}

export const FigureBackgroundContainer: FC<IFigureBackgroundContainer> = ({ children, left, right, top, bottom }) => {
  return (
    <View
      style={{
        position: 'absolute',
        left,
        right,
        top,
        bottom,
      }}
    >
      {children}
    </View>
  );
};
