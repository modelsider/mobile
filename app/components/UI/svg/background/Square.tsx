import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';
import { FigureBackgroundContainer } from './FigureBackgroundContainer';

interface ISquare {
  left?: number;
  right?: number;
  top?: number;
  bottom?: number;
}

export const Square: FC<ISquare> = ({ left, right, top, bottom }) => {
  return (
    <FigureBackgroundContainer top={top} bottom={bottom} left={left} right={right}>
      <Svg width="63.374" height="63.374" viewBox="0 0 63.374 63.374">
        <Path d="M0,0H48V48H0Z" transform="matrix(0.914, 0.407, -0.407, 0.914, 19.523, 0)" fill="#dafffc" />
      </Svg>
    </FigureBackgroundContainer>
  );
};
