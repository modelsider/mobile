import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';
import { FigureBackgroundContainer } from './FigureBackgroundContainer';

interface ICurvedLine {
  left?: number;
  right?: number;
  top?: number;
  bottom?: number;
}

export const CurvedLine: FC<ICurvedLine> = ({ left, right, top, bottom }) => {
  return (
    <FigureBackgroundContainer top={top} bottom={bottom} left={left} right={right}>
      <Svg width="116.135" height="83.139" viewBox="0 0 116.135 83.139">
        <Path
          d="M-24033.541-21939.338l-14.7-3,8.834-43.322,30.729,19.992,7.533-25.646,29.779,9.139,26.764-40.3,12.5,8.3-32.959,49.633-25.967-7.971-8.936,30.416-29.146-18.961Z"
          transform="translate(24048.24 22022.477)"
          fill="#e3e3e3"
        />
      </Svg>
    </FigureBackgroundContainer>
  );
};
