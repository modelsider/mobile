import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';
import { ADDITIONAL_COLOURS } from 'styles/colours';
import { FigureBackgroundContainer } from './FigureBackgroundContainer';

interface ICircleEmpty {
  size?: number;
  color?: string;
  left?: number;
  right?: number;
  top?: number;
  bottom?: number;
}

export const CircleEmpty: FC<ICircleEmpty> = ({
  size = 57,
  color = ADDITIONAL_COLOURS['secondary-grey'],
  left,
  right,
  top,
  bottom,
}) => {
  return (
    <FigureBackgroundContainer top={top} bottom={bottom} left={left} right={right}>
      <Svg width={`${size}`} height={`${size}`} viewBox="0 0 57 57">
        <Path
          d="M28.5,15A13.5,13.5,0,1,0,42,28.5,13.515,13.515,0,0,0,28.5,15m0-15A28.5,28.5,0,1,1,0,28.5,28.5,28.5,0,0,1,28.5,0Z"
          fill={color}
        />
      </Svg>
    </FigureBackgroundContainer>
  );
};
