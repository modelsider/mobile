import React, { FC } from 'react';
import Svg, { Circle } from 'react-native-svg';

interface IMenuIcon {}

export const MenuIcon: FC<IMenuIcon> = () => {
  return (
    <Svg width="24" height="6.617" viewBox="0 0 24 6.617">
      <Circle cx="3.309" cy="3.309" r="3.309" fill="#010101" />
      <Circle cx="3.309" cy="3.309" r="3.309" transform="translate(8.691)" fill="#010101" />
      <Circle cx="3.309" cy="3.309" r="3.309" transform="translate(17.382)" fill="#010101" />
    </Svg>
  );
};
