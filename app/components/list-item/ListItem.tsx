import React, { FC, useMemo } from 'react';
import { ArrowListItemIcon } from 'components/UI/svg/ArrowListItemIcon';
import { ROUTES } from 'navigator/constants/routes';
import { Center, Pressable, Text, useTheme } from 'native-base';

interface IListItem {
  title: string;
  icon?: FC;
  onPress: ({ routeName }: { routeName: string }) => void;
}

export const ListItem: FC<IListItem> = ({ title, icon: Icon, onPress }) => {
  const { colors, space } = useTheme();

  const withoutArrow = useMemo(() => title === ROUTES.SignOut || title === ROUTES.DeleteAccount, [title]);
  const color = withoutArrow ? colors.gray['300'] : colors.black;

  return (
    <Pressable
      flexDirection="row"
      justifyContent="space-between"
      alignItems="center"
      py={space['1.5']}
      onPress={() => onPress({ routeName: title })}
    >
      <Center flexDirection="row">
        {!!Icon && <Icon />}

        <Text fontFamily="heading" fontWeight="600" fontStyle="normal" fontSize="md" left={space['0.5']} color={color}>
          {title}
        </Text>
      </Center>
      {!withoutArrow && <ArrowListItemIcon />}
    </Pressable>
  );
};
