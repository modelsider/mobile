import React, { FC } from 'react';
import { StarRating } from 'components/rating';
import { agencyType, IAgency, noop } from 'types/types';
import { formatDate } from 'utils/getMonth';
import { formatTextWithDots } from 'utils/formatTextWithDots';
import { Pressable, Text, VStack } from 'native-base';

const SIZES = {
  2: 2,
  5: 5,
  13: 13,
  17: 17,
  20: 20,
};
const MAX_LENGTH_LOCATION = 20;

interface IAgencyItem {
  agency: IAgency;
  type: agencyType;
  isRated: boolean;
  onPress: noop;
  onLongPress?: noop;
  withDate?: boolean;
}

const AgencyItem: FC<IAgencyItem> = ({ agency, type, isRated, onPress, onLongPress, withDate = true }) => {
  const { agency_name: title, city, country, start_working_at, finish_working_at, isClosed } = agency;

  const location = city ? `${city}, ${country}` : country;
  const rating = agency?.[type]?.total_rating?.toFixed(1);
  const date = `${formatDate(start_working_at)} - ${formatDate(finish_working_at)}`;

  return (
    <Pressable flexDirection="row" justifyContent="space-between" onPress={onPress} onLongPress={onLongPress}>
      <VStack space="2xs">
        <Text
          fontFamily="heading"
          fontWeight="600"
          fontStyle="normal"
          fontSize="lg"
          color={isClosed ? 'gray.300' : 'black'}
        >
          {formatTextWithDots({ text: title })}
        </Text>

        <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="sm" color="gray.300">
          {formatTextWithDots({ text: location, length: MAX_LENGTH_LOCATION })}
        </Text>
      </VStack>

      <VStack space="2xs" alignItems="flex-end">
        <StarRating rating={rating} isStarsNumber={true} starSize={SIZES['20']} isRated={isRated} isClosed={isClosed} />

        {withDate && (
          <Text fontFamily="body" fontWeight="600" fontStyle="normal" fontSize="sm" color="gray.300">
            {date}
          </Text>
        )}
      </VStack>
    </Pressable>
  );
};

export default AgencyItem;
