export const EMOJIS = {
  hand: '👋',
  like: '👍',
  amen: '🙏',
  oops: '😬',
  hello: '👋👋🏻👋🏻👋🏽👋🏾👋🏿',
  hearts: '❤️❤️❤️',
  heart: '❤️',
  hands: '🤲🤲🏻🤲🏼🤲🏽🤲🏾🤲🏿',
  money: '🤑',
  other: '💬',
  complaints: '🤬',
  bravo: '👏',
  apartment: '🏠',
  rates: '💰',
  find: '🕵',
  app: '📱',
  turkey: '🇹🇷',
  dislike: '👎',
  done: '✅',
  error: '❌',
  congrats: '🎉',
  male: '🧔',
  female: '👩‍🦰',
  straight: '👗',
  plus: '👚',
  tall: '⭐',
  petite: '💫',
  clock: '🕒',
};
