import React, { FC } from 'react';
import { convertPtToResponsive } from 'utils/layoutUtils';
import { Text } from 'react-native';

interface IEmojiIcon {
  emojiName: string;
  fontSize?: number;
  innerStyles?: any;
  left?: number;
  bottom?: number;
}
// todo: fix!!!! with native base
export const EmojiIcon: FC<IEmojiIcon> = ({
  emojiName,
  fontSize = convertPtToResponsive(30),
  innerStyles,
  left = 5,
  bottom = 5,
}) => {
  return (
    <Text
      style={{
        fontSize,
        bottom,
        left,
        ...innerStyles,
      }}
    >
      {emojiName}
    </Text>
  );
};
