import React, { FC } from 'react';
import { LabelDate } from 'components/label/LabelDate';
import { Text, VStack } from 'native-base';

interface IProps {
  title: string;
  date: string;
}

const NewsItemHeader: FC<IProps> = ({ title, date }) => {
  return (
    <VStack space="0.5">
      <LabelDate date={date} />
      <Text fontFamily="heading" fontWeight="600" fontStyle="normal" fontSize="lg" color="black">
        {title}
      </Text>
    </VStack>
  );
};

export default NewsItemHeader;
