import React, { FC, useCallback } from 'react';
import NewsItemHeader from 'components/news/NewsItemHeader';
import ParsedText from 'react-native-parsed-text';
import { parseWord } from 'utils/parseWord';
import { INews } from 'types/types';
import { formatDate } from 'utils/getMonth';
import { Flex, useTheme } from 'native-base';
import { InAppBrowser } from 'react-native-inappbrowser-reborn';
export const PATTERN = /<(\w+)>|<(\w+ \w*)>|<(\w+ \w* \w*)>|<(\w+ \w* \w* \w*)>|<(\w+ \w* \w* \w* \w*)>/;
export interface INewsItem {
  item: INews;
}

const NewsItem: FC<INewsItem> = ({ item: { title, links, text, created_at, isNew = true } }) => {
  const { colors, space, fontSizes } = useTheme();

  const handleUrlPress = useCallback(
    async (word: string) => {
      const key = parseWord(word);
      const url = links[key];

      if (await InAppBrowser.isAvailable()) {
        await InAppBrowser.open(url, {
          modalEnabled: false,
        });
      }
    },
    [links],
  );

  const bg = isNew ? colors.teal['50'] : colors.white;

  return (
    <Flex direction="column" bg={bg} p={space['2']}>
      <NewsItemHeader title={title} date={formatDate(created_at, true)} />

      <ParsedText
        style={{
          fontSize: fontSizes.lg,
          fontFamily: 'SFUIText-Light',
          color: 'black',
        }}
        parse={[
          {
            pattern: PATTERN,
            style: { color: colors.blue['500'] },
            onPress: handleUrlPress,
            renderText: parseWord,
          },
        ]}
      >
        {text}
      </ParsedText>
    </Flex>
  );
};

export default NewsItem;
