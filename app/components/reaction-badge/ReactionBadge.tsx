import React, { FC } from 'react';
import { StyleSheet, View } from 'react-native';
import { MAIN_COLOURS } from 'styles/colours';
import { convertPtToResponsive } from 'utils/layoutUtils';
import { TextBodySmall } from 'components/typography/text-body/TextBodySmall';

interface IReactionBadge {
  number: number;
}
// todo: fix
const ReactionBadge: FC<IReactionBadge> = ({ number }) => {
  return (
    <View style={styles.container}>
      <TextBodySmall text={`+${number}`} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: MAIN_COLOURS['primary-teal'],
    width: convertPtToResponsive(23),
    height: convertPtToResponsive(17),
    borderRadius: convertPtToResponsive(3),
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: convertPtToResponsive(5),
  },
});

export default ReactionBadge;
