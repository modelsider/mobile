import React, { FC } from 'react';
import { Text } from 'native-base';

interface IProps {
  text: string;
}
// todo: check
export const Signature: FC<IProps> = ({ text }) => {
  return (
    <Text textAlign="center" fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="sm" color="black">
      {text}
    </Text>
  );
};
