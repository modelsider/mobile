import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { MAIN_COLOURS } from 'styles/colours';
import Modal from 'react-native-modal';
import { IconSvg } from 'components/iconography/IconSvg';
import { ICONS } from 'components/iconography/iconography-params';
import { GhostOopsIcon } from 'components/UI/ghost/GhostOopsIcon';
import { TextHeaderBig } from 'components/typography/text-header/TextHeaderBig';
import { FONTS } from 'styles/fonts';
import { TextBodyNormal } from 'components/typography/text-body/TextBodyNormal';

interface IErrorWorkWithAgencyLongTimeAgo {
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
}

export const ErrorWorkWithAgencyLongTimeAgo: FC<IErrorWorkWithAgencyLongTimeAgo> = ({ isVisible, setIsVisible }) => {
  return (
    <>
      <Modal
        isVisible={isVisible}
        style={styles.modal}
        animationIn="slideInUp"
        animationOut="slideOutDown"
        backdropColor={MAIN_COLOURS['tertiary-teal']}
        backdropOpacity={10}
      >
        <TouchableOpacity
          style={styles.closeButton}
          onPress={() => setIsVisible(false)}
          hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}
        >
          <IconSvg
            name={ICONS.close}
            width={25}
            height={25}
            transform="translate(14.142) rotate(45)"
            viewBox="0 0 28.284 28.285"
          />
        </TouchableOpacity>

        <GhostOopsIcon />
        <TextHeaderBig text="Oops..." textAlign="center" bottom="5%" top="5%" fontFamily={FONTS.SFUIText.regular} />

        <TextBodyNormal
          text="Seems like you have worked with this agency long time ago."
          textAlign="center"
          width={270}
          fontFamily={FONTS.SFUIText.regular}
        />

        <TextBodyNormal
          text="We only collect info not older than 3 years ago."
          textAlign="center"
          width={270}
          fontFamily={FONTS.SFUIText.regular}
          top="10%"
          bottom="10%"
        />
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  modal: {
    backgroundColor: MAIN_COLOURS['tertiary-teal'],
    alignSelf: 'center',
  },
  closeButton: {
    position: 'absolute',
    right: -22,
    top: 25,
  },
});
