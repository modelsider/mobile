import * as React from 'react';
import { StatusBar, View } from 'react-native';
import { FC } from 'react';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { resolveConditionHandler } from 'utils/resolveConditionHandler';

interface ICustomStatusBar {
  backgroundColor: string;
  isStatusBar: boolean;
  hidden?: boolean;
}

const CustomStatusBar: FC<ICustomStatusBar> = ({ backgroundColor, isStatusBar, hidden = false }) => {
  const insets = useSafeAreaInsets();

  return (
    <>
      {resolveConditionHandler(
        isStatusBar,
        <View style={{ height: insets.top, backgroundColor }}>
          <StatusBar animated={true} backgroundColor={backgroundColor} barStyle="dark-content" hidden={hidden} />
        </View>,
        null,
      )}
    </>
  );
};

export default CustomStatusBar;
