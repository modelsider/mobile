import React, { FC } from 'react';
import { WithGhostRight } from './backgrounds/WithGhostRight';
import { WithGhostLeft } from './backgrounds/WithGhostLeft';
import { ElementsOne } from './backgrounds/ElementsOne';
import { ElementsTwo } from './backgrounds/ElementsTwo';
import { DefaultBackground } from 'components/default-background';

interface IDefaultBackgroundElements {
  children: any;
  withPadding?: boolean;
  withPaddingVertical?: boolean;
  type?: 'withGhostRight' | 'withGhostLeft' | 'templateElements1' | 'templateElements2';
}

export const DefaultBackgroundElements: FC<IDefaultBackgroundElements> = ({
  children,
  withPadding = false,
  withPaddingVertical = false,
  type = 'templateElements2',
}) => {
  let template;

  if (type === 'withGhostRight') {
    template = <WithGhostRight />;
  } else if (type === 'withGhostLeft') {
    template = <WithGhostLeft />;
  } else if (type === 'templateElements1') {
    template = <ElementsOne />;
  } else {
    template = <ElementsTwo />;
  }

  return (
    <DefaultBackground withPadding={withPadding} withPaddingVertical={withPaddingVertical}>
      {template}
      {children}
    </DefaultBackground>
  );
};
