import React from 'react';
import { FigureBackgroundContainer } from 'components/UI/svg/background/FigureBackgroundContainer';
import { MAIN_COLOURS } from 'styles/colours';
import { CircleEmpty } from 'components/UI/svg/background/CircleEmpty';
import { Square } from 'components/UI/svg/background/Square';
import { Line } from 'components/UI/svg/background/Line';
import { Ghost } from 'components/UI/ghost/Ghost';
import { CurvedLine } from 'components/UI/svg/background/CurvedLine';

export const WithGhostLeft = () => {
  return (
    <>
      <Square top={70} left={-7} />
      <Line bottom={200} left={-10} />
      <CircleEmpty size={72} top={50} right={20} color={MAIN_COLOURS['secondary-teal']} />
      <CircleEmpty size={72} right={-35} top={200} color={MAIN_COLOURS['secondary-teal']} />
      <FigureBackgroundContainer top={150} left={-55}>
        <Ghost type="left" rotate={230} />
      </FigureBackgroundContainer>
      <CurvedLine right={-30} bottom={100} />
    </>
  );
};
