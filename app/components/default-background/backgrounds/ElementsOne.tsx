import React from 'react';
import { FigureBackgroundContainer } from 'components/UI/svg/background/FigureBackgroundContainer';
import { Circle } from 'components/circles/circle/Circle';
import { MAIN_COLOURS } from 'styles/colours';
import { CircleEmpty } from 'components/UI/svg/background/CircleEmpty';
import { Square } from 'components/UI/svg/background/Square';
import { Line } from 'components/UI/svg/background/Line';
import { CurvedLine } from 'components/UI/svg/background/CurvedLine';

export const ElementsOne = () => {
  return (
    <>
      <FigureBackgroundContainer top={-30} right={100}>
        <Circle color={MAIN_COLOURS['secondary-teal']} size={57} />
      </FigureBackgroundContainer>
      <CircleEmpty top={60} left={-20} />
      <Square top={200} left={-20} />
      <Line bottom={250} right={-30} />
      <CircleEmpty bottom={250} left={-20} color={MAIN_COLOURS['secondary-teal']} />
      <CircleEmpty size={72} right={-35} top={200} color={MAIN_COLOURS['secondary-teal']} />
      <CurvedLine right={-50} bottom={100} />
    </>
  );
};
