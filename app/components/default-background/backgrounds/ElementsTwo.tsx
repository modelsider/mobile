import React from 'react';
import { FigureBackgroundContainer } from 'components/UI/svg/background/FigureBackgroundContainer';
import { Circle } from 'components/circles/circle/Circle';
import { MAIN_COLOURS } from 'styles/colours';
import { CircleEmpty } from 'components/UI/svg/background/CircleEmpty';
import { Square } from 'components/UI/svg/background/Square';
import { Line } from 'components/UI/svg/background/Line';
import { CurvedLine } from 'components/UI/svg/background/CurvedLine';

export const ElementsTwo = () => {
  return (
    <>
      <FigureBackgroundContainer top={-40} left={100}>
        <Circle color={MAIN_COLOURS['secondary-teal']} size={57} />
      </FigureBackgroundContainer>
      <CircleEmpty top={150} left={-30} />
      <Square bottom={240} right={-30} />
      <Line bottom={300} left={-90} />
      <CircleEmpty bottom={150} left={-10} color={MAIN_COLOURS['secondary-teal']} />
      <CircleEmpty size={72} right={-20} top={30} color={MAIN_COLOURS['secondary-teal']} />
      <CurvedLine right={-40} bottom={100} />
    </>
  );
};
