import React from 'react';
import { FigureBackgroundContainer } from 'components/UI/svg/background/FigureBackgroundContainer';
import { Circle } from 'components/circles/circle/Circle';
import { MAIN_COLOURS } from 'styles/colours';
import { CircleEmpty } from 'components/UI/svg/background/CircleEmpty';
import { Square } from 'components/UI/svg/background/Square';
import { Line } from 'components/UI/svg/background/Line';
import { Ghost } from 'components/UI/ghost/Ghost';
import { CurvedLine } from 'components/UI/svg/background/CurvedLine';

export const WithGhostRight = () => {
  return (
    <>
      <FigureBackgroundContainer top={-30} right={100}>
        <Circle color={MAIN_COLOURS['secondary-teal']} size={57} />
      </FigureBackgroundContainer>
      <CircleEmpty top={60} left={-10} />
      <Square top={200} left={-15} />
      <Line bottom={150} left={-30} />
      <CircleEmpty bottom={60} left={-30} />
      <CircleEmpty size={72} right={-35} top={200} color={MAIN_COLOURS['secondary-teal']} />
      <FigureBackgroundContainer right={-50} bottom={220}>
        <Ghost type="right" rotate={-40} />
      </FigureBackgroundContainer>
      <CurvedLine right={-30} bottom={100} />
    </>
  );
};
