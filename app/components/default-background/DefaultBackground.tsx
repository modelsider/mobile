import React, { FC } from 'react';
import { Box, useTheme } from 'native-base';

interface IDefaultBackground {
  children: any;
  withPadding?: boolean;
  withPaddingVertical?: boolean;
  withPaddingBottom?: boolean;
}

export const DefaultBackground: FC<IDefaultBackground> = ({
  children,
  withPadding = true,
  withPaddingVertical = false,
  withPaddingBottom = false,
}) => {
  const { space } = useTheme();

  const px = withPadding ? space['2'] : 0;
  const py = withPaddingVertical ? space['2'] : 0;
  const pb = withPaddingBottom ? space['2'] : py;

  return (
    <Box flex={1} alignSelf="center" bg="white" px={px} py={py} pb={pb} width="100%">
      {children}
    </Box>
  );
};
