import React, { FC, useEffect, useRef } from 'react';
import { GhostOopsIcon } from '../UI/ghost/GhostOopsIcon';
import Lottie from 'lottie-react-native';
import { lottie } from '../../../assets';
import { Box, Center, Text, VStack } from 'native-base';

const MS_TO_PAUSE = 9000;

interface IRateAgencyWaiting {
  setIsRatingNow: (isRatingNow: boolean) => void;
}
// todo: fix
export const RateAgencyWaiting: FC<IRateAgencyWaiting> = ({ setIsRatingNow }) => {
  const animationRef = useRef<Lottie>(null);

  useEffect(() => {
    animationRef.current?.play();

    setTimeout(() => {
      animationRef.current?.pause();
      setIsRatingNow(false);
    }, MS_TO_PAUSE);
  }, [setIsRatingNow]);

  return (
    <VStack space="4" top="20%" alignItems="center">
      <Center>
        <GhostOopsIcon />

        <Text fontFamily="heading" fontWeight="400" fontStyle="normal" fontSize="4xl" color="black" textAlign="center">
          Oops...
        </Text>
      </Center>

      <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black" textAlign="center">
        {'Too many people are trying to\n submit rating for the agency'}
      </Text>

      <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black" textAlign="center">
        Please try again in
      </Text>

      <Box height="20%" width="20%">
        <Lottie ref={animationRef} source={lottie.timer} />
      </Box>
    </VStack>
  );
};
