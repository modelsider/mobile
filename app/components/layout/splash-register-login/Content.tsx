import React, { FC } from 'react';
import { Animated, StyleSheet } from 'react-native';
import { definePlatformParam } from 'utils/definePlatformParams';

interface IContent {
  children: any;
  innerStyles?: any;
}

export const Content: FC<IContent> = ({ children, innerStyles }) => {
  return <Animated.View style={[styles.content, innerStyles]}>{children}</Animated.View>;
};

const styles = StyleSheet.create({
  content: {
    bottom: definePlatformParam('3%', '0%'),
  },
});
