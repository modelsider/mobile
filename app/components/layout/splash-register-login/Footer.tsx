import React, { FC } from 'react';
import { Animated, StyleSheet } from 'react-native';
import { convertPtToResponsive } from 'utils/layoutUtils';

interface IFooter {
  children?: any;
  innerStyles?: any;
}

export const Footer: FC<IFooter> = ({ children, innerStyles }) => {
  return <Animated.View style={[styles.footer, innerStyles]}>{children}</Animated.View>;
};

const styles = StyleSheet.create({
  footer: {
    height: '20%',
    width: '100%',
    paddingBottom: convertPtToResponsive(40),
    paddingTop: '3%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
