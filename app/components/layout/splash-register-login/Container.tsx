import React, { FC } from 'react';
import { Animated, StyleSheet } from 'react-native';

interface IContainer {
  children: any;
  innerStyles?: any;
}

export const Container: FC<IContainer> = ({ children, innerStyles }) => {
  return <Animated.View style={[styles.container, innerStyles]}>{children}</Animated.View>;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
