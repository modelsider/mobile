import React, { FC } from 'react';
import { Animated, StyleSheet } from 'react-native';

interface IHeader {
  children?: any;
  innerStyles?: any;
}

export const Header: FC<IHeader> = ({ children, innerStyles }) => {
  return <Animated.View style={[styles.header, innerStyles]}>{children}</Animated.View>;
};

const styles = StyleSheet.create({
  header: {
    height: '20%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});
