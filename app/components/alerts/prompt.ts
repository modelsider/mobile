import RNPrompt from 'react-native-prompt-android';
import { AlertType } from 'react-native';

interface IPrompt {
  title: string;
  message: string;
  onCancel?: () => void;
  onConfirm: (value: string) => void;
  defaultValue?: string;
  keyboardType?: string;
  placeholder?: string;
  type?: string;
}

export const prompt = ({
  title,
  message,
  onCancel,
  onConfirm,
  defaultValue = '',
  type = 'plain-text',
  placeholder,
}: IPrompt) => {
  RNPrompt(
    title,
    message,
    [
      {
        text: 'Cancel',
        onPress: onCancel,
        style: 'destructive',
      },
      {
        text: 'Confirm',
        onPress: (value) => onConfirm(value!),
      },
    ],
    {
      type: type as AlertType,
      defaultValue,
      placeholder,
    },
  );
};
