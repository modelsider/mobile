import { Alert } from 'react-native';

const TITLE = 'Please confirm';

interface IAlert {
  title?: string;
  message: string;
  onCancel?: () => void;
  onConfirm: () => void;
}

export const alert = ({ title = TITLE, message, onCancel = () => {}, onConfirm }: IAlert) => {
  Alert.alert(title, message, [
    {
      text: 'Cancel',
      onPress: onCancel,
      style: 'destructive',
    },
    {
      text: 'Confirm',
      onPress: onConfirm,
    },
  ]);
};
