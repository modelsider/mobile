import React, { FC } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { convertPtToResponsive } from 'utils/layoutUtils';
import { MAIN_COLOURS } from 'styles/colours';

interface ITextBodyNormalBold {
  text: string;
  color?: string;
  fontFamily?: string;
}

export const TextBodyNormalBold: FC<ITextBodyNormalBold> = ({
  text,
  color = MAIN_COLOURS['primary-black'],
  fontFamily,
}) => {
  return (
    <View style={styles.container}>
      <Text style={[styles.text, { color, fontFamily }]}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  text: {
    fontSize: convertPtToResponsive(16),
    letterSpacing: 0,
    lineHeight: convertPtToResponsive(19),
  },
});
