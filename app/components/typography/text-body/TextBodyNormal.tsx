import React, { FC } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { convertPtToResponsive } from 'utils/layoutUtils';
import { ADDITIONAL_COLOURS, MAIN_COLOURS } from 'styles/colours';
import { FONTS } from 'styles/fonts';

interface ITextBodyNormal {
  text?: string;
  color?: string;
  underlineText?: string;
  textAlign?: 'center' | 'start' | 'end' | 'justify';
  numberOfLines?: number;
  width?: number | string;
  height?: number | string;
  top?: number | string;
  bottom?: number | string;
  left?: number | string;
  right?: number | string;
  fontSize?: number;
  fontFamily?: string;
  lineHeight?: number;
  disabled?: boolean;
}

export const TextBodyNormal: FC<ITextBodyNormal> = ({
  text,
  color = MAIN_COLOURS['primary-black'],
  underlineText = '',
  textAlign,
  numberOfLines,
  width,
  height,
  top,
  left,
  bottom,
  right,
  fontSize = 16,
  fontFamily = FONTS.SFUIText.semibold,
  lineHeight,
  disabled = false,
}) => {
  const validateStyles = {
    text: {
      color: disabled ? ADDITIONAL_COLOURS['primary-grey'] : color,
      textAlign: textAlign,
      width: width,
      height: height,
      paddingTop: top,
      paddingBottom: bottom,
      paddingLeft: left,
      paddingRight: right,
      fontSize: convertPtToResponsive(fontSize),
      fontFamily: fontFamily,
      lineHeight: lineHeight,
    },
  };

  return (
    <View style={styles.container}>
      <Text style={[styles.text, validateStyles.text]} numberOfLines={numberOfLines}>
        {text}
        <Text style={styles.underlineText}>{underlineText}</Text>
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  text: {
    letterSpacing: 0,
  },
  underlineText: {
    textDecorationLine: 'underline',
  },
});
