import React, { FC } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { convertPtToResponsive } from 'utils/layoutUtils';
import { FONTS } from 'styles/fonts';
import { MAIN_COLOURS } from 'styles/colours';

interface ITextBodySmall {
  text?: string;
  color?: string;
  underlineText?: string;
  textAlign?: string;
  numberOfLines?: number;
  width?: number | string;
  height?: number | string;
  top?: number | string;
  bottom?: number | string;
  left?: number | string;
  right?: number | string;
  fontSize?: number;
  fontFamily?: string;
}

export const TextBodySmall: FC<ITextBodySmall> = ({
  text,
  color = MAIN_COLOURS['primary-black'],
  underlineText = '',
  textAlign,
  numberOfLines,
  width,
  height,
  top,
  left,
  bottom,
  right,
  fontSize = 12,
  fontFamily,
}) => {
  const validateStyles = {
    text: {
      color: color,
      textAlign: textAlign,
      width: width,
      height: height,
      paddingTop: top,
      paddingBottom: bottom,
      paddingLeft: left,
      paddingRight: right,
      fontSize: convertPtToResponsive(fontSize),
      fontFamily,
    },
  };

  return (
    <View style={styles.container}>
      <Text style={[styles.text, validateStyles.text]} numberOfLines={numberOfLines}>
        {text}
        <Text style={[styles.underlineText, { fontFamily }]}>{underlineText}</Text>
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    flexDirection: 'row',
  },
  text: {
    fontSize: convertPtToResponsive(12),
    letterSpacing: 0,
    lineHeight: convertPtToResponsive(16),
    fontFamily: FONTS.SFUIText.regular,
  },
  underlineText: {
    textDecorationLine: 'underline',
    fontFamily: FONTS.SFUIText.regular,
  },
});
