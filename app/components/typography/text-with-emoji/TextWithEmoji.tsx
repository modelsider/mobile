import React, { FC } from 'react';
import { StyleSheet, View } from 'react-native';
import { TextHeaderBig } from 'components/typography/text-header/TextHeaderBig';
import { EmojiIcon } from 'components/emoji-icon/EmojiIcon';
import { FONTS } from 'styles/fonts';
import { MAIN_COLOURS } from 'styles/colours';

interface ITextWithEmoji {
  text: string;
  emojiName: string;
  color?: string;
  innerStyles?: any;
  fontSizeEmoji?: any;
  innerStylesEmoji?: any;
}

export const TextWithEmoji: FC<ITextWithEmoji> = ({
  text,
  emojiName,
  color = MAIN_COLOURS['primary-black'],
  innerStyles,
  fontSizeEmoji,
  innerStylesEmoji,
}) => {
  return (
    <View style={[styles.container, innerStyles]}>
      <TextHeaderBig fontFamily={FONTS.americanTypewriter} text={text} color={color} />
      <EmojiIcon emojiName={emojiName} fontSize={fontSizeEmoji} innerStyles={innerStylesEmoji} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
