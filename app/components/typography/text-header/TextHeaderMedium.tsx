import React, { FC } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { convertPtToResponsive } from 'utils/layoutUtils';
import { MAIN_COLOURS } from 'styles/colours';
import { FONTS } from 'styles/fonts';

interface ITextHeaderMedium {
  text?: string;
  color?: string;
  alignSelf?: string;
  top?: number | string;
  bottom?: number | string;
  left?: number | string;
  right?: number | string;
  fontWeight?: number | string;
  fontSize?: number;
  textAlign?: 'center';
  lineHeight?: number;
  fontFamily?: string;
}

export const TextHeaderMedium: FC<ITextHeaderMedium> = ({
  text,
  color = MAIN_COLOURS['primary-black'],
  alignSelf,
  top,
  left,
  bottom,
  right,
  fontSize = 21,
  textAlign,
  lineHeight = 25,
  fontFamily = FONTS.SFUIText.semibold,
}) => {
  const validateStyles = {
    text: {
      paddingTop: top,
      paddingBottom: bottom,
      paddingLeft: left,
      paddingRight: right,
      color: color,
      alignSelf: alignSelf,
      fontSize: convertPtToResponsive(fontSize),
      lineHeight: convertPtToResponsive(lineHeight),
      textAlign,
      fontFamily,
    },
  };

  return (
    <View style={styles.container}>
      <Text style={[styles.text, validateStyles.text]}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  text: {
    letterSpacing: 0,
  },
});
