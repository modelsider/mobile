import React, { FC } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { convertPtToResponsive } from 'utils/layoutUtils';
import { MAIN_COLOURS } from 'styles/colours';
import { FONTS } from 'styles/fonts';

interface ITextHeaderBig {
  text?: string;
  color?: string;
  fontFamily?: string;
  fontWeight?: string;
  textAlign?: string;
  top?: number | string;
  bottom?: number | string;
  left?: number | string;
  right?: number | string;
  fontSize?: number;
}

export const TextHeaderBig: FC<ITextHeaderBig> = ({
  text,
  color = MAIN_COLOURS['primary-black'],
  fontFamily = FONTS.SFUIText.semibold,
  fontWeight,
  textAlign,
  top,
  left,
  bottom,
  right,
  fontSize = 28,
}) => {
  const validateStyles = {
    text: {
      paddingTop: top,
      paddingBottom: bottom,
      paddingLeft: left,
      paddingRight: right,
      color: color,
      fontFamily: fontFamily,
      fontWeight: fontWeight,
      textAlign: textAlign,
      fontSize: convertPtToResponsive(fontSize),
    },
  };

  return (
    <View style={styles.container}>
      <Text style={[styles.text, validateStyles.text]}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  text: {
    fontWeight: '600',
    letterSpacing: 0,
    lineHeight: convertPtToResponsive(34),
  },
});
