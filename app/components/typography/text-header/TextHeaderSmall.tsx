import React, { FC } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { convertPtToResponsive } from 'utils/layoutUtils';
import { MAIN_COLOURS } from 'styles/colours';

interface ITextHeaderSmall {
  text: string;
  color?: string;
}

export const TextHeaderSmall: FC<ITextHeaderSmall> = ({ text, color = MAIN_COLOURS['primary-black'] }) => {
  return (
    <View style={styles.container}>
      <Text style={[styles.text, { color }]}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  text: {
    fontSize: convertPtToResponsive(18),
    fontWeight: '600',
    letterSpacing: 0,
    lineHeight: convertPtToResponsive(21),
  },
});
