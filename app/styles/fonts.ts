export const FONTS = {
  SFUIText: {
    light: 'SFUIText-Light',
    regular: 'SFUIText-Regular',
    semibold: 'SFUIText-Semibold',
    bold: 'SFUIText-Bold',
  },
  americanTypewriter: 'AmericanTypewriter',
};
