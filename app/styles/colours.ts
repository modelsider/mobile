export const MAIN_COLOURS = {
  'primary-white': '#FFFFFF',
  'primary-black': '#010101',

  'tertiary-teal': '#EFFDFC',
  'secondary-teal': '#DAFFFC',
  'primary-teal': '#B6FFF9',
  'primary-teal-2': '#92fff4',
  'primary-teal-3': '#43ffe8',

  'tertiary-grey': '#FAFAFA',
};

export const ADDITIONAL_COLOURS = {
  'secondary-grey': '#E3E3E3',
  'tertiary-grey-light': '#a9adb2',
  'primary-grey': '#999999',
  'tertiary-grey': '#777A7E',

  'secondary-red': '#FEE2E2',
  'primary-red': '#D84D4D',

  'secondary-yellow': '#FEF8CC',
  'primary-yellow': '#EFB800',

  'primary-orange': '#E6BA3F',

  'secondary-green': '#E3F9E2',
  'primary-green': '#55BF50',

  'primary-blue': '#2F8CFF',
};
