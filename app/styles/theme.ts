import { extendTheme } from 'native-base';
import { HIT_SLOP } from '../constants';

const theme = extendTheme({
  components: {
    Pressable: {
      baseStyle: ({ withOpacity = true }: { withOpacity?: boolean }) => {
        return {
          _pressed: {
            opacity: withOpacity ? 0.2 : 1,
          },
          hitSlop: HIT_SLOP,
        };
      },
    },
  },
  colors: {
    teal: {
      50: '#EFFDFC',
      100: '#DAFFFC',
      200: '#B6FFF9',
      300: '#92fff4',
      400: '#43ffe8',
    },
    gray: {
      50: '#FAFAFA',
      100: '#E3E3E3',
      200: '#a9adb2',
      300: '#999999',
      400: '#777A7E',
    },
    red: {
      100: '#FEE2E2',
      500: '#D84D4D',
    },
    yellow: {
      50: '#FEF8CC',
      500: '#EFB800',
    },
    orange: {
      300: '#E6BA3F',
    },
    blue: {
      500: '#2F8CFF',
    },
    green: {
      50: '#E3F9E2',
      500: '#55BF50',
    },
    singletons: {
      white: '#FFFFFF',
      black: '#000000',
    },
  },
  fontConfig: {
    SFUIText: {
      100: {
        normal: 'SFUIText-Light',
      },
      200: {
        normal: 'SFUIText-Light',
      },
      300: {
        normal: 'SFUIText-Light',
      },
      400: {
        normal: 'SFUIText-Regular',
      },
      500: {
        normal: 'SFUIText-Semibold',
      },
      600: {
        normal: 'SFUIText-Semibold',
      },
      700: {
        normal: 'SFUIText-Bold',
      },
      800: {
        normal: 'SFUIText-Bold',
      },
      900: {
        normal: 'SFUIText-Bold',
      },
    },
    americanTypewriter: {
      100: {
        normal: 'AmericanTypewriter',
      },
      200: {
        normal: 'AmericanTypewriter',
      },
      300: {
        normal: 'AmericanTypewriter',
      },
      400: {
        normal: 'AmericanTypewriter',
      },
      500: {
        normal: 'AmericanTypewriter',
      },
      600: {
        normal: 'AmericanTypewriter',
      },
      700: {
        normal: 'AmericanTypewriter',
      },
      800: {
        normal: 'AmericanTypewriter',
      },
      900: {
        normal: 'AmericanTypewriter',
      },
    },
  },
  fonts: {
    heading: 'SFUIText',
    body: 'SFUIText',
    mono: 'AmericanTypewriter',
  },
  config: {
    initialColorMode: 'light',
  },
});

export { theme };
