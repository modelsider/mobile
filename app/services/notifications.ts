import firestore from '@react-native-firebase/firestore';
import { COLLECTIONS } from './constants';

const saveDeviceToken = async ({ userId, deviceToken }: { userId: string; deviceToken: string }): Promise<void> => {
  try {
    await firestore().collection(COLLECTIONS.DeviceTokens).add({
      user_id: userId,
      device_token: deviceToken,
      created_at: new Date(),
    });
  } catch (e) {
    console.log('saveDeviceToken error: ', e);
    throw e;
  }
};

export { saveDeviceToken };
