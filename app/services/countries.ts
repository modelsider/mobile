import firestore from '@react-native-firebase/firestore';
import { COLLECTIONS } from './constants';
import { ICountry } from '../types/types';

const COUNTRY_MODEL = {
  code: 'code',
  id: 'id',
  name: 'name',
};

export const getCountriesFB = async (): Promise<ICountry[] | null> => {
  try {
    const countries = (
      await firestore()
        .collection(COLLECTIONS.Countries)
        .orderBy(COUNTRY_MODEL.name, 'asc')
        .get()
    ).docs.map(doc => doc.data());

    return countries as ICountry[];
  } catch (e) {
    console.log('getCountriesFB error: ', e);
    return null;
  }
};
