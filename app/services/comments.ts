import firestore from '@react-native-firebase/firestore';
import { COLLECTIONS } from './constants';
import { agencyType as typeAgency, IComment, ICommentLike, ICommentLikeProps, INewBookmark, IPost } from 'types/types';
import { getAllUsersFB, getUser, getUsersByIdsFB } from './users';
import { getBookmarksByUsersIdsAndTypeFB, getBookmarksFB } from './bookmarks';
import { DEFAULT_QUERY_LIMIT_FB } from '../constants';
import { IUserInfo } from 'store/auth/reducer';
import isUndefined from 'lodash/isUndefined';
import { getReportCommentsByUserId } from './reports';
import auth from '@react-native-firebase/auth';

const COMMENT_MODEL = {
  attachments: 'attachments',
  created_at: 'created_at',
  id: 'id',
  text: 'text',
  type: 'type',
  user_id: 'user_id',
  agency_id: 'agency_id',
  likes_count: 'likes_count',
  replies_count: 'replies_count',
  agency_type: 'agency_type',
};

const COMMENTS_LIKES_MODEL = {
  user_id: 'user_id',
  comment_id: 'comment_id',
  agency_id: 'agency_id',
  agency_type: 'agency_type',
  created_at: 'created_at',
};

const REPORT_COMMENTS_MODEL = {
  user_id_who_reported: 'user_id_who_reported',
  comment_id: 'comment_id',
  user_id: 'user_id',
  created_at: 'created_at',
};

export const getCommentByIdFB = async ({
  commentId,
  userId,
}: {
  commentId: number;
  userId: string;
}): Promise<IComment> => {
  try {
    const response = await firestore().collection(COLLECTIONS.Comments).where(COMMENT_MODEL.id, '==', +commentId).get();

    const comment = response.docs.map((doc) => ({
      ...doc.data(),
      docId: doc.id,
    })) as IComment[];
    const usersIds = comment.map(({ user_id }) => user_id);
    const users = await getUsersByIdsFB({ usersIds });
    const commentsLikes = await getCommentsLikesByIdsFB({ usersIds: [auth().currentUser?.uid!] });
    const bookmarks = await getBookmarksByUsersIdsAndTypeFB({
      usersIds: [auth().currentUser?.uid!],
      type: 'comments',
    });

    const user = users[0];
    const mappedComment = comment.map((commentItem) => {
      const isLike = commentsLikes.some(
        ({ comment_id, user_id }) => comment_id === commentItem.id && user_id === userId,
      );
      const isBookmark = bookmarks.some(({ bookmarkId, userId: id }) => bookmarkId === commentItem.id && id === userId);

      return {
        ...commentItem,
        user,
        isLike,
        isBookmark,
      };
    });
    return mappedComment[0] as IComment;
  } catch (e) {
    console.log('getCommentByIdFB error: ', e);
    throw e;
  }
};

export const updateCommentFB = async ({
  docId,
  comment,
}: {
  docId: string | undefined;
  comment: object;
}): Promise<void> => {
  try {
    await firestore().collection(COLLECTIONS.Comments).doc(docId).update(comment);
  } catch (e) {
    console.log('updateCommentFB error: ', e);
    throw e;
  }
};

export const deleteCommentFB = async ({ docId }: { docId: string }): Promise<void> => {
  try {
    await firestore().collection(COLLECTIONS.Comments).doc(docId).delete();
  } catch (e) {
    console.log('deleteCommentFB error: ', e);
    throw e;
  }
};

export const addCommentLikeFB = async ({ commentId, agencyId, agencyType }: ICommentLikeProps): Promise<void> => {
  try {
    await firestore()
      .collection(COLLECTIONS.CommentsLikes)
      .add({
        [COMMENTS_LIKES_MODEL.user_id]: auth().currentUser?.uid,
        [COMMENTS_LIKES_MODEL.comment_id]: commentId,
        [COMMENTS_LIKES_MODEL.agency_id]: agencyId,
        [COMMENTS_LIKES_MODEL.agency_type]: agencyType,
        [COMMENTS_LIKES_MODEL.created_at]: new Date(),
      });
  } catch (e) {
    console.log('addCommentLikeFB error: ', e);
    throw e;
  }
};

export const getCommentLikeFB = async ({ commentUserId, commentId }): Promise<ICommentLike> => {
  try {
    const response = await firestore()
      .collection(COLLECTIONS.CommentsLikes)
      .where(COMMENTS_LIKES_MODEL.user_id, '==', auth().currentUser?.uid)
      .where(COMMENTS_LIKES_MODEL.comment_id, '==', commentId)
      .get();

    const commentLike = response.docs.map((doc) => ({
      ...doc.data(),
      docId: doc.id,
    }));
    return commentLike[0] as ICommentLike;
  } catch (e) {
    console.log('getCommentLikeFB error: ', e);
    throw e;
  }
};

export const removeCommentLikeFB = async ({ docId }: { docId: string | undefined }): Promise<void> => {
  console.log('docId; ', docId);
  try {
    await firestore().collection(COLLECTIONS.CommentsLikes).doc(docId).delete();
  } catch (e) {
    console.log('removeCommentLikeFB error: ', e);
    throw e;
  }
};

export const likeCommentFB = async ({
  docId,
  likesCount,
  commentId,
  agencyId,
  agencyType,
}: ICommentLikeProps): Promise<void> => {
  console.log('likeCommentFB: ', docId, likesCount, commentId, agencyId, agencyType);
  try {
    await updateCommentFB({
      docId,
      comment: { likes_count: likesCount },
    });
    await addCommentLikeFB({ commentId, agencyId, agencyType });
  } catch (e) {
    console.log('likeCommentFB error: ', e);
    throw e;
  }
};

export const dislikeCommentFB = async ({
  docId,
  likesCount,
  commentId,
  commentUserId,
}: ICommentLikeProps): Promise<void> => {
  try {
    // const { docId } = await getCommentByIdFB({ commentId, userId });

    // if (docId) {
    await updateCommentFB({
      docId,
      comment: { likes_count: likesCount },
    });

    const commentLike = await getCommentLikeFB({
      commentUserId,
      commentId,
    });
    console.log('commentLike; ', commentLike);
    if (commentLike?.docId) {
      await removeCommentLikeFB({ docId: commentLike.docId });
    }
    // }
  } catch (e) {
    console.log('dislikeCommentFB error: ', e);
    throw e;
  }
};

export const getCommentsLikesByIdsFB = async ({ usersIds }: { usersIds: string[] }): Promise<ICommentLike[]> => {
  try {
    const response = await firestore()
      .collection(COLLECTIONS.CommentsLikes)
      .where(COMMENTS_LIKES_MODEL.user_id, 'in', usersIds)
      .get();

    if (response.empty) {
      return [];
    }

    const users = response.docs.map((doc) => ({
      ...doc.data(),
      docId: doc.id,
    }));

    return users as ICommentLike[];
  } catch (e) {
    console.log('getUsersByIdsFB error: ', e);
    throw e;
  }
};

export const getCommentsLikesByCommentsIdsFB = async ({
  commentsIds,
}: {
  commentsIds: number[];
}): Promise<ICommentLike[]> => {
  try {
    const response = await firestore()
      .collection(COLLECTIONS.CommentsLikes)
      .where(COMMENTS_LIKES_MODEL.comment_id, 'in', commentsIds)
      .get();

    if (response.empty) {
      return [];
    }

    const comments = response.docs.map((doc) => ({
      ...doc.data(),
      docId: doc.id,
    }));

    return comments as ICommentLike[];
  } catch (e) {
    console.log('getCommentsLikesByCommentsIdsFB error: ', e);
    throw e;
  }
};

export const getCommentsByAgencyIdFB = async ({
  agencyId,
  userId,
  startAfter,
  agencyType,
}: {
  agencyId: number;
  userId: string;
  startAfter?: string;
  agencyType: typeAgency;
}): Promise<IComment[]> => {
  try {
    let response;
    if (startAfter) {
      response = await firestore()
        .collection(COLLECTIONS.Comments)
        .where(COMMENT_MODEL.agency_id, '==', agencyId)
        .where(COMMENT_MODEL.agency_type, '==', agencyType)
        .orderBy(COMMENT_MODEL.created_at, 'desc')
        .startAfter(startAfter)
        .limit(DEFAULT_QUERY_LIMIT_FB)
        .get();
    } else {
      response = await firestore()
        .collection(COLLECTIONS.Comments)
        .where(COMMENT_MODEL.agency_id, '==', agencyId)
        .where(COMMENT_MODEL.agency_type, '==', agencyType)
        .orderBy(COMMENT_MODEL.created_at, 'desc')
        .limit(DEFAULT_QUERY_LIMIT_FB)
        .get();
    }

    if (response.empty) {
      return [];
    }

    const comments = response.docs.map((doc) => ({
      ...doc.data(),
      docId: doc.id,
    })) as IComment[];

    const commentsIds = comments.map(({ id }) => id);
    const usersIds = comments.map(({ user_id }) => user_id);

    const commentsLikes = await getCommentsLikesByCommentsIdsFB({
      commentsIds,
    });

    let users: IUserInfo[] = [];
    let bookmarks: INewBookmark[] = [];
    if (usersIds.length) {
      users = await getUsersByIdsFB({ usersIds });
      bookmarks = await getBookmarksByUsersIdsAndTypeFB({
        usersIds,
        type: 'comments',
      });
    }

    const reportComments = await getReportCommentsByUserId({ userId });

    const mappedComments = comments.map((comment) => {
      const user = users.find(({ id }) => id === comment.user_id);
      const isLike = commentsLikes.some(({ comment_id, user_id }) => comment_id === comment.id && user_id === userId);
      const isBookmark = bookmarks.some(({ bookmarkId, userId: id }) => bookmarkId === comment.id && id === userId);

      const foundedReportComment = reportComments.find((reportComment) => reportComment.comment_id === comment.id);
      const foundedReportCommentUserId = reportComments.find(
        (reportComment) => reportComment.user_id_who_reported === auth().currentUser?.uid,
      );

      const isReported = !isUndefined(foundedReportComment) && !isUndefined(foundedReportCommentUserId);

      return {
        ...comment,
        user,
        isLike,
        isBookmark,
        isReported,
      };
    });
    return mappedComments;
  } catch (e) {
    console.log('getCommentsByAgencyIdFB error: ', e);
    throw e;
  }
};

export const getCommentsByIdsFB = async ({ commentsIds }: { commentsIds: number[] }): Promise<IComment[]> => {
  try {
    const response = await firestore()
      .collection(COLLECTIONS.Comments)
      .where(COMMENT_MODEL.id, 'in', commentsIds)
      .get();

    const comments = response.docs.map((doc) => ({
      ...doc.data(),
      docId: doc.id,
    }));

    return comments as IComment[];
  } catch (e) {
    console.log('getCommentsByIdsFB error: ', e);
    throw e;
  }
};

export const getBookmarkedCommentsFB = async ({
  userId,
  startAfter = '',
}: {
  userId: string;
  startAfter?: string;
}): Promise<IComment[]> => {
  try {
    const bookmarks = await getBookmarksFB({
      userId,
      type: 'comments',
      startAfter,
    });

    if (!bookmarks.length) {
      return [];
    }

    const usersIds = bookmarks.map(({ userId }) => userId);
    const users = await getUsersByIdsFB({ usersIds });
    const commentsLikes = await getCommentsLikesByIdsFB({ usersIds: [auth().currentUser?.uid!] });

    const commentsIds = bookmarks.map((bookmark) => bookmark.bookmarkId);
    // @ts-ignore
    const comments = await getCommentsByIdsFB({ commentsIds });

    const bookmarkedComments = await Promise.all(
      comments?.map(async (comment) => {
        let user: IUserInfo | undefined;
        user = users.find(({ id }) => id === comment.user_id);
        if (isUndefined(user)) {
          user = await getUser({ id: comment?.user_id });
        }

        const isLike = commentsLikes.some(({ comment_id, user_id }) => comment_id === comment.id && user_id === userId);
        const bookmarkCreatedAt = bookmarks.find(({ userId: id }) => id === userId)?.created_at;

        return {
          ...comment,
          user,
          isLike,
          isBookmark: true,
          bookmarkCreatedAt,
        };
      }),
    );
    return bookmarkedComments;
  } catch (e) {
    console.log('getBookmarkedCommentsFB error: ', e);
    throw e;
  }
};

export const addCommentFB = async ({ comment }: { comment: IComment | IPost }): Promise<void> => {
  try {
    console.log('comment: ', comment);
    await firestore().collection(COLLECTIONS.Comments).add(comment);
  } catch (e) {
    console.log('addCommentFB error: ', e);
    throw e;
  }
};

export const isReportComment = async ({
  userIdWhoReported,
  commentId,
}: {
  userIdWhoReported: string;
  commentId: number;
}): Promise<boolean> => {
  try {
    const snapshot = await firestore()
      .collection(COLLECTIONS.ReportComments)
      .where(REPORT_COMMENTS_MODEL.user_id_who_reported, '==', userIdWhoReported)
      .where(REPORT_COMMENTS_MODEL.comment_id, '==', commentId)
      .get();

    return !snapshot.empty;
  } catch (e) {
    console.log('getReportComment error: ', e);
    throw e;
  }
};

export const addReportComment = async ({
  userIdWhoReported,
  commentId,
  userId,
}: {
  userIdWhoReported: string;
  commentId: number;
  userId: string;
}): Promise<void> => {
  try {
    const isReport = await isReportComment({ userIdWhoReported, commentId });

    if (!isReport) {
      await firestore()
        .collection(COLLECTIONS.ReportComments)
        .add({
          [REPORT_COMMENTS_MODEL.user_id_who_reported]: userIdWhoReported,
          [REPORT_COMMENTS_MODEL.comment_id]: commentId,
          [REPORT_COMMENTS_MODEL.user_id]: userId,
          [REPORT_COMMENTS_MODEL.created_at]: new Date(),
        });
    }
  } catch (e) {
    console.log('addReportComment error: ', e);
    throw e;
  }
};

export const getAllCommentsFB = async ({ startAfter }: { startAfter?: string }): Promise<{ comments: IComment[] }> => {
  try {
    let response;
    if (startAfter) {
      response = await firestore()
        .collection(COLLECTIONS.Comments)
        .orderBy(COMMENT_MODEL.created_at, 'desc')
        .startAfter(startAfter)
        .limit(10)
        .get();
    } else {
      response = await firestore()
        .collection(COLLECTIONS.Comments)
        .orderBy(COMMENT_MODEL.created_at, 'desc')
        .limit(10)
        .get();
    }

    if (response.empty) {
      return {
        comments: [],
      };
    }

    const comments = response.docs.map(
      (doc) =>
        ({
          ...doc.data(),
          docId: doc.id,
        } as IComment),
    );

    const userIdsFromComments = comments.map((comment) => comment.user_id);
    const users = await getUsersByIdsFB({ usersIds: userIdsFromComments });

    const bookmarks = await getBookmarksFB({
      userId: auth().currentUser?.uid!,
      type: 'comments',
    });
    const commentsLikes = await getCommentsLikesByIdsFB({ usersIds: [auth().currentUser?.uid!] });

    const mappedComments = await Promise.all(
      comments.map(async (comment) => {
        const user = users.find(({ id }) => id === comment.user_id);
        const isLike = commentsLikes.some(({ comment_id }) => comment_id === comment.id);
        const isBookmark = bookmarks.some(({ bookmarkId }) => bookmarkId === comment.id);

        return {
          ...comment,
          user,
          isLike,
          isBookmark,
          // agency,
        };
      }),
    );

    return {
      comments: mappedComments,
    };
  } catch (e) {
    console.log('getAllCommentsFB error: ', e);
    throw e;
  }
};
