import firestore from '@react-native-firebase/firestore';
import { COLLECTIONS } from './constants';
import { generateId } from 'utils/generateId';

const agenciesTurkey = require('../../json/agencies-turkey.json');
const countries = require('../../json/countries.json');

const mappedAgencies = agenciesTurkey.map((agency: any[]) => {
  return {
    id: generateId(),
    agency_name: agency[0],
    city: agency[1],
    country: agency[2],
    instagram: agency[4],
    web: agency[3],
    is_rating_now: false,
    mother: {
      is_rated: false,
      rate_count: 0,
      total_rating: 0,
      statistics: [
        { career_development: 0, rate_count: 0, type: 'career_development' },
        { fair_expenses: 0, rate_count: 0, type: 'fair_expenses' },
        { happy_to_work_with: 0, rate_count: 0, type: 'happy_to_work_with' },
        { payments: 0, rate_count: 0, type: 'payments' },
        { portfolio_work: 0, rate_count: 0, type: 'portfolio_work' },
        { professionalism: 0, rate_count: 0, type: 'professionalism' },
        { respect_and_care: 0, rate_count: 0, type: 'respect_and_care' },
      ],
    },
    booking: {
      is_rated: false,
      rate_count: 0,
      total_rating: 0,
      models_earned_money: 0,
      statistics: [
        {
          apartments_condition: 0,
          rate_count: 0,
          type: 'apartments_condition',
        },
        { fair_expenses: 0, rate_count: 0, type: 'fair_expenses' },
        { happy_to_work_with: 0, rate_count: 0, type: 'happy_to_work_with' },
        { payments: 0, rate_count: 0, type: 'payments' },
        { portfolio_work: 0, rate_count: 0, type: 'portfolio_work' },
        { professionalism: 0, rate_count: 0, type: 'professionalism' },
        { respect_and_care: 0, rate_count: 0, type: 'respect_and_care' },
      ],
    },
    closed_at: '',
  };
});

const addNewAgencyToDB = async (): Promise<void> => {
  try {
    let counter = 0;
    for (const agency of mappedAgencies) {
      await firestore().collection(COLLECTIONS.Agencies).add(agency);
      counter++;
      console.log(`Agency ${counter} was added`);
    }
  } catch (e) {
    console.log('addNewAgencyToDB error: ', e);
    throw e;
  }
};

const addNewCountriesToDB = async (): Promise<void> => {
  try {
    let counter = 0;
    for (const country of countries) {
      await firestore()
        .collection(COLLECTIONS.Countries)
        .add({ id: generateId(), ...country });
      counter++;
      console.log(`Country ${counter} was added`);
    }
  } catch (e) {
    console.log('addNewCountriesToDB error: ', e);
    throw e;
  }
};

export { addNewAgencyToDB, addNewCountriesToDB };
