import firestore from '@react-native-firebase/firestore';
import { COLLECTIONS } from './constants';
import { IList } from '../types/types';

const LISTS_MODEL = {
  country: 'country',
};

export const getListsFB = async (): Promise<IList[]> => {
  try {
    const lists = (await firestore().collection(COLLECTIONS.Lists).orderBy(LISTS_MODEL.country, 'asc').get()).docs.map(
      (doc) => doc.data(),
    );

    return lists as IList[];
  } catch (e) {
    console.log('getListsFB error: ', e);
    return [];
  }
};
