import { IAgency, IComment, IPost } from '../types/types';
import { getAllRatedAgenciesByDateFB } from './agencies';
import { getAllCommentsFB } from './comments';

const getFeedFB = async ({ startAfter }: { startAfter?: string }): Promise<(IComment | IPost)[]> => {
  try {
    const { comments } = await getAllCommentsFB({ startAfter });
    return comments;
  } catch (e) {
    console.log('getFeedFB error: ', e);
    throw e;
  }
};

const getRatingsFB = async ({ startAfter }: { startAfter?: string }): Promise<IAgency[]> => {
  try {
    const { agencies } = await getAllRatedAgenciesByDateFB({ startAfter });
    return agencies;
  } catch (e) {
    console.log('getRatingsFB error: ', e);
    throw e;
  }
};

export { getFeedFB, getRatingsFB };
