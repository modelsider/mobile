import { IReply } from 'types/types';
import firestore from '@react-native-firebase/firestore';
import { COLLECTIONS } from './constants';
import { getUsersByIdsFB } from './users';
import { updateCommentFB } from './comments';
import { DEFAULT_QUERY_LIMIT_FB } from '../constants';
import { store } from 'store/configureStore';

const REPLY_MODEL = {
  created_at: 'created_at',
  id: 'id',
  text: 'text',
  user_id: 'user_id',
};

const getRepliesByCommentDocIdFB = async ({
  docId,
  startAfter = '',
}: {
  docId: string;
  startAfter?: string;
}): Promise<IReply[]> => {
  try {
    let response;
    if (startAfter) {
      response = await firestore()
        .collection(COLLECTIONS.Comments)
        .doc(docId)
        .collection(COLLECTIONS.Replies)
        .orderBy(REPLY_MODEL.created_at, 'asc')
        .startAfter(startAfter)
        .limit(DEFAULT_QUERY_LIMIT_FB)
        .get();
    } else {
      response = await firestore()
        .collection(COLLECTIONS.Comments)
        .doc(docId)
        .collection(COLLECTIONS.Replies)
        .orderBy(REPLY_MODEL.created_at, 'asc')
        .limit(DEFAULT_QUERY_LIMIT_FB)
        .get();
    }

    const replies = response.docs.map((doc) => ({
      ...doc.data(),
      docId: doc.id,
    })) as IReply[];

    if (!replies.length) {
      return [];
    }

    const usersIds = replies.map(({ user_id }) => user_id);

    const users = await getUsersByIdsFB({ usersIds });

    const mappedReplies = replies.map((reply) => {
      const user = users.find(({ id }) => id === reply.user_id);

      return {
        ...reply,
        user,
      };
    });
    return mappedReplies;
  } catch (e) {
    console.log('getRepliesByCommentDocIdFB error: ', e);
    throw e;
  }
};

const addReplyFB = async ({
  commentDocId,
  reply,
  repliesCount,
}: {
  commentDocId: string;
  reply: IReply;
  repliesCount: number;
}): Promise<void> => {
  try {
    await firestore().collection(COLLECTIONS.Comments).doc(commentDocId).collection(COLLECTIONS.Replies).add(reply);

    await updateCommentFB({
      docId: commentDocId,
      comment: { replies_count: repliesCount + 1 },
    });
  } catch (e) {
    console.log('addReplyFB error: ', e);
    throw e;
  }
};

const updateReplyFB = async ({
  commentDocId,
  replyDocId,
  reply,
}: {
  commentDocId: string;
  replyDocId: string;
  reply: IReply;
}): Promise<void> => {
  try {
    await firestore()
      .collection(COLLECTIONS.Comments)
      .doc(commentDocId)
      .collection(COLLECTIONS.Replies)
      .doc(replyDocId)
      .update(reply);
  } catch (e) {
    console.log('updateReplyFB error: ', e);
    throw e;
  }
};

const deleteReplyFB = async ({
  commentDocId,
  replyDocId,
}: {
  commentDocId: string;
  replyDocId: string;
}): Promise<void> => {
  try {
    await firestore()
      .collection(COLLECTIONS.Comments)
      .doc(commentDocId)
      .collection(COLLECTIONS.Replies)
      .doc(replyDocId)
      .delete();

    await updateCommentFB({
      docId: commentDocId,
      comment: { replies_count: store.getState().comments.comment!?.replies_count - 1 },
    });
  } catch (e) {
    console.log('deleteReplyFB error: ', e);
    throw e;
  }
};

export { getRepliesByCommentDocIdFB, addReplyFB, deleteReplyFB, updateReplyFB };
