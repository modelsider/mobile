import firestore from '@react-native-firebase/firestore';
import { COLLECTIONS } from './constants';
import { agencyType, IAgency, IAgencyTab, ISuggestAgency, IWhoRatedAgency } from 'types/types';
import { getBookmarksFB, isAgencyBookmarkedByCurrentUser } from './bookmarks';
import { DEFAULT_QUERY_LIMIT_FB, templatesStatistics } from '../constants';
import isUndefined from 'lodash/isUndefined';

const AGENCY_MODEL = {
  agency_name: 'agency_name',
  city: 'city',
  country: 'country',
  id: 'id',
  instagram: 'instagram',
  web: 'web',
  mother: 'mother',
  booking: 'booking',
};

const WHO_RATED_AGENCY_MODEL = {
  user_id: 'user_id',
  agency_id: 'agency_id',
  booking: 'booking',
  mother: 'mother',
  created_at: 'created_at',
  type: 'type',
};

// agencies
export const getAgencyById = async ({
  agencyId,
  userId,
  type,
}: {
  agencyId: number;
  userId: string;
  type: agencyType;
}): Promise<IAgency> => {
  try {
    const isBookmark = await isAgencyBookmarkedByCurrentUser({
      userId,
      bookmarkId: agencyId,
    });

    const isRatedByCurrentUser = await isRatedAgencyByUserIdFB({
      userId,
      agencyId,
      type,
    });

    const agencyRatingFromCurrentUser = await getWhoRatedAgencyFB({
      agencyId,
      userId,
      type,
    });

    const snapshot = await firestore().collection(COLLECTIONS.Agencies).where(AGENCY_MODEL.id, '==', agencyId).get();

    const agency = snapshot.docs.map((doc) => ({
      ...doc.data(),
      docId: doc.id,
      isBookmark,
      isRatedByCurrentUser,
      agencyRatingFromCurrentUser: Object.keys(agencyRatingFromCurrentUser).length
        ? agencyRatingFromCurrentUser
        : templatesStatistics[type],
    }))[0];

    return agency as IAgency;
  } catch (e) {
    console.log('getAgencyById error: ', e);
    throw e;
  }
};

export const getAgenciesByIdsFB = async ({ agenciesIds }: { agenciesIds: number[] }): Promise<IAgency[]> => {
  try {
    const response = await firestore().collection(COLLECTIONS.Agencies).where(AGENCY_MODEL.id, 'in', agenciesIds).get();

    if (response.empty) {
      return [];
    }

    const agencies = response.docs.map((doc) => ({
      ...doc.data(),
      docId: doc.id,
    }));

    return agencies as IAgency[];
  } catch (e) {
    console.log('getAgenciesByIdsFB error: ', e);
    throw e;
  }
};
// agencies

// rated agencies
export const getRatedAgenciesFB = async ({
  userId,
  type,
  startAfter,
}: {
  userId: string;
  type: agencyType;
  startAfter?: string;
}): Promise<{
  mappedAgencies: IAgency[];
  whoRatedAgencies: IWhoRatedAgency[];
}> => {
  try {
    let response;
    if (startAfter) {
      response = await firestore()
        .collection(COLLECTIONS.WhoRatedAgencies)
        .where(WHO_RATED_AGENCY_MODEL.user_id, '==', userId)
        .where(WHO_RATED_AGENCY_MODEL.type, '==', type)
        .orderBy(WHO_RATED_AGENCY_MODEL.agency_id, 'desc')
        .startAfter(startAfter)
        .limit(DEFAULT_QUERY_LIMIT_FB)
        .get();
    } else {
      response = await firestore()
        .collection(COLLECTIONS.WhoRatedAgencies)
        .where(WHO_RATED_AGENCY_MODEL.user_id, '==', userId)
        .where(WHO_RATED_AGENCY_MODEL.type, '==', type)
        .limit(DEFAULT_QUERY_LIMIT_FB)
        .get();
    }

    if (response.empty) {
      return {
        mappedAgencies: [],
        whoRatedAgencies: [],
      };
    }

    const agenciesIds = response.docs.map((doc) => doc.data().agency_id);
    const whoRatedAgencies = response.docs.map((doc) => ({
      ...doc.data(),
      docId: doc.id,
    })) as IWhoRatedAgency[];

    const agencies = await getAgenciesByIdsFB({ agenciesIds });

    if (!agencies.length) {
      return {
        mappedAgencies: [],
        whoRatedAgencies: [],
      };
    }

    const mappedAgencies = agencies?.map((agency) => {
      const whoRatedAgency = whoRatedAgencies.find(
        ({ agency_id, user_id }) => agency_id === agency.id && user_id === userId,
      );

      return {
        ...agency,
        start_working_at: whoRatedAgency?.start_working_at,
        finish_working_at: whoRatedAgency?.finish_working_at,
        whoRatedAgencyDocId: whoRatedAgency?.docId,
        isClosed: agency?.closed_at?.length !== 0,
      };
    });
    return {
      mappedAgencies,
      whoRatedAgencies,
    };
  } catch (e) {
    console.log('getRatedAgenciesFB error: ', e);
    throw e;
  }
};

export const isRatedAgencyByUserIdFB = async ({
  userId,
  agencyId,
  type,
}: {
  userId: string;
  agencyId: number;
  type: agencyType;
}): Promise<boolean> => {
  try {
    let collection = await firestore()
      .collection(COLLECTIONS.WhoRatedAgencies)
      .where(WHO_RATED_AGENCY_MODEL.user_id, '==', userId)
      .where(WHO_RATED_AGENCY_MODEL.agency_id, '==', agencyId)
      .where(WHO_RATED_AGENCY_MODEL.type, '==', type)
      .get();

    return !collection.empty;
  } catch (e) {
    console.log('isRatedAgencyByUserIdFB error: ', e);
    throw e;
  }
};

export const updateRatedAgencyFB = async ({ docId, data }: { docId: string; data: IAgencyTab }): Promise<boolean> => {
  try {
    await firestore().collection(COLLECTIONS.Agencies).doc(docId).update(data);
    return true;
  } catch (e) {
    console.log('updateRatedAgencyFB error: ', e);
    return false;
  }
};
// rated agencies

// search agencies
const searchAgencyFB = async ({
  key,
  searchWord,
}: {
  key: 'agency_name' | 'country' | 'city';
  searchWord: string;
}): Promise<IAgency[]> => {
  try {
    const agencies = (
      await firestore()
        .collection(COLLECTIONS.Agencies)
        .orderBy(key, 'asc')
        .startAt(searchWord)
        .endAt(searchWord + '\uf8ff')
        .get()
    ).docs.map((doc) => doc.data());

    return agencies as IAgency[];
  } catch (e) {
    console.log('searchAgencyFB error: ', e);
    return [] as IAgency[];
  }
};

export const getSearchingAgenciesFB = async ({ searchWord }: { searchWord: string }): Promise<IAgency[]> => {
  try {
    const agenciesByAgencyName = await searchAgencyFB({
      key: 'agency_name',
      searchWord,
    });
    if (agenciesByAgencyName.length) {
      return agenciesByAgencyName;
    }

    const agenciesByCountry = await searchAgencyFB({
      key: 'country',
      searchWord,
    });
    if (agenciesByCountry.length) {
      return agenciesByCountry;
    }

    const agenciesByCity = await searchAgencyFB({ key: 'city', searchWord });
    if (agenciesByCity.length) {
      return agenciesByCity;
    }

    return [] as IAgency[];
  } catch (e) {
    console.log('getSearchingAgenciesFB error: ', e);
    return [] as IAgency[];
  }
};

export const isAgencyExistFB = async ({
  agencyName,
  country,
  city,
}: {
  agencyName: string;
  city: string;
  country: string;
}) => {
  try {
    const collection = await firestore()
      .collection(COLLECTIONS.Agencies)
      .where(AGENCY_MODEL.agency_name, '==', agencyName)
      .where(AGENCY_MODEL.city, '==', city)
      .where(AGENCY_MODEL.country, '==', country)
      .get();
    const response = collection.docs.map((doc) => doc.data());
    return !!response?.length;
  } catch (e) {
    console.log('isAgencyExistFB error: ', e);
  }
};

export const addSuggestAgencyFB = async ({ suggestedAgency }: { suggestedAgency: ISuggestAgency }) => {
  try {
    const { agency_name, country, city } = suggestedAgency;

    const isExist = await isAgencyExistFB({
      agencyName: agency_name!,
      country: country!,
      city: city!,
    });

    if (!isExist) {
      const response = await firestore().collection(COLLECTIONS.SuggestedAgencies).add(suggestedAgency);
      return response;
    }
  } catch (e) {
    console.log('addSuggestAgencyFB error: ', e);
  }
};

export const getBookmarkedAgenciesFB = async ({
  userId,
  startAfter,
}: {
  userId: number | string;
  startAfter?: string;
}) => {
  try {
    const bookmarks = await getBookmarksFB({
      userId,
      type: 'agencies',
      startAfter,
    });

    if (!bookmarks.length) {
      return [];
    }

    const agenciesIds = bookmarks?.map((bookmark) => bookmark.bookmarkId);
    const agencies = await getAgenciesByIdsFB({ agenciesIds });

    const bookmarkedAgencies = agencies?.map((agency) => ({
      ...agency,
      isBookmark: true,
    }));
    return bookmarkedAgencies;
  } catch (e) {
    console.log('getBookmarkedAgenciesFB error: ', e);
  }
};

// who rated agency
export const getWhoRatedAgencyFB = async ({
  agencyId,
  userId,
  type,
}: {
  agencyId: number;
  userId: string;
  type: agencyType;
}): Promise<IWhoRatedAgency> => {
  try {
    const response = await firestore()
      .collection(COLLECTIONS.WhoRatedAgencies)
      .where(WHO_RATED_AGENCY_MODEL.agency_id, '==', agencyId)
      .where(WHO_RATED_AGENCY_MODEL.user_id, '==', userId)
      .where(WHO_RATED_AGENCY_MODEL.type, '==', type)
      .get();

    if (response.empty) {
      return {} as IWhoRatedAgency;
    }

    const whoRatedAgency = response.docs.map((doc) => ({
      docId: doc.id,
      ...doc.data(),
    }))[0];

    return whoRatedAgency as IWhoRatedAgency;
  } catch (e) {
    console.log('getWhoRatedAgency error: ', e);
    throw e;
  }
};

export const updateWhoRatedAgencyFB = async ({
  docId,
  data,
}: {
  docId: string;
  data: IWhoRatedAgency | { start_working_at: any; finish_working_at: any };
}): Promise<boolean> => {
  try {
    await firestore().collection(COLLECTIONS.WhoRatedAgencies).doc(docId).update(data);
    return true;
  } catch (e) {
    console.log('updateWhoRatedAgencyFB error: ', e);
    return false;
  }
};

export const addWhoRatedAgencyFB = async ({ data }: { data: IWhoRatedAgency }): Promise<boolean> => {
  try {
    const { user_id: userId, agency_id: agencyId, type } = data;
    const isRatedAgency = await isRatedAgencyByUserIdFB({
      userId,
      agencyId,
      type,
    });

    if (isRatedAgency) {
      const { docId } = await getWhoRatedAgencyFB({ agencyId, userId, type });
      const response = await updateWhoRatedAgencyFB({ docId: docId!, data });
      return response;
    } else {
      await firestore().collection(COLLECTIONS.WhoRatedAgencies).add(data);
      return true;
    }
  } catch (e) {
    console.log('addWhoRatedAgencyFB error: ', e);
    return false;
  }
};

export const removeWhoRatedAgencyFB = async ({ docId }: { docId: string }): Promise<void> => {
  try {
    await firestore().collection(COLLECTIONS.WhoRatedAgencies).doc(docId).delete();
  } catch (e) {
    console.log('removeWhoRatedAgencyFB error: ', e);
  }
};
// who rated agency

export const setIsRatingAgencyNowFB = async ({
  isRatingNow,
  docId,
}: {
  isRatingNow: boolean;
  docId: string;
}): Promise<void> => {
  try {
    if (docId) {
      await firestore().collection(COLLECTIONS.Agencies).doc(docId).update({ is_rating_now: isRatingNow });
    }
  } catch (e) {
    console.log('setIsRatingAgencyNowFB error: ', e);
  }
};

export const isRatingAgencyNowFB = async ({ docId }: { docId: string }): Promise<boolean> => {
  try {
    const snapshot = (await firestore().collection(COLLECTIONS.Agencies).doc(docId).get()).data();
    return snapshot?.is_rating_now;
  } catch (e) {
    console.log('isRatingAgencyNowFB error: ', e);
    return false;
  }
};

export const setAgenciesRatingInfoFB = async (): Promise<void> => {
  try {
    const snapshot = (await firestore().collection(COLLECTIONS.AgenciesRatingInfo).get()).docs.map((doc) => {
      return {
        docId: doc.id,
        counter: doc.data()?.counter,
      };
    })[0];

    if (isUndefined(snapshot)) {
      return;
    }

    const counter = snapshot.counter;
    const docId = snapshot.docId;

    await firestore()
      .collection(COLLECTIONS.AgenciesRatingInfo)
      .doc(docId)
      .update({ counter: counter + 1 });
  } catch (e) {
    console.log('setAgenciesRatingInfoFB error: ', e);
    throw e;
  }
};

const getAgenciesFB = async (): Promise<{ agencies: IAgency[] }> => {
  try {
    const response = await firestore().collection(COLLECTIONS.Agencies).get();

    if (response.empty) {
      return {
        agencies: [],
      };
    }

    const agencies = response.docs
      .map((doc) => doc.data() as IAgency)
      .sort((a, b) => (a.agency_name.toLowerCase() < b.agency_name.toLowerCase() ? -1 : 1))
      .filter((agency) => !agency.closed_at);

    return {
      agencies,
    };
  } catch (e) {
    console.log('getAgenciesFB error: ', e);
    throw e;
  }
};

const getAllRatedAgenciesFB = async (): Promise<{
  agencies: IAgency[];
}> => {
  try {
    const { agencies } = await getAgenciesFB();

    const ratedAgencies = agencies.filter((agency) => agency.booking.is_rated || agency.mother.is_rated);

    return {
      agencies: ratedAgencies,
    };
  } catch (e) {
    console.log('getAllRatedAgenciesFB error: ', e);
    throw e;
  }
};

const getAllAgenciesByCountryFB = async ({
  country,
}: {
  country: string;
}): Promise<{
  agencies: IAgency[];
}> => {
  try {
    const { agencies } = await getAgenciesFB();

    const agenciesByCountry = agencies.filter((agency) => agency.country === country);

    return {
      agencies: agenciesByCountry,
    };
  } catch (e) {
    console.log('getAllAgenciesByCountryFB error: ', e);
    throw e;
  }
};

const getRatedOnlyAgenciesByCountryFB = async ({
  country,
}: {
  country: string;
}): Promise<{
  agencies: IAgency[];
}> => {
  try {
    const { agencies } = await getAllAgenciesByCountryFB({ country });

    const agenciesByCountry = agencies.filter((agency) => agency.booking.is_rated || agency.mother.is_rated);

    return {
      agencies: agenciesByCountry,
    };
  } catch (e) {
    console.log('getRatedOnlyAgenciesByCountryFB error: ', e);
    throw e;
  }
};

const getAllRatedAgenciesByDateFB = async ({
  startAfter,
}: {
  startAfter?: string;
}): Promise<{
  agencies: IAgency[];
}> => {
  try {
    let response;
    if (startAfter) {
      response = await firestore()
        .collection(COLLECTIONS.WhoRatedAgencies)
        .orderBy(WHO_RATED_AGENCY_MODEL.created_at, 'desc')
        .startAfter(startAfter)
        .limit(10)
        .get();
    } else {
      response = await firestore()
        .collection(COLLECTIONS.WhoRatedAgencies)
        .orderBy(WHO_RATED_AGENCY_MODEL.created_at, 'desc')
        .limit(10)
        .get();
    }

    if (response.empty) {
      return {
        agencies: [],
      };
    }

    const whoRatedAgencies = response.docs.map((doc) => doc.data() as IWhoRatedAgency);
    const agenciesIds = whoRatedAgencies.map((agency) => agency.agency_id);
    const agencies = await getAgenciesByIdsFB({ agenciesIds });

    if (!agencies.length) {
      return {
        agencies: [],
      };
    }

    const mappedAgencies = whoRatedAgencies?.map((whoRatedAgency) => {
      const agency = agencies.find(({ id }) => id === whoRatedAgency.agency_id);

      return {
        ...whoRatedAgency,
        agency_name: agency?.agency_name,
        city: agency?.city,
        country: agency?.country,
        [whoRatedAgency.type]: {
          total_rating: agency?.[whoRatedAgency.type]?.total_rating,
        },
      };
    });

    return {
      agencies: mappedAgencies as unknown as IAgency[],
    };
  } catch (e) {
    console.log('getAllRatedAgenciesByDateFB error: ', e);
    throw e;
  }
};

export {
  getAllRatedAgenciesByDateFB,
  getAgenciesFB,
  getAllRatedAgenciesFB,
  getAllAgenciesByCountryFB,
  getRatedOnlyAgenciesByCountryFB,
};
