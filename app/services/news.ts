import firestore from '@react-native-firebase/firestore';
import { COLLECTIONS } from './constants';
import { INews } from 'types/types';

const NEWS_MODEL = {
  created_at: 'created_at'
};

const getNewsCounterIdsFB = async ({
  newsIds
}: {
  newsIds: number[];
}): Promise<object> => {
  // todo: add correct interface
  try {
    let newsDocIds = {};
    for (let newsId of newsIds) {
      const docIds = await firestore()
        .collection(COLLECTIONS.NewsCounter)
        .doc(String(newsId))
        .collection('users')
        .get()
        .then(response => response.docs.map(doc => doc.id));
      newsDocIds[newsId] = docIds;
    }
    return newsDocIds;
  } catch (e) {
    console.log('getNewsCounterIdsFB error: ', e);
    throw e;
  }
};

const removeNewsCounterFB = async ({
  newsId,
  userId
}: {
  newsId: string;
  userId: string;
}): Promise<void> => {
  try {
    await firestore()
      .collection(COLLECTIONS.NewsCounter)
      .doc(String(newsId))
      .collection('users')
      .doc(String(userId))
      .delete();
  } catch (e) {
    console.log('removeNewsCounterFB error: ', e);
    throw e;
  }
};

const removeNewsCounterCollectionFB = async ({
  userId
}: {
  userId: string;
}): Promise<void> => {
  try {
    const snapshot = await firestore().collection(COLLECTIONS.News).get();
    snapshot.docs.map(({ id }) => removeNewsCounterFB({ newsId: id, userId }));
  } catch (e) {
    console.log('removeNewsCounterCollectionFB error: ', e);
    throw e;
  }
};

const getNewsFB = async ({ userId }: { userId: string }): Promise<INews[]> => {
  try {
    const snapshot = await firestore()
      .collection(COLLECTIONS.News)
      .orderBy(NEWS_MODEL.created_at, 'desc')
      .get();

    if (snapshot.empty) {
      return [] as INews[];
    }

    const news = snapshot.docs?.map(doc => doc.data());
    const newsIds = news.map(({ id }) => id);
    const newsCounterIds = await getNewsCounterIdsFB({ newsIds });

    const mappedNews = news.map(newsItem => {
      const isNew = newsCounterIds[newsItem.id].includes(String(userId));

      return {
        ...newsItem,
        isNew
      };
    });

    return mappedNews as (INews & { isNew: boolean; docId: string })[];
  } catch (e) {
    console.log('getNews error: ', e);
    throw e;
  }
};

const getNewsCounterFB = async ({
  userId
}: {
  userId: string;
}): Promise<number> => {
  try {
    const news = await getNewsFB({ userId });

    let counter = 0;
    for (let newsItem of news) {
      const userNewsCounter = await firestore()
        .collection(COLLECTIONS.NewsCounter)
        .doc(String(newsItem.id))
        .collection('users')
        .where('user_id', '==', userId)
        .get()
        .then(response => response.size);
      counter += userNewsCounter;
    }

    return counter;
  } catch (e) {
    console.log('getNewsCounterFB error: ', e);
    throw e;
  }
};

export { getNewsFB, getNewsCounterFB, removeNewsCounterCollectionFB };
