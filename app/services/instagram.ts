import axios from 'axios';
// @ts-ignore
import { URL_GRAPH_INSTAGRAM, API_INSTAGRAM, APP_ID, APP_SECRET, REDIRECT_URL } from '@env';
import qs from 'qs';

const authByInstagram = async ({ code }: { code: string }) => {
  try {
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };

    const data = qs.stringify({
      client_id: APP_ID,
      client_secret: APP_SECRET,
      grant_type: 'authorization_code',
      redirect_uri: REDIRECT_URL,
      code: code,
    });

    const response = await axios.post(`${API_INSTAGRAM}oauth/access_token`, data, { headers });
    return response.data;
  } catch (e) {
    console.log('authByInstagram error: ', e.response);
    return e.response.data;
  }
};

const getUserInfo = async (access_token: string) => {
  try {
    const response = await axios.get(`${URL_GRAPH_INSTAGRAM}me?fields=username&access_token=${access_token}`);
    return response?.data?.username;
  } catch (e) {
    console.log('requestByGraph error: ', e);
  }
};

export { authByInstagram, getUserInfo };
