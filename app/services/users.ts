import firestore from '@react-native-firebase/firestore';
import { COLLECTIONS } from './constants';
import { IUserInfo } from 'store/auth/reducer';
import { agencyType } from 'types/types';
import auth from '@react-native-firebase/auth';

const USER_MODEL = {
  id: 'id',
};

const WHO_RATED_AGENCIES_MODEL = {
  agency_id: 'agency_id',
  user_id: 'user_id',
};

export const createUser = async ({ userInfo }: { userInfo: IUserInfo }): Promise<boolean> => {
  try {
    await firestore().collection(COLLECTIONS.Users).add(userInfo);
    return true;
  } catch (e) {
    console.log('createUser error: ', e);
    return false;
  }
};

export const deleteUser = async ({ docId }: { docId: string }): Promise<boolean> => {
  try {
    await auth().currentUser?.delete();
    await firestore().collection(COLLECTIONS.Users).doc(docId).delete();
    return true;
  } catch (e) {
    console.log('deleteUser error: ', e);
    return false;
  }
};

export const getUser = async ({ id }: { id: string }): Promise<IUserInfo> => {
  try {
    const collection = await firestore().collection(COLLECTIONS.Users).where('id', '==', id).get();

    if (collection.empty) {
      return {} as IUserInfo;
    }

    const response = collection?.docs?.map((doc) => ({
      ...doc.data(),
      docId: doc?.id,
    }));
    return response[0] as IUserInfo;
  } catch (e) {
    console.log('getUser error: ', e);
    throw e;
  }
};

export const updateUser = async ({ docId, data }: { docId: string; data: object }): Promise<void> => {
  try {
    await firestore().collection(COLLECTIONS.Users).doc(docId).update(data);
  } catch (e) {
    console.log('updateUser error: ', e);
  }
};

export const getUsersByIdsFB = async ({ usersIds }: { usersIds: string[] }): Promise<IUserInfo[]> => {
  try {
    const response = await firestore().collection(COLLECTIONS.Users).where(USER_MODEL.id, 'in', usersIds).get();

    if (response.empty) {
      return [];
    }

    const users = response.docs.map((doc) => ({
      ...doc.data(),
      docId: doc.id,
    }));

    return users as IUserInfo[];
  } catch (e) {
    console.log('getUsersByIdsFB error: ', e);
    throw e;
  }
};

export const getUsersByAgencyIdFB = async ({
  agencyId,
  type,
}: {
  agencyId: number;
  type: agencyType;
}): Promise<IUserInfo[]> => {
  try {
    const response = await firestore()
      .collection(COLLECTIONS.WhoRatedAgencies)
      .where(WHO_RATED_AGENCIES_MODEL.agency_id, '==', agencyId)
      .get();

    if (response.empty) {
      return [];
    }

    const whoRatedAgency = response?.docs?.map((doc) => doc.data());
    const filteredUsers = whoRatedAgency.filter((user) => user?.hasOwnProperty(type));
    const usersIds = filteredUsers.map(({ user_id }) => user_id);

    const users = await getUsersByIdsFB({ usersIds });

    const mappedUsers = users.map((user) => {
      const createdAt = filteredUsers.find(({ agency_id }) => agency_id === agencyId)?.created_at;

      return {
        ...user,
        createdAt,
      };
    });

    return mappedUsers as IUserInfo[];
  } catch (e) {
    console.log('getUsersByAgencyIdFB error: ', e);
    throw e;
  }
};

export const getAllUsersFB = async (): Promise<IUserInfo[]> => {
  try {
    const users = (await firestore().collection(COLLECTIONS.Users).get()).docs.map((doc) => doc.data());
    return users as IUserInfo[];
  } catch (e) {
    console.log('getUsersByAgencyIdFB error: ', e);
    throw e;
  }
};
