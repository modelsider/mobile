import firestore from '@react-native-firebase/firestore';
import { COLLECTIONS } from './constants';

const EARLY_ACCESS_MODEL = {
  user_name_instagram: 'user_name_instagram',
};

const isEarlyAccessByUserNameFB = async ({ userName }: { userName: string }): Promise<boolean> => {
  try {
    const snapshot = await firestore()
      .collection(COLLECTIONS.EarlyAccess)
      .where(EARLY_ACCESS_MODEL.user_name_instagram, '==', userName)
      .get();

    return !snapshot.empty;
  } catch (e) {
    console.log('isEarlyAccessByUserNameFB error: ', e);
    return false;
  }
};

const deleteUsernameFromEarlyAccessFB = async ({ userName }: { userName: string }): Promise<void> => {
  try {
    const snapshot = (
      await firestore()
        .collection(COLLECTIONS.EarlyAccess)
        .where(EARLY_ACCESS_MODEL.user_name_instagram, '==', userName)
        .get()
    ).docs.map((doc) => ({ ...doc, docId: doc.id }))[0];

    await firestore().collection(COLLECTIONS.EarlyAccess).doc(snapshot.docId).delete();
  } catch (e) {
    console.log('removeUsernameFromEarlyAccessFB error: ', e);
  }
};

export { isEarlyAccessByUserNameFB, deleteUsernameFromEarlyAccessFB };
