import firestore from '@react-native-firebase/firestore';
import { COLLECTIONS } from './constants';
import { bookmarkType, INewBookmark } from 'types/types';
import isNull from 'lodash/isNull';
import { DEFAULT_QUERY_LIMIT_FB } from '../constants';
import auth from '@react-native-firebase/auth';

const BOOKMARK_MODEL = {
  user_id: 'userId',
  bookmark_id: 'bookmarkId',
  type: 'type',
  created_at: 'created_at',
};

export const isAgencyBookmarkedByCurrentUser = async ({
  userId,
  bookmarkId,
  type = 'agencies',
}: INewBookmark): Promise<boolean> => {
  try {
    const bookmark = await firestore()
      .collection(COLLECTIONS.Bookmarks)
      .where(BOOKMARK_MODEL.user_id, '==', userId)
      .where(BOOKMARK_MODEL.bookmark_id, '==', bookmarkId)
      .where(BOOKMARK_MODEL.type, '==', type)
      .get();
    return !bookmark.empty;
  } catch (e) {
    console.log('isAgencyBookmarkedByCurrentUser error: ', e);
    throw e;
  }
};

export const getBookmarkFB = async ({ bookmarkId, type }: INewBookmark): Promise<INewBookmark | null> => {
  try {
    const bookmark = await firestore()
      .collection(COLLECTIONS.Bookmarks)
      .where(BOOKMARK_MODEL.user_id, '==', auth().currentUser?.uid)
      .where(BOOKMARK_MODEL.bookmark_id, '==', bookmarkId)
      .where(BOOKMARK_MODEL.type, '==', type)
      .get();

    if (bookmark.empty) {
      return null;
    }

    return bookmark.docs.map((doc) => ({
      ...doc.data(),
      docId: doc.id,
    }))[0] as INewBookmark;
  } catch (e) {
    console.log('getBookmarkFB error: ', e);
    throw e;
  }
};

export const getBookmarksFB = async ({
  userId,
  type,
  startAfter,
}: INewBookmark & { startAfter?: string }): Promise<INewBookmark[]> => {
  try {
    const orderBy = type === 'agencies' ? BOOKMARK_MODEL.bookmark_id : BOOKMARK_MODEL.created_at;

    let response;
    if (startAfter) {
      response = await firestore()
        .collection(COLLECTIONS.Bookmarks)
        .where(BOOKMARK_MODEL.user_id, '==', userId)
        .where(BOOKMARK_MODEL.type, '==', type)
        .orderBy(orderBy, 'desc')
        .startAfter(startAfter)
        .limit(DEFAULT_QUERY_LIMIT_FB)
        .get();
    } else {
      response = await firestore()
        .collection(COLLECTIONS.Bookmarks)
        .where(BOOKMARK_MODEL.user_id, '==', userId)
        .where(BOOKMARK_MODEL.type, '==', type)
        .orderBy(BOOKMARK_MODEL.created_at, 'desc')
        .limit(DEFAULT_QUERY_LIMIT_FB)
        .get();
    }

    const bookmarks = response.docs.map((doc) => ({
      ...doc.data(),
      docId: doc.id,
    }));
    return bookmarks as INewBookmark[];
  } catch (e) {
    console.log('getBookmarksFB error: ', e);
    throw e;
  }
};

export const getBookmarksByUsersIdsAndTypeFB = async ({
  usersIds,
  type,
}: {
  usersIds: string[];
  type: bookmarkType;
}): Promise<INewBookmark[]> => {
  try {
    const response = await firestore()
      .collection(COLLECTIONS.Bookmarks)
      .where(BOOKMARK_MODEL.type, '==', type)
      .where(BOOKMARK_MODEL.user_id, 'in', usersIds)
      .get();

    const bookmarks = response.docs.map((doc) => ({
      ...doc.data(),
      docId: doc.id,
    }));

    return bookmarks as INewBookmark[];
  } catch (e) {
    console.log('getBookmarksByUsersIdsAndTypeFB error: ', e);
    throw e;
  }
};

export const addBookmarkFB = async ({ bookmarkId, type }: INewBookmark): Promise<void> => {
  try {
    await firestore()
      .collection(COLLECTIONS.Bookmarks)
      .add({
        [BOOKMARK_MODEL.user_id]: auth().currentUser?.uid,
        [BOOKMARK_MODEL.bookmark_id]: bookmarkId,
        [BOOKMARK_MODEL.type]: type,
        [BOOKMARK_MODEL.created_at]: new Date(),
      });
  } catch (e) {
    console.log('addBookmarkFB error: ', e);
  }
};

export const removeBookmarkFB = async ({ bookmarkId, type }: INewBookmark): Promise<void> => {
  try {
    const bookmark = await getBookmarkFB({
      bookmarkId,
      type,
    });
    await firestore().collection(COLLECTIONS.Bookmarks).doc(bookmark?.docId).delete();
  } catch (e) {
    console.log('removeBookmarkFB error: ', e);
  }
};

export const toggleBookmarkFB = async ({ userId, bookmarkId, type }: INewBookmark): Promise<void> => {
  try {
    const bookmark = await getBookmarkFB({
      userId,
      bookmarkId,
      type,
    });

    if (isNull(bookmark)) {
      await addBookmarkFB({ userId, bookmarkId, type });
    } else {
      await removeBookmarkFB({ docId: bookmark?.docId });
    }
  } catch (e) {
    console.log('toggleBookmarkFB error: ', e);
  }
};
