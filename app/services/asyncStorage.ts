import AsyncStorage from '@react-native-async-storage/async-storage';
import { ASYNC_STORAGE_STATE } from './constants';

const clearStorage = async () => await AsyncStorage.clear();

const setFCMToken = async (token: string) => {
  await AsyncStorage.setItem(ASYNC_STORAGE_STATE.fcmToken, token);
};

const getFCMToken = async () => {
  const fcmToken = await AsyncStorage.getItem(ASYNC_STORAGE_STATE.fcmToken);
  return fcmToken;
};

export { clearStorage, setFCMToken, getFCMToken };
