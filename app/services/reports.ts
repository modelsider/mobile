import { IReportComment } from '../types/types';
import firestore from '@react-native-firebase/firestore';
import { COLLECTIONS } from './constants';

const REPORT_COMMENT_MODEL = {
  comment_id: 'comment_id',
  created_at: 'created_at',
  user_id: 'user_id',
  user_id_who_reported: 'user_id_who_reported',
};

// userId -> who reported comments
const getReportCommentsByUserId = async ({ userId }: { userId: string }): Promise<IReportComment[]> => {
  try {
    const reportComments = (
      await firestore()
        .collection(COLLECTIONS.ReportComments)
        .where(REPORT_COMMENT_MODEL.user_id_who_reported, '==', userId)
        .get()
    ).docs.map((doc) => doc.data());

    return reportComments as IReportComment[];
  } catch (e) {
    console.log('getReportCommentsByUserId error: ', e);
    throw e;
  }
};

export { getReportCommentsByUserId };
