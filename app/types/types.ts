import { IUserInfo } from 'store/auth/reducer';
import { ratedStatisticsType } from '../constants';
export type DirectionType = 'left' | 'right';

export type notificationType = 'likes' | 'replies' | 'bookmarksAgencies' | 'bookmarksComments' | 'news';

export interface ISettingsNotificationsItem {
  title: string;
  value: notificationType;
  subTitle?: string;
  description: string;
  isEnabled?: boolean;
}

export type agencyLinkTypes = 'web' | 'instagram';

export interface IProgressState {
  index: number;
  border: boolean;
  check: boolean;
}

export interface IStatisticItemProps {
  type: ratedStatisticsType;
  rate_count: number;
}

export interface IMotherAgencyStatistics {
  career_development: number | (null & IStatisticItemProps);
  fair_expenses: number | (null & IStatisticItemProps);
  happy_to_work_with: number | (null & IStatisticItemProps);
  payments: number | (null & IStatisticItemProps);
  portfolio_work: number | (null & IStatisticItemProps);
  professionalism: number | (null & IStatisticItemProps);
  respect_and_care: number | (null & IStatisticItemProps);
  apartments_condition: number | (null & IStatisticItemProps);
  portfolio_work_side: number | (null & IStatisticItemProps);
  pay: number | (null & IStatisticItemProps);
  expenses: number | (null & IStatisticItemProps);
  accommodation: number | (null & IStatisticItemProps);
}

export interface IBookingAgencyStatistics {
  apartments_condition: number | (null & IStatisticItemProps);
  fair_expenses: number | (null & IStatisticItemProps);
  happy_to_work_with: number | (null & IStatisticItemProps);
  payments: number | (null & IStatisticItemProps);
  portfolio_work: number | (null & IStatisticItemProps);
  professionalism: number | (null & IStatisticItemProps);
  respect_and_care: number | (null & IStatisticItemProps);
  career_development: number | (null & IStatisticItemProps);
  portfolio_work_side: number | (null & IStatisticItemProps);
  pay: number | (null & IStatisticItemProps);
  expenses: number | (null & IStatisticItemProps);
  accommodation: number | (null & IStatisticItemProps);
}

export interface IAgencyTab {
  is_rated: boolean;
  start_working_at: any;
  finish_working_at: any;
  total_rating: number;
  rate_count: number;
  statistics: IMotherAgencyStatistics[] | IBookingAgencyStatistics[];
  models_earned_money: number;
}

export interface IAgency {
  docId?: string;
  id: number;
  agency_name: string;
  city: string;
  country: string;
  date: string;
  instagram: string;
  web: string;
  mother: IAgencyTab;
  booking: IAgencyTab;
  isBookmark?: boolean;
  start_working_at?: any;
  finish_working_at?: any;
  isRatedByCurrentUser?: boolean;
  whoRatedAgencyDocId?: string;
  agencyRatingFromCurrentUser?: IWhoRatedAgency;
  is_rating_now: boolean;
  closed_at?: any;
  isClosed?: boolean;
}

export interface IWhoRatedAgency {
  docId?: string;
  user_id: string;
  agency_id: number;
  booking?: {
    statistics: IBookingAgencyStatistics;
  };
  mother?: {
    statistics: IMotherAgencyStatistics;
  };
  type: 'mother' | 'booking';
  created_at: any;
  start_working_at: any;
  finish_working_at: any;
  models_earned_money: number;
}

export type agencyType = 'booking' | 'mother';
export type tabViewType = 'login' | 'register';
export type noop = () => void;

export type bookmarkType = 'agencies' | 'comments';

export interface INewBookmark {
  bookmarkId?: number;
  type?: bookmarkType;
  docId?: string;
  isBookmarkLocal?: boolean;
}

export type commentType = 'likes' | 'complaints' | 'apartment' | 'rates' | 'bravo' | 'other' | '';
export type userInfoType = 'gender' | 'height' | 'size';

export type imagePickerType = 'takePhoto' | 'choosePhoto';

export interface IComment {
  docId?: string;
  attachments: string[];
  created_at: any;
  id: number;
  text: string;
  types: commentType[];
  agency_type: agencyType;
  user_id: string;
  agency_id: number;
  likes_count: number;
  replies_count: number;
  user?: IUserInfo;
  isLike?: boolean;
  isBookmark?: boolean;
  is_anonymous: boolean;
  bookmarkCreatedAt?: any;
  type: 'Comment';
  isReported?: boolean;
  agency_name: string;
}

export interface IReply {
  docId?: string;
  created_at: any;
  id: number;
  text: string;
  user_id: string;
  is_anonymous: boolean;
  attachments: any[]; // todo: add correct interface
  user?: IUserInfo;
  agency_id: number;
  agency_type: agencyType;
  type: 'Reply';
}

export interface IPost {
  docId?: string;
  attachments: string[];
  created_at: any;
  id: number;
  text: string;
  types: any[];
  agency_type: '';
  user_id: string;
  agency_id: null;
  likes_count: number;
  replies_count: number;
  user?: IUserInfo;
  isLike?: boolean;
  isBookmark?: boolean;
  is_anonymous: boolean;
  bookmarkCreatedAt?: any;
  type: 'Post';
  isReported?: boolean;
  agency_name: '';
}

export interface ICommentLike {
  user_id: number;
  comment_id: number;
  docId?: string;
}

export interface ICommentLikeProps {
  docId?: string;
  likesCount?: number;
  commentId: number;
  agencyId?: number;
  agencyType?: agencyType | '';
  commentUserId?: string;
}

export interface ILink {
  [key: string]: string;
}

export interface INews {
  title: string;
  text: string;
  created_at: string;
  links: ILink;
  isNew: boolean;
}

export interface ITabViewRoute {
  isActive: boolean;
  title: string;
}

export interface IQuestionaryData {
  description: string;
  advantages?: string[];
  disadvantages?: string[];
  index: number;
  statisticType?: ratedStatisticsType;
  navigateToNext?: noop;
  navigateToYes?: noop;
  navigateToNo?: noop;
}

export interface IEarlyAccess {
  user_name_instagram: string;
}

export interface ICountry {
  id: number;
  code: string;
  name: string;
}

export interface IList {
  country: string;
}

export type searchAgenciesStatusType = 'default' | 'searching' | 'notFound';

export interface ISuggestAgency {
  agency_name?: string;
  city?: string;
  country?: string;
  instagram?: string;
  web?: string;
  comment?: string;
  user_id?: string;
  created_at?: any;
}

export type suggestAgencyType = 'agency_name' | 'country' | 'city' | 'web' | 'instagram';

export type welcomeScreenType = 'welcome' | 'rateAgency' | 'checkRatings' | 'modelsider';

export interface ISupportContent {
  isExpanded: boolean;
  title: string;
  description: string;
}

export interface IReportComment {
  comment_id: number;
  created_at: any;
  user_id: string;
  user_id_who_reported: string;
}
