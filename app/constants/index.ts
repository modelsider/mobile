// @ts-ignore
import { APP_ID, REDIRECT_URL, API_INSTAGRAM } from '@env';
export const DEFAULT_NUMBERS_OF_LETTERS_FOR_START_SEARCH = 2;

export const DEFAULT_SIZE_AGREEMENT_ICON = 16;

export const RATED_INFO = {
  defaultRatedCount: 9,
  default: 'Be the first one to rate!',
  limitation: 'Rated by less than 10 models',
};

export const RATED_STATISTICS_TITLES = {
  mother: [
    {
      title: 'Professionalism',
      type: 'professionalism',
    },
    {
      title: 'Respect and care',
      type: 'respect_and_care',
    },
    {
      title: 'Career development',
      type: 'career_development',
    },
    {
      title: 'Payments',
      type: 'payments',
    },
    {
      title: 'Portfolio work',
      type: 'portfolio_work',
    },
    {
      title: 'Fair expenses',
      type: 'fair_expenses',
    },
    {
      title: 'Happy to work with',
      type: 'happy_to_work_with',
    },
  ],
  booking: [
    {
      title: 'Professionalism',
      type: 'professionalism',
    },
    {
      title: 'Respect and care',
      type: 'respect_and_care',
    },
    {
      title: 'Apartments condition',
      type: 'apartments_condition',
    },
    {
      title: 'Payments',
      type: 'payments',
    },
    {
      title: 'Portfolio work',
      type: 'portfolio_work',
    },
    {
      title: 'Fair expenses',
      type: 'fair_expenses',
    },
    {
      title: 'Happy to work with',
      type: 'happy_to_work_with',
    },
  ],
};

export type ratedStatisticsType =
  | 'career_development'
  | 'professionalism'
  | 'respect_and_care'
  | 'apartments_condition'
  // | 'models_earned_money'
  | 'payments'
  | 'portfolio_work'
  | 'portfolio_work_side'
  | 'fair_expenses'
  | 'happy_to_work_with'
  | 'pay'
  | 'expenses'
  | 'accommodation';

export const HINT_DATA = {
  MOTHER: {
    professionalism: {
      advantages: [
        'Always follows undertakings\nwritten in the contract',
        'Responsible and helpful',
        'Provides necessary information\nabout castings, jobs, clients',
      ],
      disadvantages: ['Does not follow\nthe contract terms', 'Irresponsible workwise', 'Ignores messages'],
    },
    respectful: {
      advantages: [
        'The agency team cares about\nyour wellbeing',
        'Open and transparent',
        'Respects your choice',
        'Easy to talk to',
        'You do not experience\nanything below',
      ],
      disadvantages: ['Body-shaming', 'Racism', 'Sexism', 'Ageism', 'Harassment', 'Abuse'],
    },
    satisfied: {
      advantages: [
        'The agency discusses your\ncareer plan with you',
        'Your career plan is clear',
        'The agency matches your\nwishes and possibilities',
        'Your career plan does not\ncontradict your\npersonal life goals',
      ],
      disadvantages: [
        'The agency does not discuss\nyour career plan with you',
        'You do not know what to expect\nfrom your career',
        'The agency does not match\nyour wishes and possibilities',
        'Your personal life goals\nare not taken into account',
      ],
    },
    responsible: {
      advantages: [
        'You receive payments on time',
        'Your rates on a statement are\nthe same as you were told\nbef e a job',
        'You do not experience anything below',
      ],
      disadvantages: [
        'Delay of payment by\nthe agency without reason',
        'Change of a rate after a job',
        'Not being paid at all',
        'Being paid less than expected',
      ],
    },
    helpful: {
      advantages: [
        'The agency organises test\nshootings/ jobs for portfolio',
        'The agency collaborates with\na booking agency in a particular country\nwhere possible to make good\ntest shootings/editorials',
      ],
      disadvantages: [
        'The agency does not provide\nany test shootings',
        'The agency provides test shootings,\nbut the quality is bad',
      ],
    },
    fair: {
      advantages: ['All fees such as test shootings,\nwebsite, accommodation, flight,\netc have a fair price'],
      disadvantages: [
        'The agency unnecessarily raises\nthe price for test shootings,\naccommodation, flight, website, etc',
      ],
    },
  },
  BOOKING: {
    professionalism: {
      advantages: [
        'Always follows undertakings\nwritten in the contract',
        'Responsible and helpful ',
        'Provides necessary information\nabout castings, jobs, clients',
      ],
      disadvantages: ['Does not follow\nthe contract terms', 'Irresponsible workwise', 'Ignores messages'],
    },
    respectful: {
      advantages: [
        'The agency team cares\nabout your wellbeing',
        'Open and transparent',
        'Respects your choice',
        'Easy to talk to',
        'You do not experience\nanything below',
      ],
      disadvantages: ['Body-shaming', 'Racism', 'Sexism', 'Ageism', 'Harassment', 'Abuse'],
    },
    apartment: {
      advantages: [
        'Hot water, electricity, wi-fi,\nheater/air conditioning',
        'You have place to hold\npersonal belongings',
        'All necessary house supplies\nare in good condition',
        'You do not experience\nanything below',
      ],
      disadvantages: [
        'Cockroaches, bed bugs',
        'Hidden cameras',
        'Lack of space',
        'Strangers living with models',
        'Dangerous area',
      ],
    },
    payment: {
      advantages: ['You earned more then your\nexpenses excluding guarantee', 'You have covered all\nexpenses in full'],
      disadvantages: [
        'You did not cover expenses\nbut received your guarantee',
        'You did not cover expenses',
        'You did not earn anything',
      ],
    },
    responsible: {
      advantages: [
        'You receive payments on time',
        'Your rates on a statement\nare the same as you were told\nbefore a job',
        'You do not experience anything below',
      ],
      disadvantages: [
        'Delay of payment by the agency\nwithout reason',
        'Change of a rate after a job',
        'Not being paid at all',
        'Being paid less than expected',
      ],
    },
    helpful: {
      advantages: [
        'You improved your portfolio via \n' + 'test shootings, editorials, or any \n' + 'type of commercial work.',
      ],
      disadvantages: [
        'The agency does not provide \n' + 'any test shootings',
        'The agency provides test \n' + 'shootings, but the quality is bad',
      ],
    },
    fair: {
      advantages: ['All fees such as test shootings,\nwebsite, accommodation, flight,\netc have a fair price'],
      disadvantages: [
        'The agency unnecessarily raises\nthe price for test shootings,\naccommodation, flight, website, etc',
      ],
    },
  },
};

export const TO_PERCENT = {
  1: 20,
  2: 40,
  3: 60,
  4: 80,
  5: 100,
};

export const AGENCY_TABS = ['mother', 'booking'];

export const DEFAULT_QUERY_LIMIT_FB = 10;

export const templatesStatistics = {
  mother: [
    { professionalism: 0, rate_count: 0, type: 'professionalism' },
    { respect_and_care: 0, rate_count: 0, type: 'respect_and_care' },
    { career_development: 0, rate_count: 0, type: 'career_development' },
    { payments: 0, rate_count: 0, type: 'payments' },
    { portfolio_work: 0, rate_count: 0, type: 'portfolio_work' },
    { fair_expenses: 0, rate_count: 0, type: 'fair_expenses' },
    { happy_to_work_with: 0, rate_count: 0, type: 'happy_to_work_with' },
  ],
  booking: [
    { professionalism: 0, rate_count: 0, type: 'professionalism' },
    { respect_and_care: 0, rate_count: 0, type: 'respect_and_care' },
    {
      apartments_condition: 0,
      rate_count: 0,
      type: 'apartments_condition',
    },
    { payments: 0, rate_count: 0, type: 'payments' },
    { portfolio_work: 0, rate_count: 0, type: 'portfolio_work' },
    { fair_expenses: 0, rate_count: 0, type: 'fair_expenses' },
    { happy_to_work_with: 0, rate_count: 0, type: 'happy_to_work_with' },
  ],
};

export const statisticOrder = {
  mother: [
    'professionalism',
    'respect_and_care',
    'career_development',
    'payments',
    'portfolio_work',
    'fair_expenses',
    'happy_to_work_with',
  ],
  booking: [
    'professionalism',
    'respect_and_care',
    'apartments_condition',
    'payments',
    'portfolio_work',
    'fair_expenses',
    'happy_to_work_with',
  ],
};

export const numberOfQuestionMother = {
  professionalism: 1,
  respect_and_care: 2,
  career_development: 3,
  pay: 3,
  payments: 4,
  portfolio_work_side: 4,
  portfolio_work: 5,
  expenses: 5,
  fair_expenses: 6,
  happy_to_work_with: 7,
};

export const numberOfQuestionBooking = {
  professionalism: 1,
  respect_and_care: 2,
  accommodation: 2,
  apartments_condition: 3,
  models_earned_money: 3,
  payments: 4,
  portfolio_work_side: 4,
  portfolio_work: 5,
  expenses: 5,
  fair_expenses: 6,
  happy_to_work_with: 7,
};

export const MAX_SYMBOLS_TO_LEAVE_COMMENT = 1000;

export const uriAuthInstagram = `${API_INSTAGRAM}oauth/authorize/?client_id=${APP_ID}&redirect_uri=${REDIRECT_URL}&response_type=code&scope=user_profile,user_media`;

export const HIT_SLOP = {
  top: 20,
  left: 20,
  right: 20,
  bottom: 20,
};

export const AUTH_ERRORS = {
  'auth/email-already-in-use': 'Please login to finish registration',
  'auth/invalid-email': 'The email address is invalid',
  'auth/email-already-exists': 'The email already exists',
  'auth/wrong-password': 'The password is invalid',
  'auth/user-not-found': 'The user is not found',
  'auth/weak-password': 'The given password is too weak.',
};

export const MIN_HEIGHT = 750;
