const BOTTOM_SHEET_TITLES = {
  agency: {
    updateRating: 'Update Rating',
    updateDates: 'Update Dates',
    deleteRating: 'Delete Rating',
    message: 'Are you sure you would like to Delete your rating?',
  },
  comment: {
    agencyProfile: 'Agency Profile',
    edit: 'Edit',
    remove: 'Delete',
  },
  reportComment: {
    report: 'Report',
  },
  attachments: {
    deleteImage: 'Delete image',
  },
  post: {
    edit: 'Edit',
    remove: 'Delete',
  },
};

const bottomSheetConfig = {
  agency: [
    {
      text: BOTTOM_SHEET_TITLES.agency.updateRating,
      onPress: () => {},
    },
    {
      text: BOTTOM_SHEET_TITLES.agency.updateDates,
      onPress: () => {},
    },
    {
      text: BOTTOM_SHEET_TITLES.agency.deleteRating,
      destructive: true,
      onPress: () => {},
    },
  ],
  comment: [
    {
      text: BOTTOM_SHEET_TITLES.comment.agencyProfile,
      onPress: () => {},
    },
    {
      text: BOTTOM_SHEET_TITLES.comment.edit,
      onPress: () => {},
    },
    {
      text: BOTTOM_SHEET_TITLES.comment.remove,
      destructive: true,
      onPress: () => {},
    },
  ],
  reply: [
    {
      text: BOTTOM_SHEET_TITLES.comment.edit,
      onPress: () => {},
    },
    {
      text: BOTTOM_SHEET_TITLES.comment.remove,
      destructive: true,
      onPress: () => {},
    },
  ],
  reportComment: [
    {
      text: BOTTOM_SHEET_TITLES.comment.agencyProfile,
      onPress: () => {},
    },
    {
      text: BOTTOM_SHEET_TITLES.reportComment.report,
      onPress: () => {},
    },
  ],
  reportReply: [
    {
      text: BOTTOM_SHEET_TITLES.reportComment.report,
      onPress: () => {},
    },
  ],
  reportPost: [
    {
      text: BOTTOM_SHEET_TITLES.reportComment.report,
      onPress: () => {},
    },
  ],
  attachments: [
    {
      text: BOTTOM_SHEET_TITLES.attachments.deleteImage,
      onPress: () => {},
      destructive: true,
    },
  ],
  post: [
    {
      text: BOTTOM_SHEET_TITLES.post.edit,
      onPress: () => {},
    },
    {
      text: BOTTOM_SHEET_TITLES.post.remove,
      destructive: true,
      onPress: () => {},
    },
  ],
};

export { bottomSheetConfig, BOTTOM_SHEET_TITLES };
