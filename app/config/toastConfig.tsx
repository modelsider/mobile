import React, { FC } from 'react';
import { ToastIcon } from 'components/UI/svg/ToastIcon';
import { HStack, Text, useTheme } from 'native-base';

const ToastWrapper: FC<{ type: 'success' | 'warning' }> = ({ children, type }) => {
  const { colors } = useTheme();

  const borderColor = {
    success: colors.green['500'],
    warning: colors.orange['300'],
  };

  return (
    <HStack
      space="3"
      justifyContent="flex-start"
      alignItems="center"
      w="90%"
      h={76}
      px="7"
      borderWidth={2}
      borderRadius={8}
      borderColor={borderColor[type]}
      bg="white"
    >
      {children}
    </HStack>
  );
};

const toastConfig = {
  success: (props: JSX.IntrinsicAttributes & import('react-native-toast-message').BaseToastProps) => {
    const { text1 } = props;

    return (
      <ToastWrapper type="success">
        <ToastIcon type="success" />
        <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black">
          {text1}
        </Text>
      </ToastWrapper>
    );
  },

  warning: (props: JSX.IntrinsicAttributes & import('react-native-toast-message').BaseToastProps) => {
    const { text1 } = props;

    return (
      <ToastWrapper type="warning">
        <ToastIcon type="warning" />
        <Text fontFamily="body" fontWeight="400" fontStyle="normal" fontSize="md" color="black">
          {text1}
        </Text>
      </ToastWrapper>
    );
  },
};

export { toastConfig };
