import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import Toast from 'react-native-toast-message';
import { AppNavigation } from 'navigator/AppNavigator';
import { Linking } from 'react-native';
import { Provider, useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor, store } from 'store/configureStore';
import { selectIsLogoAppLoader, selectIsGlobalLoader, selectStatusBar, selectStatusBarBg } from 'store/app/selectors';
import CustomStatusBar from 'components/custom-status-bar/CustomStatusBar';
import { SplashScreenApp, TIME_TO_LOADING_SPLASH_APP } from 'components/splash-screen-app/SplashScreenApp';
import { logBoxApp } from 'utils/logBox';
import { navigationRef } from 'navigator/NavigationService';
import { GlobalLoader, LogoLoader } from 'components/loaders';
import { toastConfig } from 'config/toastConfig';
import { usePushNotifications, useDeepLinking, useScreenTracking, useVersionCheck } from 'hooks/index';
import { selectUserInfo } from 'store/auth/selectors';
// @ts-ignore
import { AMPLITUDE_API_KEY } from '@env';
import { Box, NativeBaseProvider, useTheme } from 'native-base';
import { theme } from 'styles/theme';
import { init } from '@amplitude/analytics-react-native';

if (__DEV__) {
  console.log('DEBUG MODE');
}

const Wrapper = () => {
  const { colors } = useTheme();

  const isStatusBar = useSelector(selectStatusBar);
  const statusBarBg = useSelector(selectStatusBarBg);
  const isGlobalLoading = useSelector(selectIsGlobalLoader);
  const isLogoAppLoading = useSelector(selectIsLogoAppLoader);
  const user = useSelector(selectUserInfo);

  const { requestUserPermission } = usePushNotifications();

  init(AMPLITUDE_API_KEY, user.email);

  useEffect(() => {
    if (user?.id) {
      setTimeout(() => {
        requestUserPermission({ userId: user?.id });
      }, 2000);
    }
  }, [requestUserPermission, user?.id]);

  useEffect(() => {
    // handles deep link when app is not already open
    Linking.getInitialURL()
      .then((url) => {
        setTimeout(async () => {
          if (url) {
            await Linking.openURL(url);
          }
        }, TIME_TO_LOADING_SPLASH_APP);
      })
      .catch(console.warn);

    return () => {
      // clears listener when component unmounts
      Linking.removeAllListeners('url');
    };
  }, []);

  return (
    <Box flex={1}>
      <CustomStatusBar backgroundColor={statusBarBg || colors.teal['50']} isStatusBar={isStatusBar} />
      <AppNavigation />
      <Toast config={toastConfig} />
      <GlobalLoader isLoading={isGlobalLoading} />
      {isLogoAppLoading && <LogoLoader />}
    </Box>
  );
};

const App = () => {
  logBoxApp();

  const { config } = useDeepLinking();
  const { handleStateChange, handleReady } = useScreenTracking();
  useVersionCheck();

  return (
    <NativeBaseProvider theme={theme}>
      <SplashScreenApp>
        <PersistGate loading={null} persistor={persistor}>
          <NavigationContainer
            ref={navigationRef}
            linking={config}
            onReady={handleReady}
            onStateChange={handleStateChange}
          >
            <Provider store={store}>
              <Wrapper />
            </Provider>
          </NavigationContainer>
        </PersistGate>
      </SplashScreenApp>
    </NativeBaseProvider>
  );
};

export default App;
