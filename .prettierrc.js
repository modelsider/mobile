module.exports = {
	bracketSpacing: true,
	bracketSameLine: false,
	singleQuote: true,
	trailingComma: 'all',
	printWidth: 120,
	tabWidth: 2,
	importOrder: ['^[./]'],
	importOrderSeparation: true,
	importOrderSortSpecifiers: true,
	importOrderCaseInsensitive: true,
	endOfLine: 'auto',
};
